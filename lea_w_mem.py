import lea_spi_wrapper as lsw
import time

count = 0
bc = 0

wrapper = lsw.lea_spi_wrapper()
kb = wrapper.format_bytes(bytearray.fromhex('9C9C82DA4441ADF032A412F10629C92E'))
iv = wrapper.format_bytes(bytearray.fromhex('28B9629CD9D92C2C8BF6A1F01F7DCCE5'))
p = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
print(p)
pb = wrapper.fragment_text(bytearray(p, 'utf-8'))
#print(pb)
#cb = bytearray.fromhex('')
c = wrapper.cbc_lea_encrypt(pb, kb, iv)
print('ciphertext(hex):')
for val in c:
    for elem in val:
        print(hex(elem)[2:].zfill(2), end='')
print()
#print('end c')
pt = wrapper.cbc_lea_decrypt(c, kb, iv)
#print(pt)
#print(pt)
print('retrieved text:')
rt = ''
rtb = bytearray()
for val in pt:
    rtb.extend(val)
print(rtb.decode('utf-8'))
print()
#print('end rt')
#print(rval)
del wrapper

