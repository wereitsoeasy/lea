onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -radix hexadecimal /complete_spi_vhd_tst/ss
add wave -noupdate -radix hexadecimal /complete_spi_vhd_tst/sck
add wave -noupdate -radix hexadecimal /complete_spi_vhd_tst/rst
add wave -noupdate -radix hexadecimal /complete_spi_vhd_tst/mosi
add wave -noupdate -radix hexadecimal /complete_spi_vhd_tst/miso
add wave -noupdate -radix hexadecimal /complete_spi_vhd_tst/clk
add wave -noupdate -divider {New Divider}
add wave -noupdate -radix hexadecimal /complete_spi_vhd_tst/i1/db/regs/write_addr
add wave -noupdate -radix hexadecimal /complete_spi_vhd_tst/i1/db/regs/status_data_in
add wave -noupdate -radix hexadecimal /complete_spi_vhd_tst/i1/db/regs/rw
add wave -noupdate -radix hexadecimal /complete_spi_vhd_tst/i1/db/regs/read_addr
add wave -noupdate -radix hexadecimal /complete_spi_vhd_tst/i1/db/regs/lea_rw
add wave -noupdate -radix hexadecimal /complete_spi_vhd_tst/i1/db/regs/lea_mem_in
add wave -noupdate -radix hexadecimal /complete_spi_vhd_tst/i1/db/regs/inst_out
add wave -noupdate -radix hexadecimal /complete_spi_vhd_tst/i1/db/regs/dbr
add wave -noupdate -radix hexadecimal /complete_spi_vhd_tst/i1/db/regs/data_out
add wave -noupdate -radix hexadecimal /complete_spi_vhd_tst/i1/db/regs/data_in
add wave -noupdate -radix hexadecimal /complete_spi_vhd_tst/i1/db/regs/command_out
add wave -noupdate -radix hexadecimal /complete_spi_vhd_tst/i1/db/regs/clk
add wave -noupdate -divider {New Divider}
add wave -noupdate -radix hexadecimal /complete_spi_vhd_tst/i1/db/reg_addr/rw
add wave -noupdate -radix hexadecimal /complete_spi_vhd_tst/i1/db/reg_addr/mem
add wave -noupdate -radix hexadecimal /complete_spi_vhd_tst/i1/db/reg_addr/data_out
add wave -noupdate -radix hexadecimal /complete_spi_vhd_tst/i1/db/reg_addr/data_length
add wave -noupdate -radix hexadecimal /complete_spi_vhd_tst/i1/db/reg_addr/data_in
add wave -noupdate -radix hexadecimal /complete_spi_vhd_tst/i1/db/reg_addr/clr
add wave -noupdate -radix hexadecimal /complete_spi_vhd_tst/i1/db/reg_addr/clk
add wave -noupdate -divider {New Divider}
add wave -noupdate -radix hexadecimal /complete_spi_vhd_tst/i1/db/control_logic/state
add wave -noupdate -radix hexadecimal /complete_spi_vhd_tst/i1/db/control_logic/ss
add wave -noupdate -radix hexadecimal /complete_spi_vhd_tst/i1/db/control_logic/spi_busy
add wave -noupdate -radix hexadecimal /complete_spi_vhd_tst/i1/db/control_logic/rst
add wave -noupdate -radix hexadecimal /complete_spi_vhd_tst/i1/db/control_logic/reg_wr_in
add wave -noupdate -radix hexadecimal /complete_spi_vhd_tst/i1/db/control_logic/reg_addr_in
add wave -noupdate -radix hexadecimal /complete_spi_vhd_tst/i1/db/control_logic/next_state
add wave -noupdate -radix hexadecimal /complete_spi_vhd_tst/i1/db/control_logic/lea_busy
add wave -noupdate -radix hexadecimal /complete_spi_vhd_tst/i1/db/control_logic/control_signals
add wave -noupdate -radix hexadecimal /complete_spi_vhd_tst/i1/db/control_logic/cmd_in
add wave -noupdate -radix hexadecimal /complete_spi_vhd_tst/i1/db/control_logic/clk
add wave -noupdate -divider {New Divider}
add wave -noupdate -radix hexadecimal /complete_spi_vhd_tst/i1/ssl/trns_busy
add wave -noupdate -radix hexadecimal /complete_spi_vhd_tst/i1/ssl/ss
add wave -noupdate -radix hexadecimal /complete_spi_vhd_tst/i1/ssl/sck
add wave -noupdate -radix hexadecimal /complete_spi_vhd_tst/i1/ssl/recv_busy
add wave -noupdate -radix hexadecimal /complete_spi_vhd_tst/i1/ssl/p_data_out
add wave -noupdate -radix hexadecimal /complete_spi_vhd_tst/i1/ssl/p_data_in
add wave -noupdate -radix hexadecimal /complete_spi_vhd_tst/i1/ssl/mosi
add wave -noupdate -radix hexadecimal /complete_spi_vhd_tst/i1/ssl/miso
add wave -noupdate -radix hexadecimal /complete_spi_vhd_tst/i1/ssl/circle
add wave -noupdate -radix hexadecimal /complete_spi_vhd_tst/i1/ssl/busy
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {869 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {4569 ps}
