-- Copyright (C) 1991-2013 Altera Corporation
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, Altera MegaCore Function License 
-- Agreement, or other applicable license agreement, including, 
-- without limitation, that your use is for the sole purpose of 
-- programming logic devices manufactured by Altera and sold by 
-- Altera or its authorized distributors.  Please refer to the 
-- applicable agreement for further details.

-- ***************************************************************************
-- This file contains a Vhdl test bench template that is freely editable to   
-- suit user's needs .Comments are provided in each section to help the user  
-- fill out necessary details.                                                
-- ***************************************************************************
-- Generated on "11/10/2020 09:38:35"
                                                            
-- Vhdl Test Bench template for design  :  complete_spi
-- 
-- Simulation tool : ModelSim-Altera (VHDL)
-- 

LIBRARY ieee;                                               
USE ieee.std_logic_1164.all;                                

ENTITY complete_spi_vhd_tst IS
END complete_spi_vhd_tst;
ARCHITECTURE complete_spi_arch OF complete_spi_vhd_tst IS
-- constants                                                 
-- signals                                                   
SIGNAL clk : STD_LOGIC := '0';
SIGNAL miso : STD_LOGIC;
SIGNAL mosi : STD_LOGIC;
SIGNAL rst : STD_LOGIC;
SIGNAL sck : STD_LOGIC := '0';
SIGNAL ss : STD_LOGIC;
COMPONENT complete_spi
	PORT (
	clk : IN STD_LOGIC;
	miso : BUFFER STD_LOGIC;
	mosi : IN STD_LOGIC;
	rst : IN STD_LOGIC;
	sck : IN STD_LOGIC;
	ss : IN STD_LOGIC
	);
END COMPONENT;
BEGIN
	i1 : complete_spi
	PORT MAP (
-- list connections between master ports and signals
	clk => clk,
	miso => miso,
	mosi => mosi,
	rst => rst,
	sck => sck,
	ss => ss
	);            
	clk <= not clk after 1 ps;
always : PROCESS                                              
BEGIN                                                         
   
	rst <= '1';
	wait for 6 ns;
	rst <= '0';
	wait for 6 ns;
	
	ss <= '1';
	wait for 5 ns;
	ss <= '0';
	wait for 16 ns;
	-- bit 0
	mosi <= '1';
	wait for 2 ns;
	-- bit 1
	sck <= not sck;
	wait for 1 ns;
	sck <= not sck;
	mosi <= '0';
	wait for 1 ns;
	-- bit 2
	sck <= not sck;
	wait for 1 ns;
	sck <= not sck;
	mosi <= '0';
	wait for 1 ns;
	-- bit 3
	sck <= not sck;
	wait for 1 ns;
	sck <= not sck;
	mosi <= '0';
	wait for 1 ns;
	-- bit 4
	sck <= not sck;
	wait for 1 ns;
	sck <= not sck;
	mosi <= '0';
	wait for 1 ns;
	-- bit 5
	sck <= not sck;
	wait for 1 ns;
	sck <= not sck;
	mosi <= '0';
	wait for 1 ns;
	-- bit 6
	sck <= not sck;
	wait for 1 ns;
	sck <= not sck;
	mosi <= '0';
	wait for 1 ns;
	-- bit 7
	sck <= not sck;
	wait for 1 ns;
	sck <= not sck;
	mosi <= '0';
	wait for 1 ns;
	sck <= not sck;
	wait for 1 ns;
	sck <= not sck;
	wait for 16 ns;
	-- bit 0
	mosi <= '1';
	wait for 2 ns;
	-- bit 1
	sck <= not sck;
	wait for 1 ns;
	sck <= not sck;
	mosi <= '0';
	wait for 1 ns;
	-- bit 2
	sck <= not sck;
	wait for 1 ns;
	sck <= not sck;
	mosi <= '1';
	wait for 1 ns;
	-- bit 3
	sck <= not sck;
	wait for 1 ns;
	sck <= not sck;
	mosi <= '0';
	wait for 1 ns;
	-- bit 4
	sck <= not sck;
	wait for 1 ns;
	sck <= not sck;
	mosi <= '1';
	wait for 1 ns;
	-- bit 5
	sck <= not sck;
	wait for 1 ns;
	sck <= not sck;
	mosi <= '0';
	wait for 1 ns;
	-- bit 6
	sck <= not sck;
	wait for 1 ns;
	sck <= not sck;
	mosi <= '1';
	wait for 1 ns;
	-- bit 7
	sck <= not sck;
	wait for 1 ns;
	sck <= not sck;
	mosi <= '0';
	wait for 1 ns;
	sck <= not sck;
	wait for 1 ns;
	sck <= not sck;
	wait for 16 ns;
	ss <= '1';
	 
WAIT;                                                        
END PROCESS always;                                          
END complete_spi_arch;
