-- Copyright (C) 1991-2013 Altera Corporation
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, Altera MegaCore Function License 
-- Agreement, or other applicable license agreement, including, 
-- without limitation, that your use is for the sole purpose of 
-- programming logic devices manufactured by Altera and sold by 
-- Altera or its authorized distributors.  Please refer to the 
-- applicable agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus II 32-bit"
-- VERSION "Version 13.1.0 Build 162 10/23/2013 SJ Web Edition"

-- DATE "11/22/2020 20:33:57"

-- 
-- Device: Altera EP4CE10F17C8 Package FBGA256
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL) only
-- 

LIBRARY ALTERA;
LIBRARY CYCLONEIVE;
LIBRARY IEEE;
USE ALTERA.ALTERA_PRIMITIVES_COMPONENTS.ALL;
USE CYCLONEIVE.CYCLONEIVE_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	complete_spi IS
    PORT (
	mosi : IN std_logic;
	ss : IN std_logic;
	sck : IN std_logic;
	clk : IN std_logic;
	rst : IN std_logic;
	miso : OUT std_logic
	);
END complete_spi;

-- Design Ports Information
-- miso	=>  Location: PIN_D6,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ss	=>  Location: PIN_C8,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- clk	=>  Location: PIN_E1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- sck	=>  Location: PIN_E8,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- rst	=>  Location: PIN_A13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- mosi	=>  Location: PIN_E7,	 I/O Standard: 2.5 V,	 Current Strength: Default


ARCHITECTURE structure OF complete_spi IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_mosi : std_logic;
SIGNAL ww_ss : std_logic;
SIGNAL ww_sck : std_logic;
SIGNAL ww_clk : std_logic;
SIGNAL ww_rst : std_logic;
SIGNAL ww_miso : std_logic;
SIGNAL \clk~inputclkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \miso~output_o\ : std_logic;
SIGNAL \sck~input_o\ : std_logic;
SIGNAL \ssl|trns|index[0]~32_combout\ : std_logic;
SIGNAL \~GND~combout\ : std_logic;
SIGNAL \rst~input_o\ : std_logic;
SIGNAL \ssl|trns|index[2]~37\ : std_logic;
SIGNAL \ssl|trns|index[3]~38_combout\ : std_logic;
SIGNAL \ss~input_o\ : std_logic;
SIGNAL \ssl|trns|index[3]~39\ : std_logic;
SIGNAL \ssl|trns|index[4]~40_combout\ : std_logic;
SIGNAL \ssl|trns|index[4]~41\ : std_logic;
SIGNAL \ssl|trns|index[5]~42_combout\ : std_logic;
SIGNAL \ssl|trns|index[5]~43\ : std_logic;
SIGNAL \ssl|trns|index[6]~44_combout\ : std_logic;
SIGNAL \ssl|trns|index[6]~45\ : std_logic;
SIGNAL \ssl|trns|index[7]~46_combout\ : std_logic;
SIGNAL \ssl|trns|index[7]~47\ : std_logic;
SIGNAL \ssl|trns|index[8]~48_combout\ : std_logic;
SIGNAL \ssl|trns|index[8]~49\ : std_logic;
SIGNAL \ssl|trns|index[9]~50_combout\ : std_logic;
SIGNAL \ssl|trns|index[9]~51\ : std_logic;
SIGNAL \ssl|trns|index[10]~52_combout\ : std_logic;
SIGNAL \ssl|trns|index[10]~53\ : std_logic;
SIGNAL \ssl|trns|index[11]~54_combout\ : std_logic;
SIGNAL \ssl|trns|index[11]~55\ : std_logic;
SIGNAL \ssl|trns|index[12]~56_combout\ : std_logic;
SIGNAL \ssl|trns|index[12]~57\ : std_logic;
SIGNAL \ssl|trns|index[13]~58_combout\ : std_logic;
SIGNAL \ssl|trns|index[13]~59\ : std_logic;
SIGNAL \ssl|trns|index[14]~60_combout\ : std_logic;
SIGNAL \ssl|trns|index[14]~61\ : std_logic;
SIGNAL \ssl|trns|index[15]~62_combout\ : std_logic;
SIGNAL \ssl|trns|index[15]~63\ : std_logic;
SIGNAL \ssl|trns|index[16]~64_combout\ : std_logic;
SIGNAL \ssl|trns|index[16]~65\ : std_logic;
SIGNAL \ssl|trns|index[17]~66_combout\ : std_logic;
SIGNAL \ssl|trns|index[17]~67\ : std_logic;
SIGNAL \ssl|trns|index[18]~68_combout\ : std_logic;
SIGNAL \ssl|trns|index[18]~69\ : std_logic;
SIGNAL \ssl|trns|index[19]~70_combout\ : std_logic;
SIGNAL \ssl|trns|index[19]~71\ : std_logic;
SIGNAL \ssl|trns|index[20]~72_combout\ : std_logic;
SIGNAL \ssl|trns|index[20]~73\ : std_logic;
SIGNAL \ssl|trns|index[21]~74_combout\ : std_logic;
SIGNAL \ssl|trns|index[21]~75\ : std_logic;
SIGNAL \ssl|trns|index[22]~76_combout\ : std_logic;
SIGNAL \ssl|trns|index[22]~77\ : std_logic;
SIGNAL \ssl|trns|index[23]~78_combout\ : std_logic;
SIGNAL \ssl|trns|index[23]~79\ : std_logic;
SIGNAL \ssl|trns|index[24]~80_combout\ : std_logic;
SIGNAL \ssl|trns|index[24]~81\ : std_logic;
SIGNAL \ssl|trns|index[25]~82_combout\ : std_logic;
SIGNAL \ssl|trns|index[25]~83\ : std_logic;
SIGNAL \ssl|trns|index[26]~84_combout\ : std_logic;
SIGNAL \ssl|trns|index[26]~85\ : std_logic;
SIGNAL \ssl|trns|index[27]~86_combout\ : std_logic;
SIGNAL \ssl|trns|index[27]~87\ : std_logic;
SIGNAL \ssl|trns|index[28]~88_combout\ : std_logic;
SIGNAL \ssl|trns|index[28]~89\ : std_logic;
SIGNAL \ssl|trns|index[29]~90_combout\ : std_logic;
SIGNAL \ssl|trns|index[29]~91\ : std_logic;
SIGNAL \ssl|trns|index[30]~92_combout\ : std_logic;
SIGNAL \ssl|trns|index[30]~93\ : std_logic;
SIGNAL \ssl|trns|index[31]~94_combout\ : std_logic;
SIGNAL \ssl|trns|LessThan0~0_combout\ : std_logic;
SIGNAL \ssl|busy~7_combout\ : std_logic;
SIGNAL \ssl|busy~6_combout\ : std_logic;
SIGNAL \ssl|busy~5_combout\ : std_logic;
SIGNAL \ssl|busy~3_combout\ : std_logic;
SIGNAL \ssl|busy~1_combout\ : std_logic;
SIGNAL \ssl|busy~2_combout\ : std_logic;
SIGNAL \ssl|busy~0_combout\ : std_logic;
SIGNAL \ssl|busy~4_combout\ : std_logic;
SIGNAL \ssl|busy~8_combout\ : std_logic;
SIGNAL \ssl|trns|LessThan0~1_combout\ : std_logic;
SIGNAL \ssl|trns|index[0]~33\ : std_logic;
SIGNAL \ssl|trns|index[1]~34_combout\ : std_logic;
SIGNAL \ssl|trns|index[1]~35\ : std_logic;
SIGNAL \ssl|trns|index[2]~36_combout\ : std_logic;
SIGNAL \clk~input_o\ : std_logic;
SIGNAL \clk~inputclkctrl_outclk\ : std_logic;
SIGNAL \mosi~input_o\ : std_logic;
SIGNAL \ssl|recv|index[0]~32_combout\ : std_logic;
SIGNAL \ssl|recv|index[1]~35\ : std_logic;
SIGNAL \ssl|recv|index[2]~36_combout\ : std_logic;
SIGNAL \ssl|recv|index[2]~37\ : std_logic;
SIGNAL \ssl|recv|index[3]~38_combout\ : std_logic;
SIGNAL \ssl|recv|index[3]~39\ : std_logic;
SIGNAL \ssl|recv|index[4]~40_combout\ : std_logic;
SIGNAL \ssl|recv|index[4]~41\ : std_logic;
SIGNAL \ssl|recv|index[5]~42_combout\ : std_logic;
SIGNAL \ssl|recv|index[5]~43\ : std_logic;
SIGNAL \ssl|recv|index[6]~44_combout\ : std_logic;
SIGNAL \ssl|recv|index[6]~45\ : std_logic;
SIGNAL \ssl|recv|index[7]~46_combout\ : std_logic;
SIGNAL \ssl|recv|index[7]~47\ : std_logic;
SIGNAL \ssl|recv|index[8]~48_combout\ : std_logic;
SIGNAL \ssl|recv|index[8]~49\ : std_logic;
SIGNAL \ssl|recv|index[9]~50_combout\ : std_logic;
SIGNAL \ssl|recv|index[9]~51\ : std_logic;
SIGNAL \ssl|recv|index[10]~52_combout\ : std_logic;
SIGNAL \ssl|recv|index[10]~53\ : std_logic;
SIGNAL \ssl|recv|index[11]~54_combout\ : std_logic;
SIGNAL \ssl|recv|index[11]~55\ : std_logic;
SIGNAL \ssl|recv|index[12]~56_combout\ : std_logic;
SIGNAL \ssl|recv|index[12]~57\ : std_logic;
SIGNAL \ssl|recv|index[13]~58_combout\ : std_logic;
SIGNAL \ssl|recv|index[13]~59\ : std_logic;
SIGNAL \ssl|recv|index[14]~60_combout\ : std_logic;
SIGNAL \ssl|recv|index[14]~61\ : std_logic;
SIGNAL \ssl|recv|index[15]~62_combout\ : std_logic;
SIGNAL \ssl|recv|index[15]~63\ : std_logic;
SIGNAL \ssl|recv|index[16]~64_combout\ : std_logic;
SIGNAL \ssl|recv|index[16]~65\ : std_logic;
SIGNAL \ssl|recv|index[17]~66_combout\ : std_logic;
SIGNAL \ssl|recv|index[17]~67\ : std_logic;
SIGNAL \ssl|recv|index[18]~68_combout\ : std_logic;
SIGNAL \ssl|recv|index[18]~69\ : std_logic;
SIGNAL \ssl|recv|index[19]~70_combout\ : std_logic;
SIGNAL \ssl|recv|index[19]~feeder_combout\ : std_logic;
SIGNAL \ssl|recv|index[19]~71\ : std_logic;
SIGNAL \ssl|recv|index[20]~72_combout\ : std_logic;
SIGNAL \ssl|recv|index[20]~feeder_combout\ : std_logic;
SIGNAL \ssl|recv|index[20]~73\ : std_logic;
SIGNAL \ssl|recv|index[21]~74_combout\ : std_logic;
SIGNAL \ssl|recv|index[21]~feeder_combout\ : std_logic;
SIGNAL \ssl|recv|index[21]~75\ : std_logic;
SIGNAL \ssl|recv|index[22]~76_combout\ : std_logic;
SIGNAL \ssl|recv|index[22]~feeder_combout\ : std_logic;
SIGNAL \ssl|recv|index[22]~77\ : std_logic;
SIGNAL \ssl|recv|index[23]~78_combout\ : std_logic;
SIGNAL \ssl|recv|index[23]~feeder_combout\ : std_logic;
SIGNAL \ssl|recv|index[23]~79\ : std_logic;
SIGNAL \ssl|recv|index[24]~80_combout\ : std_logic;
SIGNAL \ssl|recv|index[24]~feeder_combout\ : std_logic;
SIGNAL \ssl|recv|index[24]~81\ : std_logic;
SIGNAL \ssl|recv|index[25]~82_combout\ : std_logic;
SIGNAL \ssl|recv|index[25]~feeder_combout\ : std_logic;
SIGNAL \ssl|recv|index[25]~83\ : std_logic;
SIGNAL \ssl|recv|index[26]~84_combout\ : std_logic;
SIGNAL \ssl|recv|index[26]~feeder_combout\ : std_logic;
SIGNAL \ssl|recv|index[26]~85\ : std_logic;
SIGNAL \ssl|recv|index[27]~86_combout\ : std_logic;
SIGNAL \ssl|recv|index[27]~87\ : std_logic;
SIGNAL \ssl|recv|index[28]~88_combout\ : std_logic;
SIGNAL \ssl|recv|index[28]~feeder_combout\ : std_logic;
SIGNAL \ssl|recv|index[28]~89\ : std_logic;
SIGNAL \ssl|recv|index[29]~90_combout\ : std_logic;
SIGNAL \ssl|recv|index[29]~91\ : std_logic;
SIGNAL \ssl|recv|index[30]~92_combout\ : std_logic;
SIGNAL \ssl|recv|index[30]~93\ : std_logic;
SIGNAL \ssl|recv|index[31]~94_combout\ : std_logic;
SIGNAL \ssl|recv|LessThan0~0_combout\ : std_logic;
SIGNAL \ssl|busy~15_combout\ : std_logic;
SIGNAL \ssl|busy~16_combout\ : std_logic;
SIGNAL \ssl|busy~14_combout\ : std_logic;
SIGNAL \ssl|busy~10_combout\ : std_logic;
SIGNAL \ssl|busy~9_combout\ : std_logic;
SIGNAL \ssl|busy~12_combout\ : std_logic;
SIGNAL \ssl|busy~11_combout\ : std_logic;
SIGNAL \ssl|busy~13_combout\ : std_logic;
SIGNAL \ssl|busy~17_combout\ : std_logic;
SIGNAL \ssl|recv|LessThan0~1_combout\ : std_logic;
SIGNAL \ssl|recv|index[0]~33\ : std_logic;
SIGNAL \ssl|recv|index[1]~34_combout\ : std_logic;
SIGNAL \ssl|recv|Decoder0~3_combout\ : std_logic;
SIGNAL \ssl|recv|data_temp[5]~3_combout\ : std_logic;
SIGNAL \ssl|busy~18_combout\ : std_logic;
SIGNAL \ssl|busy~20_combout\ : std_logic;
SIGNAL \ssl|busy~19_combout\ : std_logic;
SIGNAL \ssl|busy~21_combout\ : std_logic;
SIGNAL \ssl|busy~combout\ : std_logic;
SIGNAL \sf_sb0~q\ : std_logic;
SIGNAL \sf_sb1~feeder_combout\ : std_logic;
SIGNAL \sf_sb1~q\ : std_logic;
SIGNAL \sf_ss0~q\ : std_logic;
SIGNAL \ssl|recv|Decoder0~5_combout\ : std_logic;
SIGNAL \ssl|recv|data_temp[1]~5_combout\ : std_logic;
SIGNAL \ssl|recv|Decoder0~4_combout\ : std_logic;
SIGNAL \ssl|recv|data_temp[6]~4_combout\ : std_logic;
SIGNAL \db|regs|Decoder0~1_combout\ : std_logic;
SIGNAL \ssl|recv|Decoder0~1_combout\ : std_logic;
SIGNAL \ssl|recv|data_temp[3]~1_combout\ : std_logic;
SIGNAL \ssl|recv|Decoder0~2_combout\ : std_logic;
SIGNAL \ssl|recv|data_temp[4]~2_combout\ : std_logic;
SIGNAL \ssl|recv|Decoder0~7_combout\ : std_logic;
SIGNAL \ssl|recv|data_temp[7]~7_combout\ : std_logic;
SIGNAL \db|reg_wr|mem~q\ : std_logic;
SIGNAL \db|regs|dbr[0][0]~0_combout\ : std_logic;
SIGNAL \db|control_logic|Selector2~0_combout\ : std_logic;
SIGNAL \db|control_logic|state.s4~q\ : std_logic;
SIGNAL \db|control_logic|next_state.s5~0_combout\ : std_logic;
SIGNAL \db|control_logic|state.s5~q\ : std_logic;
SIGNAL \db|control_logic|next_state.s6~0_combout\ : std_logic;
SIGNAL \db|control_logic|state.s6~q\ : std_logic;
SIGNAL \db|regs|dbr[0][0]~1_combout\ : std_logic;
SIGNAL \db|regs|Decoder0~4_combout\ : std_logic;
SIGNAL \db|regs|dbr[0][1]~q\ : std_logic;
SIGNAL \ssl|recv|Decoder0~6_combout\ : std_logic;
SIGNAL \ssl|recv|data_temp[0]~6_combout\ : std_logic;
SIGNAL \db|regs|dbr[0][0]~q\ : std_logic;
SIGNAL \db|regs|dbr[0][7]~feeder_combout\ : std_logic;
SIGNAL \db|regs|dbr[0][7]~q\ : std_logic;
SIGNAL \db|regs|dbr[0][6]~q\ : std_logic;
SIGNAL \db|regs|dbr[0][4]~feeder_combout\ : std_logic;
SIGNAL \db|regs|dbr[0][4]~q\ : std_logic;
SIGNAL \db|regs|dbr[0][5]~q\ : std_logic;
SIGNAL \ssl|recv|Decoder0~0_combout\ : std_logic;
SIGNAL \ssl|recv|data_temp[2]~0_combout\ : std_logic;
SIGNAL \db|regs|dbr[0][2]~q\ : std_logic;
SIGNAL \db|regs|dbr[0][3]~q\ : std_logic;
SIGNAL \db|control_logic|Equal2~2_combout\ : std_logic;
SIGNAL \db|control_logic|Equal2~4_combout\ : std_logic;
SIGNAL \db|control_logic|Selector3~0_combout\ : std_logic;
SIGNAL \db|control_logic|state.s7~q\ : std_logic;
SIGNAL \db|control_logic|process_1~0_combout\ : std_logic;
SIGNAL \db|control_logic|next_state.s9~0_combout\ : std_logic;
SIGNAL \db|control_logic|next_state.s9~1_combout\ : std_logic;
SIGNAL \db|control_logic|state.s9~q\ : std_logic;
SIGNAL \db|control_logic|next_state.s8~0_combout\ : std_logic;
SIGNAL \db|control_logic|state.s8~q\ : std_logic;
SIGNAL \db|control_logic|Equal2~3_combout\ : std_logic;
SIGNAL \db|control_logic|Selector0~0_combout\ : std_logic;
SIGNAL \db|control_logic|Selector0~1_combout\ : std_logic;
SIGNAL \db|control_logic|Selector0~2_combout\ : std_logic;
SIGNAL \db|control_logic|state.s0~q\ : std_logic;
SIGNAL \db|control_logic|Selector1~0_combout\ : std_logic;
SIGNAL \db|control_logic|state.s1~q\ : std_logic;
SIGNAL \db|control_logic|next_state.s2~0_combout\ : std_logic;
SIGNAL \db|control_logic|state.s2~q\ : std_logic;
SIGNAL \db|control_logic|next_state.s3~0_combout\ : std_logic;
SIGNAL \db|control_logic|state.s3~q\ : std_logic;
SIGNAL \db|regs|LessThan1~0_combout\ : std_logic;
SIGNAL \db|regs|dbr[4][2]~feeder_combout\ : std_logic;
SIGNAL \db|regs|Decoder0~0_combout\ : std_logic;
SIGNAL \db|regs|Decoder0~7_combout\ : std_logic;
SIGNAL \db|regs|dbr[4][2]~q\ : std_logic;
SIGNAL \db|regs|dbr[8][2]~feeder_combout\ : std_logic;
SIGNAL \db|regs|dbr[8][2]~q\ : std_logic;
SIGNAL \db|regs|dbr[5][2]~feeder_combout\ : std_logic;
SIGNAL \db|regs|Decoder0~6_combout\ : std_logic;
SIGNAL \db|regs|dbr[5][2]~q\ : std_logic;
SIGNAL \db|regs|dbr[9][2]~q\ : std_logic;
SIGNAL \db|regs|data_out[2]~1_combout\ : std_logic;
SIGNAL \db|regs|data_out[2]~2_combout\ : std_logic;
SIGNAL \db|regs|dbr[2][2]~feeder_combout\ : std_logic;
SIGNAL \db|regs|Decoder0~3_combout\ : std_logic;
SIGNAL \db|regs|dbr[2][2]~q\ : std_logic;
SIGNAL \db|regs|dbr[6][2]~feeder_combout\ : std_logic;
SIGNAL \db|regs|dbr[6][2]~q\ : std_logic;
SIGNAL \db|regs|dbr[3][2]~feeder_combout\ : std_logic;
SIGNAL \db|regs|Decoder0~5_combout\ : std_logic;
SIGNAL \db|regs|dbr[3][2]~q\ : std_logic;
SIGNAL \db|regs|dbr[7][2]~q\ : std_logic;
SIGNAL \db|regs|Mux5~2_combout\ : std_logic;
SIGNAL \db|regs|Mux5~3_combout\ : std_logic;
SIGNAL \db|regs|Mux5~0_combout\ : std_logic;
SIGNAL \db|regs|Decoder0~2_combout\ : std_logic;
SIGNAL \db|regs|dbr[1][2]~q\ : std_logic;
SIGNAL \db|regs|Mux5~1_combout\ : std_logic;
SIGNAL \db|regs|data_out[2]~0_combout\ : std_logic;
SIGNAL \db|regs|data_out[2]~3_combout\ : std_logic;
SIGNAL \db|regs|dbr[5][1]~feeder_combout\ : std_logic;
SIGNAL \db|regs|dbr[5][1]~q\ : std_logic;
SIGNAL \db|regs|dbr[9][1]~feeder_combout\ : std_logic;
SIGNAL \db|regs|dbr[9][1]~q\ : std_logic;
SIGNAL \db|regs|dbr[4][1]~feeder_combout\ : std_logic;
SIGNAL \db|regs|dbr[4][1]~q\ : std_logic;
SIGNAL \db|regs|dbr[8][1]~q\ : std_logic;
SIGNAL \db|regs|data_out[1]~5_combout\ : std_logic;
SIGNAL \db|regs|dbr[3][1]~feeder_combout\ : std_logic;
SIGNAL \db|regs|dbr[3][1]~q\ : std_logic;
SIGNAL \db|regs|dbr[1][1]~q\ : std_logic;
SIGNAL \db|regs|dbr[2][1]~feeder_combout\ : std_logic;
SIGNAL \db|regs|dbr[2][1]~q\ : std_logic;
SIGNAL \db|regs|Mux6~2_combout\ : std_logic;
SIGNAL \db|regs|Mux6~3_combout\ : std_logic;
SIGNAL \db|regs|dbr[6][1]~feeder_combout\ : std_logic;
SIGNAL \db|regs|dbr[6][1]~q\ : std_logic;
SIGNAL \db|regs|dbr[7][1]~q\ : std_logic;
SIGNAL \db|regs|Mux6~0_combout\ : std_logic;
SIGNAL \db|regs|Mux6~1_combout\ : std_logic;
SIGNAL \db|regs|data_out[1]~4_combout\ : std_logic;
SIGNAL \db|regs|data_out[1]~6_combout\ : std_logic;
SIGNAL \db|regs|dbr[5][0]~feeder_combout\ : std_logic;
SIGNAL \db|regs|dbr[5][0]~q\ : std_logic;
SIGNAL \db|regs|dbr[9][0]~q\ : std_logic;
SIGNAL \db|regs|data_out[0]~8_combout\ : std_logic;
SIGNAL \db|regs|dbr[4][0]~feeder_combout\ : std_logic;
SIGNAL \db|regs|dbr[4][0]~q\ : std_logic;
SIGNAL \db|regs|dbr[8][0]~q\ : std_logic;
SIGNAL \db|regs|dbr[10][0]~feeder_combout\ : std_logic;
SIGNAL \db|regs|dbr[10][0]~q\ : std_logic;
SIGNAL \db|regs|data_out[0]~7_combout\ : std_logic;
SIGNAL \db|regs|data_out[0]~9_combout\ : std_logic;
SIGNAL \db|regs|dbr[2][0]~feeder_combout\ : std_logic;
SIGNAL \db|regs|dbr[2][0]~q\ : std_logic;
SIGNAL \db|regs|dbr[6][0]~feeder_combout\ : std_logic;
SIGNAL \db|regs|dbr[6][0]~q\ : std_logic;
SIGNAL \db|regs|dbr[3][0]~feeder_combout\ : std_logic;
SIGNAL \db|regs|dbr[3][0]~q\ : std_logic;
SIGNAL \db|regs|dbr[7][0]~q\ : std_logic;
SIGNAL \db|regs|Mux7~0_combout\ : std_logic;
SIGNAL \db|regs|Mux7~1_combout\ : std_logic;
SIGNAL \db|regs|Mux7~2_combout\ : std_logic;
SIGNAL \db|regs|dbr[1][0]~q\ : std_logic;
SIGNAL \db|regs|Mux7~3_combout\ : std_logic;
SIGNAL \db|regs|data_out[0]~10_combout\ : std_logic;
SIGNAL \db|regs|data_out[0]~11_combout\ : std_logic;
SIGNAL \ssl|trns|Mux0~0_combout\ : std_logic;
SIGNAL \db|regs|dbr[5][3]~feeder_combout\ : std_logic;
SIGNAL \db|regs|dbr[5][3]~q\ : std_logic;
SIGNAL \db|regs|dbr[4][3]~feeder_combout\ : std_logic;
SIGNAL \db|regs|dbr[4][3]~q\ : std_logic;
SIGNAL \db|regs|Mux4~0_combout\ : std_logic;
SIGNAL \db|regs|dbr[3][3]~feeder_combout\ : std_logic;
SIGNAL \db|regs|dbr[3][3]~q\ : std_logic;
SIGNAL \db|regs|dbr[7][3]~q\ : std_logic;
SIGNAL \db|regs|dbr[2][3]~feeder_combout\ : std_logic;
SIGNAL \db|regs|dbr[2][3]~q\ : std_logic;
SIGNAL \db|regs|dbr[6][3]~feeder_combout\ : std_logic;
SIGNAL \db|regs|dbr[6][3]~q\ : std_logic;
SIGNAL \db|regs|Mux4~1_combout\ : std_logic;
SIGNAL \db|regs|Mux4~2_combout\ : std_logic;
SIGNAL \db|regs|dbr[1][3]~q\ : std_logic;
SIGNAL \db|regs|Mux4~3_combout\ : std_logic;
SIGNAL \db|regs|data_out[3]~12_combout\ : std_logic;
SIGNAL \db|regs|dbr[8][3]~q\ : std_logic;
SIGNAL \db|regs|dbr[9][3]~feeder_combout\ : std_logic;
SIGNAL \db|regs|dbr[9][3]~q\ : std_logic;
SIGNAL \db|regs|data_out[3]~13_combout\ : std_logic;
SIGNAL \db|regs|data_out[3]~14_combout\ : std_logic;
SIGNAL \ssl|trns|Mux0~1_combout\ : std_logic;
SIGNAL \db|regs|dbr[4][6]~feeder_combout\ : std_logic;
SIGNAL \db|regs|dbr[4][6]~q\ : std_logic;
SIGNAL \db|regs|dbr[8][6]~q\ : std_logic;
SIGNAL \db|regs|dbr[5][6]~feeder_combout\ : std_logic;
SIGNAL \db|regs|dbr[5][6]~q\ : std_logic;
SIGNAL \db|regs|dbr[9][6]~feeder_combout\ : std_logic;
SIGNAL \db|regs|dbr[9][6]~q\ : std_logic;
SIGNAL \db|regs|data_out[6]~19_combout\ : std_logic;
SIGNAL \db|regs|Mux1~0_combout\ : std_logic;
SIGNAL \db|regs|dbr[3][6]~feeder_combout\ : std_logic;
SIGNAL \db|regs|dbr[3][6]~q\ : std_logic;
SIGNAL \db|regs|dbr[7][6]~q\ : std_logic;
SIGNAL \db|regs|dbr[2][6]~feeder_combout\ : std_logic;
SIGNAL \db|regs|dbr[2][6]~q\ : std_logic;
SIGNAL \db|regs|dbr[6][6]~feeder_combout\ : std_logic;
SIGNAL \db|regs|dbr[6][6]~q\ : std_logic;
SIGNAL \db|regs|Mux1~1_combout\ : std_logic;
SIGNAL \db|regs|Mux1~2_combout\ : std_logic;
SIGNAL \db|regs|dbr[1][6]~q\ : std_logic;
SIGNAL \db|regs|Mux1~3_combout\ : std_logic;
SIGNAL \db|regs|data_out[6]~18_combout\ : std_logic;
SIGNAL \db|regs|data_out[6]~20_combout\ : std_logic;
SIGNAL \db|regs|dbr[2][4]~feeder_combout\ : std_logic;
SIGNAL \db|regs|dbr[2][4]~q\ : std_logic;
SIGNAL \db|regs|dbr[6][4]~feeder_combout\ : std_logic;
SIGNAL \db|regs|dbr[6][4]~q\ : std_logic;
SIGNAL \db|regs|dbr[3][4]~feeder_combout\ : std_logic;
SIGNAL \db|regs|dbr[3][4]~q\ : std_logic;
SIGNAL \db|regs|dbr[7][4]~q\ : std_logic;
SIGNAL \db|regs|dbr[5][4]~feeder_combout\ : std_logic;
SIGNAL \db|regs|dbr[5][4]~q\ : std_logic;
SIGNAL \db|regs|dbr[4][4]~feeder_combout\ : std_logic;
SIGNAL \db|regs|dbr[4][4]~q\ : std_logic;
SIGNAL \db|regs|Mux3~0_combout\ : std_logic;
SIGNAL \db|regs|Mux3~1_combout\ : std_logic;
SIGNAL \db|regs|Mux3~2_combout\ : std_logic;
SIGNAL \db|regs|dbr[1][4]~q\ : std_logic;
SIGNAL \db|regs|Mux3~3_combout\ : std_logic;
SIGNAL \db|regs|data_out[4]~21_combout\ : std_logic;
SIGNAL \db|regs|dbr[8][4]~q\ : std_logic;
SIGNAL \db|regs|dbr[9][4]~feeder_combout\ : std_logic;
SIGNAL \db|regs|dbr[9][4]~q\ : std_logic;
SIGNAL \db|regs|data_out[4]~22_combout\ : std_logic;
SIGNAL \db|regs|data_out[4]~23_combout\ : std_logic;
SIGNAL \ssl|trns|Mux0~2_combout\ : std_logic;
SIGNAL \db|regs|dbr[2][5]~feeder_combout\ : std_logic;
SIGNAL \db|regs|dbr[2][5]~q\ : std_logic;
SIGNAL \db|regs|dbr[6][5]~feeder_combout\ : std_logic;
SIGNAL \db|regs|dbr[6][5]~q\ : std_logic;
SIGNAL \db|regs|dbr[3][5]~feeder_combout\ : std_logic;
SIGNAL \db|regs|dbr[3][5]~q\ : std_logic;
SIGNAL \db|regs|dbr[7][5]~q\ : std_logic;
SIGNAL \db|regs|dbr[4][5]~feeder_combout\ : std_logic;
SIGNAL \db|regs|dbr[4][5]~q\ : std_logic;
SIGNAL \db|regs|dbr[5][5]~feeder_combout\ : std_logic;
SIGNAL \db|regs|dbr[5][5]~q\ : std_logic;
SIGNAL \db|regs|Mux2~0_combout\ : std_logic;
SIGNAL \db|regs|Mux2~1_combout\ : std_logic;
SIGNAL \db|regs|Mux2~2_combout\ : std_logic;
SIGNAL \db|regs|dbr[1][5]~q\ : std_logic;
SIGNAL \db|regs|Mux2~3_combout\ : std_logic;
SIGNAL \db|regs|data_out[5]~15_combout\ : std_logic;
SIGNAL \db|regs|dbr[8][5]~q\ : std_logic;
SIGNAL \db|regs|dbr[9][5]~feeder_combout\ : std_logic;
SIGNAL \db|regs|dbr[9][5]~q\ : std_logic;
SIGNAL \db|regs|data_out[5]~16_combout\ : std_logic;
SIGNAL \db|regs|data_out[5]~17_combout\ : std_logic;
SIGNAL \db|regs|dbr[2][7]~feeder_combout\ : std_logic;
SIGNAL \db|regs|dbr[2][7]~q\ : std_logic;
SIGNAL \db|regs|dbr[6][7]~feeder_combout\ : std_logic;
SIGNAL \db|regs|dbr[6][7]~q\ : std_logic;
SIGNAL \db|regs|dbr[3][7]~feeder_combout\ : std_logic;
SIGNAL \db|regs|dbr[3][7]~q\ : std_logic;
SIGNAL \db|regs|dbr[7][7]~q\ : std_logic;
SIGNAL \db|regs|dbr[4][7]~feeder_combout\ : std_logic;
SIGNAL \db|regs|dbr[4][7]~q\ : std_logic;
SIGNAL \db|regs|dbr[5][7]~feeder_combout\ : std_logic;
SIGNAL \db|regs|dbr[5][7]~q\ : std_logic;
SIGNAL \db|regs|Mux0~0_combout\ : std_logic;
SIGNAL \db|regs|Mux0~1_combout\ : std_logic;
SIGNAL \db|regs|Mux0~2_combout\ : std_logic;
SIGNAL \db|regs|dbr[1][7]~q\ : std_logic;
SIGNAL \db|regs|Mux0~3_combout\ : std_logic;
SIGNAL \db|regs|data_out[7]~24_combout\ : std_logic;
SIGNAL \db|regs|dbr[8][7]~q\ : std_logic;
SIGNAL \db|regs|dbr[9][7]~feeder_combout\ : std_logic;
SIGNAL \db|regs|dbr[9][7]~q\ : std_logic;
SIGNAL \db|regs|data_out[7]~25_combout\ : std_logic;
SIGNAL \db|regs|data_out[7]~26_combout\ : std_logic;
SIGNAL \ssl|trns|Mux0~3_combout\ : std_logic;
SIGNAL \ssl|trns|Mux0~4_combout\ : std_logic;
SIGNAL \db|reg_addr|mem\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \ssl|recv|data_temp\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \ssl|recv|index\ : std_logic_vector(31 DOWNTO 0);
SIGNAL \ssl|trns|index\ : std_logic_vector(31 DOWNTO 0);
SIGNAL \ALT_INV_clk~inputclkctrl_outclk\ : std_logic;
SIGNAL \ALT_INV_rst~input_o\ : std_logic;
SIGNAL \ALT_INV_sck~input_o\ : std_logic;
SIGNAL \ALT_INV_ss~input_o\ : std_logic;

BEGIN

ww_mosi <= mosi;
ww_ss <= ss;
ww_sck <= sck;
ww_clk <= clk;
ww_rst <= rst;
miso <= ww_miso;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;

\clk~inputclkctrl_INCLK_bus\ <= (vcc & vcc & vcc & \clk~input_o\);
\ALT_INV_clk~inputclkctrl_outclk\ <= NOT \clk~inputclkctrl_outclk\;
\ALT_INV_rst~input_o\ <= NOT \rst~input_o\;
\ALT_INV_sck~input_o\ <= NOT \sck~input_o\;
\ALT_INV_ss~input_o\ <= NOT \ss~input_o\;

-- Location: IOOBUF_X3_Y24_N9
\miso~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \ssl|trns|Mux0~4_combout\,
	oe => \ALT_INV_ss~input_o\,
	devoe => ww_devoe,
	o => \miso~output_o\);

-- Location: IOIBUF_X13_Y24_N15
\sck~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_sck,
	o => \sck~input_o\);

-- Location: LCCOMB_X13_Y21_N0
\ssl|trns|index[0]~32\ : cycloneive_lcell_comb
-- Equation(s):
-- \ssl|trns|index[0]~32_combout\ = !\ssl|trns|index\(0)
-- \ssl|trns|index[0]~33\ = CARRY(!\ssl|trns|index\(0))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100110011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \ssl|trns|index\(0),
	combout => \ssl|trns|index[0]~32_combout\,
	cout => \ssl|trns|index[0]~33\);

-- Location: LCCOMB_X12_Y20_N14
\~GND\ : cycloneive_lcell_comb
-- Equation(s):
-- \~GND~combout\ = GND

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	combout => \~GND~combout\);

-- Location: IOIBUF_X30_Y24_N8
\rst~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_rst,
	o => \rst~input_o\);

-- Location: LCCOMB_X13_Y21_N4
\ssl|trns|index[2]~36\ : cycloneive_lcell_comb
-- Equation(s):
-- \ssl|trns|index[2]~36_combout\ = (\ssl|trns|index\(2) & (!\ssl|trns|index[1]~35\)) # (!\ssl|trns|index\(2) & ((\ssl|trns|index[1]~35\) # (GND)))
-- \ssl|trns|index[2]~37\ = CARRY((!\ssl|trns|index[1]~35\) # (!\ssl|trns|index\(2)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \ssl|trns|index\(2),
	datad => VCC,
	cin => \ssl|trns|index[1]~35\,
	combout => \ssl|trns|index[2]~36_combout\,
	cout => \ssl|trns|index[2]~37\);

-- Location: LCCOMB_X13_Y21_N6
\ssl|trns|index[3]~38\ : cycloneive_lcell_comb
-- Equation(s):
-- \ssl|trns|index[3]~38_combout\ = (\ssl|trns|index\(3) & (\ssl|trns|index[2]~37\ & VCC)) # (!\ssl|trns|index\(3) & (!\ssl|trns|index[2]~37\))
-- \ssl|trns|index[3]~39\ = CARRY((!\ssl|trns|index\(3) & !\ssl|trns|index[2]~37\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100000101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \ssl|trns|index\(3),
	datad => VCC,
	cin => \ssl|trns|index[2]~37\,
	combout => \ssl|trns|index[3]~38_combout\,
	cout => \ssl|trns|index[3]~39\);

-- Location: IOIBUF_X13_Y24_N1
\ss~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_ss,
	o => \ss~input_o\);

-- Location: FF_X13_Y21_N7
\ssl|trns|index[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_sck~input_o\,
	d => \ssl|trns|index[3]~38_combout\,
	asdata => \~GND~combout\,
	clrn => \ALT_INV_rst~input_o\,
	sload => \ssl|trns|LessThan0~1_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \ssl|trns|index\(3));

-- Location: LCCOMB_X13_Y21_N8
\ssl|trns|index[4]~40\ : cycloneive_lcell_comb
-- Equation(s):
-- \ssl|trns|index[4]~40_combout\ = (\ssl|trns|index\(4) & ((GND) # (!\ssl|trns|index[3]~39\))) # (!\ssl|trns|index\(4) & (\ssl|trns|index[3]~39\ $ (GND)))
-- \ssl|trns|index[4]~41\ = CARRY((\ssl|trns|index\(4)) # (!\ssl|trns|index[3]~39\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \ssl|trns|index\(4),
	datad => VCC,
	cin => \ssl|trns|index[3]~39\,
	combout => \ssl|trns|index[4]~40_combout\,
	cout => \ssl|trns|index[4]~41\);

-- Location: FF_X13_Y21_N9
\ssl|trns|index[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_sck~input_o\,
	d => \ssl|trns|index[4]~40_combout\,
	asdata => \~GND~combout\,
	clrn => \ALT_INV_rst~input_o\,
	sload => \ssl|trns|LessThan0~1_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \ssl|trns|index\(4));

-- Location: LCCOMB_X13_Y21_N10
\ssl|trns|index[5]~42\ : cycloneive_lcell_comb
-- Equation(s):
-- \ssl|trns|index[5]~42_combout\ = (\ssl|trns|index\(5) & (\ssl|trns|index[4]~41\ & VCC)) # (!\ssl|trns|index\(5) & (!\ssl|trns|index[4]~41\))
-- \ssl|trns|index[5]~43\ = CARRY((!\ssl|trns|index\(5) & !\ssl|trns|index[4]~41\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100000101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \ssl|trns|index\(5),
	datad => VCC,
	cin => \ssl|trns|index[4]~41\,
	combout => \ssl|trns|index[5]~42_combout\,
	cout => \ssl|trns|index[5]~43\);

-- Location: FF_X13_Y21_N11
\ssl|trns|index[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_sck~input_o\,
	d => \ssl|trns|index[5]~42_combout\,
	asdata => \~GND~combout\,
	clrn => \ALT_INV_rst~input_o\,
	sload => \ssl|trns|LessThan0~1_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \ssl|trns|index\(5));

-- Location: LCCOMB_X13_Y21_N12
\ssl|trns|index[6]~44\ : cycloneive_lcell_comb
-- Equation(s):
-- \ssl|trns|index[6]~44_combout\ = (\ssl|trns|index\(6) & ((GND) # (!\ssl|trns|index[5]~43\))) # (!\ssl|trns|index\(6) & (\ssl|trns|index[5]~43\ $ (GND)))
-- \ssl|trns|index[6]~45\ = CARRY((\ssl|trns|index\(6)) # (!\ssl|trns|index[5]~43\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101010101111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \ssl|trns|index\(6),
	datad => VCC,
	cin => \ssl|trns|index[5]~43\,
	combout => \ssl|trns|index[6]~44_combout\,
	cout => \ssl|trns|index[6]~45\);

-- Location: FF_X13_Y21_N13
\ssl|trns|index[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_sck~input_o\,
	d => \ssl|trns|index[6]~44_combout\,
	asdata => \~GND~combout\,
	clrn => \ALT_INV_rst~input_o\,
	sload => \ssl|trns|LessThan0~1_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \ssl|trns|index\(6));

-- Location: LCCOMB_X13_Y21_N14
\ssl|trns|index[7]~46\ : cycloneive_lcell_comb
-- Equation(s):
-- \ssl|trns|index[7]~46_combout\ = (\ssl|trns|index\(7) & (\ssl|trns|index[6]~45\ & VCC)) # (!\ssl|trns|index\(7) & (!\ssl|trns|index[6]~45\))
-- \ssl|trns|index[7]~47\ = CARRY((!\ssl|trns|index\(7) & !\ssl|trns|index[6]~45\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \ssl|trns|index\(7),
	datad => VCC,
	cin => \ssl|trns|index[6]~45\,
	combout => \ssl|trns|index[7]~46_combout\,
	cout => \ssl|trns|index[7]~47\);

-- Location: FF_X13_Y21_N15
\ssl|trns|index[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_sck~input_o\,
	d => \ssl|trns|index[7]~46_combout\,
	asdata => \~GND~combout\,
	clrn => \ALT_INV_rst~input_o\,
	sload => \ssl|trns|LessThan0~1_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \ssl|trns|index\(7));

-- Location: LCCOMB_X13_Y21_N16
\ssl|trns|index[8]~48\ : cycloneive_lcell_comb
-- Equation(s):
-- \ssl|trns|index[8]~48_combout\ = (\ssl|trns|index\(8) & ((GND) # (!\ssl|trns|index[7]~47\))) # (!\ssl|trns|index\(8) & (\ssl|trns|index[7]~47\ $ (GND)))
-- \ssl|trns|index[8]~49\ = CARRY((\ssl|trns|index\(8)) # (!\ssl|trns|index[7]~47\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \ssl|trns|index\(8),
	datad => VCC,
	cin => \ssl|trns|index[7]~47\,
	combout => \ssl|trns|index[8]~48_combout\,
	cout => \ssl|trns|index[8]~49\);

-- Location: FF_X13_Y21_N17
\ssl|trns|index[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_sck~input_o\,
	d => \ssl|trns|index[8]~48_combout\,
	asdata => \~GND~combout\,
	clrn => \ALT_INV_rst~input_o\,
	sload => \ssl|trns|LessThan0~1_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \ssl|trns|index\(8));

-- Location: LCCOMB_X13_Y21_N18
\ssl|trns|index[9]~50\ : cycloneive_lcell_comb
-- Equation(s):
-- \ssl|trns|index[9]~50_combout\ = (\ssl|trns|index\(9) & (\ssl|trns|index[8]~49\ & VCC)) # (!\ssl|trns|index\(9) & (!\ssl|trns|index[8]~49\))
-- \ssl|trns|index[9]~51\ = CARRY((!\ssl|trns|index\(9) & !\ssl|trns|index[8]~49\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \ssl|trns|index\(9),
	datad => VCC,
	cin => \ssl|trns|index[8]~49\,
	combout => \ssl|trns|index[9]~50_combout\,
	cout => \ssl|trns|index[9]~51\);

-- Location: FF_X13_Y21_N19
\ssl|trns|index[9]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_sck~input_o\,
	d => \ssl|trns|index[9]~50_combout\,
	asdata => \~GND~combout\,
	clrn => \ALT_INV_rst~input_o\,
	sload => \ssl|trns|LessThan0~1_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \ssl|trns|index\(9));

-- Location: LCCOMB_X13_Y21_N20
\ssl|trns|index[10]~52\ : cycloneive_lcell_comb
-- Equation(s):
-- \ssl|trns|index[10]~52_combout\ = (\ssl|trns|index\(10) & ((GND) # (!\ssl|trns|index[9]~51\))) # (!\ssl|trns|index\(10) & (\ssl|trns|index[9]~51\ $ (GND)))
-- \ssl|trns|index[10]~53\ = CARRY((\ssl|trns|index\(10)) # (!\ssl|trns|index[9]~51\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \ssl|trns|index\(10),
	datad => VCC,
	cin => \ssl|trns|index[9]~51\,
	combout => \ssl|trns|index[10]~52_combout\,
	cout => \ssl|trns|index[10]~53\);

-- Location: FF_X13_Y21_N21
\ssl|trns|index[10]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_sck~input_o\,
	d => \ssl|trns|index[10]~52_combout\,
	asdata => \~GND~combout\,
	clrn => \ALT_INV_rst~input_o\,
	sload => \ssl|trns|LessThan0~1_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \ssl|trns|index\(10));

-- Location: LCCOMB_X13_Y21_N22
\ssl|trns|index[11]~54\ : cycloneive_lcell_comb
-- Equation(s):
-- \ssl|trns|index[11]~54_combout\ = (\ssl|trns|index\(11) & (\ssl|trns|index[10]~53\ & VCC)) # (!\ssl|trns|index\(11) & (!\ssl|trns|index[10]~53\))
-- \ssl|trns|index[11]~55\ = CARRY((!\ssl|trns|index\(11) & !\ssl|trns|index[10]~53\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100000101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \ssl|trns|index\(11),
	datad => VCC,
	cin => \ssl|trns|index[10]~53\,
	combout => \ssl|trns|index[11]~54_combout\,
	cout => \ssl|trns|index[11]~55\);

-- Location: FF_X13_Y21_N23
\ssl|trns|index[11]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_sck~input_o\,
	d => \ssl|trns|index[11]~54_combout\,
	asdata => \~GND~combout\,
	clrn => \ALT_INV_rst~input_o\,
	sload => \ssl|trns|LessThan0~1_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \ssl|trns|index\(11));

-- Location: LCCOMB_X13_Y21_N24
\ssl|trns|index[12]~56\ : cycloneive_lcell_comb
-- Equation(s):
-- \ssl|trns|index[12]~56_combout\ = (\ssl|trns|index\(12) & ((GND) # (!\ssl|trns|index[11]~55\))) # (!\ssl|trns|index\(12) & (\ssl|trns|index[11]~55\ $ (GND)))
-- \ssl|trns|index[12]~57\ = CARRY((\ssl|trns|index\(12)) # (!\ssl|trns|index[11]~55\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \ssl|trns|index\(12),
	datad => VCC,
	cin => \ssl|trns|index[11]~55\,
	combout => \ssl|trns|index[12]~56_combout\,
	cout => \ssl|trns|index[12]~57\);

-- Location: FF_X13_Y21_N25
\ssl|trns|index[12]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_sck~input_o\,
	d => \ssl|trns|index[12]~56_combout\,
	asdata => \~GND~combout\,
	clrn => \ALT_INV_rst~input_o\,
	sload => \ssl|trns|LessThan0~1_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \ssl|trns|index\(12));

-- Location: LCCOMB_X13_Y21_N26
\ssl|trns|index[13]~58\ : cycloneive_lcell_comb
-- Equation(s):
-- \ssl|trns|index[13]~58_combout\ = (\ssl|trns|index\(13) & (\ssl|trns|index[12]~57\ & VCC)) # (!\ssl|trns|index\(13) & (!\ssl|trns|index[12]~57\))
-- \ssl|trns|index[13]~59\ = CARRY((!\ssl|trns|index\(13) & !\ssl|trns|index[12]~57\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100000101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \ssl|trns|index\(13),
	datad => VCC,
	cin => \ssl|trns|index[12]~57\,
	combout => \ssl|trns|index[13]~58_combout\,
	cout => \ssl|trns|index[13]~59\);

-- Location: FF_X13_Y21_N27
\ssl|trns|index[13]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_sck~input_o\,
	d => \ssl|trns|index[13]~58_combout\,
	asdata => \~GND~combout\,
	clrn => \ALT_INV_rst~input_o\,
	sload => \ssl|trns|LessThan0~1_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \ssl|trns|index\(13));

-- Location: LCCOMB_X13_Y21_N28
\ssl|trns|index[14]~60\ : cycloneive_lcell_comb
-- Equation(s):
-- \ssl|trns|index[14]~60_combout\ = (\ssl|trns|index\(14) & ((GND) # (!\ssl|trns|index[13]~59\))) # (!\ssl|trns|index\(14) & (\ssl|trns|index[13]~59\ $ (GND)))
-- \ssl|trns|index[14]~61\ = CARRY((\ssl|trns|index\(14)) # (!\ssl|trns|index[13]~59\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \ssl|trns|index\(14),
	datad => VCC,
	cin => \ssl|trns|index[13]~59\,
	combout => \ssl|trns|index[14]~60_combout\,
	cout => \ssl|trns|index[14]~61\);

-- Location: FF_X13_Y21_N29
\ssl|trns|index[14]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_sck~input_o\,
	d => \ssl|trns|index[14]~60_combout\,
	asdata => \~GND~combout\,
	clrn => \ALT_INV_rst~input_o\,
	sload => \ssl|trns|LessThan0~1_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \ssl|trns|index\(14));

-- Location: LCCOMB_X13_Y21_N30
\ssl|trns|index[15]~62\ : cycloneive_lcell_comb
-- Equation(s):
-- \ssl|trns|index[15]~62_combout\ = (\ssl|trns|index\(15) & (\ssl|trns|index[14]~61\ & VCC)) # (!\ssl|trns|index\(15) & (!\ssl|trns|index[14]~61\))
-- \ssl|trns|index[15]~63\ = CARRY((!\ssl|trns|index\(15) & !\ssl|trns|index[14]~61\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100000101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \ssl|trns|index\(15),
	datad => VCC,
	cin => \ssl|trns|index[14]~61\,
	combout => \ssl|trns|index[15]~62_combout\,
	cout => \ssl|trns|index[15]~63\);

-- Location: FF_X13_Y21_N31
\ssl|trns|index[15]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_sck~input_o\,
	d => \ssl|trns|index[15]~62_combout\,
	asdata => \~GND~combout\,
	clrn => \ALT_INV_rst~input_o\,
	sload => \ssl|trns|LessThan0~1_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \ssl|trns|index\(15));

-- Location: LCCOMB_X13_Y20_N0
\ssl|trns|index[16]~64\ : cycloneive_lcell_comb
-- Equation(s):
-- \ssl|trns|index[16]~64_combout\ = (\ssl|trns|index\(16) & ((GND) # (!\ssl|trns|index[15]~63\))) # (!\ssl|trns|index\(16) & (\ssl|trns|index[15]~63\ $ (GND)))
-- \ssl|trns|index[16]~65\ = CARRY((\ssl|trns|index\(16)) # (!\ssl|trns|index[15]~63\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \ssl|trns|index\(16),
	datad => VCC,
	cin => \ssl|trns|index[15]~63\,
	combout => \ssl|trns|index[16]~64_combout\,
	cout => \ssl|trns|index[16]~65\);

-- Location: FF_X13_Y20_N1
\ssl|trns|index[16]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_sck~input_o\,
	d => \ssl|trns|index[16]~64_combout\,
	asdata => \~GND~combout\,
	clrn => \ALT_INV_rst~input_o\,
	sload => \ssl|trns|LessThan0~1_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \ssl|trns|index\(16));

-- Location: LCCOMB_X13_Y20_N2
\ssl|trns|index[17]~66\ : cycloneive_lcell_comb
-- Equation(s):
-- \ssl|trns|index[17]~66_combout\ = (\ssl|trns|index\(17) & (\ssl|trns|index[16]~65\ & VCC)) # (!\ssl|trns|index\(17) & (!\ssl|trns|index[16]~65\))
-- \ssl|trns|index[17]~67\ = CARRY((!\ssl|trns|index\(17) & !\ssl|trns|index[16]~65\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \ssl|trns|index\(17),
	datad => VCC,
	cin => \ssl|trns|index[16]~65\,
	combout => \ssl|trns|index[17]~66_combout\,
	cout => \ssl|trns|index[17]~67\);

-- Location: FF_X13_Y20_N3
\ssl|trns|index[17]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_sck~input_o\,
	d => \ssl|trns|index[17]~66_combout\,
	asdata => \~GND~combout\,
	clrn => \ALT_INV_rst~input_o\,
	sload => \ssl|trns|LessThan0~1_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \ssl|trns|index\(17));

-- Location: LCCOMB_X13_Y20_N4
\ssl|trns|index[18]~68\ : cycloneive_lcell_comb
-- Equation(s):
-- \ssl|trns|index[18]~68_combout\ = (\ssl|trns|index\(18) & ((GND) # (!\ssl|trns|index[17]~67\))) # (!\ssl|trns|index\(18) & (\ssl|trns|index[17]~67\ $ (GND)))
-- \ssl|trns|index[18]~69\ = CARRY((\ssl|trns|index\(18)) # (!\ssl|trns|index[17]~67\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \ssl|trns|index\(18),
	datad => VCC,
	cin => \ssl|trns|index[17]~67\,
	combout => \ssl|trns|index[18]~68_combout\,
	cout => \ssl|trns|index[18]~69\);

-- Location: FF_X13_Y20_N5
\ssl|trns|index[18]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_sck~input_o\,
	d => \ssl|trns|index[18]~68_combout\,
	asdata => \~GND~combout\,
	clrn => \ALT_INV_rst~input_o\,
	sload => \ssl|trns|LessThan0~1_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \ssl|trns|index\(18));

-- Location: LCCOMB_X13_Y20_N6
\ssl|trns|index[19]~70\ : cycloneive_lcell_comb
-- Equation(s):
-- \ssl|trns|index[19]~70_combout\ = (\ssl|trns|index\(19) & (\ssl|trns|index[18]~69\ & VCC)) # (!\ssl|trns|index\(19) & (!\ssl|trns|index[18]~69\))
-- \ssl|trns|index[19]~71\ = CARRY((!\ssl|trns|index\(19) & !\ssl|trns|index[18]~69\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100000101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \ssl|trns|index\(19),
	datad => VCC,
	cin => \ssl|trns|index[18]~69\,
	combout => \ssl|trns|index[19]~70_combout\,
	cout => \ssl|trns|index[19]~71\);

-- Location: FF_X13_Y20_N7
\ssl|trns|index[19]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_sck~input_o\,
	d => \ssl|trns|index[19]~70_combout\,
	asdata => \~GND~combout\,
	clrn => \ALT_INV_rst~input_o\,
	sload => \ssl|trns|LessThan0~1_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \ssl|trns|index\(19));

-- Location: LCCOMB_X13_Y20_N8
\ssl|trns|index[20]~72\ : cycloneive_lcell_comb
-- Equation(s):
-- \ssl|trns|index[20]~72_combout\ = (\ssl|trns|index\(20) & ((GND) # (!\ssl|trns|index[19]~71\))) # (!\ssl|trns|index\(20) & (\ssl|trns|index[19]~71\ $ (GND)))
-- \ssl|trns|index[20]~73\ = CARRY((\ssl|trns|index\(20)) # (!\ssl|trns|index[19]~71\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \ssl|trns|index\(20),
	datad => VCC,
	cin => \ssl|trns|index[19]~71\,
	combout => \ssl|trns|index[20]~72_combout\,
	cout => \ssl|trns|index[20]~73\);

-- Location: FF_X13_Y20_N9
\ssl|trns|index[20]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_sck~input_o\,
	d => \ssl|trns|index[20]~72_combout\,
	asdata => \~GND~combout\,
	clrn => \ALT_INV_rst~input_o\,
	sload => \ssl|trns|LessThan0~1_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \ssl|trns|index\(20));

-- Location: LCCOMB_X13_Y20_N10
\ssl|trns|index[21]~74\ : cycloneive_lcell_comb
-- Equation(s):
-- \ssl|trns|index[21]~74_combout\ = (\ssl|trns|index\(21) & (\ssl|trns|index[20]~73\ & VCC)) # (!\ssl|trns|index\(21) & (!\ssl|trns|index[20]~73\))
-- \ssl|trns|index[21]~75\ = CARRY((!\ssl|trns|index\(21) & !\ssl|trns|index[20]~73\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100000101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \ssl|trns|index\(21),
	datad => VCC,
	cin => \ssl|trns|index[20]~73\,
	combout => \ssl|trns|index[21]~74_combout\,
	cout => \ssl|trns|index[21]~75\);

-- Location: FF_X13_Y20_N11
\ssl|trns|index[21]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_sck~input_o\,
	d => \ssl|trns|index[21]~74_combout\,
	asdata => \~GND~combout\,
	clrn => \ALT_INV_rst~input_o\,
	sload => \ssl|trns|LessThan0~1_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \ssl|trns|index\(21));

-- Location: LCCOMB_X13_Y20_N12
\ssl|trns|index[22]~76\ : cycloneive_lcell_comb
-- Equation(s):
-- \ssl|trns|index[22]~76_combout\ = (\ssl|trns|index\(22) & ((GND) # (!\ssl|trns|index[21]~75\))) # (!\ssl|trns|index\(22) & (\ssl|trns|index[21]~75\ $ (GND)))
-- \ssl|trns|index[22]~77\ = CARRY((\ssl|trns|index\(22)) # (!\ssl|trns|index[21]~75\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101010101111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \ssl|trns|index\(22),
	datad => VCC,
	cin => \ssl|trns|index[21]~75\,
	combout => \ssl|trns|index[22]~76_combout\,
	cout => \ssl|trns|index[22]~77\);

-- Location: FF_X13_Y20_N13
\ssl|trns|index[22]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_sck~input_o\,
	d => \ssl|trns|index[22]~76_combout\,
	asdata => \~GND~combout\,
	clrn => \ALT_INV_rst~input_o\,
	sload => \ssl|trns|LessThan0~1_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \ssl|trns|index\(22));

-- Location: LCCOMB_X13_Y20_N14
\ssl|trns|index[23]~78\ : cycloneive_lcell_comb
-- Equation(s):
-- \ssl|trns|index[23]~78_combout\ = (\ssl|trns|index\(23) & (\ssl|trns|index[22]~77\ & VCC)) # (!\ssl|trns|index\(23) & (!\ssl|trns|index[22]~77\))
-- \ssl|trns|index[23]~79\ = CARRY((!\ssl|trns|index\(23) & !\ssl|trns|index[22]~77\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \ssl|trns|index\(23),
	datad => VCC,
	cin => \ssl|trns|index[22]~77\,
	combout => \ssl|trns|index[23]~78_combout\,
	cout => \ssl|trns|index[23]~79\);

-- Location: FF_X13_Y20_N15
\ssl|trns|index[23]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_sck~input_o\,
	d => \ssl|trns|index[23]~78_combout\,
	asdata => \~GND~combout\,
	clrn => \ALT_INV_rst~input_o\,
	sload => \ssl|trns|LessThan0~1_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \ssl|trns|index\(23));

-- Location: LCCOMB_X13_Y20_N16
\ssl|trns|index[24]~80\ : cycloneive_lcell_comb
-- Equation(s):
-- \ssl|trns|index[24]~80_combout\ = (\ssl|trns|index\(24) & ((GND) # (!\ssl|trns|index[23]~79\))) # (!\ssl|trns|index\(24) & (\ssl|trns|index[23]~79\ $ (GND)))
-- \ssl|trns|index[24]~81\ = CARRY((\ssl|trns|index\(24)) # (!\ssl|trns|index[23]~79\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \ssl|trns|index\(24),
	datad => VCC,
	cin => \ssl|trns|index[23]~79\,
	combout => \ssl|trns|index[24]~80_combout\,
	cout => \ssl|trns|index[24]~81\);

-- Location: FF_X13_Y20_N17
\ssl|trns|index[24]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_sck~input_o\,
	d => \ssl|trns|index[24]~80_combout\,
	asdata => \~GND~combout\,
	clrn => \ALT_INV_rst~input_o\,
	sload => \ssl|trns|LessThan0~1_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \ssl|trns|index\(24));

-- Location: LCCOMB_X13_Y20_N18
\ssl|trns|index[25]~82\ : cycloneive_lcell_comb
-- Equation(s):
-- \ssl|trns|index[25]~82_combout\ = (\ssl|trns|index\(25) & (\ssl|trns|index[24]~81\ & VCC)) # (!\ssl|trns|index\(25) & (!\ssl|trns|index[24]~81\))
-- \ssl|trns|index[25]~83\ = CARRY((!\ssl|trns|index\(25) & !\ssl|trns|index[24]~81\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \ssl|trns|index\(25),
	datad => VCC,
	cin => \ssl|trns|index[24]~81\,
	combout => \ssl|trns|index[25]~82_combout\,
	cout => \ssl|trns|index[25]~83\);

-- Location: FF_X13_Y20_N19
\ssl|trns|index[25]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_sck~input_o\,
	d => \ssl|trns|index[25]~82_combout\,
	asdata => \~GND~combout\,
	clrn => \ALT_INV_rst~input_o\,
	sload => \ssl|trns|LessThan0~1_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \ssl|trns|index\(25));

-- Location: LCCOMB_X13_Y20_N20
\ssl|trns|index[26]~84\ : cycloneive_lcell_comb
-- Equation(s):
-- \ssl|trns|index[26]~84_combout\ = (\ssl|trns|index\(26) & ((GND) # (!\ssl|trns|index[25]~83\))) # (!\ssl|trns|index\(26) & (\ssl|trns|index[25]~83\ $ (GND)))
-- \ssl|trns|index[26]~85\ = CARRY((\ssl|trns|index\(26)) # (!\ssl|trns|index[25]~83\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \ssl|trns|index\(26),
	datad => VCC,
	cin => \ssl|trns|index[25]~83\,
	combout => \ssl|trns|index[26]~84_combout\,
	cout => \ssl|trns|index[26]~85\);

-- Location: FF_X13_Y20_N21
\ssl|trns|index[26]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_sck~input_o\,
	d => \ssl|trns|index[26]~84_combout\,
	asdata => \~GND~combout\,
	clrn => \ALT_INV_rst~input_o\,
	sload => \ssl|trns|LessThan0~1_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \ssl|trns|index\(26));

-- Location: LCCOMB_X13_Y20_N22
\ssl|trns|index[27]~86\ : cycloneive_lcell_comb
-- Equation(s):
-- \ssl|trns|index[27]~86_combout\ = (\ssl|trns|index\(27) & (\ssl|trns|index[26]~85\ & VCC)) # (!\ssl|trns|index\(27) & (!\ssl|trns|index[26]~85\))
-- \ssl|trns|index[27]~87\ = CARRY((!\ssl|trns|index\(27) & !\ssl|trns|index[26]~85\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100000101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \ssl|trns|index\(27),
	datad => VCC,
	cin => \ssl|trns|index[26]~85\,
	combout => \ssl|trns|index[27]~86_combout\,
	cout => \ssl|trns|index[27]~87\);

-- Location: FF_X13_Y20_N23
\ssl|trns|index[27]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_sck~input_o\,
	d => \ssl|trns|index[27]~86_combout\,
	asdata => \~GND~combout\,
	clrn => \ALT_INV_rst~input_o\,
	sload => \ssl|trns|LessThan0~1_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \ssl|trns|index\(27));

-- Location: LCCOMB_X13_Y20_N24
\ssl|trns|index[28]~88\ : cycloneive_lcell_comb
-- Equation(s):
-- \ssl|trns|index[28]~88_combout\ = (\ssl|trns|index\(28) & ((GND) # (!\ssl|trns|index[27]~87\))) # (!\ssl|trns|index\(28) & (\ssl|trns|index[27]~87\ $ (GND)))
-- \ssl|trns|index[28]~89\ = CARRY((\ssl|trns|index\(28)) # (!\ssl|trns|index[27]~87\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \ssl|trns|index\(28),
	datad => VCC,
	cin => \ssl|trns|index[27]~87\,
	combout => \ssl|trns|index[28]~88_combout\,
	cout => \ssl|trns|index[28]~89\);

-- Location: FF_X13_Y20_N25
\ssl|trns|index[28]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_sck~input_o\,
	d => \ssl|trns|index[28]~88_combout\,
	asdata => \~GND~combout\,
	clrn => \ALT_INV_rst~input_o\,
	sload => \ssl|trns|LessThan0~1_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \ssl|trns|index\(28));

-- Location: LCCOMB_X13_Y20_N26
\ssl|trns|index[29]~90\ : cycloneive_lcell_comb
-- Equation(s):
-- \ssl|trns|index[29]~90_combout\ = (\ssl|trns|index\(29) & (\ssl|trns|index[28]~89\ & VCC)) # (!\ssl|trns|index\(29) & (!\ssl|trns|index[28]~89\))
-- \ssl|trns|index[29]~91\ = CARRY((!\ssl|trns|index\(29) & !\ssl|trns|index[28]~89\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100000101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \ssl|trns|index\(29),
	datad => VCC,
	cin => \ssl|trns|index[28]~89\,
	combout => \ssl|trns|index[29]~90_combout\,
	cout => \ssl|trns|index[29]~91\);

-- Location: FF_X13_Y20_N27
\ssl|trns|index[29]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_sck~input_o\,
	d => \ssl|trns|index[29]~90_combout\,
	asdata => \~GND~combout\,
	clrn => \ALT_INV_rst~input_o\,
	sload => \ssl|trns|LessThan0~1_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \ssl|trns|index\(29));

-- Location: LCCOMB_X13_Y20_N28
\ssl|trns|index[30]~92\ : cycloneive_lcell_comb
-- Equation(s):
-- \ssl|trns|index[30]~92_combout\ = (\ssl|trns|index\(30) & ((GND) # (!\ssl|trns|index[29]~91\))) # (!\ssl|trns|index\(30) & (\ssl|trns|index[29]~91\ $ (GND)))
-- \ssl|trns|index[30]~93\ = CARRY((\ssl|trns|index\(30)) # (!\ssl|trns|index[29]~91\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \ssl|trns|index\(30),
	datad => VCC,
	cin => \ssl|trns|index[29]~91\,
	combout => \ssl|trns|index[30]~92_combout\,
	cout => \ssl|trns|index[30]~93\);

-- Location: FF_X13_Y20_N29
\ssl|trns|index[30]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_sck~input_o\,
	d => \ssl|trns|index[30]~92_combout\,
	asdata => \~GND~combout\,
	clrn => \ALT_INV_rst~input_o\,
	sload => \ssl|trns|LessThan0~1_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \ssl|trns|index\(30));

-- Location: LCCOMB_X13_Y20_N30
\ssl|trns|index[31]~94\ : cycloneive_lcell_comb
-- Equation(s):
-- \ssl|trns|index[31]~94_combout\ = \ssl|trns|index\(31) $ (!\ssl|trns|index[30]~93\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010110100101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \ssl|trns|index\(31),
	cin => \ssl|trns|index[30]~93\,
	combout => \ssl|trns|index[31]~94_combout\);

-- Location: FF_X13_Y20_N31
\ssl|trns|index[31]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_sck~input_o\,
	d => \ssl|trns|index[31]~94_combout\,
	asdata => \~GND~combout\,
	clrn => \ALT_INV_rst~input_o\,
	sload => \ssl|trns|LessThan0~1_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \ssl|trns|index\(31));

-- Location: LCCOMB_X12_Y21_N22
\ssl|trns|LessThan0~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \ssl|trns|LessThan0~0_combout\ = ((!\ssl|trns|index\(1)) # (!\ssl|trns|index\(0))) # (!\ssl|trns|index\(2))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101111111111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \ssl|trns|index\(2),
	datac => \ssl|trns|index\(0),
	datad => \ssl|trns|index\(1),
	combout => \ssl|trns|LessThan0~0_combout\);

-- Location: LCCOMB_X12_Y20_N22
\ssl|busy~7\ : cycloneive_lcell_comb
-- Equation(s):
-- \ssl|busy~7_combout\ = (!\ssl|trns|index\(27) & (!\ssl|trns|index\(28) & (!\ssl|trns|index\(29) & !\ssl|trns|index\(30))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \ssl|trns|index\(27),
	datab => \ssl|trns|index\(28),
	datac => \ssl|trns|index\(29),
	datad => \ssl|trns|index\(30),
	combout => \ssl|busy~7_combout\);

-- Location: LCCOMB_X12_Y20_N24
\ssl|busy~6\ : cycloneive_lcell_comb
-- Equation(s):
-- \ssl|busy~6_combout\ = (!\ssl|trns|index\(26) & (!\ssl|trns|index\(25) & (!\ssl|trns|index\(23) & !\ssl|trns|index\(24))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \ssl|trns|index\(26),
	datab => \ssl|trns|index\(25),
	datac => \ssl|trns|index\(23),
	datad => \ssl|trns|index\(24),
	combout => \ssl|busy~6_combout\);

-- Location: LCCOMB_X14_Y20_N12
\ssl|busy~5\ : cycloneive_lcell_comb
-- Equation(s):
-- \ssl|busy~5_combout\ = (!\ssl|trns|index\(22) & (!\ssl|trns|index\(19) & (!\ssl|trns|index\(21) & !\ssl|trns|index\(20))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \ssl|trns|index\(22),
	datab => \ssl|trns|index\(19),
	datac => \ssl|trns|index\(21),
	datad => \ssl|trns|index\(20),
	combout => \ssl|busy~5_combout\);

-- Location: LCCOMB_X12_Y20_N12
\ssl|busy~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \ssl|busy~3_combout\ = (!\ssl|trns|index\(18) & (!\ssl|trns|index\(16) & (!\ssl|trns|index\(17) & !\ssl|trns|index\(15))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \ssl|trns|index\(18),
	datab => \ssl|trns|index\(16),
	datac => \ssl|trns|index\(17),
	datad => \ssl|trns|index\(15),
	combout => \ssl|busy~3_combout\);

-- Location: LCCOMB_X12_Y21_N6
\ssl|busy~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \ssl|busy~1_combout\ = (!\ssl|trns|index\(10) & (!\ssl|trns|index\(9) & (!\ssl|trns|index\(7) & !\ssl|trns|index\(8))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \ssl|trns|index\(10),
	datab => \ssl|trns|index\(9),
	datac => \ssl|trns|index\(7),
	datad => \ssl|trns|index\(8),
	combout => \ssl|busy~1_combout\);

-- Location: LCCOMB_X12_Y21_N20
\ssl|busy~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \ssl|busy~2_combout\ = (!\ssl|trns|index\(11) & (!\ssl|trns|index\(14) & (!\ssl|trns|index\(12) & !\ssl|trns|index\(13))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \ssl|trns|index\(11),
	datab => \ssl|trns|index\(14),
	datac => \ssl|trns|index\(12),
	datad => \ssl|trns|index\(13),
	combout => \ssl|busy~2_combout\);

-- Location: LCCOMB_X12_Y21_N28
\ssl|busy~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \ssl|busy~0_combout\ = (!\ssl|trns|index\(3) & (!\ssl|trns|index\(4) & (!\ssl|trns|index\(6) & !\ssl|trns|index\(5))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \ssl|trns|index\(3),
	datab => \ssl|trns|index\(4),
	datac => \ssl|trns|index\(6),
	datad => \ssl|trns|index\(5),
	combout => \ssl|busy~0_combout\);

-- Location: LCCOMB_X12_Y20_N6
\ssl|busy~4\ : cycloneive_lcell_comb
-- Equation(s):
-- \ssl|busy~4_combout\ = (\ssl|busy~3_combout\ & (\ssl|busy~1_combout\ & (\ssl|busy~2_combout\ & \ssl|busy~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \ssl|busy~3_combout\,
	datab => \ssl|busy~1_combout\,
	datac => \ssl|busy~2_combout\,
	datad => \ssl|busy~0_combout\,
	combout => \ssl|busy~4_combout\);

-- Location: LCCOMB_X12_Y20_N20
\ssl|busy~8\ : cycloneive_lcell_comb
-- Equation(s):
-- \ssl|busy~8_combout\ = (\ssl|busy~7_combout\ & (\ssl|busy~6_combout\ & (\ssl|busy~5_combout\ & \ssl|busy~4_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \ssl|busy~7_combout\,
	datab => \ssl|busy~6_combout\,
	datac => \ssl|busy~5_combout\,
	datad => \ssl|busy~4_combout\,
	combout => \ssl|busy~8_combout\);

-- Location: LCCOMB_X12_Y20_N10
\ssl|trns|LessThan0~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \ssl|trns|LessThan0~1_combout\ = (\ssl|trns|index\(31)) # ((!\ssl|trns|LessThan0~0_combout\ & \ssl|busy~8_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010111110101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \ssl|trns|index\(31),
	datac => \ssl|trns|LessThan0~0_combout\,
	datad => \ssl|busy~8_combout\,
	combout => \ssl|trns|LessThan0~1_combout\);

-- Location: FF_X13_Y21_N1
\ssl|trns|index[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_sck~input_o\,
	d => \ssl|trns|index[0]~32_combout\,
	asdata => \~GND~combout\,
	clrn => \ALT_INV_rst~input_o\,
	sload => \ssl|trns|LessThan0~1_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \ssl|trns|index\(0));

-- Location: LCCOMB_X13_Y21_N2
\ssl|trns|index[1]~34\ : cycloneive_lcell_comb
-- Equation(s):
-- \ssl|trns|index[1]~34_combout\ = (\ssl|trns|index\(1) & (\ssl|trns|index[0]~33\ $ (GND))) # (!\ssl|trns|index\(1) & (!\ssl|trns|index[0]~33\ & VCC))
-- \ssl|trns|index[1]~35\ = CARRY((\ssl|trns|index\(1) & !\ssl|trns|index[0]~33\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \ssl|trns|index\(1),
	datad => VCC,
	cin => \ssl|trns|index[0]~33\,
	combout => \ssl|trns|index[1]~34_combout\,
	cout => \ssl|trns|index[1]~35\);

-- Location: FF_X13_Y21_N3
\ssl|trns|index[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_sck~input_o\,
	d => \ssl|trns|index[1]~34_combout\,
	asdata => \~GND~combout\,
	clrn => \ALT_INV_rst~input_o\,
	sload => \ssl|trns|LessThan0~1_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \ssl|trns|index\(1));

-- Location: FF_X13_Y21_N5
\ssl|trns|index[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_sck~input_o\,
	d => \ssl|trns|index[2]~36_combout\,
	asdata => \~GND~combout\,
	clrn => \ALT_INV_rst~input_o\,
	sload => \ssl|trns|LessThan0~1_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \ssl|trns|index\(2));

-- Location: IOIBUF_X0_Y11_N8
\clk~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_clk,
	o => \clk~input_o\);

-- Location: CLKCTRL_G2
\clk~inputclkctrl\ : cycloneive_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \clk~inputclkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \clk~inputclkctrl_outclk\);

-- Location: IOIBUF_X7_Y24_N1
\mosi~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_mosi,
	o => \mosi~input_o\);

-- Location: LCCOMB_X11_Y20_N0
\ssl|recv|index[0]~32\ : cycloneive_lcell_comb
-- Equation(s):
-- \ssl|recv|index[0]~32_combout\ = !\ssl|recv|index\(0)
-- \ssl|recv|index[0]~33\ = CARRY(!\ssl|recv|index\(0))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100110011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \ssl|recv|index\(0),
	combout => \ssl|recv|index[0]~32_combout\,
	cout => \ssl|recv|index[0]~33\);

-- Location: LCCOMB_X11_Y20_N2
\ssl|recv|index[1]~34\ : cycloneive_lcell_comb
-- Equation(s):
-- \ssl|recv|index[1]~34_combout\ = (\ssl|recv|index\(1) & (\ssl|recv|index[0]~33\ $ (GND))) # (!\ssl|recv|index\(1) & (!\ssl|recv|index[0]~33\ & VCC))
-- \ssl|recv|index[1]~35\ = CARRY((\ssl|recv|index\(1) & !\ssl|recv|index[0]~33\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \ssl|recv|index\(1),
	datad => VCC,
	cin => \ssl|recv|index[0]~33\,
	combout => \ssl|recv|index[1]~34_combout\,
	cout => \ssl|recv|index[1]~35\);

-- Location: LCCOMB_X11_Y20_N4
\ssl|recv|index[2]~36\ : cycloneive_lcell_comb
-- Equation(s):
-- \ssl|recv|index[2]~36_combout\ = (\ssl|recv|index\(2) & (!\ssl|recv|index[1]~35\)) # (!\ssl|recv|index\(2) & ((\ssl|recv|index[1]~35\) # (GND)))
-- \ssl|recv|index[2]~37\ = CARRY((!\ssl|recv|index[1]~35\) # (!\ssl|recv|index\(2)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \ssl|recv|index\(2),
	datad => VCC,
	cin => \ssl|recv|index[1]~35\,
	combout => \ssl|recv|index[2]~36_combout\,
	cout => \ssl|recv|index[2]~37\);

-- Location: FF_X11_Y20_N5
\ssl|recv|index[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \sck~input_o\,
	d => \ssl|recv|index[2]~36_combout\,
	asdata => \~GND~combout\,
	clrn => \ALT_INV_rst~input_o\,
	sload => \ssl|recv|LessThan0~1_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \ssl|recv|index\(2));

-- Location: LCCOMB_X11_Y20_N6
\ssl|recv|index[3]~38\ : cycloneive_lcell_comb
-- Equation(s):
-- \ssl|recv|index[3]~38_combout\ = (\ssl|recv|index\(3) & (\ssl|recv|index[2]~37\ & VCC)) # (!\ssl|recv|index\(3) & (!\ssl|recv|index[2]~37\))
-- \ssl|recv|index[3]~39\ = CARRY((!\ssl|recv|index\(3) & !\ssl|recv|index[2]~37\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100000101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \ssl|recv|index\(3),
	datad => VCC,
	cin => \ssl|recv|index[2]~37\,
	combout => \ssl|recv|index[3]~38_combout\,
	cout => \ssl|recv|index[3]~39\);

-- Location: FF_X11_Y20_N7
\ssl|recv|index[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \sck~input_o\,
	d => \ssl|recv|index[3]~38_combout\,
	asdata => \~GND~combout\,
	clrn => \ALT_INV_rst~input_o\,
	sload => \ssl|recv|LessThan0~1_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \ssl|recv|index\(3));

-- Location: LCCOMB_X11_Y20_N8
\ssl|recv|index[4]~40\ : cycloneive_lcell_comb
-- Equation(s):
-- \ssl|recv|index[4]~40_combout\ = (\ssl|recv|index\(4) & ((GND) # (!\ssl|recv|index[3]~39\))) # (!\ssl|recv|index\(4) & (\ssl|recv|index[3]~39\ $ (GND)))
-- \ssl|recv|index[4]~41\ = CARRY((\ssl|recv|index\(4)) # (!\ssl|recv|index[3]~39\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \ssl|recv|index\(4),
	datad => VCC,
	cin => \ssl|recv|index[3]~39\,
	combout => \ssl|recv|index[4]~40_combout\,
	cout => \ssl|recv|index[4]~41\);

-- Location: FF_X11_Y20_N9
\ssl|recv|index[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \sck~input_o\,
	d => \ssl|recv|index[4]~40_combout\,
	asdata => \~GND~combout\,
	clrn => \ALT_INV_rst~input_o\,
	sload => \ssl|recv|LessThan0~1_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \ssl|recv|index\(4));

-- Location: LCCOMB_X11_Y20_N10
\ssl|recv|index[5]~42\ : cycloneive_lcell_comb
-- Equation(s):
-- \ssl|recv|index[5]~42_combout\ = (\ssl|recv|index\(5) & (\ssl|recv|index[4]~41\ & VCC)) # (!\ssl|recv|index\(5) & (!\ssl|recv|index[4]~41\))
-- \ssl|recv|index[5]~43\ = CARRY((!\ssl|recv|index\(5) & !\ssl|recv|index[4]~41\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100000101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \ssl|recv|index\(5),
	datad => VCC,
	cin => \ssl|recv|index[4]~41\,
	combout => \ssl|recv|index[5]~42_combout\,
	cout => \ssl|recv|index[5]~43\);

-- Location: FF_X11_Y20_N11
\ssl|recv|index[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \sck~input_o\,
	d => \ssl|recv|index[5]~42_combout\,
	asdata => \~GND~combout\,
	clrn => \ALT_INV_rst~input_o\,
	sload => \ssl|recv|LessThan0~1_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \ssl|recv|index\(5));

-- Location: LCCOMB_X11_Y20_N12
\ssl|recv|index[6]~44\ : cycloneive_lcell_comb
-- Equation(s):
-- \ssl|recv|index[6]~44_combout\ = (\ssl|recv|index\(6) & ((GND) # (!\ssl|recv|index[5]~43\))) # (!\ssl|recv|index\(6) & (\ssl|recv|index[5]~43\ $ (GND)))
-- \ssl|recv|index[6]~45\ = CARRY((\ssl|recv|index\(6)) # (!\ssl|recv|index[5]~43\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101010101111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \ssl|recv|index\(6),
	datad => VCC,
	cin => \ssl|recv|index[5]~43\,
	combout => \ssl|recv|index[6]~44_combout\,
	cout => \ssl|recv|index[6]~45\);

-- Location: FF_X11_Y20_N13
\ssl|recv|index[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \sck~input_o\,
	d => \ssl|recv|index[6]~44_combout\,
	asdata => \~GND~combout\,
	clrn => \ALT_INV_rst~input_o\,
	sload => \ssl|recv|LessThan0~1_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \ssl|recv|index\(6));

-- Location: LCCOMB_X11_Y20_N14
\ssl|recv|index[7]~46\ : cycloneive_lcell_comb
-- Equation(s):
-- \ssl|recv|index[7]~46_combout\ = (\ssl|recv|index\(7) & (\ssl|recv|index[6]~45\ & VCC)) # (!\ssl|recv|index\(7) & (!\ssl|recv|index[6]~45\))
-- \ssl|recv|index[7]~47\ = CARRY((!\ssl|recv|index\(7) & !\ssl|recv|index[6]~45\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \ssl|recv|index\(7),
	datad => VCC,
	cin => \ssl|recv|index[6]~45\,
	combout => \ssl|recv|index[7]~46_combout\,
	cout => \ssl|recv|index[7]~47\);

-- Location: FF_X11_Y20_N15
\ssl|recv|index[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \sck~input_o\,
	d => \ssl|recv|index[7]~46_combout\,
	asdata => \~GND~combout\,
	clrn => \ALT_INV_rst~input_o\,
	sload => \ssl|recv|LessThan0~1_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \ssl|recv|index\(7));

-- Location: LCCOMB_X11_Y20_N16
\ssl|recv|index[8]~48\ : cycloneive_lcell_comb
-- Equation(s):
-- \ssl|recv|index[8]~48_combout\ = (\ssl|recv|index\(8) & ((GND) # (!\ssl|recv|index[7]~47\))) # (!\ssl|recv|index\(8) & (\ssl|recv|index[7]~47\ $ (GND)))
-- \ssl|recv|index[8]~49\ = CARRY((\ssl|recv|index\(8)) # (!\ssl|recv|index[7]~47\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \ssl|recv|index\(8),
	datad => VCC,
	cin => \ssl|recv|index[7]~47\,
	combout => \ssl|recv|index[8]~48_combout\,
	cout => \ssl|recv|index[8]~49\);

-- Location: FF_X11_Y20_N17
\ssl|recv|index[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \sck~input_o\,
	d => \ssl|recv|index[8]~48_combout\,
	asdata => \~GND~combout\,
	clrn => \ALT_INV_rst~input_o\,
	sload => \ssl|recv|LessThan0~1_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \ssl|recv|index\(8));

-- Location: LCCOMB_X11_Y20_N18
\ssl|recv|index[9]~50\ : cycloneive_lcell_comb
-- Equation(s):
-- \ssl|recv|index[9]~50_combout\ = (\ssl|recv|index\(9) & (\ssl|recv|index[8]~49\ & VCC)) # (!\ssl|recv|index\(9) & (!\ssl|recv|index[8]~49\))
-- \ssl|recv|index[9]~51\ = CARRY((!\ssl|recv|index\(9) & !\ssl|recv|index[8]~49\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \ssl|recv|index\(9),
	datad => VCC,
	cin => \ssl|recv|index[8]~49\,
	combout => \ssl|recv|index[9]~50_combout\,
	cout => \ssl|recv|index[9]~51\);

-- Location: FF_X11_Y20_N19
\ssl|recv|index[9]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \sck~input_o\,
	d => \ssl|recv|index[9]~50_combout\,
	asdata => \~GND~combout\,
	clrn => \ALT_INV_rst~input_o\,
	sload => \ssl|recv|LessThan0~1_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \ssl|recv|index\(9));

-- Location: LCCOMB_X11_Y20_N20
\ssl|recv|index[10]~52\ : cycloneive_lcell_comb
-- Equation(s):
-- \ssl|recv|index[10]~52_combout\ = (\ssl|recv|index\(10) & ((GND) # (!\ssl|recv|index[9]~51\))) # (!\ssl|recv|index\(10) & (\ssl|recv|index[9]~51\ $ (GND)))
-- \ssl|recv|index[10]~53\ = CARRY((\ssl|recv|index\(10)) # (!\ssl|recv|index[9]~51\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \ssl|recv|index\(10),
	datad => VCC,
	cin => \ssl|recv|index[9]~51\,
	combout => \ssl|recv|index[10]~52_combout\,
	cout => \ssl|recv|index[10]~53\);

-- Location: FF_X11_Y20_N21
\ssl|recv|index[10]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \sck~input_o\,
	d => \ssl|recv|index[10]~52_combout\,
	asdata => \~GND~combout\,
	clrn => \ALT_INV_rst~input_o\,
	sload => \ssl|recv|LessThan0~1_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \ssl|recv|index\(10));

-- Location: LCCOMB_X11_Y20_N22
\ssl|recv|index[11]~54\ : cycloneive_lcell_comb
-- Equation(s):
-- \ssl|recv|index[11]~54_combout\ = (\ssl|recv|index\(11) & (\ssl|recv|index[10]~53\ & VCC)) # (!\ssl|recv|index\(11) & (!\ssl|recv|index[10]~53\))
-- \ssl|recv|index[11]~55\ = CARRY((!\ssl|recv|index\(11) & !\ssl|recv|index[10]~53\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100000101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \ssl|recv|index\(11),
	datad => VCC,
	cin => \ssl|recv|index[10]~53\,
	combout => \ssl|recv|index[11]~54_combout\,
	cout => \ssl|recv|index[11]~55\);

-- Location: FF_X11_Y20_N23
\ssl|recv|index[11]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \sck~input_o\,
	d => \ssl|recv|index[11]~54_combout\,
	asdata => \~GND~combout\,
	clrn => \ALT_INV_rst~input_o\,
	sload => \ssl|recv|LessThan0~1_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \ssl|recv|index\(11));

-- Location: LCCOMB_X11_Y20_N24
\ssl|recv|index[12]~56\ : cycloneive_lcell_comb
-- Equation(s):
-- \ssl|recv|index[12]~56_combout\ = (\ssl|recv|index\(12) & ((GND) # (!\ssl|recv|index[11]~55\))) # (!\ssl|recv|index\(12) & (\ssl|recv|index[11]~55\ $ (GND)))
-- \ssl|recv|index[12]~57\ = CARRY((\ssl|recv|index\(12)) # (!\ssl|recv|index[11]~55\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \ssl|recv|index\(12),
	datad => VCC,
	cin => \ssl|recv|index[11]~55\,
	combout => \ssl|recv|index[12]~56_combout\,
	cout => \ssl|recv|index[12]~57\);

-- Location: FF_X11_Y20_N25
\ssl|recv|index[12]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \sck~input_o\,
	d => \ssl|recv|index[12]~56_combout\,
	asdata => \~GND~combout\,
	clrn => \ALT_INV_rst~input_o\,
	sload => \ssl|recv|LessThan0~1_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \ssl|recv|index\(12));

-- Location: LCCOMB_X11_Y20_N26
\ssl|recv|index[13]~58\ : cycloneive_lcell_comb
-- Equation(s):
-- \ssl|recv|index[13]~58_combout\ = (\ssl|recv|index\(13) & (\ssl|recv|index[12]~57\ & VCC)) # (!\ssl|recv|index\(13) & (!\ssl|recv|index[12]~57\))
-- \ssl|recv|index[13]~59\ = CARRY((!\ssl|recv|index\(13) & !\ssl|recv|index[12]~57\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100000101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \ssl|recv|index\(13),
	datad => VCC,
	cin => \ssl|recv|index[12]~57\,
	combout => \ssl|recv|index[13]~58_combout\,
	cout => \ssl|recv|index[13]~59\);

-- Location: FF_X11_Y20_N27
\ssl|recv|index[13]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \sck~input_o\,
	d => \ssl|recv|index[13]~58_combout\,
	asdata => \~GND~combout\,
	clrn => \ALT_INV_rst~input_o\,
	sload => \ssl|recv|LessThan0~1_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \ssl|recv|index\(13));

-- Location: LCCOMB_X11_Y20_N28
\ssl|recv|index[14]~60\ : cycloneive_lcell_comb
-- Equation(s):
-- \ssl|recv|index[14]~60_combout\ = (\ssl|recv|index\(14) & ((GND) # (!\ssl|recv|index[13]~59\))) # (!\ssl|recv|index\(14) & (\ssl|recv|index[13]~59\ $ (GND)))
-- \ssl|recv|index[14]~61\ = CARRY((\ssl|recv|index\(14)) # (!\ssl|recv|index[13]~59\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \ssl|recv|index\(14),
	datad => VCC,
	cin => \ssl|recv|index[13]~59\,
	combout => \ssl|recv|index[14]~60_combout\,
	cout => \ssl|recv|index[14]~61\);

-- Location: FF_X11_Y20_N29
\ssl|recv|index[14]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \sck~input_o\,
	d => \ssl|recv|index[14]~60_combout\,
	asdata => \~GND~combout\,
	clrn => \ALT_INV_rst~input_o\,
	sload => \ssl|recv|LessThan0~1_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \ssl|recv|index\(14));

-- Location: LCCOMB_X11_Y20_N30
\ssl|recv|index[15]~62\ : cycloneive_lcell_comb
-- Equation(s):
-- \ssl|recv|index[15]~62_combout\ = (\ssl|recv|index\(15) & (\ssl|recv|index[14]~61\ & VCC)) # (!\ssl|recv|index\(15) & (!\ssl|recv|index[14]~61\))
-- \ssl|recv|index[15]~63\ = CARRY((!\ssl|recv|index\(15) & !\ssl|recv|index[14]~61\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100000101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \ssl|recv|index\(15),
	datad => VCC,
	cin => \ssl|recv|index[14]~61\,
	combout => \ssl|recv|index[15]~62_combout\,
	cout => \ssl|recv|index[15]~63\);

-- Location: FF_X11_Y20_N31
\ssl|recv|index[15]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \sck~input_o\,
	d => \ssl|recv|index[15]~62_combout\,
	asdata => \~GND~combout\,
	clrn => \ALT_INV_rst~input_o\,
	sload => \ssl|recv|LessThan0~1_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \ssl|recv|index\(15));

-- Location: LCCOMB_X11_Y19_N0
\ssl|recv|index[16]~64\ : cycloneive_lcell_comb
-- Equation(s):
-- \ssl|recv|index[16]~64_combout\ = (\ssl|recv|index\(16) & ((GND) # (!\ssl|recv|index[15]~63\))) # (!\ssl|recv|index\(16) & (\ssl|recv|index[15]~63\ $ (GND)))
-- \ssl|recv|index[16]~65\ = CARRY((\ssl|recv|index\(16)) # (!\ssl|recv|index[15]~63\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \ssl|recv|index\(16),
	datad => VCC,
	cin => \ssl|recv|index[15]~63\,
	combout => \ssl|recv|index[16]~64_combout\,
	cout => \ssl|recv|index[16]~65\);

-- Location: FF_X11_Y19_N1
\ssl|recv|index[16]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \sck~input_o\,
	d => \ssl|recv|index[16]~64_combout\,
	asdata => \~GND~combout\,
	clrn => \ALT_INV_rst~input_o\,
	sload => \ssl|recv|LessThan0~1_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \ssl|recv|index\(16));

-- Location: LCCOMB_X11_Y19_N2
\ssl|recv|index[17]~66\ : cycloneive_lcell_comb
-- Equation(s):
-- \ssl|recv|index[17]~66_combout\ = (\ssl|recv|index\(17) & (\ssl|recv|index[16]~65\ & VCC)) # (!\ssl|recv|index\(17) & (!\ssl|recv|index[16]~65\))
-- \ssl|recv|index[17]~67\ = CARRY((!\ssl|recv|index\(17) & !\ssl|recv|index[16]~65\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \ssl|recv|index\(17),
	datad => VCC,
	cin => \ssl|recv|index[16]~65\,
	combout => \ssl|recv|index[17]~66_combout\,
	cout => \ssl|recv|index[17]~67\);

-- Location: FF_X11_Y19_N3
\ssl|recv|index[17]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \sck~input_o\,
	d => \ssl|recv|index[17]~66_combout\,
	asdata => \~GND~combout\,
	clrn => \ALT_INV_rst~input_o\,
	sload => \ssl|recv|LessThan0~1_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \ssl|recv|index\(17));

-- Location: LCCOMB_X11_Y19_N4
\ssl|recv|index[18]~68\ : cycloneive_lcell_comb
-- Equation(s):
-- \ssl|recv|index[18]~68_combout\ = (\ssl|recv|index\(18) & ((GND) # (!\ssl|recv|index[17]~67\))) # (!\ssl|recv|index\(18) & (\ssl|recv|index[17]~67\ $ (GND)))
-- \ssl|recv|index[18]~69\ = CARRY((\ssl|recv|index\(18)) # (!\ssl|recv|index[17]~67\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \ssl|recv|index\(18),
	datad => VCC,
	cin => \ssl|recv|index[17]~67\,
	combout => \ssl|recv|index[18]~68_combout\,
	cout => \ssl|recv|index[18]~69\);

-- Location: FF_X11_Y19_N5
\ssl|recv|index[18]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \sck~input_o\,
	d => \ssl|recv|index[18]~68_combout\,
	asdata => \~GND~combout\,
	clrn => \ALT_INV_rst~input_o\,
	sload => \ssl|recv|LessThan0~1_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \ssl|recv|index\(18));

-- Location: LCCOMB_X11_Y19_N6
\ssl|recv|index[19]~70\ : cycloneive_lcell_comb
-- Equation(s):
-- \ssl|recv|index[19]~70_combout\ = (\ssl|recv|index\(19) & (\ssl|recv|index[18]~69\ & VCC)) # (!\ssl|recv|index\(19) & (!\ssl|recv|index[18]~69\))
-- \ssl|recv|index[19]~71\ = CARRY((!\ssl|recv|index\(19) & !\ssl|recv|index[18]~69\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \ssl|recv|index\(19),
	datad => VCC,
	cin => \ssl|recv|index[18]~69\,
	combout => \ssl|recv|index[19]~70_combout\,
	cout => \ssl|recv|index[19]~71\);

-- Location: LCCOMB_X12_Y19_N24
\ssl|recv|index[19]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \ssl|recv|index[19]~feeder_combout\ = \ssl|recv|index[19]~70_combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \ssl|recv|index[19]~70_combout\,
	combout => \ssl|recv|index[19]~feeder_combout\);

-- Location: FF_X12_Y19_N25
\ssl|recv|index[19]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \sck~input_o\,
	d => \ssl|recv|index[19]~feeder_combout\,
	asdata => \~GND~combout\,
	clrn => \ALT_INV_rst~input_o\,
	sload => \ssl|recv|LessThan0~1_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \ssl|recv|index\(19));

-- Location: LCCOMB_X11_Y19_N8
\ssl|recv|index[20]~72\ : cycloneive_lcell_comb
-- Equation(s):
-- \ssl|recv|index[20]~72_combout\ = (\ssl|recv|index\(20) & ((GND) # (!\ssl|recv|index[19]~71\))) # (!\ssl|recv|index\(20) & (\ssl|recv|index[19]~71\ $ (GND)))
-- \ssl|recv|index[20]~73\ = CARRY((\ssl|recv|index\(20)) # (!\ssl|recv|index[19]~71\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \ssl|recv|index\(20),
	datad => VCC,
	cin => \ssl|recv|index[19]~71\,
	combout => \ssl|recv|index[20]~72_combout\,
	cout => \ssl|recv|index[20]~73\);

-- Location: LCCOMB_X12_Y19_N14
\ssl|recv|index[20]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \ssl|recv|index[20]~feeder_combout\ = \ssl|recv|index[20]~72_combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \ssl|recv|index[20]~72_combout\,
	combout => \ssl|recv|index[20]~feeder_combout\);

-- Location: FF_X12_Y19_N15
\ssl|recv|index[20]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \sck~input_o\,
	d => \ssl|recv|index[20]~feeder_combout\,
	asdata => \~GND~combout\,
	clrn => \ALT_INV_rst~input_o\,
	sload => \ssl|recv|LessThan0~1_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \ssl|recv|index\(20));

-- Location: LCCOMB_X11_Y19_N10
\ssl|recv|index[21]~74\ : cycloneive_lcell_comb
-- Equation(s):
-- \ssl|recv|index[21]~74_combout\ = (\ssl|recv|index\(21) & (\ssl|recv|index[20]~73\ & VCC)) # (!\ssl|recv|index\(21) & (!\ssl|recv|index[20]~73\))
-- \ssl|recv|index[21]~75\ = CARRY((!\ssl|recv|index\(21) & !\ssl|recv|index[20]~73\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \ssl|recv|index\(21),
	datad => VCC,
	cin => \ssl|recv|index[20]~73\,
	combout => \ssl|recv|index[21]~74_combout\,
	cout => \ssl|recv|index[21]~75\);

-- Location: LCCOMB_X12_Y19_N12
\ssl|recv|index[21]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \ssl|recv|index[21]~feeder_combout\ = \ssl|recv|index[21]~74_combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \ssl|recv|index[21]~74_combout\,
	combout => \ssl|recv|index[21]~feeder_combout\);

-- Location: FF_X12_Y19_N13
\ssl|recv|index[21]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \sck~input_o\,
	d => \ssl|recv|index[21]~feeder_combout\,
	asdata => \~GND~combout\,
	clrn => \ALT_INV_rst~input_o\,
	sload => \ssl|recv|LessThan0~1_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \ssl|recv|index\(21));

-- Location: LCCOMB_X11_Y19_N12
\ssl|recv|index[22]~76\ : cycloneive_lcell_comb
-- Equation(s):
-- \ssl|recv|index[22]~76_combout\ = (\ssl|recv|index\(22) & ((GND) # (!\ssl|recv|index[21]~75\))) # (!\ssl|recv|index\(22) & (\ssl|recv|index[21]~75\ $ (GND)))
-- \ssl|recv|index[22]~77\ = CARRY((\ssl|recv|index\(22)) # (!\ssl|recv|index[21]~75\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101010101111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \ssl|recv|index\(22),
	datad => VCC,
	cin => \ssl|recv|index[21]~75\,
	combout => \ssl|recv|index[22]~76_combout\,
	cout => \ssl|recv|index[22]~77\);

-- Location: LCCOMB_X12_Y19_N6
\ssl|recv|index[22]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \ssl|recv|index[22]~feeder_combout\ = \ssl|recv|index[22]~76_combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \ssl|recv|index[22]~76_combout\,
	combout => \ssl|recv|index[22]~feeder_combout\);

-- Location: FF_X12_Y19_N7
\ssl|recv|index[22]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \sck~input_o\,
	d => \ssl|recv|index[22]~feeder_combout\,
	asdata => \~GND~combout\,
	clrn => \ALT_INV_rst~input_o\,
	sload => \ssl|recv|LessThan0~1_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \ssl|recv|index\(22));

-- Location: LCCOMB_X11_Y19_N14
\ssl|recv|index[23]~78\ : cycloneive_lcell_comb
-- Equation(s):
-- \ssl|recv|index[23]~78_combout\ = (\ssl|recv|index\(23) & (\ssl|recv|index[22]~77\ & VCC)) # (!\ssl|recv|index\(23) & (!\ssl|recv|index[22]~77\))
-- \ssl|recv|index[23]~79\ = CARRY((!\ssl|recv|index\(23) & !\ssl|recv|index[22]~77\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \ssl|recv|index\(23),
	datad => VCC,
	cin => \ssl|recv|index[22]~77\,
	combout => \ssl|recv|index[23]~78_combout\,
	cout => \ssl|recv|index[23]~79\);

-- Location: LCCOMB_X12_Y23_N16
\ssl|recv|index[23]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \ssl|recv|index[23]~feeder_combout\ = \ssl|recv|index[23]~78_combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \ssl|recv|index[23]~78_combout\,
	combout => \ssl|recv|index[23]~feeder_combout\);

-- Location: FF_X12_Y23_N17
\ssl|recv|index[23]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \sck~input_o\,
	d => \ssl|recv|index[23]~feeder_combout\,
	asdata => \~GND~combout\,
	clrn => \ALT_INV_rst~input_o\,
	sload => \ssl|recv|LessThan0~1_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \ssl|recv|index\(23));

-- Location: LCCOMB_X11_Y19_N16
\ssl|recv|index[24]~80\ : cycloneive_lcell_comb
-- Equation(s):
-- \ssl|recv|index[24]~80_combout\ = (\ssl|recv|index\(24) & ((GND) # (!\ssl|recv|index[23]~79\))) # (!\ssl|recv|index\(24) & (\ssl|recv|index[23]~79\ $ (GND)))
-- \ssl|recv|index[24]~81\ = CARRY((\ssl|recv|index\(24)) # (!\ssl|recv|index[23]~79\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101010101111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \ssl|recv|index\(24),
	datad => VCC,
	cin => \ssl|recv|index[23]~79\,
	combout => \ssl|recv|index[24]~80_combout\,
	cout => \ssl|recv|index[24]~81\);

-- Location: LCCOMB_X12_Y23_N18
\ssl|recv|index[24]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \ssl|recv|index[24]~feeder_combout\ = \ssl|recv|index[24]~80_combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \ssl|recv|index[24]~80_combout\,
	combout => \ssl|recv|index[24]~feeder_combout\);

-- Location: FF_X12_Y23_N19
\ssl|recv|index[24]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \sck~input_o\,
	d => \ssl|recv|index[24]~feeder_combout\,
	asdata => \~GND~combout\,
	clrn => \ALT_INV_rst~input_o\,
	sload => \ssl|recv|LessThan0~1_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \ssl|recv|index\(24));

-- Location: LCCOMB_X11_Y19_N18
\ssl|recv|index[25]~82\ : cycloneive_lcell_comb
-- Equation(s):
-- \ssl|recv|index[25]~82_combout\ = (\ssl|recv|index\(25) & (\ssl|recv|index[24]~81\ & VCC)) # (!\ssl|recv|index\(25) & (!\ssl|recv|index[24]~81\))
-- \ssl|recv|index[25]~83\ = CARRY((!\ssl|recv|index\(25) & !\ssl|recv|index[24]~81\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \ssl|recv|index\(25),
	datad => VCC,
	cin => \ssl|recv|index[24]~81\,
	combout => \ssl|recv|index[25]~82_combout\,
	cout => \ssl|recv|index[25]~83\);

-- Location: LCCOMB_X12_Y23_N8
\ssl|recv|index[25]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \ssl|recv|index[25]~feeder_combout\ = \ssl|recv|index[25]~82_combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \ssl|recv|index[25]~82_combout\,
	combout => \ssl|recv|index[25]~feeder_combout\);

-- Location: FF_X12_Y23_N9
\ssl|recv|index[25]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \sck~input_o\,
	d => \ssl|recv|index[25]~feeder_combout\,
	asdata => \~GND~combout\,
	clrn => \ALT_INV_rst~input_o\,
	sload => \ssl|recv|LessThan0~1_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \ssl|recv|index\(25));

-- Location: LCCOMB_X11_Y19_N20
\ssl|recv|index[26]~84\ : cycloneive_lcell_comb
-- Equation(s):
-- \ssl|recv|index[26]~84_combout\ = (\ssl|recv|index\(26) & ((GND) # (!\ssl|recv|index[25]~83\))) # (!\ssl|recv|index\(26) & (\ssl|recv|index[25]~83\ $ (GND)))
-- \ssl|recv|index[26]~85\ = CARRY((\ssl|recv|index\(26)) # (!\ssl|recv|index[25]~83\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \ssl|recv|index\(26),
	datad => VCC,
	cin => \ssl|recv|index[25]~83\,
	combout => \ssl|recv|index[26]~84_combout\,
	cout => \ssl|recv|index[26]~85\);

-- Location: LCCOMB_X12_Y23_N22
\ssl|recv|index[26]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \ssl|recv|index[26]~feeder_combout\ = \ssl|recv|index[26]~84_combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \ssl|recv|index[26]~84_combout\,
	combout => \ssl|recv|index[26]~feeder_combout\);

-- Location: FF_X12_Y23_N23
\ssl|recv|index[26]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \sck~input_o\,
	d => \ssl|recv|index[26]~feeder_combout\,
	asdata => \~GND~combout\,
	clrn => \ALT_INV_rst~input_o\,
	sload => \ssl|recv|LessThan0~1_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \ssl|recv|index\(26));

-- Location: LCCOMB_X11_Y19_N22
\ssl|recv|index[27]~86\ : cycloneive_lcell_comb
-- Equation(s):
-- \ssl|recv|index[27]~86_combout\ = (\ssl|recv|index\(27) & (\ssl|recv|index[26]~85\ & VCC)) # (!\ssl|recv|index\(27) & (!\ssl|recv|index[26]~85\))
-- \ssl|recv|index[27]~87\ = CARRY((!\ssl|recv|index\(27) & !\ssl|recv|index[26]~85\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100000101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \ssl|recv|index\(27),
	datad => VCC,
	cin => \ssl|recv|index[26]~85\,
	combout => \ssl|recv|index[27]~86_combout\,
	cout => \ssl|recv|index[27]~87\);

-- Location: FF_X11_Y19_N23
\ssl|recv|index[27]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \sck~input_o\,
	d => \ssl|recv|index[27]~86_combout\,
	asdata => \~GND~combout\,
	clrn => \ALT_INV_rst~input_o\,
	sload => \ssl|recv|LessThan0~1_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \ssl|recv|index\(27));

-- Location: LCCOMB_X11_Y19_N24
\ssl|recv|index[28]~88\ : cycloneive_lcell_comb
-- Equation(s):
-- \ssl|recv|index[28]~88_combout\ = (\ssl|recv|index\(28) & ((GND) # (!\ssl|recv|index[27]~87\))) # (!\ssl|recv|index\(28) & (\ssl|recv|index[27]~87\ $ (GND)))
-- \ssl|recv|index[28]~89\ = CARRY((\ssl|recv|index\(28)) # (!\ssl|recv|index[27]~87\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \ssl|recv|index\(28),
	datad => VCC,
	cin => \ssl|recv|index[27]~87\,
	combout => \ssl|recv|index[28]~88_combout\,
	cout => \ssl|recv|index[28]~89\);

-- Location: LCCOMB_X12_Y19_N8
\ssl|recv|index[28]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \ssl|recv|index[28]~feeder_combout\ = \ssl|recv|index[28]~88_combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \ssl|recv|index[28]~88_combout\,
	combout => \ssl|recv|index[28]~feeder_combout\);

-- Location: FF_X12_Y19_N9
\ssl|recv|index[28]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \sck~input_o\,
	d => \ssl|recv|index[28]~feeder_combout\,
	asdata => \~GND~combout\,
	clrn => \ALT_INV_rst~input_o\,
	sload => \ssl|recv|LessThan0~1_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \ssl|recv|index\(28));

-- Location: LCCOMB_X11_Y19_N26
\ssl|recv|index[29]~90\ : cycloneive_lcell_comb
-- Equation(s):
-- \ssl|recv|index[29]~90_combout\ = (\ssl|recv|index\(29) & (\ssl|recv|index[28]~89\ & VCC)) # (!\ssl|recv|index\(29) & (!\ssl|recv|index[28]~89\))
-- \ssl|recv|index[29]~91\ = CARRY((!\ssl|recv|index\(29) & !\ssl|recv|index[28]~89\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100000101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \ssl|recv|index\(29),
	datad => VCC,
	cin => \ssl|recv|index[28]~89\,
	combout => \ssl|recv|index[29]~90_combout\,
	cout => \ssl|recv|index[29]~91\);

-- Location: FF_X11_Y19_N27
\ssl|recv|index[29]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \sck~input_o\,
	d => \ssl|recv|index[29]~90_combout\,
	asdata => \~GND~combout\,
	clrn => \ALT_INV_rst~input_o\,
	sload => \ssl|recv|LessThan0~1_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \ssl|recv|index\(29));

-- Location: LCCOMB_X11_Y19_N28
\ssl|recv|index[30]~92\ : cycloneive_lcell_comb
-- Equation(s):
-- \ssl|recv|index[30]~92_combout\ = (\ssl|recv|index\(30) & ((GND) # (!\ssl|recv|index[29]~91\))) # (!\ssl|recv|index\(30) & (\ssl|recv|index[29]~91\ $ (GND)))
-- \ssl|recv|index[30]~93\ = CARRY((\ssl|recv|index\(30)) # (!\ssl|recv|index[29]~91\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \ssl|recv|index\(30),
	datad => VCC,
	cin => \ssl|recv|index[29]~91\,
	combout => \ssl|recv|index[30]~92_combout\,
	cout => \ssl|recv|index[30]~93\);

-- Location: FF_X11_Y19_N29
\ssl|recv|index[30]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \sck~input_o\,
	d => \ssl|recv|index[30]~92_combout\,
	asdata => \~GND~combout\,
	clrn => \ALT_INV_rst~input_o\,
	sload => \ssl|recv|LessThan0~1_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \ssl|recv|index\(30));

-- Location: LCCOMB_X11_Y19_N30
\ssl|recv|index[31]~94\ : cycloneive_lcell_comb
-- Equation(s):
-- \ssl|recv|index[31]~94_combout\ = \ssl|recv|index\(31) $ (!\ssl|recv|index[30]~93\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010110100101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \ssl|recv|index\(31),
	cin => \ssl|recv|index[30]~93\,
	combout => \ssl|recv|index[31]~94_combout\);

-- Location: FF_X11_Y19_N31
\ssl|recv|index[31]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \sck~input_o\,
	d => \ssl|recv|index[31]~94_combout\,
	asdata => \~GND~combout\,
	clrn => \ALT_INV_rst~input_o\,
	sload => \ssl|recv|LessThan0~1_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \ssl|recv|index\(31));

-- Location: LCCOMB_X10_Y20_N8
\ssl|recv|LessThan0~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \ssl|recv|LessThan0~0_combout\ = ((!\ssl|recv|index\(2)) # (!\ssl|recv|index\(0))) # (!\ssl|recv|index\(1))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101111111111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \ssl|recv|index\(1),
	datac => \ssl|recv|index\(0),
	datad => \ssl|recv|index\(2),
	combout => \ssl|recv|LessThan0~0_combout\);

-- Location: LCCOMB_X12_Y23_N14
\ssl|busy~15\ : cycloneive_lcell_comb
-- Equation(s):
-- \ssl|busy~15_combout\ = (!\ssl|recv|index\(26) & (!\ssl|recv|index\(24) & (!\ssl|recv|index\(25) & !\ssl|recv|index\(23))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \ssl|recv|index\(26),
	datab => \ssl|recv|index\(24),
	datac => \ssl|recv|index\(25),
	datad => \ssl|recv|index\(23),
	combout => \ssl|busy~15_combout\);

-- Location: LCCOMB_X12_Y19_N0
\ssl|busy~16\ : cycloneive_lcell_comb
-- Equation(s):
-- \ssl|busy~16_combout\ = (!\ssl|recv|index\(28) & (!\ssl|recv|index\(27) & (!\ssl|recv|index\(29) & !\ssl|recv|index\(30))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \ssl|recv|index\(28),
	datab => \ssl|recv|index\(27),
	datac => \ssl|recv|index\(29),
	datad => \ssl|recv|index\(30),
	combout => \ssl|busy~16_combout\);

-- Location: LCCOMB_X12_Y19_N22
\ssl|busy~14\ : cycloneive_lcell_comb
-- Equation(s):
-- \ssl|busy~14_combout\ = (!\ssl|recv|index\(22) & (!\ssl|recv|index\(19) & (!\ssl|recv|index\(20) & !\ssl|recv|index\(21))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \ssl|recv|index\(22),
	datab => \ssl|recv|index\(19),
	datac => \ssl|recv|index\(20),
	datad => \ssl|recv|index\(21),
	combout => \ssl|busy~14_combout\);

-- Location: LCCOMB_X10_Y20_N24
\ssl|busy~10\ : cycloneive_lcell_comb
-- Equation(s):
-- \ssl|busy~10_combout\ = (!\ssl|recv|index\(10) & (!\ssl|recv|index\(9) & (!\ssl|recv|index\(7) & !\ssl|recv|index\(8))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \ssl|recv|index\(10),
	datab => \ssl|recv|index\(9),
	datac => \ssl|recv|index\(7),
	datad => \ssl|recv|index\(8),
	combout => \ssl|busy~10_combout\);

-- Location: LCCOMB_X10_Y20_N6
\ssl|busy~9\ : cycloneive_lcell_comb
-- Equation(s):
-- \ssl|busy~9_combout\ = (!\ssl|recv|index\(3) & (!\ssl|recv|index\(4) & (!\ssl|recv|index\(6) & !\ssl|recv|index\(5))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \ssl|recv|index\(3),
	datab => \ssl|recv|index\(4),
	datac => \ssl|recv|index\(6),
	datad => \ssl|recv|index\(5),
	combout => \ssl|busy~9_combout\);

-- Location: LCCOMB_X12_Y20_N8
\ssl|busy~12\ : cycloneive_lcell_comb
-- Equation(s):
-- \ssl|busy~12_combout\ = (!\ssl|recv|index\(18) & (!\ssl|recv|index\(16) & (!\ssl|recv|index\(17) & !\ssl|recv|index\(15))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \ssl|recv|index\(18),
	datab => \ssl|recv|index\(16),
	datac => \ssl|recv|index\(17),
	datad => \ssl|recv|index\(15),
	combout => \ssl|busy~12_combout\);

-- Location: LCCOMB_X10_Y20_N18
\ssl|busy~11\ : cycloneive_lcell_comb
-- Equation(s):
-- \ssl|busy~11_combout\ = (!\ssl|recv|index\(11) & (!\ssl|recv|index\(13) & (!\ssl|recv|index\(12) & !\ssl|recv|index\(14))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \ssl|recv|index\(11),
	datab => \ssl|recv|index\(13),
	datac => \ssl|recv|index\(12),
	datad => \ssl|recv|index\(14),
	combout => \ssl|busy~11_combout\);

-- Location: LCCOMB_X12_Y20_N18
\ssl|busy~13\ : cycloneive_lcell_comb
-- Equation(s):
-- \ssl|busy~13_combout\ = (\ssl|busy~10_combout\ & (\ssl|busy~9_combout\ & (\ssl|busy~12_combout\ & \ssl|busy~11_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \ssl|busy~10_combout\,
	datab => \ssl|busy~9_combout\,
	datac => \ssl|busy~12_combout\,
	datad => \ssl|busy~11_combout\,
	combout => \ssl|busy~13_combout\);

-- Location: LCCOMB_X12_Y20_N16
\ssl|busy~17\ : cycloneive_lcell_comb
-- Equation(s):
-- \ssl|busy~17_combout\ = (\ssl|busy~15_combout\ & (\ssl|busy~16_combout\ & (\ssl|busy~14_combout\ & \ssl|busy~13_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \ssl|busy~15_combout\,
	datab => \ssl|busy~16_combout\,
	datac => \ssl|busy~14_combout\,
	datad => \ssl|busy~13_combout\,
	combout => \ssl|busy~17_combout\);

-- Location: LCCOMB_X12_Y20_N30
\ssl|recv|LessThan0~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \ssl|recv|LessThan0~1_combout\ = (\ssl|recv|index\(31)) # ((!\ssl|recv|LessThan0~0_combout\ & \ssl|busy~17_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010111110101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \ssl|recv|index\(31),
	datac => \ssl|recv|LessThan0~0_combout\,
	datad => \ssl|busy~17_combout\,
	combout => \ssl|recv|LessThan0~1_combout\);

-- Location: FF_X11_Y20_N1
\ssl|recv|index[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \sck~input_o\,
	d => \ssl|recv|index[0]~32_combout\,
	asdata => \~GND~combout\,
	clrn => \ALT_INV_rst~input_o\,
	sload => \ssl|recv|LessThan0~1_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \ssl|recv|index\(0));

-- Location: FF_X11_Y20_N3
\ssl|recv|index[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \sck~input_o\,
	d => \ssl|recv|index[1]~34_combout\,
	asdata => \~GND~combout\,
	clrn => \ALT_INV_rst~input_o\,
	sload => \ssl|recv|LessThan0~1_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \ssl|recv|index\(1));

-- Location: LCCOMB_X18_Y23_N18
\ssl|recv|Decoder0~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \ssl|recv|Decoder0~3_combout\ = (\ssl|recv|index\(1) & (!\ssl|recv|index\(0) & (!\ssl|recv|index\(2) & !\ss~input_o\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \ssl|recv|index\(1),
	datab => \ssl|recv|index\(0),
	datac => \ssl|recv|index\(2),
	datad => \ss~input_o\,
	combout => \ssl|recv|Decoder0~3_combout\);

-- Location: LCCOMB_X18_Y23_N22
\ssl|recv|data_temp[5]~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \ssl|recv|data_temp[5]~3_combout\ = (\ssl|recv|Decoder0~3_combout\ & (\mosi~input_o\)) # (!\ssl|recv|Decoder0~3_combout\ & ((\ssl|recv|data_temp\(5))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \mosi~input_o\,
	datac => \ssl|recv|data_temp\(5),
	datad => \ssl|recv|Decoder0~3_combout\,
	combout => \ssl|recv|data_temp[5]~3_combout\);

-- Location: FF_X18_Y23_N23
\ssl|recv|data_temp[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \sck~input_o\,
	d => \ssl|recv|data_temp[5]~3_combout\,
	clrn => \ALT_INV_rst~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \ssl|recv|data_temp\(5));

-- Location: LCCOMB_X12_Y20_N26
\ssl|busy~18\ : cycloneive_lcell_comb
-- Equation(s):
-- \ssl|busy~18_combout\ = (((!\ssl|busy~5_combout\) # (!\ssl|busy~14_combout\)) # (!\ssl|busy~6_combout\)) # (!\ssl|busy~7_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111111111111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \ssl|busy~7_combout\,
	datab => \ssl|busy~6_combout\,
	datac => \ssl|busy~14_combout\,
	datad => \ssl|busy~5_combout\,
	combout => \ssl|busy~18_combout\);

-- Location: LCCOMB_X12_Y21_N30
\ssl|busy~20\ : cycloneive_lcell_comb
-- Equation(s):
-- \ssl|busy~20_combout\ = (\ssl|trns|index\(1)) # ((\ssl|trns|index\(2)) # ((\ssl|trns|index\(0)) # (\ssl|trns|index\(31))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111111110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \ssl|trns|index\(1),
	datab => \ssl|trns|index\(2),
	datac => \ssl|trns|index\(0),
	datad => \ssl|trns|index\(31),
	combout => \ssl|busy~20_combout\);

-- Location: LCCOMB_X12_Y19_N10
\ssl|busy~19\ : cycloneive_lcell_comb
-- Equation(s):
-- \ssl|busy~19_combout\ = (\ssl|recv|index\(1)) # ((\ssl|recv|index\(31)) # ((\ssl|recv|index\(0)) # (\ssl|recv|index\(2))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111111110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \ssl|recv|index\(1),
	datab => \ssl|recv|index\(31),
	datac => \ssl|recv|index\(0),
	datad => \ssl|recv|index\(2),
	combout => \ssl|busy~19_combout\);

-- Location: LCCOMB_X12_Y20_N28
\ssl|busy~21\ : cycloneive_lcell_comb
-- Equation(s):
-- \ssl|busy~21_combout\ = (((\ssl|busy~20_combout\) # (\ssl|busy~19_combout\)) # (!\ssl|busy~16_combout\)) # (!\ssl|busy~15_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111110111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \ssl|busy~15_combout\,
	datab => \ssl|busy~16_combout\,
	datac => \ssl|busy~20_combout\,
	datad => \ssl|busy~19_combout\,
	combout => \ssl|busy~21_combout\);

-- Location: LCCOMB_X12_Y20_N4
\ssl|busy\ : cycloneive_lcell_comb
-- Equation(s):
-- \ssl|busy~combout\ = (((\ssl|busy~18_combout\) # (\ssl|busy~21_combout\)) # (!\ssl|busy~13_combout\)) # (!\ssl|busy~4_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111110111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \ssl|busy~4_combout\,
	datab => \ssl|busy~13_combout\,
	datac => \ssl|busy~18_combout\,
	datad => \ssl|busy~21_combout\,
	combout => \ssl|busy~combout\);

-- Location: FF_X12_Y20_N5
sf_sb0 : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \ssl|busy~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sf_sb0~q\);

-- Location: LCCOMB_X12_Y23_N24
\sf_sb1~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \sf_sb1~feeder_combout\ = \sf_sb0~q\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \sf_sb0~q\,
	combout => \sf_sb1~feeder_combout\);

-- Location: FF_X12_Y23_N25
sf_sb1 : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \sf_sb1~feeder_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sf_sb1~q\);

-- Location: FF_X18_Y20_N19
sf_ss0 : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	asdata => \ss~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \sf_ss0~q\);

-- Location: LCCOMB_X14_Y23_N22
\ssl|recv|Decoder0~5\ : cycloneive_lcell_comb
-- Equation(s):
-- \ssl|recv|Decoder0~5_combout\ = (!\ss~input_o\ & (!\ssl|recv|index\(0) & (\ssl|recv|index\(2) & \ssl|recv|index\(1))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \ss~input_o\,
	datab => \ssl|recv|index\(0),
	datac => \ssl|recv|index\(2),
	datad => \ssl|recv|index\(1),
	combout => \ssl|recv|Decoder0~5_combout\);

-- Location: LCCOMB_X14_Y23_N26
\ssl|recv|data_temp[1]~5\ : cycloneive_lcell_comb
-- Equation(s):
-- \ssl|recv|data_temp[1]~5_combout\ = (\ssl|recv|Decoder0~5_combout\ & (\mosi~input_o\)) # (!\ssl|recv|Decoder0~5_combout\ & ((\ssl|recv|data_temp\(1))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101100011011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \ssl|recv|Decoder0~5_combout\,
	datab => \mosi~input_o\,
	datac => \ssl|recv|data_temp\(1),
	combout => \ssl|recv|data_temp[1]~5_combout\);

-- Location: FF_X14_Y23_N27
\ssl|recv|data_temp[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \sck~input_o\,
	d => \ssl|recv|data_temp[1]~5_combout\,
	clrn => \ALT_INV_rst~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \ssl|recv|data_temp\(1));

-- Location: LCCOMB_X18_Y23_N2
\ssl|recv|Decoder0~4\ : cycloneive_lcell_comb
-- Equation(s):
-- \ssl|recv|Decoder0~4_combout\ = (!\ssl|recv|index\(1) & (\ssl|recv|index\(0) & (!\ssl|recv|index\(2) & !\ss~input_o\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \ssl|recv|index\(1),
	datab => \ssl|recv|index\(0),
	datac => \ssl|recv|index\(2),
	datad => \ss~input_o\,
	combout => \ssl|recv|Decoder0~4_combout\);

-- Location: LCCOMB_X18_Y23_N8
\ssl|recv|data_temp[6]~4\ : cycloneive_lcell_comb
-- Equation(s):
-- \ssl|recv|data_temp[6]~4_combout\ = (\ssl|recv|Decoder0~4_combout\ & (\mosi~input_o\)) # (!\ssl|recv|Decoder0~4_combout\ & ((\ssl|recv|data_temp\(6))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \mosi~input_o\,
	datac => \ssl|recv|data_temp\(6),
	datad => \ssl|recv|Decoder0~4_combout\,
	combout => \ssl|recv|data_temp[6]~4_combout\);

-- Location: FF_X18_Y23_N9
\ssl|recv|data_temp[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \sck~input_o\,
	d => \ssl|recv|data_temp[6]~4_combout\,
	clrn => \ALT_INV_rst~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \ssl|recv|data_temp\(6));

-- Location: FF_X18_Y23_N11
\db|reg_addr|mem[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	asdata => \ssl|recv|data_temp\(6),
	sload => VCC,
	ena => \db|control_logic|state.s3~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \db|reg_addr|mem\(3));

-- Location: LCCOMB_X18_Y23_N16
\db|regs|Decoder0~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|regs|Decoder0~1_combout\ = (!\db|reg_addr|mem\(2) & !\db|reg_addr|mem\(3))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \db|reg_addr|mem\(2),
	datad => \db|reg_addr|mem\(3),
	combout => \db|regs|Decoder0~1_combout\);

-- Location: LCCOMB_X18_Y23_N6
\ssl|recv|Decoder0~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \ssl|recv|Decoder0~1_combout\ = (!\ssl|recv|index\(1) & (!\ssl|recv|index\(0) & (\ssl|recv|index\(2) & !\ss~input_o\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \ssl|recv|index\(1),
	datab => \ssl|recv|index\(0),
	datac => \ssl|recv|index\(2),
	datad => \ss~input_o\,
	combout => \ssl|recv|Decoder0~1_combout\);

-- Location: LCCOMB_X18_Y23_N14
\ssl|recv|data_temp[3]~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \ssl|recv|data_temp[3]~1_combout\ = (\ssl|recv|Decoder0~1_combout\ & (\mosi~input_o\)) # (!\ssl|recv|Decoder0~1_combout\ & ((\ssl|recv|data_temp\(3))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \mosi~input_o\,
	datac => \ssl|recv|data_temp\(3),
	datad => \ssl|recv|Decoder0~1_combout\,
	combout => \ssl|recv|data_temp[3]~1_combout\);

-- Location: FF_X18_Y23_N15
\ssl|recv|data_temp[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \sck~input_o\,
	d => \ssl|recv|data_temp[3]~1_combout\,
	clrn => \ALT_INV_rst~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \ssl|recv|data_temp\(3));

-- Location: FF_X18_Y23_N27
\db|reg_addr|mem[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	asdata => \ssl|recv|data_temp\(3),
	sload => VCC,
	ena => \db|control_logic|state.s3~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \db|reg_addr|mem\(0));

-- Location: LCCOMB_X14_Y23_N24
\ssl|recv|Decoder0~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \ssl|recv|Decoder0~2_combout\ = (!\ss~input_o\ & (\ssl|recv|index\(0) & (!\ssl|recv|index\(2) & \ssl|recv|index\(1))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000010000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \ss~input_o\,
	datab => \ssl|recv|index\(0),
	datac => \ssl|recv|index\(2),
	datad => \ssl|recv|index\(1),
	combout => \ssl|recv|Decoder0~2_combout\);

-- Location: LCCOMB_X18_Y23_N30
\ssl|recv|data_temp[4]~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \ssl|recv|data_temp[4]~2_combout\ = (\ssl|recv|Decoder0~2_combout\ & (\mosi~input_o\)) # (!\ssl|recv|Decoder0~2_combout\ & ((\ssl|recv|data_temp\(4))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \mosi~input_o\,
	datac => \ssl|recv|data_temp\(4),
	datad => \ssl|recv|Decoder0~2_combout\,
	combout => \ssl|recv|data_temp[4]~2_combout\);

-- Location: FF_X18_Y23_N31
\ssl|recv|data_temp[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \sck~input_o\,
	d => \ssl|recv|data_temp[4]~2_combout\,
	clrn => \ALT_INV_rst~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \ssl|recv|data_temp\(4));

-- Location: FF_X18_Y23_N25
\db|reg_addr|mem[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	asdata => \ssl|recv|data_temp\(4),
	sload => VCC,
	ena => \db|control_logic|state.s3~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \db|reg_addr|mem\(1));

-- Location: LCCOMB_X14_Y23_N6
\ssl|recv|Decoder0~7\ : cycloneive_lcell_comb
-- Equation(s):
-- \ssl|recv|Decoder0~7_combout\ = (!\ss~input_o\ & (!\ssl|recv|index\(0) & (!\ssl|recv|index\(2) & !\ssl|recv|index\(1))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \ss~input_o\,
	datab => \ssl|recv|index\(0),
	datac => \ssl|recv|index\(2),
	datad => \ssl|recv|index\(1),
	combout => \ssl|recv|Decoder0~7_combout\);

-- Location: LCCOMB_X14_Y23_N12
\ssl|recv|data_temp[7]~7\ : cycloneive_lcell_comb
-- Equation(s):
-- \ssl|recv|data_temp[7]~7_combout\ = (\ssl|recv|Decoder0~7_combout\ & (\mosi~input_o\)) # (!\ssl|recv|Decoder0~7_combout\ & ((\ssl|recv|data_temp\(7))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \mosi~input_o\,
	datac => \ssl|recv|data_temp\(7),
	datad => \ssl|recv|Decoder0~7_combout\,
	combout => \ssl|recv|data_temp[7]~7_combout\);

-- Location: FF_X14_Y23_N13
\ssl|recv|data_temp[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \sck~input_o\,
	d => \ssl|recv|data_temp[7]~7_combout\,
	clrn => \ALT_INV_rst~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \ssl|recv|data_temp\(7));

-- Location: FF_X19_Y23_N13
\db|reg_wr|mem\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	asdata => \ssl|recv|data_temp\(7),
	clrn => \db|control_logic|state.s0~q\,
	sload => VCC,
	ena => \db|control_logic|state.s3~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \db|reg_wr|mem~q\);

-- Location: LCCOMB_X19_Y23_N12
\db|regs|dbr[0][0]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|regs|dbr[0][0]~0_combout\ = (!\db|reg_addr|mem\(3) & (\db|reg_wr|mem~q\ & ((!\db|reg_addr|mem\(1)) # (!\db|reg_addr|mem\(2)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \db|reg_addr|mem\(3),
	datab => \db|reg_addr|mem\(2),
	datac => \db|reg_wr|mem~q\,
	datad => \db|reg_addr|mem\(1),
	combout => \db|regs|dbr[0][0]~0_combout\);

-- Location: LCCOMB_X19_Y23_N26
\db|control_logic|Selector2~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|control_logic|Selector2~0_combout\ = (\db|control_logic|state.s3~q\) # ((!\sf_sb1~q\ & \db|control_logic|state.s4~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \sf_sb1~q\,
	datac => \db|control_logic|state.s4~q\,
	datad => \db|control_logic|state.s3~q\,
	combout => \db|control_logic|Selector2~0_combout\);

-- Location: FF_X19_Y23_N27
\db|control_logic|state.s4\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \db|control_logic|Selector2~0_combout\,
	clrn => \ALT_INV_rst~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \db|control_logic|state.s4~q\);

-- Location: LCCOMB_X19_Y23_N22
\db|control_logic|next_state.s5~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|control_logic|next_state.s5~0_combout\ = (\sf_sb1~q\ & ((\db|control_logic|state.s4~q\) # (\db|control_logic|state.s5~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \db|control_logic|state.s4~q\,
	datac => \db|control_logic|state.s5~q\,
	datad => \sf_sb1~q\,
	combout => \db|control_logic|next_state.s5~0_combout\);

-- Location: FF_X19_Y23_N23
\db|control_logic|state.s5\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \db|control_logic|next_state.s5~0_combout\,
	clrn => \ALT_INV_rst~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \db|control_logic|state.s5~q\);

-- Location: LCCOMB_X19_Y23_N10
\db|control_logic|next_state.s6~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|control_logic|next_state.s6~0_combout\ = (\db|control_logic|state.s5~q\ & !\sf_sb1~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \db|control_logic|state.s5~q\,
	datad => \sf_sb1~q\,
	combout => \db|control_logic|next_state.s6~0_combout\);

-- Location: FF_X19_Y23_N11
\db|control_logic|state.s6\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \db|control_logic|next_state.s6~0_combout\,
	clrn => \ALT_INV_rst~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \db|control_logic|state.s6~q\);

-- Location: LCCOMB_X19_Y23_N18
\db|regs|dbr[0][0]~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|regs|dbr[0][0]~1_combout\ = (\db|regs|dbr[0][0]~0_combout\ & \db|control_logic|state.s6~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \db|regs|dbr[0][0]~0_combout\,
	datad => \db|control_logic|state.s6~q\,
	combout => \db|regs|dbr[0][0]~1_combout\);

-- Location: LCCOMB_X18_Y20_N0
\db|regs|Decoder0~4\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|regs|Decoder0~4_combout\ = (\db|regs|Decoder0~1_combout\ & (!\db|reg_addr|mem\(0) & (!\db|reg_addr|mem\(1) & \db|regs|dbr[0][0]~1_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000001000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \db|regs|Decoder0~1_combout\,
	datab => \db|reg_addr|mem\(0),
	datac => \db|reg_addr|mem\(1),
	datad => \db|regs|dbr[0][0]~1_combout\,
	combout => \db|regs|Decoder0~4_combout\);

-- Location: FF_X18_Y20_N27
\db|regs|dbr[0][1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	asdata => \ssl|recv|data_temp\(1),
	sload => VCC,
	ena => \db|regs|Decoder0~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \db|regs|dbr[0][1]~q\);

-- Location: LCCOMB_X14_Y23_N8
\ssl|recv|Decoder0~6\ : cycloneive_lcell_comb
-- Equation(s):
-- \ssl|recv|Decoder0~6_combout\ = (!\ss~input_o\ & (\ssl|recv|index\(0) & (\ssl|recv|index\(2) & \ssl|recv|index\(1))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \ss~input_o\,
	datab => \ssl|recv|index\(0),
	datac => \ssl|recv|index\(2),
	datad => \ssl|recv|index\(1),
	combout => \ssl|recv|Decoder0~6_combout\);

-- Location: LCCOMB_X19_Y22_N10
\ssl|recv|data_temp[0]~6\ : cycloneive_lcell_comb
-- Equation(s):
-- \ssl|recv|data_temp[0]~6_combout\ = (\ssl|recv|Decoder0~6_combout\ & (\mosi~input_o\)) # (!\ssl|recv|Decoder0~6_combout\ & ((\ssl|recv|data_temp\(0))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \mosi~input_o\,
	datac => \ssl|recv|data_temp\(0),
	datad => \ssl|recv|Decoder0~6_combout\,
	combout => \ssl|recv|data_temp[0]~6_combout\);

-- Location: FF_X19_Y22_N11
\ssl|recv|data_temp[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \sck~input_o\,
	d => \ssl|recv|data_temp[0]~6_combout\,
	clrn => \ALT_INV_rst~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \ssl|recv|data_temp\(0));

-- Location: FF_X18_Y20_N3
\db|regs|dbr[0][0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	asdata => \ssl|recv|data_temp\(0),
	sload => VCC,
	ena => \db|regs|Decoder0~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \db|regs|dbr[0][0]~q\);

-- Location: LCCOMB_X18_Y20_N20
\db|regs|dbr[0][7]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|regs|dbr[0][7]~feeder_combout\ = \ssl|recv|data_temp\(7)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \ssl|recv|data_temp\(7),
	combout => \db|regs|dbr[0][7]~feeder_combout\);

-- Location: FF_X18_Y20_N21
\db|regs|dbr[0][7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	d => \db|regs|dbr[0][7]~feeder_combout\,
	ena => \db|regs|Decoder0~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \db|regs|dbr[0][7]~q\);

-- Location: FF_X18_Y20_N13
\db|regs|dbr[0][6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	asdata => \ssl|recv|data_temp\(6),
	sload => VCC,
	ena => \db|regs|Decoder0~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \db|regs|dbr[0][6]~q\);

-- Location: LCCOMB_X18_Y20_N6
\db|regs|dbr[0][4]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|regs|dbr[0][4]~feeder_combout\ = \ssl|recv|data_temp\(4)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \ssl|recv|data_temp\(4),
	combout => \db|regs|dbr[0][4]~feeder_combout\);

-- Location: FF_X18_Y20_N7
\db|regs|dbr[0][4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	d => \db|regs|dbr[0][4]~feeder_combout\,
	ena => \db|regs|Decoder0~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \db|regs|dbr[0][4]~q\);

-- Location: FF_X18_Y20_N31
\db|regs|dbr[0][5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	asdata => \ssl|recv|data_temp\(5),
	sload => VCC,
	ena => \db|regs|Decoder0~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \db|regs|dbr[0][5]~q\);

-- Location: LCCOMB_X14_Y23_N30
\ssl|recv|Decoder0~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \ssl|recv|Decoder0~0_combout\ = (!\ss~input_o\ & (\ssl|recv|index\(0) & (\ssl|recv|index\(2) & !\ssl|recv|index\(1))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000001000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \ss~input_o\,
	datab => \ssl|recv|index\(0),
	datac => \ssl|recv|index\(2),
	datad => \ssl|recv|index\(1),
	combout => \ssl|recv|Decoder0~0_combout\);

-- Location: LCCOMB_X17_Y22_N6
\ssl|recv|data_temp[2]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \ssl|recv|data_temp[2]~0_combout\ = (\ssl|recv|Decoder0~0_combout\ & (\mosi~input_o\)) # (!\ssl|recv|Decoder0~0_combout\ & ((\ssl|recv|data_temp\(2))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011100010111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \mosi~input_o\,
	datab => \ssl|recv|Decoder0~0_combout\,
	datac => \ssl|recv|data_temp\(2),
	combout => \ssl|recv|data_temp[2]~0_combout\);

-- Location: FF_X17_Y22_N7
\ssl|recv|data_temp[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \sck~input_o\,
	d => \ssl|recv|data_temp[2]~0_combout\,
	clrn => \ALT_INV_rst~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \ssl|recv|data_temp\(2));

-- Location: FF_X18_Y20_N17
\db|regs|dbr[0][2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	asdata => \ssl|recv|data_temp\(2),
	sload => VCC,
	ena => \db|regs|Decoder0~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \db|regs|dbr[0][2]~q\);

-- Location: FF_X18_Y20_N11
\db|regs|dbr[0][3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	asdata => \ssl|recv|data_temp\(3),
	sload => VCC,
	ena => \db|regs|Decoder0~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \db|regs|dbr[0][3]~q\);

-- Location: LCCOMB_X18_Y20_N16
\db|control_logic|Equal2~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|control_logic|Equal2~2_combout\ = (!\db|regs|dbr[0][4]~q\ & (!\db|regs|dbr[0][5]~q\ & (!\db|regs|dbr[0][2]~q\ & !\db|regs|dbr[0][3]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \db|regs|dbr[0][4]~q\,
	datab => \db|regs|dbr[0][5]~q\,
	datac => \db|regs|dbr[0][2]~q\,
	datad => \db|regs|dbr[0][3]~q\,
	combout => \db|control_logic|Equal2~2_combout\);

-- Location: LCCOMB_X18_Y20_N12
\db|control_logic|Equal2~4\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|control_logic|Equal2~4_combout\ = (!\db|regs|dbr[0][7]~q\ & (!\db|regs|dbr[0][6]~q\ & \db|control_logic|Equal2~2_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000001100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \db|regs|dbr[0][7]~q\,
	datac => \db|regs|dbr[0][6]~q\,
	datad => \db|control_logic|Equal2~2_combout\,
	combout => \db|control_logic|Equal2~4_combout\);

-- Location: LCCOMB_X19_Y23_N4
\db|control_logic|Selector3~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|control_logic|Selector3~0_combout\ = (\db|control_logic|state.s6~q\) # ((\db|control_logic|state.s7~q\ & !\sf_ss0~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \db|control_logic|state.s6~q\,
	datac => \db|control_logic|state.s7~q\,
	datad => \sf_ss0~q\,
	combout => \db|control_logic|Selector3~0_combout\);

-- Location: FF_X19_Y23_N5
\db|control_logic|state.s7\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \db|control_logic|Selector3~0_combout\,
	clrn => \ALT_INV_rst~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \db|control_logic|state.s7~q\);

-- Location: LCCOMB_X18_Y23_N20
\db|control_logic|process_1~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|control_logic|process_1~0_combout\ = (!\db|reg_addr|mem\(0) & (!\db|reg_addr|mem\(1) & (\db|reg_wr|mem~q\ & \db|regs|Decoder0~1_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \db|reg_addr|mem\(0),
	datab => \db|reg_addr|mem\(1),
	datac => \db|reg_wr|mem~q\,
	datad => \db|regs|Decoder0~1_combout\,
	combout => \db|control_logic|process_1~0_combout\);

-- Location: LCCOMB_X18_Y20_N18
\db|control_logic|next_state.s9~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|control_logic|next_state.s9~0_combout\ = (\db|control_logic|state.s7~q\ & (\sf_ss0~q\ & \db|control_logic|process_1~0_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \db|control_logic|state.s7~q\,
	datac => \sf_ss0~q\,
	datad => \db|control_logic|process_1~0_combout\,
	combout => \db|control_logic|next_state.s9~0_combout\);

-- Location: LCCOMB_X18_Y20_N14
\db|control_logic|next_state.s9~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|control_logic|next_state.s9~1_combout\ = (\db|regs|dbr[0][1]~q\ & (!\db|regs|dbr[0][0]~q\ & (\db|control_logic|Equal2~4_combout\ & \db|control_logic|next_state.s9~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \db|regs|dbr[0][1]~q\,
	datab => \db|regs|dbr[0][0]~q\,
	datac => \db|control_logic|Equal2~4_combout\,
	datad => \db|control_logic|next_state.s9~0_combout\,
	combout => \db|control_logic|next_state.s9~1_combout\);

-- Location: FF_X18_Y20_N15
\db|control_logic|state.s9\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \db|control_logic|next_state.s9~1_combout\,
	clrn => \ALT_INV_rst~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \db|control_logic|state.s9~q\);

-- Location: LCCOMB_X18_Y20_N24
\db|control_logic|next_state.s8~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|control_logic|next_state.s8~0_combout\ = (!\db|regs|dbr[0][1]~q\ & (\db|regs|dbr[0][0]~q\ & (\db|control_logic|Equal2~4_combout\ & \db|control_logic|next_state.s9~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \db|regs|dbr[0][1]~q\,
	datab => \db|regs|dbr[0][0]~q\,
	datac => \db|control_logic|Equal2~4_combout\,
	datad => \db|control_logic|next_state.s9~0_combout\,
	combout => \db|control_logic|next_state.s8~0_combout\);

-- Location: FF_X18_Y20_N25
\db|control_logic|state.s8\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \db|control_logic|next_state.s8~0_combout\,
	clrn => \ALT_INV_rst~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \db|control_logic|state.s8~q\);

-- Location: LCCOMB_X18_Y20_N2
\db|control_logic|Equal2~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|control_logic|Equal2~3_combout\ = (!\db|regs|dbr[0][6]~q\ & !\db|regs|dbr[0][7]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000001010101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \db|regs|dbr[0][6]~q\,
	datad => \db|regs|dbr[0][7]~q\,
	combout => \db|control_logic|Equal2~3_combout\);

-- Location: LCCOMB_X18_Y20_N26
\db|control_logic|Selector0~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|control_logic|Selector0~0_combout\ = ((\db|regs|dbr[0][0]~q\ $ (!\db|regs|dbr[0][1]~q\)) # (!\db|control_logic|Equal2~2_combout\)) # (!\db|control_logic|Equal2~3_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011011111111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \db|regs|dbr[0][0]~q\,
	datab => \db|control_logic|Equal2~3_combout\,
	datac => \db|regs|dbr[0][1]~q\,
	datad => \db|control_logic|Equal2~2_combout\,
	combout => \db|control_logic|Selector0~0_combout\);

-- Location: LCCOMB_X19_Y23_N28
\db|control_logic|Selector0~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|control_logic|Selector0~1_combout\ = ((\db|control_logic|state.s7~q\ & ((\db|control_logic|Selector0~0_combout\) # (!\db|control_logic|process_1~0_combout\)))) # (!\db|control_logic|state.s0~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010111100101111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \db|control_logic|state.s7~q\,
	datab => \db|control_logic|process_1~0_combout\,
	datac => \db|control_logic|state.s0~q\,
	datad => \db|control_logic|Selector0~0_combout\,
	combout => \db|control_logic|Selector0~1_combout\);

-- Location: LCCOMB_X19_Y23_N20
\db|control_logic|Selector0~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|control_logic|Selector0~2_combout\ = (!\db|control_logic|state.s9~q\ & (!\db|control_logic|state.s8~q\ & ((!\db|control_logic|Selector0~1_combout\) # (!\sf_ss0~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000100000011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \sf_ss0~q\,
	datab => \db|control_logic|state.s9~q\,
	datac => \db|control_logic|state.s8~q\,
	datad => \db|control_logic|Selector0~1_combout\,
	combout => \db|control_logic|Selector0~2_combout\);

-- Location: FF_X19_Y23_N21
\db|control_logic|state.s0\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \db|control_logic|Selector0~2_combout\,
	clrn => \ALT_INV_rst~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \db|control_logic|state.s0~q\);

-- Location: LCCOMB_X19_Y23_N14
\db|control_logic|Selector1~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|control_logic|Selector1~0_combout\ = (\db|control_logic|state.s0~q\ & (((\db|control_logic|state.s1~q\ & !\sf_sb1~q\)))) # (!\db|control_logic|state.s0~q\ & (((\db|control_logic|state.s1~q\ & !\sf_sb1~q\)) # (!\sf_ss0~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000111110001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \db|control_logic|state.s0~q\,
	datab => \sf_ss0~q\,
	datac => \db|control_logic|state.s1~q\,
	datad => \sf_sb1~q\,
	combout => \db|control_logic|Selector1~0_combout\);

-- Location: FF_X19_Y23_N15
\db|control_logic|state.s1\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \db|control_logic|Selector1~0_combout\,
	clrn => \ALT_INV_rst~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \db|control_logic|state.s1~q\);

-- Location: LCCOMB_X19_Y23_N6
\db|control_logic|next_state.s2~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|control_logic|next_state.s2~0_combout\ = (\sf_sb1~q\ & ((\db|control_logic|state.s1~q\) # (\db|control_logic|state.s2~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \db|control_logic|state.s1~q\,
	datac => \db|control_logic|state.s2~q\,
	datad => \sf_sb1~q\,
	combout => \db|control_logic|next_state.s2~0_combout\);

-- Location: FF_X19_Y23_N7
\db|control_logic|state.s2\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \db|control_logic|next_state.s2~0_combout\,
	clrn => \ALT_INV_rst~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \db|control_logic|state.s2~q\);

-- Location: LCCOMB_X19_Y23_N24
\db|control_logic|next_state.s3~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|control_logic|next_state.s3~0_combout\ = (!\sf_sb1~q\ & \db|control_logic|state.s2~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \sf_sb1~q\,
	datad => \db|control_logic|state.s2~q\,
	combout => \db|control_logic|next_state.s3~0_combout\);

-- Location: FF_X19_Y23_N25
\db|control_logic|state.s3\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \db|control_logic|next_state.s3~0_combout\,
	clrn => \ALT_INV_rst~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \db|control_logic|state.s3~q\);

-- Location: FF_X18_Y23_N5
\db|reg_addr|mem[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	asdata => \ssl|recv|data_temp\(5),
	sload => VCC,
	ena => \db|control_logic|state.s3~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \db|reg_addr|mem\(2));

-- Location: LCCOMB_X18_Y23_N26
\db|regs|LessThan1~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|regs|LessThan1~0_combout\ = (\db|reg_addr|mem\(3) & ((\db|reg_addr|mem\(2)) # ((\db|reg_addr|mem\(0) & \db|reg_addr|mem\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100100010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \db|reg_addr|mem\(2),
	datab => \db|reg_addr|mem\(3),
	datac => \db|reg_addr|mem\(0),
	datad => \db|reg_addr|mem\(1),
	combout => \db|regs|LessThan1~0_combout\);

-- Location: LCCOMB_X18_Y21_N24
\db|regs|dbr[4][2]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|regs|dbr[4][2]~feeder_combout\ = \ssl|recv|data_temp\(2)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \ssl|recv|data_temp\(2),
	combout => \db|regs|dbr[4][2]~feeder_combout\);

-- Location: LCCOMB_X18_Y23_N10
\db|regs|Decoder0~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|regs|Decoder0~0_combout\ = (!\db|reg_addr|mem\(3) & \db|reg_addr|mem\(2))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \db|reg_addr|mem\(3),
	datad => \db|reg_addr|mem\(2),
	combout => \db|regs|Decoder0~0_combout\);

-- Location: LCCOMB_X19_Y23_N0
\db|regs|Decoder0~7\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|regs|Decoder0~7_combout\ = (!\db|reg_addr|mem\(0) & (!\db|reg_addr|mem\(1) & (\db|regs|Decoder0~0_combout\ & \db|regs|dbr[0][0]~1_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \db|reg_addr|mem\(0),
	datab => \db|reg_addr|mem\(1),
	datac => \db|regs|Decoder0~0_combout\,
	datad => \db|regs|dbr[0][0]~1_combout\,
	combout => \db|regs|Decoder0~7_combout\);

-- Location: FF_X18_Y21_N25
\db|regs|dbr[4][2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	d => \db|regs|dbr[4][2]~feeder_combout\,
	ena => \db|regs|Decoder0~7_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \db|regs|dbr[4][2]~q\);

-- Location: LCCOMB_X19_Y21_N26
\db|regs|dbr[8][2]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|regs|dbr[8][2]~feeder_combout\ = \db|regs|dbr[4][2]~q\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \db|regs|dbr[4][2]~q\,
	combout => \db|regs|dbr[8][2]~feeder_combout\);

-- Location: FF_X19_Y21_N27
\db|regs|dbr[8][2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	d => \db|regs|dbr[8][2]~feeder_combout\,
	ena => \db|control_logic|state.s9~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \db|regs|dbr[8][2]~q\);

-- Location: LCCOMB_X17_Y22_N24
\db|regs|dbr[5][2]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|regs|dbr[5][2]~feeder_combout\ = \ssl|recv|data_temp\(2)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \ssl|recv|data_temp\(2),
	combout => \db|regs|dbr[5][2]~feeder_combout\);

-- Location: LCCOMB_X19_Y23_N30
\db|regs|Decoder0~6\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|regs|Decoder0~6_combout\ = (\db|reg_addr|mem\(0) & (!\db|reg_addr|mem\(1) & (\db|regs|Decoder0~0_combout\ & \db|regs|dbr[0][0]~1_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \db|reg_addr|mem\(0),
	datab => \db|reg_addr|mem\(1),
	datac => \db|regs|Decoder0~0_combout\,
	datad => \db|regs|dbr[0][0]~1_combout\,
	combout => \db|regs|Decoder0~6_combout\);

-- Location: FF_X17_Y22_N25
\db|regs|dbr[5][2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	d => \db|regs|dbr[5][2]~feeder_combout\,
	ena => \db|regs|Decoder0~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \db|regs|dbr[5][2]~q\);

-- Location: FF_X19_Y21_N1
\db|regs|dbr[9][2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	asdata => \db|regs|dbr[5][2]~q\,
	sload => VCC,
	ena => \db|control_logic|state.s9~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \db|regs|dbr[9][2]~q\);

-- Location: LCCOMB_X18_Y23_N4
\db|regs|data_out[2]~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|regs|data_out[2]~1_combout\ = (\db|reg_addr|mem\(3) & (!\db|reg_addr|mem\(2) & !\db|reg_addr|mem\(1)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \db|reg_addr|mem\(3),
	datac => \db|reg_addr|mem\(2),
	datad => \db|reg_addr|mem\(1),
	combout => \db|regs|data_out[2]~1_combout\);

-- Location: LCCOMB_X19_Y21_N0
\db|regs|data_out[2]~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|regs|data_out[2]~2_combout\ = (\db|regs|data_out[2]~1_combout\ & ((\db|reg_addr|mem\(0) & ((\db|regs|dbr[9][2]~q\))) # (!\db|reg_addr|mem\(0) & (\db|regs|dbr[8][2]~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110001000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \db|regs|dbr[8][2]~q\,
	datab => \db|reg_addr|mem\(0),
	datac => \db|regs|dbr[9][2]~q\,
	datad => \db|regs|data_out[2]~1_combout\,
	combout => \db|regs|data_out[2]~2_combout\);

-- Location: LCCOMB_X18_Y22_N6
\db|regs|dbr[2][2]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|regs|dbr[2][2]~feeder_combout\ = \ssl|recv|data_temp\(2)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \ssl|recv|data_temp\(2),
	combout => \db|regs|dbr[2][2]~feeder_combout\);

-- Location: LCCOMB_X19_Y23_N2
\db|regs|Decoder0~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|regs|Decoder0~3_combout\ = (\db|regs|Decoder0~1_combout\ & (\db|reg_addr|mem\(1) & (!\db|reg_addr|mem\(0) & \db|regs|dbr[0][0]~1_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000100000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \db|regs|Decoder0~1_combout\,
	datab => \db|reg_addr|mem\(1),
	datac => \db|reg_addr|mem\(0),
	datad => \db|regs|dbr[0][0]~1_combout\,
	combout => \db|regs|Decoder0~3_combout\);

-- Location: FF_X18_Y22_N7
\db|regs|dbr[2][2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	d => \db|regs|dbr[2][2]~feeder_combout\,
	ena => \db|regs|Decoder0~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \db|regs|dbr[2][2]~q\);

-- Location: LCCOMB_X17_Y21_N18
\db|regs|dbr[6][2]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|regs|dbr[6][2]~feeder_combout\ = \db|regs|dbr[2][2]~q\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \db|regs|dbr[2][2]~q\,
	combout => \db|regs|dbr[6][2]~feeder_combout\);

-- Location: FF_X17_Y21_N19
\db|regs|dbr[6][2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	d => \db|regs|dbr[6][2]~feeder_combout\,
	ena => \db|control_logic|state.s9~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \db|regs|dbr[6][2]~q\);

-- Location: LCCOMB_X17_Y21_N14
\db|regs|dbr[3][2]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|regs|dbr[3][2]~feeder_combout\ = \ssl|recv|data_temp\(2)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \ssl|recv|data_temp\(2),
	combout => \db|regs|dbr[3][2]~feeder_combout\);

-- Location: LCCOMB_X19_Y23_N8
\db|regs|Decoder0~5\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|regs|Decoder0~5_combout\ = (\db|regs|Decoder0~1_combout\ & (\db|reg_addr|mem\(1) & (\db|reg_addr|mem\(0) & \db|regs|dbr[0][0]~1_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \db|regs|Decoder0~1_combout\,
	datab => \db|reg_addr|mem\(1),
	datac => \db|reg_addr|mem\(0),
	datad => \db|regs|dbr[0][0]~1_combout\,
	combout => \db|regs|Decoder0~5_combout\);

-- Location: FF_X17_Y21_N15
\db|regs|dbr[3][2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	d => \db|regs|dbr[3][2]~feeder_combout\,
	ena => \db|regs|Decoder0~5_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \db|regs|dbr[3][2]~q\);

-- Location: FF_X17_Y21_N7
\db|regs|dbr[7][2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	asdata => \db|regs|dbr[3][2]~q\,
	sload => VCC,
	ena => \db|control_logic|state.s9~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \db|regs|dbr[7][2]~q\);

-- Location: LCCOMB_X18_Y21_N6
\db|regs|Mux5~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|regs|Mux5~2_combout\ = (\db|reg_addr|mem\(0) & ((\db|reg_addr|mem\(1)) # ((\db|regs|dbr[5][2]~q\)))) # (!\db|reg_addr|mem\(0) & (!\db|reg_addr|mem\(1) & ((\db|regs|dbr[4][2]~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011100110101000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \db|reg_addr|mem\(0),
	datab => \db|reg_addr|mem\(1),
	datac => \db|regs|dbr[5][2]~q\,
	datad => \db|regs|dbr[4][2]~q\,
	combout => \db|regs|Mux5~2_combout\);

-- Location: LCCOMB_X17_Y21_N6
\db|regs|Mux5~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|regs|Mux5~3_combout\ = (\db|reg_addr|mem\(1) & ((\db|regs|Mux5~2_combout\ & ((\db|regs|dbr[7][2]~q\))) # (!\db|regs|Mux5~2_combout\ & (\db|regs|dbr[6][2]~q\)))) # (!\db|reg_addr|mem\(1) & (((\db|regs|Mux5~2_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111010110001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \db|reg_addr|mem\(1),
	datab => \db|regs|dbr[6][2]~q\,
	datac => \db|regs|dbr[7][2]~q\,
	datad => \db|regs|Mux5~2_combout\,
	combout => \db|regs|Mux5~3_combout\);

-- Location: LCCOMB_X18_Y22_N20
\db|regs|Mux5~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|regs|Mux5~0_combout\ = (\db|reg_addr|mem\(1) & ((\db|regs|dbr[2][2]~q\) # ((\db|reg_addr|mem\(0))))) # (!\db|reg_addr|mem\(1) & (((!\db|reg_addr|mem\(0) & \db|regs|dbr[0][2]~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100101111001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \db|regs|dbr[2][2]~q\,
	datab => \db|reg_addr|mem\(1),
	datac => \db|reg_addr|mem\(0),
	datad => \db|regs|dbr[0][2]~q\,
	combout => \db|regs|Mux5~0_combout\);

-- Location: LCCOMB_X18_Y22_N14
\db|regs|Decoder0~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|regs|Decoder0~2_combout\ = (\db|reg_addr|mem\(0) & (\db|regs|Decoder0~1_combout\ & (!\db|reg_addr|mem\(1) & \db|regs|dbr[0][0]~1_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000100000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \db|reg_addr|mem\(0),
	datab => \db|regs|Decoder0~1_combout\,
	datac => \db|reg_addr|mem\(1),
	datad => \db|regs|dbr[0][0]~1_combout\,
	combout => \db|regs|Decoder0~2_combout\);

-- Location: FF_X18_Y22_N17
\db|regs|dbr[1][2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	asdata => \ssl|recv|data_temp\(2),
	sload => VCC,
	ena => \db|regs|Decoder0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \db|regs|dbr[1][2]~q\);

-- Location: LCCOMB_X18_Y22_N16
\db|regs|Mux5~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|regs|Mux5~1_combout\ = (\db|reg_addr|mem\(0) & ((\db|regs|Mux5~0_combout\ & ((\db|regs|dbr[3][2]~q\))) # (!\db|regs|Mux5~0_combout\ & (\db|regs|dbr[1][2]~q\)))) # (!\db|reg_addr|mem\(0) & (\db|regs|Mux5~0_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110110001100100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \db|reg_addr|mem\(0),
	datab => \db|regs|Mux5~0_combout\,
	datac => \db|regs|dbr[1][2]~q\,
	datad => \db|regs|dbr[3][2]~q\,
	combout => \db|regs|Mux5~1_combout\);

-- Location: LCCOMB_X18_Y22_N22
\db|regs|data_out[2]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|regs|data_out[2]~0_combout\ = (\db|regs|Mux5~3_combout\ & ((\db|regs|Decoder0~0_combout\) # ((\db|regs|Decoder0~1_combout\ & \db|regs|Mux5~1_combout\)))) # (!\db|regs|Mux5~3_combout\ & (((\db|regs|Decoder0~1_combout\ & \db|regs|Mux5~1_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \db|regs|Mux5~3_combout\,
	datab => \db|regs|Decoder0~0_combout\,
	datac => \db|regs|Decoder0~1_combout\,
	datad => \db|regs|Mux5~1_combout\,
	combout => \db|regs|data_out[2]~0_combout\);

-- Location: LCCOMB_X19_Y23_N16
\db|regs|data_out[2]~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|regs|data_out[2]~3_combout\ = (\db|regs|data_out[2]~2_combout\) # ((\db|regs|data_out[2]~0_combout\) # ((\db|regs|LessThan1~0_combout\ & \db|regs|dbr[0][2]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \db|regs|LessThan1~0_combout\,
	datab => \db|regs|dbr[0][2]~q\,
	datac => \db|regs|data_out[2]~2_combout\,
	datad => \db|regs|data_out[2]~0_combout\,
	combout => \db|regs|data_out[2]~3_combout\);

-- Location: LCCOMB_X13_Y23_N28
\db|regs|dbr[5][1]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|regs|dbr[5][1]~feeder_combout\ = \ssl|recv|data_temp\(1)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \ssl|recv|data_temp\(1),
	combout => \db|regs|dbr[5][1]~feeder_combout\);

-- Location: FF_X13_Y23_N29
\db|regs|dbr[5][1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	d => \db|regs|dbr[5][1]~feeder_combout\,
	ena => \db|regs|Decoder0~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \db|regs|dbr[5][1]~q\);

-- Location: LCCOMB_X17_Y21_N10
\db|regs|dbr[9][1]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|regs|dbr[9][1]~feeder_combout\ = \db|regs|dbr[5][1]~q\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \db|regs|dbr[5][1]~q\,
	combout => \db|regs|dbr[9][1]~feeder_combout\);

-- Location: FF_X17_Y21_N11
\db|regs|dbr[9][1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	d => \db|regs|dbr[9][1]~feeder_combout\,
	ena => \db|control_logic|state.s9~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \db|regs|dbr[9][1]~q\);

-- Location: LCCOMB_X16_Y23_N20
\db|regs|dbr[4][1]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|regs|dbr[4][1]~feeder_combout\ = \ssl|recv|data_temp\(1)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \ssl|recv|data_temp\(1),
	combout => \db|regs|dbr[4][1]~feeder_combout\);

-- Location: FF_X16_Y23_N21
\db|regs|dbr[4][1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	d => \db|regs|dbr[4][1]~feeder_combout\,
	ena => \db|regs|Decoder0~7_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \db|regs|dbr[4][1]~q\);

-- Location: FF_X17_Y21_N31
\db|regs|dbr[8][1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	asdata => \db|regs|dbr[4][1]~q\,
	sload => VCC,
	ena => \db|control_logic|state.s9~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \db|regs|dbr[8][1]~q\);

-- Location: LCCOMB_X17_Y21_N30
\db|regs|data_out[1]~5\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|regs|data_out[1]~5_combout\ = (\db|regs|data_out[2]~1_combout\ & ((\db|reg_addr|mem\(0) & (\db|regs|dbr[9][1]~q\)) # (!\db|reg_addr|mem\(0) & ((\db|regs|dbr[8][1]~q\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011100000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \db|regs|dbr[9][1]~q\,
	datab => \db|reg_addr|mem\(0),
	datac => \db|regs|dbr[8][1]~q\,
	datad => \db|regs|data_out[2]~1_combout\,
	combout => \db|regs|data_out[1]~5_combout\);

-- Location: LCCOMB_X16_Y23_N6
\db|regs|dbr[3][1]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|regs|dbr[3][1]~feeder_combout\ = \ssl|recv|data_temp\(1)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \ssl|recv|data_temp\(1),
	combout => \db|regs|dbr[3][1]~feeder_combout\);

-- Location: FF_X16_Y23_N7
\db|regs|dbr[3][1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	d => \db|regs|dbr[3][1]~feeder_combout\,
	ena => \db|regs|Decoder0~5_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \db|regs|dbr[3][1]~q\);

-- Location: FF_X18_Y22_N13
\db|regs|dbr[1][1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	asdata => \ssl|recv|data_temp\(1),
	sload => VCC,
	ena => \db|regs|Decoder0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \db|regs|dbr[1][1]~q\);

-- Location: LCCOMB_X21_Y23_N16
\db|regs|dbr[2][1]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|regs|dbr[2][1]~feeder_combout\ = \ssl|recv|data_temp\(1)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \ssl|recv|data_temp\(1),
	combout => \db|regs|dbr[2][1]~feeder_combout\);

-- Location: FF_X21_Y23_N17
\db|regs|dbr[2][1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	d => \db|regs|dbr[2][1]~feeder_combout\,
	ena => \db|regs|Decoder0~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \db|regs|dbr[2][1]~q\);

-- Location: LCCOMB_X18_Y23_N24
\db|regs|Mux6~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|regs|Mux6~2_combout\ = (\db|reg_addr|mem\(0) & (((\db|reg_addr|mem\(1))))) # (!\db|reg_addr|mem\(0) & ((\db|reg_addr|mem\(1) & ((\db|regs|dbr[2][1]~q\))) # (!\db|reg_addr|mem\(1) & (\db|regs|dbr[0][1]~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111010010100100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \db|reg_addr|mem\(0),
	datab => \db|regs|dbr[0][1]~q\,
	datac => \db|reg_addr|mem\(1),
	datad => \db|regs|dbr[2][1]~q\,
	combout => \db|regs|Mux6~2_combout\);

-- Location: LCCOMB_X18_Y22_N12
\db|regs|Mux6~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|regs|Mux6~3_combout\ = (\db|reg_addr|mem\(0) & ((\db|regs|Mux6~2_combout\ & (\db|regs|dbr[3][1]~q\)) # (!\db|regs|Mux6~2_combout\ & ((\db|regs|dbr[1][1]~q\))))) # (!\db|reg_addr|mem\(0) & (((\db|regs|Mux6~2_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101110110100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \db|reg_addr|mem\(0),
	datab => \db|regs|dbr[3][1]~q\,
	datac => \db|regs|dbr[1][1]~q\,
	datad => \db|regs|Mux6~2_combout\,
	combout => \db|regs|Mux6~3_combout\);

-- Location: LCCOMB_X19_Y21_N4
\db|regs|dbr[6][1]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|regs|dbr[6][1]~feeder_combout\ = \db|regs|dbr[2][1]~q\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \db|regs|dbr[2][1]~q\,
	combout => \db|regs|dbr[6][1]~feeder_combout\);

-- Location: FF_X19_Y21_N5
\db|regs|dbr[6][1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	d => \db|regs|dbr[6][1]~feeder_combout\,
	ena => \db|control_logic|state.s9~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \db|regs|dbr[6][1]~q\);

-- Location: FF_X17_Y23_N1
\db|regs|dbr[7][1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	asdata => \db|regs|dbr[3][1]~q\,
	sload => VCC,
	ena => \db|control_logic|state.s9~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \db|regs|dbr[7][1]~q\);

-- Location: LCCOMB_X17_Y22_N22
\db|regs|Mux6~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|regs|Mux6~0_combout\ = (\db|reg_addr|mem\(0) & ((\db|regs|dbr[5][1]~q\) # ((\db|reg_addr|mem\(1))))) # (!\db|reg_addr|mem\(0) & (((!\db|reg_addr|mem\(1) & \db|regs|dbr[4][1]~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010110110101000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \db|reg_addr|mem\(0),
	datab => \db|regs|dbr[5][1]~q\,
	datac => \db|reg_addr|mem\(1),
	datad => \db|regs|dbr[4][1]~q\,
	combout => \db|regs|Mux6~0_combout\);

-- Location: LCCOMB_X17_Y23_N0
\db|regs|Mux6~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|regs|Mux6~1_combout\ = (\db|reg_addr|mem\(1) & ((\db|regs|Mux6~0_combout\ & ((\db|regs|dbr[7][1]~q\))) # (!\db|regs|Mux6~0_combout\ & (\db|regs|dbr[6][1]~q\)))) # (!\db|reg_addr|mem\(1) & (((\db|regs|Mux6~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111010110001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \db|reg_addr|mem\(1),
	datab => \db|regs|dbr[6][1]~q\,
	datac => \db|regs|dbr[7][1]~q\,
	datad => \db|regs|Mux6~0_combout\,
	combout => \db|regs|Mux6~1_combout\);

-- Location: LCCOMB_X17_Y23_N18
\db|regs|data_out[1]~4\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|regs|data_out[1]~4_combout\ = (\db|regs|Decoder0~0_combout\ & ((\db|regs|Mux6~1_combout\) # ((\db|regs|Decoder0~1_combout\ & \db|regs|Mux6~3_combout\)))) # (!\db|regs|Decoder0~0_combout\ & (\db|regs|Decoder0~1_combout\ & (\db|regs|Mux6~3_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110101011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \db|regs|Decoder0~0_combout\,
	datab => \db|regs|Decoder0~1_combout\,
	datac => \db|regs|Mux6~3_combout\,
	datad => \db|regs|Mux6~1_combout\,
	combout => \db|regs|data_out[1]~4_combout\);

-- Location: LCCOMB_X18_Y20_N8
\db|regs|data_out[1]~6\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|regs|data_out[1]~6_combout\ = (\db|regs|data_out[1]~5_combout\) # ((\db|regs|data_out[1]~4_combout\) # ((\db|regs|dbr[0][1]~q\ & \db|regs|LessThan1~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111011111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \db|regs|dbr[0][1]~q\,
	datab => \db|regs|data_out[1]~5_combout\,
	datac => \db|regs|data_out[1]~4_combout\,
	datad => \db|regs|LessThan1~0_combout\,
	combout => \db|regs|data_out[1]~6_combout\);

-- Location: LCCOMB_X19_Y22_N28
\db|regs|dbr[5][0]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|regs|dbr[5][0]~feeder_combout\ = \ssl|recv|data_temp\(0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \ssl|recv|data_temp\(0),
	combout => \db|regs|dbr[5][0]~feeder_combout\);

-- Location: FF_X19_Y22_N29
\db|regs|dbr[5][0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	d => \db|regs|dbr[5][0]~feeder_combout\,
	ena => \db|regs|Decoder0~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \db|regs|dbr[5][0]~q\);

-- Location: FF_X19_Y20_N5
\db|regs|dbr[9][0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	asdata => \db|regs|dbr[5][0]~q\,
	sload => VCC,
	ena => \db|control_logic|state.s9~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \db|regs|dbr[9][0]~q\);

-- Location: LCCOMB_X19_Y20_N4
\db|regs|data_out[0]~8\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|regs|data_out[0]~8_combout\ = (!\db|reg_addr|mem\(1) & (\db|regs|dbr[9][0]~q\ & \db|reg_addr|mem\(0)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \db|reg_addr|mem\(1),
	datac => \db|regs|dbr[9][0]~q\,
	datad => \db|reg_addr|mem\(0),
	combout => \db|regs|data_out[0]~8_combout\);

-- Location: LCCOMB_X21_Y23_N26
\db|regs|dbr[4][0]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|regs|dbr[4][0]~feeder_combout\ = \ssl|recv|data_temp\(0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \ssl|recv|data_temp\(0),
	combout => \db|regs|dbr[4][0]~feeder_combout\);

-- Location: FF_X21_Y23_N27
\db|regs|dbr[4][0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	d => \db|regs|dbr[4][0]~feeder_combout\,
	ena => \db|regs|Decoder0~7_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \db|regs|dbr[4][0]~q\);

-- Location: FF_X19_Y21_N7
\db|regs|dbr[8][0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	asdata => \db|regs|dbr[4][0]~q\,
	sload => VCC,
	ena => \db|control_logic|state.s9~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \db|regs|dbr[8][0]~q\);

-- Location: LCCOMB_X21_Y21_N4
\db|regs|dbr[10][0]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|regs|dbr[10][0]~feeder_combout\ = \db|control_logic|state.s8~q\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \db|control_logic|state.s8~q\,
	combout => \db|regs|dbr[10][0]~feeder_combout\);

-- Location: FF_X21_Y21_N5
\db|regs|dbr[10][0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	d => \db|regs|dbr[10][0]~feeder_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \db|regs|dbr[10][0]~q\);

-- Location: LCCOMB_X19_Y21_N6
\db|regs|data_out[0]~7\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|regs|data_out[0]~7_combout\ = (!\db|reg_addr|mem\(0) & ((\db|reg_addr|mem\(1) & ((\db|regs|dbr[10][0]~q\))) # (!\db|reg_addr|mem\(1) & (\db|regs|dbr[8][0]~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001000010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \db|reg_addr|mem\(1),
	datab => \db|reg_addr|mem\(0),
	datac => \db|regs|dbr[8][0]~q\,
	datad => \db|regs|dbr[10][0]~q\,
	combout => \db|regs|data_out[0]~7_combout\);

-- Location: LCCOMB_X18_Y23_N12
\db|regs|data_out[0]~9\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|regs|data_out[0]~9_combout\ = (\db|reg_addr|mem\(3) & (!\db|reg_addr|mem\(2) & ((\db|regs|data_out[0]~8_combout\) # (\db|regs|data_out[0]~7_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \db|regs|data_out[0]~8_combout\,
	datab => \db|regs|data_out[0]~7_combout\,
	datac => \db|reg_addr|mem\(3),
	datad => \db|reg_addr|mem\(2),
	combout => \db|regs|data_out[0]~9_combout\);

-- Location: LCCOMB_X19_Y20_N28
\db|regs|dbr[2][0]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|regs|dbr[2][0]~feeder_combout\ = \ssl|recv|data_temp\(0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \ssl|recv|data_temp\(0),
	combout => \db|regs|dbr[2][0]~feeder_combout\);

-- Location: FF_X19_Y20_N29
\db|regs|dbr[2][0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	d => \db|regs|dbr[2][0]~feeder_combout\,
	ena => \db|regs|Decoder0~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \db|regs|dbr[2][0]~q\);

-- Location: LCCOMB_X19_Y20_N22
\db|regs|dbr[6][0]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|regs|dbr[6][0]~feeder_combout\ = \db|regs|dbr[2][0]~q\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \db|regs|dbr[2][0]~q\,
	combout => \db|regs|dbr[6][0]~feeder_combout\);

-- Location: FF_X19_Y20_N23
\db|regs|dbr[6][0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	d => \db|regs|dbr[6][0]~feeder_combout\,
	ena => \db|control_logic|state.s9~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \db|regs|dbr[6][0]~q\);

-- Location: LCCOMB_X21_Y22_N8
\db|regs|dbr[3][0]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|regs|dbr[3][0]~feeder_combout\ = \ssl|recv|data_temp\(0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \ssl|recv|data_temp\(0),
	combout => \db|regs|dbr[3][0]~feeder_combout\);

-- Location: FF_X21_Y22_N9
\db|regs|dbr[3][0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	d => \db|regs|dbr[3][0]~feeder_combout\,
	ena => \db|regs|Decoder0~5_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \db|regs|dbr[3][0]~q\);

-- Location: FF_X19_Y20_N3
\db|regs|dbr[7][0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	asdata => \db|regs|dbr[3][0]~q\,
	sload => VCC,
	ena => \db|control_logic|state.s9~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \db|regs|dbr[7][0]~q\);

-- Location: LCCOMB_X19_Y20_N0
\db|regs|Mux7~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|regs|Mux7~0_combout\ = (\db|reg_addr|mem\(1) & (((\db|reg_addr|mem\(0))))) # (!\db|reg_addr|mem\(1) & ((\db|reg_addr|mem\(0) & ((\db|regs|dbr[5][0]~q\))) # (!\db|reg_addr|mem\(0) & (\db|regs|dbr[4][0]~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101001000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \db|reg_addr|mem\(1),
	datab => \db|regs|dbr[4][0]~q\,
	datac => \db|regs|dbr[5][0]~q\,
	datad => \db|reg_addr|mem\(0),
	combout => \db|regs|Mux7~0_combout\);

-- Location: LCCOMB_X19_Y20_N2
\db|regs|Mux7~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|regs|Mux7~1_combout\ = (\db|reg_addr|mem\(1) & ((\db|regs|Mux7~0_combout\ & ((\db|regs|dbr[7][0]~q\))) # (!\db|regs|Mux7~0_combout\ & (\db|regs|dbr[6][0]~q\)))) # (!\db|reg_addr|mem\(1) & (((\db|regs|Mux7~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111010110001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \db|reg_addr|mem\(1),
	datab => \db|regs|dbr[6][0]~q\,
	datac => \db|regs|dbr[7][0]~q\,
	datad => \db|regs|Mux7~0_combout\,
	combout => \db|regs|Mux7~1_combout\);

-- Location: LCCOMB_X19_Y20_N10
\db|regs|Mux7~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|regs|Mux7~2_combout\ = (\db|reg_addr|mem\(0) & (((\db|reg_addr|mem\(1))))) # (!\db|reg_addr|mem\(0) & ((\db|reg_addr|mem\(1) & ((\db|regs|dbr[2][0]~q\))) # (!\db|reg_addr|mem\(1) & (\db|regs|dbr[0][0]~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111010010100100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \db|reg_addr|mem\(0),
	datab => \db|regs|dbr[0][0]~q\,
	datac => \db|reg_addr|mem\(1),
	datad => \db|regs|dbr[2][0]~q\,
	combout => \db|regs|Mux7~2_combout\);

-- Location: FF_X18_Y22_N3
\db|regs|dbr[1][0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	asdata => \ssl|recv|data_temp\(0),
	sload => VCC,
	ena => \db|regs|Decoder0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \db|regs|dbr[1][0]~q\);

-- Location: LCCOMB_X18_Y22_N2
\db|regs|Mux7~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|regs|Mux7~3_combout\ = (\db|reg_addr|mem\(0) & ((\db|regs|Mux7~2_combout\ & ((\db|regs|dbr[3][0]~q\))) # (!\db|regs|Mux7~2_combout\ & (\db|regs|dbr[1][0]~q\)))) # (!\db|reg_addr|mem\(0) & (\db|regs|Mux7~2_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110110001100100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \db|reg_addr|mem\(0),
	datab => \db|regs|Mux7~2_combout\,
	datac => \db|regs|dbr[1][0]~q\,
	datad => \db|regs|dbr[3][0]~q\,
	combout => \db|regs|Mux7~3_combout\);

-- Location: LCCOMB_X18_Y23_N28
\db|regs|data_out[0]~10\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|regs|data_out[0]~10_combout\ = (!\db|reg_addr|mem\(3) & ((\db|reg_addr|mem\(2) & (\db|regs|Mux7~1_combout\)) # (!\db|reg_addr|mem\(2) & ((\db|regs|Mux7~3_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000101000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \db|regs|Mux7~1_combout\,
	datab => \db|regs|Mux7~3_combout\,
	datac => \db|reg_addr|mem\(3),
	datad => \db|reg_addr|mem\(2),
	combout => \db|regs|data_out[0]~10_combout\);

-- Location: LCCOMB_X18_Y23_N0
\db|regs|data_out[0]~11\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|regs|data_out[0]~11_combout\ = (\db|regs|data_out[0]~9_combout\) # ((\db|regs|data_out[0]~10_combout\) # ((\db|regs|LessThan1~0_combout\ & \db|regs|dbr[0][0]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \db|regs|data_out[0]~9_combout\,
	datab => \db|regs|LessThan1~0_combout\,
	datac => \db|regs|dbr[0][0]~q\,
	datad => \db|regs|data_out[0]~10_combout\,
	combout => \db|regs|data_out[0]~11_combout\);

-- Location: LCCOMB_X18_Y20_N4
\ssl|trns|Mux0~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \ssl|trns|Mux0~0_combout\ = (\ssl|trns|index\(1) & ((\ssl|trns|index\(0) & ((\db|regs|data_out[0]~11_combout\))) # (!\ssl|trns|index\(0) & (\db|regs|data_out[1]~6_combout\)))) # (!\ssl|trns|index\(1) & (((!\ssl|trns|index\(0)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100101100001011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \db|regs|data_out[1]~6_combout\,
	datab => \ssl|trns|index\(1),
	datac => \ssl|trns|index\(0),
	datad => \db|regs|data_out[0]~11_combout\,
	combout => \ssl|trns|Mux0~0_combout\);

-- Location: LCCOMB_X19_Y22_N30
\db|regs|dbr[5][3]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|regs|dbr[5][3]~feeder_combout\ = \ssl|recv|data_temp\(3)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \ssl|recv|data_temp\(3),
	combout => \db|regs|dbr[5][3]~feeder_combout\);

-- Location: FF_X19_Y22_N31
\db|regs|dbr[5][3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	d => \db|regs|dbr[5][3]~feeder_combout\,
	ena => \db|regs|Decoder0~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \db|regs|dbr[5][3]~q\);

-- Location: LCCOMB_X21_Y23_N24
\db|regs|dbr[4][3]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|regs|dbr[4][3]~feeder_combout\ = \ssl|recv|data_temp\(3)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \ssl|recv|data_temp\(3),
	combout => \db|regs|dbr[4][3]~feeder_combout\);

-- Location: FF_X21_Y23_N25
\db|regs|dbr[4][3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	d => \db|regs|dbr[4][3]~feeder_combout\,
	ena => \db|regs|Decoder0~7_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \db|regs|dbr[4][3]~q\);

-- Location: LCCOMB_X18_Y21_N12
\db|regs|Mux4~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|regs|Mux4~0_combout\ = (\db|reg_addr|mem\(0) & ((\db|reg_addr|mem\(1)) # ((\db|regs|dbr[5][3]~q\)))) # (!\db|reg_addr|mem\(0) & (!\db|reg_addr|mem\(1) & ((\db|regs|dbr[4][3]~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011100110101000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \db|reg_addr|mem\(0),
	datab => \db|reg_addr|mem\(1),
	datac => \db|regs|dbr[5][3]~q\,
	datad => \db|regs|dbr[4][3]~q\,
	combout => \db|regs|Mux4~0_combout\);

-- Location: LCCOMB_X16_Y23_N24
\db|regs|dbr[3][3]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|regs|dbr[3][3]~feeder_combout\ = \ssl|recv|data_temp\(3)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \ssl|recv|data_temp\(3),
	combout => \db|regs|dbr[3][3]~feeder_combout\);

-- Location: FF_X16_Y23_N25
\db|regs|dbr[3][3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	d => \db|regs|dbr[3][3]~feeder_combout\,
	ena => \db|regs|Decoder0~5_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \db|regs|dbr[3][3]~q\);

-- Location: FF_X17_Y21_N25
\db|regs|dbr[7][3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	asdata => \db|regs|dbr[3][3]~q\,
	sload => VCC,
	ena => \db|control_logic|state.s9~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \db|regs|dbr[7][3]~q\);

-- Location: LCCOMB_X18_Y22_N10
\db|regs|dbr[2][3]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|regs|dbr[2][3]~feeder_combout\ = \ssl|recv|data_temp\(3)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \ssl|recv|data_temp\(3),
	combout => \db|regs|dbr[2][3]~feeder_combout\);

-- Location: FF_X18_Y22_N11
\db|regs|dbr[2][3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	d => \db|regs|dbr[2][3]~feeder_combout\,
	ena => \db|regs|Decoder0~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \db|regs|dbr[2][3]~q\);

-- Location: LCCOMB_X17_Y21_N12
\db|regs|dbr[6][3]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|regs|dbr[6][3]~feeder_combout\ = \db|regs|dbr[2][3]~q\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \db|regs|dbr[2][3]~q\,
	combout => \db|regs|dbr[6][3]~feeder_combout\);

-- Location: FF_X17_Y21_N13
\db|regs|dbr[6][3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	d => \db|regs|dbr[6][3]~feeder_combout\,
	ena => \db|control_logic|state.s9~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \db|regs|dbr[6][3]~q\);

-- Location: LCCOMB_X17_Y21_N24
\db|regs|Mux4~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|regs|Mux4~1_combout\ = (\db|reg_addr|mem\(1) & ((\db|regs|Mux4~0_combout\ & (\db|regs|dbr[7][3]~q\)) # (!\db|regs|Mux4~0_combout\ & ((\db|regs|dbr[6][3]~q\))))) # (!\db|reg_addr|mem\(1) & (\db|regs|Mux4~0_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110011011000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \db|reg_addr|mem\(1),
	datab => \db|regs|Mux4~0_combout\,
	datac => \db|regs|dbr[7][3]~q\,
	datad => \db|regs|dbr[6][3]~q\,
	combout => \db|regs|Mux4~1_combout\);

-- Location: LCCOMB_X17_Y22_N20
\db|regs|Mux4~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|regs|Mux4~2_combout\ = (\db|reg_addr|mem\(1) & ((\db|reg_addr|mem\(0)) # ((\db|regs|dbr[2][3]~q\)))) # (!\db|reg_addr|mem\(1) & (!\db|reg_addr|mem\(0) & (\db|regs|dbr[0][3]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101010011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \db|reg_addr|mem\(1),
	datab => \db|reg_addr|mem\(0),
	datac => \db|regs|dbr[0][3]~q\,
	datad => \db|regs|dbr[2][3]~q\,
	combout => \db|regs|Mux4~2_combout\);

-- Location: FF_X18_Y22_N29
\db|regs|dbr[1][3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	asdata => \ssl|recv|data_temp\(3),
	sload => VCC,
	ena => \db|regs|Decoder0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \db|regs|dbr[1][3]~q\);

-- Location: LCCOMB_X18_Y22_N28
\db|regs|Mux4~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|regs|Mux4~3_combout\ = (\db|reg_addr|mem\(0) & ((\db|regs|Mux4~2_combout\ & ((\db|regs|dbr[3][3]~q\))) # (!\db|regs|Mux4~2_combout\ & (\db|regs|dbr[1][3]~q\)))) # (!\db|reg_addr|mem\(0) & (\db|regs|Mux4~2_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110110001100100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \db|reg_addr|mem\(0),
	datab => \db|regs|Mux4~2_combout\,
	datac => \db|regs|dbr[1][3]~q\,
	datad => \db|regs|dbr[3][3]~q\,
	combout => \db|regs|Mux4~3_combout\);

-- Location: LCCOMB_X18_Y22_N8
\db|regs|data_out[3]~12\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|regs|data_out[3]~12_combout\ = (\db|regs|Mux4~1_combout\ & ((\db|regs|Decoder0~0_combout\) # ((\db|regs|Decoder0~1_combout\ & \db|regs|Mux4~3_combout\)))) # (!\db|regs|Mux4~1_combout\ & (((\db|regs|Decoder0~1_combout\ & \db|regs|Mux4~3_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \db|regs|Mux4~1_combout\,
	datab => \db|regs|Decoder0~0_combout\,
	datac => \db|regs|Decoder0~1_combout\,
	datad => \db|regs|Mux4~3_combout\,
	combout => \db|regs|data_out[3]~12_combout\);

-- Location: FF_X19_Y21_N23
\db|regs|dbr[8][3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	asdata => \db|regs|dbr[4][3]~q\,
	sload => VCC,
	ena => \db|control_logic|state.s9~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \db|regs|dbr[8][3]~q\);

-- Location: LCCOMB_X19_Y21_N24
\db|regs|dbr[9][3]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|regs|dbr[9][3]~feeder_combout\ = \db|regs|dbr[5][3]~q\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \db|regs|dbr[5][3]~q\,
	combout => \db|regs|dbr[9][3]~feeder_combout\);

-- Location: FF_X19_Y21_N25
\db|regs|dbr[9][3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	d => \db|regs|dbr[9][3]~feeder_combout\,
	ena => \db|control_logic|state.s9~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \db|regs|dbr[9][3]~q\);

-- Location: LCCOMB_X19_Y21_N22
\db|regs|data_out[3]~13\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|regs|data_out[3]~13_combout\ = (\db|regs|data_out[2]~1_combout\ & ((\db|reg_addr|mem\(0) & ((\db|regs|dbr[9][3]~q\))) # (!\db|reg_addr|mem\(0) & (\db|regs|dbr[8][3]~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010100000100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \db|regs|data_out[2]~1_combout\,
	datab => \db|reg_addr|mem\(0),
	datac => \db|regs|dbr[8][3]~q\,
	datad => \db|regs|dbr[9][3]~q\,
	combout => \db|regs|data_out[3]~13_combout\);

-- Location: LCCOMB_X18_Y20_N10
\db|regs|data_out[3]~14\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|regs|data_out[3]~14_combout\ = (\db|regs|data_out[3]~12_combout\) # ((\db|regs|data_out[3]~13_combout\) # ((\db|regs|LessThan1~0_combout\ & \db|regs|dbr[0][3]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111101100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \db|regs|LessThan1~0_combout\,
	datab => \db|regs|data_out[3]~12_combout\,
	datac => \db|regs|dbr[0][3]~q\,
	datad => \db|regs|data_out[3]~13_combout\,
	combout => \db|regs|data_out[3]~14_combout\);

-- Location: LCCOMB_X18_Y20_N28
\ssl|trns|Mux0~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \ssl|trns|Mux0~1_combout\ = (\ssl|trns|Mux0~0_combout\ & (((\db|regs|data_out[3]~14_combout\) # (\ssl|trns|index\(1))))) # (!\ssl|trns|Mux0~0_combout\ & (\db|regs|data_out[2]~3_combout\ & ((!\ssl|trns|index\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \db|regs|data_out[2]~3_combout\,
	datab => \ssl|trns|Mux0~0_combout\,
	datac => \db|regs|data_out[3]~14_combout\,
	datad => \ssl|trns|index\(1),
	combout => \ssl|trns|Mux0~1_combout\);

-- Location: LCCOMB_X18_Y21_N26
\db|regs|dbr[4][6]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|regs|dbr[4][6]~feeder_combout\ = \ssl|recv|data_temp\(6)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \ssl|recv|data_temp\(6),
	combout => \db|regs|dbr[4][6]~feeder_combout\);

-- Location: FF_X18_Y21_N27
\db|regs|dbr[4][6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	d => \db|regs|dbr[4][6]~feeder_combout\,
	ena => \db|regs|Decoder0~7_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \db|regs|dbr[4][6]~q\);

-- Location: FF_X19_Y21_N13
\db|regs|dbr[8][6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	asdata => \db|regs|dbr[4][6]~q\,
	sload => VCC,
	ena => \db|control_logic|state.s9~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \db|regs|dbr[8][6]~q\);

-- Location: LCCOMB_X18_Y21_N16
\db|regs|dbr[5][6]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|regs|dbr[5][6]~feeder_combout\ = \ssl|recv|data_temp\(6)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \ssl|recv|data_temp\(6),
	combout => \db|regs|dbr[5][6]~feeder_combout\);

-- Location: FF_X18_Y21_N17
\db|regs|dbr[5][6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	d => \db|regs|dbr[5][6]~feeder_combout\,
	ena => \db|regs|Decoder0~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \db|regs|dbr[5][6]~q\);

-- Location: LCCOMB_X19_Y21_N2
\db|regs|dbr[9][6]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|regs|dbr[9][6]~feeder_combout\ = \db|regs|dbr[5][6]~q\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \db|regs|dbr[5][6]~q\,
	combout => \db|regs|dbr[9][6]~feeder_combout\);

-- Location: FF_X19_Y21_N3
\db|regs|dbr[9][6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	d => \db|regs|dbr[9][6]~feeder_combout\,
	ena => \db|control_logic|state.s9~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \db|regs|dbr[9][6]~q\);

-- Location: LCCOMB_X19_Y21_N12
\db|regs|data_out[6]~19\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|regs|data_out[6]~19_combout\ = (\db|regs|data_out[2]~1_combout\ & ((\db|reg_addr|mem\(0) & ((\db|regs|dbr[9][6]~q\))) # (!\db|reg_addr|mem\(0) & (\db|regs|dbr[8][6]~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010100000100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \db|regs|data_out[2]~1_combout\,
	datab => \db|reg_addr|mem\(0),
	datac => \db|regs|dbr[8][6]~q\,
	datad => \db|regs|dbr[9][6]~q\,
	combout => \db|regs|data_out[6]~19_combout\);

-- Location: LCCOMB_X19_Y21_N8
\db|regs|Mux1~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|regs|Mux1~0_combout\ = (\db|reg_addr|mem\(0) & (((\db|regs|dbr[5][6]~q\) # (\db|reg_addr|mem\(1))))) # (!\db|reg_addr|mem\(0) & (\db|regs|dbr[4][6]~q\ & ((!\db|reg_addr|mem\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \db|regs|dbr[4][6]~q\,
	datab => \db|reg_addr|mem\(0),
	datac => \db|regs|dbr[5][6]~q\,
	datad => \db|reg_addr|mem\(1),
	combout => \db|regs|Mux1~0_combout\);

-- Location: LCCOMB_X17_Y21_N26
\db|regs|dbr[3][6]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|regs|dbr[3][6]~feeder_combout\ = \ssl|recv|data_temp\(6)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \ssl|recv|data_temp\(6),
	combout => \db|regs|dbr[3][6]~feeder_combout\);

-- Location: FF_X17_Y21_N27
\db|regs|dbr[3][6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	d => \db|regs|dbr[3][6]~feeder_combout\,
	ena => \db|regs|Decoder0~5_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \db|regs|dbr[3][6]~q\);

-- Location: FF_X17_Y21_N5
\db|regs|dbr[7][6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	asdata => \db|regs|dbr[3][6]~q\,
	sload => VCC,
	ena => \db|control_logic|state.s9~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \db|regs|dbr[7][6]~q\);

-- Location: LCCOMB_X17_Y23_N12
\db|regs|dbr[2][6]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|regs|dbr[2][6]~feeder_combout\ = \ssl|recv|data_temp\(6)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \ssl|recv|data_temp\(6),
	combout => \db|regs|dbr[2][6]~feeder_combout\);

-- Location: FF_X17_Y23_N13
\db|regs|dbr[2][6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	d => \db|regs|dbr[2][6]~feeder_combout\,
	ena => \db|regs|Decoder0~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \db|regs|dbr[2][6]~q\);

-- Location: LCCOMB_X17_Y21_N20
\db|regs|dbr[6][6]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|regs|dbr[6][6]~feeder_combout\ = \db|regs|dbr[2][6]~q\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \db|regs|dbr[2][6]~q\,
	combout => \db|regs|dbr[6][6]~feeder_combout\);

-- Location: FF_X17_Y21_N21
\db|regs|dbr[6][6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	d => \db|regs|dbr[6][6]~feeder_combout\,
	ena => \db|control_logic|state.s9~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \db|regs|dbr[6][6]~q\);

-- Location: LCCOMB_X17_Y21_N4
\db|regs|Mux1~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|regs|Mux1~1_combout\ = (\db|reg_addr|mem\(1) & ((\db|regs|Mux1~0_combout\ & (\db|regs|dbr[7][6]~q\)) # (!\db|regs|Mux1~0_combout\ & ((\db|regs|dbr[6][6]~q\))))) # (!\db|reg_addr|mem\(1) & (\db|regs|Mux1~0_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110011011000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \db|reg_addr|mem\(1),
	datab => \db|regs|Mux1~0_combout\,
	datac => \db|regs|dbr[7][6]~q\,
	datad => \db|regs|dbr[6][6]~q\,
	combout => \db|regs|Mux1~1_combout\);

-- Location: LCCOMB_X17_Y23_N14
\db|regs|Mux1~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|regs|Mux1~2_combout\ = (\db|reg_addr|mem\(1) & ((\db|reg_addr|mem\(0)) # ((\db|regs|dbr[2][6]~q\)))) # (!\db|reg_addr|mem\(1) & (!\db|reg_addr|mem\(0) & (\db|regs|dbr[0][6]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101010011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \db|reg_addr|mem\(1),
	datab => \db|reg_addr|mem\(0),
	datac => \db|regs|dbr[0][6]~q\,
	datad => \db|regs|dbr[2][6]~q\,
	combout => \db|regs|Mux1~2_combout\);

-- Location: FF_X18_Y22_N5
\db|regs|dbr[1][6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	asdata => \ssl|recv|data_temp\(6),
	sload => VCC,
	ena => \db|regs|Decoder0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \db|regs|dbr[1][6]~q\);

-- Location: LCCOMB_X18_Y22_N4
\db|regs|Mux1~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|regs|Mux1~3_combout\ = (\db|reg_addr|mem\(0) & ((\db|regs|Mux1~2_combout\ & ((\db|regs|dbr[3][6]~q\))) # (!\db|regs|Mux1~2_combout\ & (\db|regs|dbr[1][6]~q\)))) # (!\db|reg_addr|mem\(0) & (\db|regs|Mux1~2_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110110001100100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \db|reg_addr|mem\(0),
	datab => \db|regs|Mux1~2_combout\,
	datac => \db|regs|dbr[1][6]~q\,
	datad => \db|regs|dbr[3][6]~q\,
	combout => \db|regs|Mux1~3_combout\);

-- Location: LCCOMB_X17_Y23_N8
\db|regs|data_out[6]~18\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|regs|data_out[6]~18_combout\ = (\db|regs|Decoder0~0_combout\ & ((\db|regs|Mux1~1_combout\) # ((\db|regs|Decoder0~1_combout\ & \db|regs|Mux1~3_combout\)))) # (!\db|regs|Decoder0~0_combout\ & (\db|regs|Decoder0~1_combout\ & 
-- ((\db|regs|Mux1~3_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110110010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \db|regs|Decoder0~0_combout\,
	datab => \db|regs|Decoder0~1_combout\,
	datac => \db|regs|Mux1~1_combout\,
	datad => \db|regs|Mux1~3_combout\,
	combout => \db|regs|data_out[6]~18_combout\);

-- Location: LCCOMB_X17_Y23_N2
\db|regs|data_out[6]~20\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|regs|data_out[6]~20_combout\ = (\db|regs|data_out[6]~19_combout\) # ((\db|regs|data_out[6]~18_combout\) # ((\db|regs|LessThan1~0_combout\ & \db|regs|dbr[0][6]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111011111010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \db|regs|data_out[6]~19_combout\,
	datab => \db|regs|LessThan1~0_combout\,
	datac => \db|regs|data_out[6]~18_combout\,
	datad => \db|regs|dbr[0][6]~q\,
	combout => \db|regs|data_out[6]~20_combout\);

-- Location: LCCOMB_X17_Y23_N20
\db|regs|dbr[2][4]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|regs|dbr[2][4]~feeder_combout\ = \ssl|recv|data_temp\(4)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \ssl|recv|data_temp\(4),
	combout => \db|regs|dbr[2][4]~feeder_combout\);

-- Location: FF_X17_Y23_N21
\db|regs|dbr[2][4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	d => \db|regs|dbr[2][4]~feeder_combout\,
	ena => \db|regs|Decoder0~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \db|regs|dbr[2][4]~q\);

-- Location: LCCOMB_X17_Y21_N28
\db|regs|dbr[6][4]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|regs|dbr[6][4]~feeder_combout\ = \db|regs|dbr[2][4]~q\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \db|regs|dbr[2][4]~q\,
	combout => \db|regs|dbr[6][4]~feeder_combout\);

-- Location: FF_X17_Y21_N29
\db|regs|dbr[6][4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	d => \db|regs|dbr[6][4]~feeder_combout\,
	ena => \db|control_logic|state.s9~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \db|regs|dbr[6][4]~q\);

-- Location: LCCOMB_X19_Y21_N14
\db|regs|dbr[3][4]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|regs|dbr[3][4]~feeder_combout\ = \ssl|recv|data_temp\(4)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \ssl|recv|data_temp\(4),
	combout => \db|regs|dbr[3][4]~feeder_combout\);

-- Location: FF_X19_Y21_N15
\db|regs|dbr[3][4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	d => \db|regs|dbr[3][4]~feeder_combout\,
	ena => \db|regs|Decoder0~5_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \db|regs|dbr[3][4]~q\);

-- Location: FF_X17_Y21_N1
\db|regs|dbr[7][4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	asdata => \db|regs|dbr[3][4]~q\,
	sload => VCC,
	ena => \db|control_logic|state.s9~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \db|regs|dbr[7][4]~q\);

-- Location: LCCOMB_X18_Y21_N8
\db|regs|dbr[5][4]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|regs|dbr[5][4]~feeder_combout\ = \ssl|recv|data_temp\(4)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \ssl|recv|data_temp\(4),
	combout => \db|regs|dbr[5][4]~feeder_combout\);

-- Location: FF_X18_Y21_N9
\db|regs|dbr[5][4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	d => \db|regs|dbr[5][4]~feeder_combout\,
	ena => \db|regs|Decoder0~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \db|regs|dbr[5][4]~q\);

-- Location: LCCOMB_X18_Y21_N10
\db|regs|dbr[4][4]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|regs|dbr[4][4]~feeder_combout\ = \ssl|recv|data_temp\(4)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \ssl|recv|data_temp\(4),
	combout => \db|regs|dbr[4][4]~feeder_combout\);

-- Location: FF_X18_Y21_N11
\db|regs|dbr[4][4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	d => \db|regs|dbr[4][4]~feeder_combout\,
	ena => \db|regs|Decoder0~7_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \db|regs|dbr[4][4]~q\);

-- Location: LCCOMB_X18_Y21_N28
\db|regs|Mux3~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|regs|Mux3~0_combout\ = (\db|reg_addr|mem\(0) & ((\db|regs|dbr[5][4]~q\) # ((\db|reg_addr|mem\(1))))) # (!\db|reg_addr|mem\(0) & (((!\db|reg_addr|mem\(1) & \db|regs|dbr[4][4]~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010110110101000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \db|reg_addr|mem\(0),
	datab => \db|regs|dbr[5][4]~q\,
	datac => \db|reg_addr|mem\(1),
	datad => \db|regs|dbr[4][4]~q\,
	combout => \db|regs|Mux3~0_combout\);

-- Location: LCCOMB_X17_Y21_N0
\db|regs|Mux3~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|regs|Mux3~1_combout\ = (\db|reg_addr|mem\(1) & ((\db|regs|Mux3~0_combout\ & ((\db|regs|dbr[7][4]~q\))) # (!\db|regs|Mux3~0_combout\ & (\db|regs|dbr[6][4]~q\)))) # (!\db|reg_addr|mem\(1) & (((\db|regs|Mux3~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111010110001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \db|reg_addr|mem\(1),
	datab => \db|regs|dbr[6][4]~q\,
	datac => \db|regs|dbr[7][4]~q\,
	datad => \db|regs|Mux3~0_combout\,
	combout => \db|regs|Mux3~1_combout\);

-- Location: LCCOMB_X17_Y23_N22
\db|regs|Mux3~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|regs|Mux3~2_combout\ = (\db|reg_addr|mem\(1) & ((\db|regs|dbr[2][4]~q\) # ((\db|reg_addr|mem\(0))))) # (!\db|reg_addr|mem\(1) & (((\db|regs|dbr[0][4]~q\ & !\db|reg_addr|mem\(0)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000010101100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \db|regs|dbr[2][4]~q\,
	datab => \db|regs|dbr[0][4]~q\,
	datac => \db|reg_addr|mem\(1),
	datad => \db|reg_addr|mem\(0),
	combout => \db|regs|Mux3~2_combout\);

-- Location: FF_X18_Y22_N27
\db|regs|dbr[1][4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	asdata => \ssl|recv|data_temp\(4),
	sload => VCC,
	ena => \db|regs|Decoder0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \db|regs|dbr[1][4]~q\);

-- Location: LCCOMB_X18_Y22_N26
\db|regs|Mux3~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|regs|Mux3~3_combout\ = (\db|reg_addr|mem\(0) & ((\db|regs|Mux3~2_combout\ & ((\db|regs|dbr[3][4]~q\))) # (!\db|regs|Mux3~2_combout\ & (\db|regs|dbr[1][4]~q\)))) # (!\db|reg_addr|mem\(0) & (\db|regs|Mux3~2_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110110001100100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \db|reg_addr|mem\(0),
	datab => \db|regs|Mux3~2_combout\,
	datac => \db|regs|dbr[1][4]~q\,
	datad => \db|regs|dbr[3][4]~q\,
	combout => \db|regs|Mux3~3_combout\);

-- Location: LCCOMB_X17_Y22_N26
\db|regs|data_out[4]~21\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|regs|data_out[4]~21_combout\ = (\db|regs|Decoder0~1_combout\ & ((\db|regs|Mux3~3_combout\) # ((\db|regs|Mux3~1_combout\ & \db|regs|Decoder0~0_combout\)))) # (!\db|regs|Decoder0~1_combout\ & (\db|regs|Mux3~1_combout\ & (\db|regs|Decoder0~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110101011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \db|regs|Decoder0~1_combout\,
	datab => \db|regs|Mux3~1_combout\,
	datac => \db|regs|Decoder0~0_combout\,
	datad => \db|regs|Mux3~3_combout\,
	combout => \db|regs|data_out[4]~21_combout\);

-- Location: FF_X19_Y21_N19
\db|regs|dbr[8][4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	asdata => \db|regs|dbr[4][4]~q\,
	sload => VCC,
	ena => \db|control_logic|state.s9~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \db|regs|dbr[8][4]~q\);

-- Location: LCCOMB_X19_Y21_N16
\db|regs|dbr[9][4]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|regs|dbr[9][4]~feeder_combout\ = \db|regs|dbr[5][4]~q\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \db|regs|dbr[5][4]~q\,
	combout => \db|regs|dbr[9][4]~feeder_combout\);

-- Location: FF_X19_Y21_N17
\db|regs|dbr[9][4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	d => \db|regs|dbr[9][4]~feeder_combout\,
	ena => \db|control_logic|state.s9~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \db|regs|dbr[9][4]~q\);

-- Location: LCCOMB_X19_Y21_N18
\db|regs|data_out[4]~22\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|regs|data_out[4]~22_combout\ = (\db|regs|data_out[2]~1_combout\ & ((\db|reg_addr|mem\(0) & ((\db|regs|dbr[9][4]~q\))) # (!\db|reg_addr|mem\(0) & (\db|regs|dbr[8][4]~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010100000100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \db|regs|data_out[2]~1_combout\,
	datab => \db|reg_addr|mem\(0),
	datac => \db|regs|dbr[8][4]~q\,
	datad => \db|regs|dbr[9][4]~q\,
	combout => \db|regs|data_out[4]~22_combout\);

-- Location: LCCOMB_X17_Y23_N28
\db|regs|data_out[4]~23\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|regs|data_out[4]~23_combout\ = (\db|regs|data_out[4]~21_combout\) # ((\db|regs|data_out[4]~22_combout\) # ((\db|regs|LessThan1~0_combout\ & \db|regs|dbr[0][4]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \db|regs|data_out[4]~21_combout\,
	datab => \db|regs|LessThan1~0_combout\,
	datac => \db|regs|dbr[0][4]~q\,
	datad => \db|regs|data_out[4]~22_combout\,
	combout => \db|regs|data_out[4]~23_combout\);

-- Location: LCCOMB_X17_Y23_N30
\ssl|trns|Mux0~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \ssl|trns|Mux0~2_combout\ = (\ssl|trns|index\(1) & (((\ssl|trns|index\(0) & \db|regs|data_out[4]~23_combout\)))) # (!\ssl|trns|index\(1) & ((\db|regs|data_out[6]~20_combout\) # ((!\ssl|trns|index\(0)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110010101000101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \ssl|trns|index\(1),
	datab => \db|regs|data_out[6]~20_combout\,
	datac => \ssl|trns|index\(0),
	datad => \db|regs|data_out[4]~23_combout\,
	combout => \ssl|trns|Mux0~2_combout\);

-- Location: LCCOMB_X18_Y22_N24
\db|regs|dbr[2][5]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|regs|dbr[2][5]~feeder_combout\ = \ssl|recv|data_temp\(5)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \ssl|recv|data_temp\(5),
	combout => \db|regs|dbr[2][5]~feeder_combout\);

-- Location: FF_X18_Y22_N25
\db|regs|dbr[2][5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	d => \db|regs|dbr[2][5]~feeder_combout\,
	ena => \db|regs|Decoder0~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \db|regs|dbr[2][5]~q\);

-- Location: LCCOMB_X17_Y21_N8
\db|regs|dbr[6][5]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|regs|dbr[6][5]~feeder_combout\ = \db|regs|dbr[2][5]~q\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \db|regs|dbr[2][5]~q\,
	combout => \db|regs|dbr[6][5]~feeder_combout\);

-- Location: FF_X17_Y21_N9
\db|regs|dbr[6][5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	d => \db|regs|dbr[6][5]~feeder_combout\,
	ena => \db|control_logic|state.s9~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \db|regs|dbr[6][5]~q\);

-- Location: LCCOMB_X16_Y23_N10
\db|regs|dbr[3][5]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|regs|dbr[3][5]~feeder_combout\ = \ssl|recv|data_temp\(5)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \ssl|recv|data_temp\(5),
	combout => \db|regs|dbr[3][5]~feeder_combout\);

-- Location: FF_X16_Y23_N11
\db|regs|dbr[3][5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	d => \db|regs|dbr[3][5]~feeder_combout\,
	ena => \db|regs|Decoder0~5_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \db|regs|dbr[3][5]~q\);

-- Location: FF_X17_Y21_N17
\db|regs|dbr[7][5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	asdata => \db|regs|dbr[3][5]~q\,
	sload => VCC,
	ena => \db|control_logic|state.s9~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \db|regs|dbr[7][5]~q\);

-- Location: LCCOMB_X18_Y21_N4
\db|regs|dbr[4][5]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|regs|dbr[4][5]~feeder_combout\ = \ssl|recv|data_temp\(5)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \ssl|recv|data_temp\(5),
	combout => \db|regs|dbr[4][5]~feeder_combout\);

-- Location: FF_X18_Y21_N5
\db|regs|dbr[4][5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	d => \db|regs|dbr[4][5]~feeder_combout\,
	ena => \db|regs|Decoder0~7_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \db|regs|dbr[4][5]~q\);

-- Location: LCCOMB_X18_Y21_N14
\db|regs|dbr[5][5]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|regs|dbr[5][5]~feeder_combout\ = \ssl|recv|data_temp\(5)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \ssl|recv|data_temp\(5),
	combout => \db|regs|dbr[5][5]~feeder_combout\);

-- Location: FF_X18_Y21_N15
\db|regs|dbr[5][5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	d => \db|regs|dbr[5][5]~feeder_combout\,
	ena => \db|regs|Decoder0~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \db|regs|dbr[5][5]~q\);

-- Location: LCCOMB_X18_Y21_N22
\db|regs|Mux2~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|regs|Mux2~0_combout\ = (\db|reg_addr|mem\(1) & (((\db|reg_addr|mem\(0))))) # (!\db|reg_addr|mem\(1) & ((\db|reg_addr|mem\(0) & ((\db|regs|dbr[5][5]~q\))) # (!\db|reg_addr|mem\(0) & (\db|regs|dbr[4][5]~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111010010100100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \db|reg_addr|mem\(1),
	datab => \db|regs|dbr[4][5]~q\,
	datac => \db|reg_addr|mem\(0),
	datad => \db|regs|dbr[5][5]~q\,
	combout => \db|regs|Mux2~0_combout\);

-- Location: LCCOMB_X17_Y21_N16
\db|regs|Mux2~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|regs|Mux2~1_combout\ = (\db|reg_addr|mem\(1) & ((\db|regs|Mux2~0_combout\ & ((\db|regs|dbr[7][5]~q\))) # (!\db|regs|Mux2~0_combout\ & (\db|regs|dbr[6][5]~q\)))) # (!\db|reg_addr|mem\(1) & (((\db|regs|Mux2~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111010110001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \db|reg_addr|mem\(1),
	datab => \db|regs|dbr[6][5]~q\,
	datac => \db|regs|dbr[7][5]~q\,
	datad => \db|regs|Mux2~0_combout\,
	combout => \db|regs|Mux2~1_combout\);

-- Location: LCCOMB_X18_Y22_N18
\db|regs|Mux2~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|regs|Mux2~2_combout\ = (\db|reg_addr|mem\(0) & (((\db|reg_addr|mem\(1))))) # (!\db|reg_addr|mem\(0) & ((\db|reg_addr|mem\(1) & (\db|regs|dbr[2][5]~q\)) # (!\db|reg_addr|mem\(1) & ((\db|regs|dbr[0][5]~q\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \db|reg_addr|mem\(0),
	datab => \db|regs|dbr[2][5]~q\,
	datac => \db|regs|dbr[0][5]~q\,
	datad => \db|reg_addr|mem\(1),
	combout => \db|regs|Mux2~2_combout\);

-- Location: FF_X18_Y22_N31
\db|regs|dbr[1][5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	asdata => \ssl|recv|data_temp\(5),
	sload => VCC,
	ena => \db|regs|Decoder0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \db|regs|dbr[1][5]~q\);

-- Location: LCCOMB_X18_Y22_N30
\db|regs|Mux2~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|regs|Mux2~3_combout\ = (\db|reg_addr|mem\(0) & ((\db|regs|Mux2~2_combout\ & ((\db|regs|dbr[3][5]~q\))) # (!\db|regs|Mux2~2_combout\ & (\db|regs|dbr[1][5]~q\)))) # (!\db|reg_addr|mem\(0) & (\db|regs|Mux2~2_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110110001100100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \db|reg_addr|mem\(0),
	datab => \db|regs|Mux2~2_combout\,
	datac => \db|regs|dbr[1][5]~q\,
	datad => \db|regs|dbr[3][5]~q\,
	combout => \db|regs|Mux2~3_combout\);

-- Location: LCCOMB_X19_Y22_N16
\db|regs|data_out[5]~15\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|regs|data_out[5]~15_combout\ = (\db|regs|Decoder0~1_combout\ & ((\db|regs|Mux2~3_combout\) # ((\db|regs|Mux2~1_combout\ & \db|regs|Decoder0~0_combout\)))) # (!\db|regs|Decoder0~1_combout\ & (\db|regs|Mux2~1_combout\ & (\db|regs|Decoder0~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110101011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \db|regs|Decoder0~1_combout\,
	datab => \db|regs|Mux2~1_combout\,
	datac => \db|regs|Decoder0~0_combout\,
	datad => \db|regs|Mux2~3_combout\,
	combout => \db|regs|data_out[5]~15_combout\);

-- Location: FF_X19_Y21_N31
\db|regs|dbr[8][5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	asdata => \db|regs|dbr[4][5]~q\,
	sload => VCC,
	ena => \db|control_logic|state.s9~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \db|regs|dbr[8][5]~q\);

-- Location: LCCOMB_X19_Y21_N28
\db|regs|dbr[9][5]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|regs|dbr[9][5]~feeder_combout\ = \db|regs|dbr[5][5]~q\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \db|regs|dbr[5][5]~q\,
	combout => \db|regs|dbr[9][5]~feeder_combout\);

-- Location: FF_X19_Y21_N29
\db|regs|dbr[9][5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	d => \db|regs|dbr[9][5]~feeder_combout\,
	ena => \db|control_logic|state.s9~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \db|regs|dbr[9][5]~q\);

-- Location: LCCOMB_X19_Y21_N30
\db|regs|data_out[5]~16\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|regs|data_out[5]~16_combout\ = (\db|regs|data_out[2]~1_combout\ & ((\db|reg_addr|mem\(0) & ((\db|regs|dbr[9][5]~q\))) # (!\db|reg_addr|mem\(0) & (\db|regs|dbr[8][5]~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010100000100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \db|regs|data_out[2]~1_combout\,
	datab => \db|reg_addr|mem\(0),
	datac => \db|regs|dbr[8][5]~q\,
	datad => \db|regs|dbr[9][5]~q\,
	combout => \db|regs|data_out[5]~16_combout\);

-- Location: LCCOMB_X18_Y20_N30
\db|regs|data_out[5]~17\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|regs|data_out[5]~17_combout\ = (\db|regs|data_out[5]~15_combout\) # ((\db|regs|data_out[5]~16_combout\) # ((\db|regs|LessThan1~0_combout\ & \db|regs|dbr[0][5]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111101100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \db|regs|LessThan1~0_combout\,
	datab => \db|regs|data_out[5]~15_combout\,
	datac => \db|regs|dbr[0][5]~q\,
	datad => \db|regs|data_out[5]~16_combout\,
	combout => \db|regs|data_out[5]~17_combout\);

-- Location: LCCOMB_X17_Y23_N6
\db|regs|dbr[2][7]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|regs|dbr[2][7]~feeder_combout\ = \ssl|recv|data_temp\(7)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \ssl|recv|data_temp\(7),
	combout => \db|regs|dbr[2][7]~feeder_combout\);

-- Location: FF_X17_Y23_N7
\db|regs|dbr[2][7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	d => \db|regs|dbr[2][7]~feeder_combout\,
	ena => \db|regs|Decoder0~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \db|regs|dbr[2][7]~q\);

-- Location: LCCOMB_X17_Y21_N2
\db|regs|dbr[6][7]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|regs|dbr[6][7]~feeder_combout\ = \db|regs|dbr[2][7]~q\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \db|regs|dbr[2][7]~q\,
	combout => \db|regs|dbr[6][7]~feeder_combout\);

-- Location: FF_X17_Y21_N3
\db|regs|dbr[6][7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	d => \db|regs|dbr[6][7]~feeder_combout\,
	ena => \db|control_logic|state.s9~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \db|regs|dbr[6][7]~q\);

-- Location: LCCOMB_X16_Y23_N16
\db|regs|dbr[3][7]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|regs|dbr[3][7]~feeder_combout\ = \ssl|recv|data_temp\(7)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \ssl|recv|data_temp\(7),
	combout => \db|regs|dbr[3][7]~feeder_combout\);

-- Location: FF_X16_Y23_N17
\db|regs|dbr[3][7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	d => \db|regs|dbr[3][7]~feeder_combout\,
	ena => \db|regs|Decoder0~5_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \db|regs|dbr[3][7]~q\);

-- Location: FF_X17_Y23_N25
\db|regs|dbr[7][7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	asdata => \db|regs|dbr[3][7]~q\,
	sload => VCC,
	ena => \db|control_logic|state.s9~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \db|regs|dbr[7][7]~q\);

-- Location: LCCOMB_X14_Y23_N28
\db|regs|dbr[4][7]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|regs|dbr[4][7]~feeder_combout\ = \ssl|recv|data_temp\(7)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \ssl|recv|data_temp\(7),
	combout => \db|regs|dbr[4][7]~feeder_combout\);

-- Location: FF_X14_Y23_N29
\db|regs|dbr[4][7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	d => \db|regs|dbr[4][7]~feeder_combout\,
	ena => \db|regs|Decoder0~7_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \db|regs|dbr[4][7]~q\);

-- Location: LCCOMB_X16_Y21_N24
\db|regs|dbr[5][7]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|regs|dbr[5][7]~feeder_combout\ = \ssl|recv|data_temp\(7)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \ssl|recv|data_temp\(7),
	combout => \db|regs|dbr[5][7]~feeder_combout\);

-- Location: FF_X16_Y21_N25
\db|regs|dbr[5][7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	d => \db|regs|dbr[5][7]~feeder_combout\,
	ena => \db|regs|Decoder0~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \db|regs|dbr[5][7]~q\);

-- Location: LCCOMB_X17_Y22_N28
\db|regs|Mux0~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|regs|Mux0~0_combout\ = (\db|reg_addr|mem\(1) & (((\db|reg_addr|mem\(0))))) # (!\db|reg_addr|mem\(1) & ((\db|reg_addr|mem\(0) & ((\db|regs|dbr[5][7]~q\))) # (!\db|reg_addr|mem\(0) & (\db|regs|dbr[4][7]~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110000100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \db|regs|dbr[4][7]~q\,
	datab => \db|reg_addr|mem\(1),
	datac => \db|regs|dbr[5][7]~q\,
	datad => \db|reg_addr|mem\(0),
	combout => \db|regs|Mux0~0_combout\);

-- Location: LCCOMB_X17_Y23_N24
\db|regs|Mux0~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|regs|Mux0~1_combout\ = (\db|reg_addr|mem\(1) & ((\db|regs|Mux0~0_combout\ & ((\db|regs|dbr[7][7]~q\))) # (!\db|regs|Mux0~0_combout\ & (\db|regs|dbr[6][7]~q\)))) # (!\db|reg_addr|mem\(1) & (((\db|regs|Mux0~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111010110001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \db|reg_addr|mem\(1),
	datab => \db|regs|dbr[6][7]~q\,
	datac => \db|regs|dbr[7][7]~q\,
	datad => \db|regs|Mux0~0_combout\,
	combout => \db|regs|Mux0~1_combout\);

-- Location: LCCOMB_X17_Y21_N22
\db|regs|Mux0~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|regs|Mux0~2_combout\ = (\db|reg_addr|mem\(1) & ((\db|reg_addr|mem\(0)) # ((\db|regs|dbr[2][7]~q\)))) # (!\db|reg_addr|mem\(1) & (!\db|reg_addr|mem\(0) & (\db|regs|dbr[0][7]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101010011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \db|reg_addr|mem\(1),
	datab => \db|reg_addr|mem\(0),
	datac => \db|regs|dbr[0][7]~q\,
	datad => \db|regs|dbr[2][7]~q\,
	combout => \db|regs|Mux0~2_combout\);

-- Location: FF_X18_Y22_N1
\db|regs|dbr[1][7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	asdata => \ssl|recv|data_temp\(7),
	sload => VCC,
	ena => \db|regs|Decoder0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \db|regs|dbr[1][7]~q\);

-- Location: LCCOMB_X18_Y22_N0
\db|regs|Mux0~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|regs|Mux0~3_combout\ = (\db|reg_addr|mem\(0) & ((\db|regs|Mux0~2_combout\ & ((\db|regs|dbr[3][7]~q\))) # (!\db|regs|Mux0~2_combout\ & (\db|regs|dbr[1][7]~q\)))) # (!\db|reg_addr|mem\(0) & (\db|regs|Mux0~2_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110110001100100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \db|reg_addr|mem\(0),
	datab => \db|regs|Mux0~2_combout\,
	datac => \db|regs|dbr[1][7]~q\,
	datad => \db|regs|dbr[3][7]~q\,
	combout => \db|regs|Mux0~3_combout\);

-- Location: LCCOMB_X17_Y23_N4
\db|regs|data_out[7]~24\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|regs|data_out[7]~24_combout\ = (\db|regs|Decoder0~0_combout\ & ((\db|regs|Mux0~1_combout\) # ((\db|regs|Mux0~3_combout\ & \db|regs|Decoder0~1_combout\)))) # (!\db|regs|Decoder0~0_combout\ & (((\db|regs|Mux0~3_combout\ & 
-- \db|regs|Decoder0~1_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \db|regs|Decoder0~0_combout\,
	datab => \db|regs|Mux0~1_combout\,
	datac => \db|regs|Mux0~3_combout\,
	datad => \db|regs|Decoder0~1_combout\,
	combout => \db|regs|data_out[7]~24_combout\);

-- Location: FF_X19_Y21_N11
\db|regs|dbr[8][7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	asdata => \db|regs|dbr[4][7]~q\,
	sload => VCC,
	ena => \db|control_logic|state.s9~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \db|regs|dbr[8][7]~q\);

-- Location: LCCOMB_X19_Y21_N20
\db|regs|dbr[9][7]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|regs|dbr[9][7]~feeder_combout\ = \db|regs|dbr[5][7]~q\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \db|regs|dbr[5][7]~q\,
	combout => \db|regs|dbr[9][7]~feeder_combout\);

-- Location: FF_X19_Y21_N21
\db|regs|dbr[9][7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	d => \db|regs|dbr[9][7]~feeder_combout\,
	ena => \db|control_logic|state.s9~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \db|regs|dbr[9][7]~q\);

-- Location: LCCOMB_X19_Y21_N10
\db|regs|data_out[7]~25\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|regs|data_out[7]~25_combout\ = (\db|regs|data_out[2]~1_combout\ & ((\db|reg_addr|mem\(0) & ((\db|regs|dbr[9][7]~q\))) # (!\db|reg_addr|mem\(0) & (\db|regs|dbr[8][7]~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010100000100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \db|regs|data_out[2]~1_combout\,
	datab => \db|reg_addr|mem\(0),
	datac => \db|regs|dbr[8][7]~q\,
	datad => \db|regs|dbr[9][7]~q\,
	combout => \db|regs|data_out[7]~25_combout\);

-- Location: LCCOMB_X18_Y20_N22
\db|regs|data_out[7]~26\ : cycloneive_lcell_comb
-- Equation(s):
-- \db|regs|data_out[7]~26_combout\ = (\db|regs|data_out[7]~24_combout\) # ((\db|regs|data_out[7]~25_combout\) # ((\db|regs|LessThan1~0_combout\ & \db|regs|dbr[0][7]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \db|regs|LessThan1~0_combout\,
	datab => \db|regs|dbr[0][7]~q\,
	datac => \db|regs|data_out[7]~24_combout\,
	datad => \db|regs|data_out[7]~25_combout\,
	combout => \db|regs|data_out[7]~26_combout\);

-- Location: LCCOMB_X17_Y23_N26
\ssl|trns|Mux0~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \ssl|trns|Mux0~3_combout\ = (\ssl|trns|Mux0~2_combout\ & (((\ssl|trns|index\(0)) # (\db|regs|data_out[7]~26_combout\)))) # (!\ssl|trns|Mux0~2_combout\ & (\db|regs|data_out[5]~17_combout\ & (!\ssl|trns|index\(0))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010111010100100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \ssl|trns|Mux0~2_combout\,
	datab => \db|regs|data_out[5]~17_combout\,
	datac => \ssl|trns|index\(0),
	datad => \db|regs|data_out[7]~26_combout\,
	combout => \ssl|trns|Mux0~3_combout\);

-- Location: LCCOMB_X12_Y21_N4
\ssl|trns|Mux0~4\ : cycloneive_lcell_comb
-- Equation(s):
-- \ssl|trns|Mux0~4_combout\ = (\ssl|trns|index\(2) & (\ssl|trns|Mux0~1_combout\)) # (!\ssl|trns|index\(2) & ((\ssl|trns|Mux0~3_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111010110100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \ssl|trns|index\(2),
	datac => \ssl|trns|Mux0~1_combout\,
	datad => \ssl|trns|Mux0~3_combout\,
	combout => \ssl|trns|Mux0~4_combout\);

ww_miso <= \miso~output_o\;
END structure;


