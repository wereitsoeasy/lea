transcript on
if {[file exists rtl_work]} {
	vdel -lib rtl_work -all
}
vlib rtl_work
vmap work rtl_work

vcom -93 -work work {/home/mauricio/lea/spi/complete_spi/spi_slave.vhd}
vcom -93 -work work {/home/mauricio/lea/spi/complete_spi/receiver.vhd}
vcom -93 -work work {/home/mauricio/lea/spi/complete_spi/data_bridge.vhd}
vcom -93 -work work {/home/mauricio/lea/spi/complete_spi/transmitter.vhd}
vcom -93 -work work {/home/mauricio/lea/spi/complete_spi/db_reg_single.vhd}
vcom -93 -work work {/home/mauricio/lea/spi/complete_spi/db_regs.vhd}
vcom -93 -work work {/home/mauricio/lea/spi/complete_spi/db_reg.vhd}
vcom -93 -work work {/home/mauricio/lea/spi/complete_spi/db_control_logic.vhd}
vcom -93 -work work {/home/mauricio/lea/spi/complete_spi/complete_spi.vhd}
vcom -93 -work work {/home/mauricio/lea/spi/complete_spi/clk_div.vhd}

