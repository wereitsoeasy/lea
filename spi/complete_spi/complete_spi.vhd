library ieee;
use ieee.std_logic_1164.all;

entity complete_spi is
	port(
		mosi, ss, sck, clk, rst: in std_logic;
		miso : out std_logic
	);
end complete_spi;

architecture complete_spi_arch of complete_spi is

	signal spi_in, spi_out : std_logic_vector(7 downto 0);
	signal spi_busy, divd_clk, unused_bit, sf_ss0, sf_ss1, sf_sb0, sf_sb1 : std_logic;
	signal unused_bits : std_logic_vector(37 downto 0);
	
	component data_bridge
		port(
			clk, lea_busy, spi_busy, ss, rst : in std_logic;
			inst_out : out std_logic_vector(37 downto 0);
			lea_mem_in : in std_logic_vector(31 downto 0);
			spi_in : in std_logic_vector(7 downto 0);
			spi_out : out std_logic_vector(7 downto 0);
			lea_exec : out std_logic
		);
	end component;

	--component clk_div
	--	port(
	--		clk, rst : in std_logic;
	--		clk_out : out std_logic
	--	);
	--end component;
	
	component spi_slave
		port(
			sck, rst : in std_logic;
			ss : in std_logic;
			mosi : in std_logic;
			miso : out std_logic;
			p_data_in : in std_logic_vector (7 downto 0);
			p_data_out : out std_logic_vector (7 downto 0);
			busy : out std_logic
		);
	end component;
	
begin
	
	db: data_bridge
	port map(
		clk => clk,  
		lea_busy => unused_bit,
		spi_busy => sf_sb1,
		ss => sf_ss1, 
		rst => rst,
		inst_out => unused_bits,
		lea_mem_in => unused_bits(31 downto 0),
		spi_in =>spi_out,
		spi_out => spi_in,
		lea_exec => unused_bit
	);
	
	ssl: spi_slave
	port map(
		rst => rst,
		sck => sck,
		ss => ss,
		mosi => mosi,
		miso => miso,
		p_data_in => spi_in,
		p_data_out => spi_out,
		busy => spi_busy
	);

	process(clk)
	begin
		if rising_edge(clk) then
			sf_ss0 <= ss;
			sf_ss1 <= sf_ss0;
			sf_sb0 <= spi_busy;
			sf_sb1 <= sf_sb0;
		end if;
	end process;
		
end complete_spi_arch;