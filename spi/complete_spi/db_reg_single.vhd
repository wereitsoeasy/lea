library ieee;
use ieee.std_logic_1164.all;

entity db_reg_single is
	port(
		clk, clr, rw : in std_logic;
		data_in : in std_logic;
		data_out : out std_logic
	);
end db_reg_single;

architecture behavioral of db_reg_single is

	signal mem : std_logic;
	
begin

	reg: process(clk, rw, data_in, clr) is
	begin
		if clr = '1' then
			mem <= '0';
		elsif(falling_edge(clk) and rw = '1') then
			mem <= data_in;
		end if;
	end process;

	data_out <= mem;

end behavioral;