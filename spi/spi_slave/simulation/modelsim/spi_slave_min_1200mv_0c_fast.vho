-- Copyright (C) 1991-2013 Altera Corporation
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, Altera MegaCore Function License 
-- Agreement, or other applicable license agreement, including, 
-- without limitation, that your use is for the sole purpose of 
-- programming logic devices manufactured by Altera and sold by 
-- Altera or its authorized distributors.  Please refer to the 
-- applicable agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus II 32-bit"
-- VERSION "Version 13.1.0 Build 162 10/23/2013 SJ Web Edition"

-- DATE "11/22/2020 18:10:24"

-- 
-- Device: Altera EP4CE10F17C8 Package FBGA256
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL) only
-- 

LIBRARY ALTERA;
LIBRARY CYCLONEIVE;
LIBRARY IEEE;
USE ALTERA.ALTERA_PRIMITIVES_COMPONENTS.ALL;
USE CYCLONEIVE.CYCLONEIVE_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	spi_slave IS
    PORT (
	sck : IN std_logic;
	rst : IN std_logic;
	ss : IN std_logic;
	mosi : IN std_logic;
	miso : BUFFER std_logic;
	busy : BUFFER std_logic
	);
END spi_slave;

-- Design Ports Information
-- miso	=>  Location: PIN_D6,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- busy	=>  Location: PIN_J13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ss	=>  Location: PIN_C8,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- sck	=>  Location: PIN_E8,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- rst	=>  Location: PIN_A13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- mosi	=>  Location: PIN_E7,	 I/O Standard: 2.5 V,	 Current Strength: Default


ARCHITECTURE structure OF spi_slave IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_sck : std_logic;
SIGNAL ww_rst : std_logic;
SIGNAL ww_ss : std_logic;
SIGNAL ww_mosi : std_logic;
SIGNAL ww_miso : std_logic;
SIGNAL ww_busy : std_logic;
SIGNAL \miso~output_o\ : std_logic;
SIGNAL \busy~output_o\ : std_logic;
SIGNAL \sck~input_o\ : std_logic;
SIGNAL \mosi~input_o\ : std_logic;
SIGNAL \ss~input_o\ : std_logic;
SIGNAL \rst~input_o\ : std_logic;
SIGNAL \recv|Decoder0~0_combout\ : std_logic;
SIGNAL \recv|index[0]~32_combout\ : std_logic;
SIGNAL \~GND~combout\ : std_logic;
SIGNAL \recv|index[2]~37\ : std_logic;
SIGNAL \recv|index[3]~38_combout\ : std_logic;
SIGNAL \recv|index[3]~39\ : std_logic;
SIGNAL \recv|index[4]~40_combout\ : std_logic;
SIGNAL \recv|index[4]~41\ : std_logic;
SIGNAL \recv|index[5]~42_combout\ : std_logic;
SIGNAL \recv|index[5]~43\ : std_logic;
SIGNAL \recv|index[6]~44_combout\ : std_logic;
SIGNAL \recv|index[6]~45\ : std_logic;
SIGNAL \recv|index[7]~46_combout\ : std_logic;
SIGNAL \recv|index[7]~47\ : std_logic;
SIGNAL \recv|index[8]~48_combout\ : std_logic;
SIGNAL \recv|index[8]~49\ : std_logic;
SIGNAL \recv|index[9]~50_combout\ : std_logic;
SIGNAL \recv|index[9]~51\ : std_logic;
SIGNAL \recv|index[10]~52_combout\ : std_logic;
SIGNAL \recv|index[10]~53\ : std_logic;
SIGNAL \recv|index[11]~54_combout\ : std_logic;
SIGNAL \recv|index[11]~55\ : std_logic;
SIGNAL \recv|index[12]~56_combout\ : std_logic;
SIGNAL \recv|index[12]~57\ : std_logic;
SIGNAL \recv|index[13]~58_combout\ : std_logic;
SIGNAL \recv|index[13]~59\ : std_logic;
SIGNAL \recv|index[14]~60_combout\ : std_logic;
SIGNAL \recv|index[14]~61\ : std_logic;
SIGNAL \recv|index[15]~62_combout\ : std_logic;
SIGNAL \recv|index[15]~63\ : std_logic;
SIGNAL \recv|index[16]~64_combout\ : std_logic;
SIGNAL \recv|index[16]~65\ : std_logic;
SIGNAL \recv|index[17]~66_combout\ : std_logic;
SIGNAL \recv|index[17]~67\ : std_logic;
SIGNAL \recv|index[18]~68_combout\ : std_logic;
SIGNAL \recv|index[18]~69\ : std_logic;
SIGNAL \recv|index[19]~70_combout\ : std_logic;
SIGNAL \recv|index[19]~71\ : std_logic;
SIGNAL \recv|index[20]~72_combout\ : std_logic;
SIGNAL \recv|index[20]~73\ : std_logic;
SIGNAL \recv|index[21]~74_combout\ : std_logic;
SIGNAL \recv|index[21]~75\ : std_logic;
SIGNAL \recv|index[22]~76_combout\ : std_logic;
SIGNAL \recv|index[22]~77\ : std_logic;
SIGNAL \recv|index[23]~78_combout\ : std_logic;
SIGNAL \recv|index[23]~79\ : std_logic;
SIGNAL \recv|index[24]~80_combout\ : std_logic;
SIGNAL \recv|index[24]~81\ : std_logic;
SIGNAL \recv|index[25]~82_combout\ : std_logic;
SIGNAL \recv|index[25]~83\ : std_logic;
SIGNAL \recv|index[26]~84_combout\ : std_logic;
SIGNAL \recv|index[26]~85\ : std_logic;
SIGNAL \recv|index[27]~86_combout\ : std_logic;
SIGNAL \recv|index[27]~87\ : std_logic;
SIGNAL \recv|index[28]~88_combout\ : std_logic;
SIGNAL \recv|index[28]~89\ : std_logic;
SIGNAL \recv|index[29]~90_combout\ : std_logic;
SIGNAL \recv|index[29]~91\ : std_logic;
SIGNAL \recv|index[30]~92_combout\ : std_logic;
SIGNAL \recv|index[30]~93\ : std_logic;
SIGNAL \recv|index[31]~94_combout\ : std_logic;
SIGNAL \recv|LessThan0~0_combout\ : std_logic;
SIGNAL \busy~8_combout\ : std_logic;
SIGNAL \busy~7_combout\ : std_logic;
SIGNAL \busy~9_combout\ : std_logic;
SIGNAL \busy~3_combout\ : std_logic;
SIGNAL \busy~2_combout\ : std_logic;
SIGNAL \busy~4_combout\ : std_logic;
SIGNAL \busy~5_combout\ : std_logic;
SIGNAL \busy~6_combout\ : std_logic;
SIGNAL \busy~10_combout\ : std_logic;
SIGNAL \recv|LessThan0~1_combout\ : std_logic;
SIGNAL \recv|index[0]~33\ : std_logic;
SIGNAL \recv|index[1]~34_combout\ : std_logic;
SIGNAL \recv|index[1]~35\ : std_logic;
SIGNAL \recv|index[2]~36_combout\ : std_logic;
SIGNAL \recv|Decoder0~8_combout\ : std_logic;
SIGNAL \recv|data_temp[7]~7_combout\ : std_logic;
SIGNAL \recv|Decoder0~5_combout\ : std_logic;
SIGNAL \recv|data_temp[5]~4_combout\ : std_logic;
SIGNAL \trns|index[0]~32_combout\ : std_logic;
SIGNAL \trns|index[0]~33\ : std_logic;
SIGNAL \trns|index[1]~34_combout\ : std_logic;
SIGNAL \trns|index[1]~35\ : std_logic;
SIGNAL \trns|index[2]~36_combout\ : std_logic;
SIGNAL \trns|index[2]~37\ : std_logic;
SIGNAL \trns|index[3]~38_combout\ : std_logic;
SIGNAL \trns|index[3]~39\ : std_logic;
SIGNAL \trns|index[4]~40_combout\ : std_logic;
SIGNAL \trns|index[4]~41\ : std_logic;
SIGNAL \trns|index[5]~42_combout\ : std_logic;
SIGNAL \trns|index[5]~43\ : std_logic;
SIGNAL \trns|index[6]~44_combout\ : std_logic;
SIGNAL \trns|index[6]~45\ : std_logic;
SIGNAL \trns|index[7]~46_combout\ : std_logic;
SIGNAL \trns|index[7]~47\ : std_logic;
SIGNAL \trns|index[8]~48_combout\ : std_logic;
SIGNAL \trns|index[8]~49\ : std_logic;
SIGNAL \trns|index[9]~50_combout\ : std_logic;
SIGNAL \trns|index[9]~51\ : std_logic;
SIGNAL \trns|index[10]~52_combout\ : std_logic;
SIGNAL \trns|index[10]~53\ : std_logic;
SIGNAL \trns|index[11]~54_combout\ : std_logic;
SIGNAL \trns|index[11]~55\ : std_logic;
SIGNAL \trns|index[12]~56_combout\ : std_logic;
SIGNAL \trns|index[12]~57\ : std_logic;
SIGNAL \trns|index[13]~58_combout\ : std_logic;
SIGNAL \trns|index[13]~59\ : std_logic;
SIGNAL \trns|index[14]~60_combout\ : std_logic;
SIGNAL \trns|index[14]~61\ : std_logic;
SIGNAL \trns|index[15]~62_combout\ : std_logic;
SIGNAL \trns|index[15]~63\ : std_logic;
SIGNAL \trns|index[16]~64_combout\ : std_logic;
SIGNAL \trns|index[16]~65\ : std_logic;
SIGNAL \trns|index[17]~66_combout\ : std_logic;
SIGNAL \trns|index[17]~67\ : std_logic;
SIGNAL \trns|index[18]~68_combout\ : std_logic;
SIGNAL \trns|index[18]~69\ : std_logic;
SIGNAL \trns|index[19]~70_combout\ : std_logic;
SIGNAL \trns|index[19]~71\ : std_logic;
SIGNAL \trns|index[20]~72_combout\ : std_logic;
SIGNAL \trns|index[20]~73\ : std_logic;
SIGNAL \trns|index[21]~74_combout\ : std_logic;
SIGNAL \trns|index[21]~75\ : std_logic;
SIGNAL \trns|index[22]~76_combout\ : std_logic;
SIGNAL \trns|index[22]~77\ : std_logic;
SIGNAL \trns|index[23]~78_combout\ : std_logic;
SIGNAL \trns|index[23]~79\ : std_logic;
SIGNAL \trns|index[24]~80_combout\ : std_logic;
SIGNAL \trns|index[24]~81\ : std_logic;
SIGNAL \trns|index[25]~82_combout\ : std_logic;
SIGNAL \trns|index[25]~83\ : std_logic;
SIGNAL \trns|index[26]~84_combout\ : std_logic;
SIGNAL \trns|index[26]~85\ : std_logic;
SIGNAL \trns|index[27]~86_combout\ : std_logic;
SIGNAL \trns|index[27]~87\ : std_logic;
SIGNAL \trns|index[28]~88_combout\ : std_logic;
SIGNAL \trns|index[28]~89\ : std_logic;
SIGNAL \trns|index[29]~90_combout\ : std_logic;
SIGNAL \trns|index[29]~91\ : std_logic;
SIGNAL \trns|index[30]~92_combout\ : std_logic;
SIGNAL \trns|index[30]~93\ : std_logic;
SIGNAL \trns|index[31]~94_combout\ : std_logic;
SIGNAL \busy~18_combout\ : std_logic;
SIGNAL \trns|LessThan0~0_combout\ : std_logic;
SIGNAL \busy~11_combout\ : std_logic;
SIGNAL \busy~12_combout\ : std_logic;
SIGNAL \busy~13_combout\ : std_logic;
SIGNAL \busy~14_combout\ : std_logic;
SIGNAL \busy~15_combout\ : std_logic;
SIGNAL \busy~16_combout\ : std_logic;
SIGNAL \busy~17_combout\ : std_logic;
SIGNAL \trns|LessThan0~1_combout\ : std_logic;
SIGNAL \trns|LessThan0~2_combout\ : std_logic;
SIGNAL \recv|Decoder0~7_combout\ : std_logic;
SIGNAL \recv|data_temp[4]~6_combout\ : std_logic;
SIGNAL \recv|Decoder0~6_combout\ : std_logic;
SIGNAL \recv|data_temp[6]~5_combout\ : std_logic;
SIGNAL \trns|Mux0~2_combout\ : std_logic;
SIGNAL \trns|Mux0~3_combout\ : std_logic;
SIGNAL \recv|Decoder0~1_combout\ : std_logic;
SIGNAL \recv|data_temp[2]~0_combout\ : std_logic;
SIGNAL \recv|Decoder0~4_combout\ : std_logic;
SIGNAL \recv|data_temp[3]~3_combout\ : std_logic;
SIGNAL \recv|Decoder0~3_combout\ : std_logic;
SIGNAL \recv|data_temp[0]~2_combout\ : std_logic;
SIGNAL \recv|Decoder0~2_combout\ : std_logic;
SIGNAL \recv|data_temp[1]~1_combout\ : std_logic;
SIGNAL \trns|Mux0~0_combout\ : std_logic;
SIGNAL \trns|Mux0~1_combout\ : std_logic;
SIGNAL \trns|Mux0~4_combout\ : std_logic;
SIGNAL \busy~0_combout\ : std_logic;
SIGNAL \busy~1_combout\ : std_logic;
SIGNAL \busy~19_combout\ : std_logic;
SIGNAL \busy~20_combout\ : std_logic;
SIGNAL \trns|index\ : std_logic_vector(31 DOWNTO 0);
SIGNAL \recv|data_temp\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \recv|index\ : std_logic_vector(31 DOWNTO 0);
SIGNAL \ALT_INV_rst~input_o\ : std_logic;
SIGNAL \ALT_INV_sck~input_o\ : std_logic;
SIGNAL \ALT_INV_ss~input_o\ : std_logic;

BEGIN

ww_sck <= sck;
ww_rst <= rst;
ww_ss <= ss;
ww_mosi <= mosi;
miso <= ww_miso;
busy <= ww_busy;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;
\ALT_INV_rst~input_o\ <= NOT \rst~input_o\;
\ALT_INV_sck~input_o\ <= NOT \sck~input_o\;
\ALT_INV_ss~input_o\ <= NOT \ss~input_o\;

-- Location: IOOBUF_X3_Y24_N9
\miso~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \trns|Mux0~4_combout\,
	oe => \ALT_INV_ss~input_o\,
	devoe => ww_devoe,
	o => \miso~output_o\);

-- Location: IOOBUF_X34_Y11_N2
\busy~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \busy~20_combout\,
	devoe => ww_devoe,
	o => \busy~output_o\);

-- Location: IOIBUF_X13_Y24_N15
\sck~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_sck,
	o => \sck~input_o\);

-- Location: IOIBUF_X7_Y24_N1
\mosi~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_mosi,
	o => \mosi~input_o\);

-- Location: IOIBUF_X13_Y24_N1
\ss~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_ss,
	o => \ss~input_o\);

-- Location: IOIBUF_X30_Y24_N8
\rst~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_rst,
	o => \rst~input_o\);

-- Location: LCCOMB_X21_Y18_N2
\recv|Decoder0~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \recv|Decoder0~0_combout\ = (\ss~input_o\) # (\rst~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \ss~input_o\,
	datad => \rst~input_o\,
	combout => \recv|Decoder0~0_combout\);

-- Location: LCCOMB_X21_Y17_N0
\recv|index[0]~32\ : cycloneive_lcell_comb
-- Equation(s):
-- \recv|index[0]~32_combout\ = !\recv|index\(0)
-- \recv|index[0]~33\ = CARRY(!\recv|index\(0))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100110011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \recv|index\(0),
	combout => \recv|index[0]~32_combout\,
	cout => \recv|index[0]~33\);

-- Location: LCCOMB_X21_Y13_N4
\~GND\ : cycloneive_lcell_comb
-- Equation(s):
-- \~GND~combout\ = GND

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	combout => \~GND~combout\);

-- Location: LCCOMB_X21_Y17_N4
\recv|index[2]~36\ : cycloneive_lcell_comb
-- Equation(s):
-- \recv|index[2]~36_combout\ = (\recv|index\(2) & (!\recv|index[1]~35\)) # (!\recv|index\(2) & ((\recv|index[1]~35\) # (GND)))
-- \recv|index[2]~37\ = CARRY((!\recv|index[1]~35\) # (!\recv|index\(2)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \recv|index\(2),
	datad => VCC,
	cin => \recv|index[1]~35\,
	combout => \recv|index[2]~36_combout\,
	cout => \recv|index[2]~37\);

-- Location: LCCOMB_X21_Y17_N6
\recv|index[3]~38\ : cycloneive_lcell_comb
-- Equation(s):
-- \recv|index[3]~38_combout\ = (\recv|index\(3) & (\recv|index[2]~37\ & VCC)) # (!\recv|index\(3) & (!\recv|index[2]~37\))
-- \recv|index[3]~39\ = CARRY((!\recv|index\(3) & !\recv|index[2]~37\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100000101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \recv|index\(3),
	datad => VCC,
	cin => \recv|index[2]~37\,
	combout => \recv|index[3]~38_combout\,
	cout => \recv|index[3]~39\);

-- Location: FF_X21_Y17_N7
\recv|index[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \sck~input_o\,
	d => \recv|index[3]~38_combout\,
	asdata => \~GND~combout\,
	clrn => \ALT_INV_rst~input_o\,
	sload => \recv|LessThan0~1_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \recv|index\(3));

-- Location: LCCOMB_X21_Y17_N8
\recv|index[4]~40\ : cycloneive_lcell_comb
-- Equation(s):
-- \recv|index[4]~40_combout\ = (\recv|index\(4) & ((GND) # (!\recv|index[3]~39\))) # (!\recv|index\(4) & (\recv|index[3]~39\ $ (GND)))
-- \recv|index[4]~41\ = CARRY((\recv|index\(4)) # (!\recv|index[3]~39\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \recv|index\(4),
	datad => VCC,
	cin => \recv|index[3]~39\,
	combout => \recv|index[4]~40_combout\,
	cout => \recv|index[4]~41\);

-- Location: FF_X21_Y17_N9
\recv|index[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \sck~input_o\,
	d => \recv|index[4]~40_combout\,
	asdata => \~GND~combout\,
	clrn => \ALT_INV_rst~input_o\,
	sload => \recv|LessThan0~1_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \recv|index\(4));

-- Location: LCCOMB_X21_Y17_N10
\recv|index[5]~42\ : cycloneive_lcell_comb
-- Equation(s):
-- \recv|index[5]~42_combout\ = (\recv|index\(5) & (\recv|index[4]~41\ & VCC)) # (!\recv|index\(5) & (!\recv|index[4]~41\))
-- \recv|index[5]~43\ = CARRY((!\recv|index\(5) & !\recv|index[4]~41\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100000101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \recv|index\(5),
	datad => VCC,
	cin => \recv|index[4]~41\,
	combout => \recv|index[5]~42_combout\,
	cout => \recv|index[5]~43\);

-- Location: FF_X21_Y17_N11
\recv|index[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \sck~input_o\,
	d => \recv|index[5]~42_combout\,
	asdata => \~GND~combout\,
	clrn => \ALT_INV_rst~input_o\,
	sload => \recv|LessThan0~1_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \recv|index\(5));

-- Location: LCCOMB_X21_Y17_N12
\recv|index[6]~44\ : cycloneive_lcell_comb
-- Equation(s):
-- \recv|index[6]~44_combout\ = (\recv|index\(6) & ((GND) # (!\recv|index[5]~43\))) # (!\recv|index\(6) & (\recv|index[5]~43\ $ (GND)))
-- \recv|index[6]~45\ = CARRY((\recv|index\(6)) # (!\recv|index[5]~43\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101010101111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \recv|index\(6),
	datad => VCC,
	cin => \recv|index[5]~43\,
	combout => \recv|index[6]~44_combout\,
	cout => \recv|index[6]~45\);

-- Location: FF_X21_Y17_N13
\recv|index[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \sck~input_o\,
	d => \recv|index[6]~44_combout\,
	asdata => \~GND~combout\,
	clrn => \ALT_INV_rst~input_o\,
	sload => \recv|LessThan0~1_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \recv|index\(6));

-- Location: LCCOMB_X21_Y17_N14
\recv|index[7]~46\ : cycloneive_lcell_comb
-- Equation(s):
-- \recv|index[7]~46_combout\ = (\recv|index\(7) & (\recv|index[6]~45\ & VCC)) # (!\recv|index\(7) & (!\recv|index[6]~45\))
-- \recv|index[7]~47\ = CARRY((!\recv|index\(7) & !\recv|index[6]~45\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \recv|index\(7),
	datad => VCC,
	cin => \recv|index[6]~45\,
	combout => \recv|index[7]~46_combout\,
	cout => \recv|index[7]~47\);

-- Location: FF_X21_Y17_N15
\recv|index[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \sck~input_o\,
	d => \recv|index[7]~46_combout\,
	asdata => \~GND~combout\,
	clrn => \ALT_INV_rst~input_o\,
	sload => \recv|LessThan0~1_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \recv|index\(7));

-- Location: LCCOMB_X21_Y17_N16
\recv|index[8]~48\ : cycloneive_lcell_comb
-- Equation(s):
-- \recv|index[8]~48_combout\ = (\recv|index\(8) & ((GND) # (!\recv|index[7]~47\))) # (!\recv|index\(8) & (\recv|index[7]~47\ $ (GND)))
-- \recv|index[8]~49\ = CARRY((\recv|index\(8)) # (!\recv|index[7]~47\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \recv|index\(8),
	datad => VCC,
	cin => \recv|index[7]~47\,
	combout => \recv|index[8]~48_combout\,
	cout => \recv|index[8]~49\);

-- Location: FF_X21_Y17_N17
\recv|index[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \sck~input_o\,
	d => \recv|index[8]~48_combout\,
	asdata => \~GND~combout\,
	clrn => \ALT_INV_rst~input_o\,
	sload => \recv|LessThan0~1_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \recv|index\(8));

-- Location: LCCOMB_X21_Y17_N18
\recv|index[9]~50\ : cycloneive_lcell_comb
-- Equation(s):
-- \recv|index[9]~50_combout\ = (\recv|index\(9) & (\recv|index[8]~49\ & VCC)) # (!\recv|index\(9) & (!\recv|index[8]~49\))
-- \recv|index[9]~51\ = CARRY((!\recv|index\(9) & !\recv|index[8]~49\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \recv|index\(9),
	datad => VCC,
	cin => \recv|index[8]~49\,
	combout => \recv|index[9]~50_combout\,
	cout => \recv|index[9]~51\);

-- Location: FF_X21_Y17_N19
\recv|index[9]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \sck~input_o\,
	d => \recv|index[9]~50_combout\,
	asdata => \~GND~combout\,
	clrn => \ALT_INV_rst~input_o\,
	sload => \recv|LessThan0~1_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \recv|index\(9));

-- Location: LCCOMB_X21_Y17_N20
\recv|index[10]~52\ : cycloneive_lcell_comb
-- Equation(s):
-- \recv|index[10]~52_combout\ = (\recv|index\(10) & ((GND) # (!\recv|index[9]~51\))) # (!\recv|index\(10) & (\recv|index[9]~51\ $ (GND)))
-- \recv|index[10]~53\ = CARRY((\recv|index\(10)) # (!\recv|index[9]~51\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \recv|index\(10),
	datad => VCC,
	cin => \recv|index[9]~51\,
	combout => \recv|index[10]~52_combout\,
	cout => \recv|index[10]~53\);

-- Location: FF_X21_Y17_N21
\recv|index[10]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \sck~input_o\,
	d => \recv|index[10]~52_combout\,
	asdata => \~GND~combout\,
	clrn => \ALT_INV_rst~input_o\,
	sload => \recv|LessThan0~1_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \recv|index\(10));

-- Location: LCCOMB_X21_Y17_N22
\recv|index[11]~54\ : cycloneive_lcell_comb
-- Equation(s):
-- \recv|index[11]~54_combout\ = (\recv|index\(11) & (\recv|index[10]~53\ & VCC)) # (!\recv|index\(11) & (!\recv|index[10]~53\))
-- \recv|index[11]~55\ = CARRY((!\recv|index\(11) & !\recv|index[10]~53\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100000101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \recv|index\(11),
	datad => VCC,
	cin => \recv|index[10]~53\,
	combout => \recv|index[11]~54_combout\,
	cout => \recv|index[11]~55\);

-- Location: FF_X21_Y17_N23
\recv|index[11]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \sck~input_o\,
	d => \recv|index[11]~54_combout\,
	asdata => \~GND~combout\,
	clrn => \ALT_INV_rst~input_o\,
	sload => \recv|LessThan0~1_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \recv|index\(11));

-- Location: LCCOMB_X21_Y17_N24
\recv|index[12]~56\ : cycloneive_lcell_comb
-- Equation(s):
-- \recv|index[12]~56_combout\ = (\recv|index\(12) & ((GND) # (!\recv|index[11]~55\))) # (!\recv|index\(12) & (\recv|index[11]~55\ $ (GND)))
-- \recv|index[12]~57\ = CARRY((\recv|index\(12)) # (!\recv|index[11]~55\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \recv|index\(12),
	datad => VCC,
	cin => \recv|index[11]~55\,
	combout => \recv|index[12]~56_combout\,
	cout => \recv|index[12]~57\);

-- Location: FF_X21_Y17_N25
\recv|index[12]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \sck~input_o\,
	d => \recv|index[12]~56_combout\,
	asdata => \~GND~combout\,
	clrn => \ALT_INV_rst~input_o\,
	sload => \recv|LessThan0~1_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \recv|index\(12));

-- Location: LCCOMB_X21_Y17_N26
\recv|index[13]~58\ : cycloneive_lcell_comb
-- Equation(s):
-- \recv|index[13]~58_combout\ = (\recv|index\(13) & (\recv|index[12]~57\ & VCC)) # (!\recv|index\(13) & (!\recv|index[12]~57\))
-- \recv|index[13]~59\ = CARRY((!\recv|index\(13) & !\recv|index[12]~57\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100000101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \recv|index\(13),
	datad => VCC,
	cin => \recv|index[12]~57\,
	combout => \recv|index[13]~58_combout\,
	cout => \recv|index[13]~59\);

-- Location: FF_X21_Y17_N27
\recv|index[13]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \sck~input_o\,
	d => \recv|index[13]~58_combout\,
	asdata => \~GND~combout\,
	clrn => \ALT_INV_rst~input_o\,
	sload => \recv|LessThan0~1_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \recv|index\(13));

-- Location: LCCOMB_X21_Y17_N28
\recv|index[14]~60\ : cycloneive_lcell_comb
-- Equation(s):
-- \recv|index[14]~60_combout\ = (\recv|index\(14) & ((GND) # (!\recv|index[13]~59\))) # (!\recv|index\(14) & (\recv|index[13]~59\ $ (GND)))
-- \recv|index[14]~61\ = CARRY((\recv|index\(14)) # (!\recv|index[13]~59\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \recv|index\(14),
	datad => VCC,
	cin => \recv|index[13]~59\,
	combout => \recv|index[14]~60_combout\,
	cout => \recv|index[14]~61\);

-- Location: FF_X21_Y17_N29
\recv|index[14]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \sck~input_o\,
	d => \recv|index[14]~60_combout\,
	asdata => \~GND~combout\,
	clrn => \ALT_INV_rst~input_o\,
	sload => \recv|LessThan0~1_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \recv|index\(14));

-- Location: LCCOMB_X21_Y17_N30
\recv|index[15]~62\ : cycloneive_lcell_comb
-- Equation(s):
-- \recv|index[15]~62_combout\ = (\recv|index\(15) & (\recv|index[14]~61\ & VCC)) # (!\recv|index\(15) & (!\recv|index[14]~61\))
-- \recv|index[15]~63\ = CARRY((!\recv|index\(15) & !\recv|index[14]~61\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100000101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \recv|index\(15),
	datad => VCC,
	cin => \recv|index[14]~61\,
	combout => \recv|index[15]~62_combout\,
	cout => \recv|index[15]~63\);

-- Location: FF_X21_Y17_N31
\recv|index[15]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \sck~input_o\,
	d => \recv|index[15]~62_combout\,
	asdata => \~GND~combout\,
	clrn => \ALT_INV_rst~input_o\,
	sload => \recv|LessThan0~1_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \recv|index\(15));

-- Location: LCCOMB_X21_Y16_N0
\recv|index[16]~64\ : cycloneive_lcell_comb
-- Equation(s):
-- \recv|index[16]~64_combout\ = (\recv|index\(16) & ((GND) # (!\recv|index[15]~63\))) # (!\recv|index\(16) & (\recv|index[15]~63\ $ (GND)))
-- \recv|index[16]~65\ = CARRY((\recv|index\(16)) # (!\recv|index[15]~63\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \recv|index\(16),
	datad => VCC,
	cin => \recv|index[15]~63\,
	combout => \recv|index[16]~64_combout\,
	cout => \recv|index[16]~65\);

-- Location: FF_X21_Y16_N1
\recv|index[16]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \sck~input_o\,
	d => \recv|index[16]~64_combout\,
	asdata => \~GND~combout\,
	clrn => \ALT_INV_rst~input_o\,
	sload => \recv|LessThan0~1_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \recv|index\(16));

-- Location: LCCOMB_X21_Y16_N2
\recv|index[17]~66\ : cycloneive_lcell_comb
-- Equation(s):
-- \recv|index[17]~66_combout\ = (\recv|index\(17) & (\recv|index[16]~65\ & VCC)) # (!\recv|index\(17) & (!\recv|index[16]~65\))
-- \recv|index[17]~67\ = CARRY((!\recv|index\(17) & !\recv|index[16]~65\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \recv|index\(17),
	datad => VCC,
	cin => \recv|index[16]~65\,
	combout => \recv|index[17]~66_combout\,
	cout => \recv|index[17]~67\);

-- Location: FF_X21_Y16_N3
\recv|index[17]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \sck~input_o\,
	d => \recv|index[17]~66_combout\,
	asdata => \~GND~combout\,
	clrn => \ALT_INV_rst~input_o\,
	sload => \recv|LessThan0~1_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \recv|index\(17));

-- Location: LCCOMB_X21_Y16_N4
\recv|index[18]~68\ : cycloneive_lcell_comb
-- Equation(s):
-- \recv|index[18]~68_combout\ = (\recv|index\(18) & ((GND) # (!\recv|index[17]~67\))) # (!\recv|index\(18) & (\recv|index[17]~67\ $ (GND)))
-- \recv|index[18]~69\ = CARRY((\recv|index\(18)) # (!\recv|index[17]~67\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \recv|index\(18),
	datad => VCC,
	cin => \recv|index[17]~67\,
	combout => \recv|index[18]~68_combout\,
	cout => \recv|index[18]~69\);

-- Location: FF_X21_Y16_N5
\recv|index[18]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \sck~input_o\,
	d => \recv|index[18]~68_combout\,
	asdata => \~GND~combout\,
	clrn => \ALT_INV_rst~input_o\,
	sload => \recv|LessThan0~1_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \recv|index\(18));

-- Location: LCCOMB_X21_Y16_N6
\recv|index[19]~70\ : cycloneive_lcell_comb
-- Equation(s):
-- \recv|index[19]~70_combout\ = (\recv|index\(19) & (\recv|index[18]~69\ & VCC)) # (!\recv|index\(19) & (!\recv|index[18]~69\))
-- \recv|index[19]~71\ = CARRY((!\recv|index\(19) & !\recv|index[18]~69\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100000101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \recv|index\(19),
	datad => VCC,
	cin => \recv|index[18]~69\,
	combout => \recv|index[19]~70_combout\,
	cout => \recv|index[19]~71\);

-- Location: FF_X21_Y16_N7
\recv|index[19]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \sck~input_o\,
	d => \recv|index[19]~70_combout\,
	asdata => \~GND~combout\,
	clrn => \ALT_INV_rst~input_o\,
	sload => \recv|LessThan0~1_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \recv|index\(19));

-- Location: LCCOMB_X21_Y16_N8
\recv|index[20]~72\ : cycloneive_lcell_comb
-- Equation(s):
-- \recv|index[20]~72_combout\ = (\recv|index\(20) & ((GND) # (!\recv|index[19]~71\))) # (!\recv|index\(20) & (\recv|index[19]~71\ $ (GND)))
-- \recv|index[20]~73\ = CARRY((\recv|index\(20)) # (!\recv|index[19]~71\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \recv|index\(20),
	datad => VCC,
	cin => \recv|index[19]~71\,
	combout => \recv|index[20]~72_combout\,
	cout => \recv|index[20]~73\);

-- Location: FF_X21_Y16_N9
\recv|index[20]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \sck~input_o\,
	d => \recv|index[20]~72_combout\,
	asdata => \~GND~combout\,
	clrn => \ALT_INV_rst~input_o\,
	sload => \recv|LessThan0~1_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \recv|index\(20));

-- Location: LCCOMB_X21_Y16_N10
\recv|index[21]~74\ : cycloneive_lcell_comb
-- Equation(s):
-- \recv|index[21]~74_combout\ = (\recv|index\(21) & (\recv|index[20]~73\ & VCC)) # (!\recv|index\(21) & (!\recv|index[20]~73\))
-- \recv|index[21]~75\ = CARRY((!\recv|index\(21) & !\recv|index[20]~73\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100000101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \recv|index\(21),
	datad => VCC,
	cin => \recv|index[20]~73\,
	combout => \recv|index[21]~74_combout\,
	cout => \recv|index[21]~75\);

-- Location: FF_X21_Y16_N11
\recv|index[21]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \sck~input_o\,
	d => \recv|index[21]~74_combout\,
	asdata => \~GND~combout\,
	clrn => \ALT_INV_rst~input_o\,
	sload => \recv|LessThan0~1_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \recv|index\(21));

-- Location: LCCOMB_X21_Y16_N12
\recv|index[22]~76\ : cycloneive_lcell_comb
-- Equation(s):
-- \recv|index[22]~76_combout\ = (\recv|index\(22) & ((GND) # (!\recv|index[21]~75\))) # (!\recv|index\(22) & (\recv|index[21]~75\ $ (GND)))
-- \recv|index[22]~77\ = CARRY((\recv|index\(22)) # (!\recv|index[21]~75\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101010101111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \recv|index\(22),
	datad => VCC,
	cin => \recv|index[21]~75\,
	combout => \recv|index[22]~76_combout\,
	cout => \recv|index[22]~77\);

-- Location: FF_X21_Y16_N13
\recv|index[22]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \sck~input_o\,
	d => \recv|index[22]~76_combout\,
	asdata => \~GND~combout\,
	clrn => \ALT_INV_rst~input_o\,
	sload => \recv|LessThan0~1_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \recv|index\(22));

-- Location: LCCOMB_X21_Y16_N14
\recv|index[23]~78\ : cycloneive_lcell_comb
-- Equation(s):
-- \recv|index[23]~78_combout\ = (\recv|index\(23) & (\recv|index[22]~77\ & VCC)) # (!\recv|index\(23) & (!\recv|index[22]~77\))
-- \recv|index[23]~79\ = CARRY((!\recv|index\(23) & !\recv|index[22]~77\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \recv|index\(23),
	datad => VCC,
	cin => \recv|index[22]~77\,
	combout => \recv|index[23]~78_combout\,
	cout => \recv|index[23]~79\);

-- Location: FF_X21_Y16_N15
\recv|index[23]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \sck~input_o\,
	d => \recv|index[23]~78_combout\,
	asdata => \~GND~combout\,
	clrn => \ALT_INV_rst~input_o\,
	sload => \recv|LessThan0~1_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \recv|index\(23));

-- Location: LCCOMB_X21_Y16_N16
\recv|index[24]~80\ : cycloneive_lcell_comb
-- Equation(s):
-- \recv|index[24]~80_combout\ = (\recv|index\(24) & ((GND) # (!\recv|index[23]~79\))) # (!\recv|index\(24) & (\recv|index[23]~79\ $ (GND)))
-- \recv|index[24]~81\ = CARRY((\recv|index\(24)) # (!\recv|index[23]~79\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \recv|index\(24),
	datad => VCC,
	cin => \recv|index[23]~79\,
	combout => \recv|index[24]~80_combout\,
	cout => \recv|index[24]~81\);

-- Location: FF_X21_Y16_N17
\recv|index[24]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \sck~input_o\,
	d => \recv|index[24]~80_combout\,
	asdata => \~GND~combout\,
	clrn => \ALT_INV_rst~input_o\,
	sload => \recv|LessThan0~1_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \recv|index\(24));

-- Location: LCCOMB_X21_Y16_N18
\recv|index[25]~82\ : cycloneive_lcell_comb
-- Equation(s):
-- \recv|index[25]~82_combout\ = (\recv|index\(25) & (\recv|index[24]~81\ & VCC)) # (!\recv|index\(25) & (!\recv|index[24]~81\))
-- \recv|index[25]~83\ = CARRY((!\recv|index\(25) & !\recv|index[24]~81\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \recv|index\(25),
	datad => VCC,
	cin => \recv|index[24]~81\,
	combout => \recv|index[25]~82_combout\,
	cout => \recv|index[25]~83\);

-- Location: FF_X21_Y16_N19
\recv|index[25]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \sck~input_o\,
	d => \recv|index[25]~82_combout\,
	asdata => \~GND~combout\,
	clrn => \ALT_INV_rst~input_o\,
	sload => \recv|LessThan0~1_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \recv|index\(25));

-- Location: LCCOMB_X21_Y16_N20
\recv|index[26]~84\ : cycloneive_lcell_comb
-- Equation(s):
-- \recv|index[26]~84_combout\ = (\recv|index\(26) & ((GND) # (!\recv|index[25]~83\))) # (!\recv|index\(26) & (\recv|index[25]~83\ $ (GND)))
-- \recv|index[26]~85\ = CARRY((\recv|index\(26)) # (!\recv|index[25]~83\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \recv|index\(26),
	datad => VCC,
	cin => \recv|index[25]~83\,
	combout => \recv|index[26]~84_combout\,
	cout => \recv|index[26]~85\);

-- Location: FF_X21_Y16_N21
\recv|index[26]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \sck~input_o\,
	d => \recv|index[26]~84_combout\,
	asdata => \~GND~combout\,
	clrn => \ALT_INV_rst~input_o\,
	sload => \recv|LessThan0~1_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \recv|index\(26));

-- Location: LCCOMB_X21_Y16_N22
\recv|index[27]~86\ : cycloneive_lcell_comb
-- Equation(s):
-- \recv|index[27]~86_combout\ = (\recv|index\(27) & (\recv|index[26]~85\ & VCC)) # (!\recv|index\(27) & (!\recv|index[26]~85\))
-- \recv|index[27]~87\ = CARRY((!\recv|index\(27) & !\recv|index[26]~85\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100000101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \recv|index\(27),
	datad => VCC,
	cin => \recv|index[26]~85\,
	combout => \recv|index[27]~86_combout\,
	cout => \recv|index[27]~87\);

-- Location: FF_X21_Y16_N23
\recv|index[27]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \sck~input_o\,
	d => \recv|index[27]~86_combout\,
	asdata => \~GND~combout\,
	clrn => \ALT_INV_rst~input_o\,
	sload => \recv|LessThan0~1_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \recv|index\(27));

-- Location: LCCOMB_X21_Y16_N24
\recv|index[28]~88\ : cycloneive_lcell_comb
-- Equation(s):
-- \recv|index[28]~88_combout\ = (\recv|index\(28) & ((GND) # (!\recv|index[27]~87\))) # (!\recv|index\(28) & (\recv|index[27]~87\ $ (GND)))
-- \recv|index[28]~89\ = CARRY((\recv|index\(28)) # (!\recv|index[27]~87\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \recv|index\(28),
	datad => VCC,
	cin => \recv|index[27]~87\,
	combout => \recv|index[28]~88_combout\,
	cout => \recv|index[28]~89\);

-- Location: FF_X21_Y16_N25
\recv|index[28]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \sck~input_o\,
	d => \recv|index[28]~88_combout\,
	asdata => \~GND~combout\,
	clrn => \ALT_INV_rst~input_o\,
	sload => \recv|LessThan0~1_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \recv|index\(28));

-- Location: LCCOMB_X21_Y16_N26
\recv|index[29]~90\ : cycloneive_lcell_comb
-- Equation(s):
-- \recv|index[29]~90_combout\ = (\recv|index\(29) & (\recv|index[28]~89\ & VCC)) # (!\recv|index\(29) & (!\recv|index[28]~89\))
-- \recv|index[29]~91\ = CARRY((!\recv|index\(29) & !\recv|index[28]~89\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100000101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \recv|index\(29),
	datad => VCC,
	cin => \recv|index[28]~89\,
	combout => \recv|index[29]~90_combout\,
	cout => \recv|index[29]~91\);

-- Location: FF_X21_Y16_N27
\recv|index[29]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \sck~input_o\,
	d => \recv|index[29]~90_combout\,
	asdata => \~GND~combout\,
	clrn => \ALT_INV_rst~input_o\,
	sload => \recv|LessThan0~1_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \recv|index\(29));

-- Location: LCCOMB_X21_Y16_N28
\recv|index[30]~92\ : cycloneive_lcell_comb
-- Equation(s):
-- \recv|index[30]~92_combout\ = (\recv|index\(30) & ((GND) # (!\recv|index[29]~91\))) # (!\recv|index\(30) & (\recv|index[29]~91\ $ (GND)))
-- \recv|index[30]~93\ = CARRY((\recv|index\(30)) # (!\recv|index[29]~91\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \recv|index\(30),
	datad => VCC,
	cin => \recv|index[29]~91\,
	combout => \recv|index[30]~92_combout\,
	cout => \recv|index[30]~93\);

-- Location: FF_X21_Y16_N29
\recv|index[30]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \sck~input_o\,
	d => \recv|index[30]~92_combout\,
	asdata => \~GND~combout\,
	clrn => \ALT_INV_rst~input_o\,
	sload => \recv|LessThan0~1_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \recv|index\(30));

-- Location: LCCOMB_X21_Y16_N30
\recv|index[31]~94\ : cycloneive_lcell_comb
-- Equation(s):
-- \recv|index[31]~94_combout\ = \recv|index\(31) $ (!\recv|index[30]~93\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010110100101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \recv|index\(31),
	cin => \recv|index[30]~93\,
	combout => \recv|index[31]~94_combout\);

-- Location: FF_X21_Y16_N31
\recv|index[31]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \sck~input_o\,
	d => \recv|index[31]~94_combout\,
	asdata => \~GND~combout\,
	clrn => \ALT_INV_rst~input_o\,
	sload => \recv|LessThan0~1_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \recv|index\(31));

-- Location: LCCOMB_X22_Y17_N30
\recv|LessThan0~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \recv|LessThan0~0_combout\ = ((!\recv|index\(2)) # (!\recv|index\(1))) # (!\recv|index\(0))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111111101111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \recv|index\(0),
	datab => \recv|index\(1),
	datac => \recv|index\(2),
	combout => \recv|LessThan0~0_combout\);

-- Location: LCCOMB_X22_Y17_N12
\busy~8\ : cycloneive_lcell_comb
-- Equation(s):
-- \busy~8_combout\ = (!\recv|index\(23) & (!\recv|index\(24) & (!\recv|index\(25) & !\recv|index\(26))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \recv|index\(23),
	datab => \recv|index\(24),
	datac => \recv|index\(25),
	datad => \recv|index\(26),
	combout => \busy~8_combout\);

-- Location: LCCOMB_X22_Y17_N2
\busy~7\ : cycloneive_lcell_comb
-- Equation(s):
-- \busy~7_combout\ = (!\recv|index\(19) & (!\recv|index\(21) & (!\recv|index\(20) & !\recv|index\(22))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \recv|index\(19),
	datab => \recv|index\(21),
	datac => \recv|index\(20),
	datad => \recv|index\(22),
	combout => \busy~7_combout\);

-- Location: LCCOMB_X22_Y17_N22
\busy~9\ : cycloneive_lcell_comb
-- Equation(s):
-- \busy~9_combout\ = (!\recv|index\(29) & (!\recv|index\(27) & (!\recv|index\(28) & !\recv|index\(30))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \recv|index\(29),
	datab => \recv|index\(27),
	datac => \recv|index\(28),
	datad => \recv|index\(30),
	combout => \busy~9_combout\);

-- Location: LCCOMB_X22_Y17_N26
\busy~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \busy~3_combout\ = (!\recv|index\(7) & (!\recv|index\(9) & (!\recv|index\(8) & !\recv|index\(10))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \recv|index\(7),
	datab => \recv|index\(9),
	datac => \recv|index\(8),
	datad => \recv|index\(10),
	combout => \busy~3_combout\);

-- Location: LCCOMB_X22_Y17_N16
\busy~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \busy~2_combout\ = (!\recv|index\(5) & (!\recv|index\(3) & (!\recv|index\(4) & !\recv|index\(6))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \recv|index\(5),
	datab => \recv|index\(3),
	datac => \recv|index\(4),
	datad => \recv|index\(6),
	combout => \busy~2_combout\);

-- Location: LCCOMB_X22_Y17_N4
\busy~4\ : cycloneive_lcell_comb
-- Equation(s):
-- \busy~4_combout\ = (!\recv|index\(13) & (!\recv|index\(11) & (!\recv|index\(12) & !\recv|index\(14))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \recv|index\(13),
	datab => \recv|index\(11),
	datac => \recv|index\(12),
	datad => \recv|index\(14),
	combout => \busy~4_combout\);

-- Location: LCCOMB_X22_Y17_N10
\busy~5\ : cycloneive_lcell_comb
-- Equation(s):
-- \busy~5_combout\ = (!\recv|index\(15) & (!\recv|index\(17) & (!\recv|index\(16) & !\recv|index\(18))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \recv|index\(15),
	datab => \recv|index\(17),
	datac => \recv|index\(16),
	datad => \recv|index\(18),
	combout => \busy~5_combout\);

-- Location: LCCOMB_X22_Y17_N28
\busy~6\ : cycloneive_lcell_comb
-- Equation(s):
-- \busy~6_combout\ = (\busy~3_combout\ & (\busy~2_combout\ & (\busy~4_combout\ & \busy~5_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \busy~3_combout\,
	datab => \busy~2_combout\,
	datac => \busy~4_combout\,
	datad => \busy~5_combout\,
	combout => \busy~6_combout\);

-- Location: LCCOMB_X22_Y17_N24
\busy~10\ : cycloneive_lcell_comb
-- Equation(s):
-- \busy~10_combout\ = (\busy~8_combout\ & (\busy~7_combout\ & (\busy~9_combout\ & \busy~6_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \busy~8_combout\,
	datab => \busy~7_combout\,
	datac => \busy~9_combout\,
	datad => \busy~6_combout\,
	combout => \busy~10_combout\);

-- Location: LCCOMB_X22_Y17_N20
\recv|LessThan0~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \recv|LessThan0~1_combout\ = (\recv|index\(31)) # ((!\recv|LessThan0~0_combout\ & \busy~10_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100111111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \recv|index\(31),
	datac => \recv|LessThan0~0_combout\,
	datad => \busy~10_combout\,
	combout => \recv|LessThan0~1_combout\);

-- Location: FF_X21_Y17_N1
\recv|index[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \sck~input_o\,
	d => \recv|index[0]~32_combout\,
	asdata => \~GND~combout\,
	clrn => \ALT_INV_rst~input_o\,
	sload => \recv|LessThan0~1_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \recv|index\(0));

-- Location: LCCOMB_X21_Y17_N2
\recv|index[1]~34\ : cycloneive_lcell_comb
-- Equation(s):
-- \recv|index[1]~34_combout\ = (\recv|index\(1) & (\recv|index[0]~33\ $ (GND))) # (!\recv|index\(1) & (!\recv|index[0]~33\ & VCC))
-- \recv|index[1]~35\ = CARRY((\recv|index\(1) & !\recv|index[0]~33\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \recv|index\(1),
	datad => VCC,
	cin => \recv|index[0]~33\,
	combout => \recv|index[1]~34_combout\,
	cout => \recv|index[1]~35\);

-- Location: FF_X21_Y17_N3
\recv|index[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \sck~input_o\,
	d => \recv|index[1]~34_combout\,
	asdata => \~GND~combout\,
	clrn => \ALT_INV_rst~input_o\,
	sload => \recv|LessThan0~1_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \recv|index\(1));

-- Location: FF_X21_Y17_N5
\recv|index[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \sck~input_o\,
	d => \recv|index[2]~36_combout\,
	asdata => \~GND~combout\,
	clrn => \ALT_INV_rst~input_o\,
	sload => \recv|LessThan0~1_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \recv|index\(2));

-- Location: LCCOMB_X17_Y18_N18
\recv|Decoder0~8\ : cycloneive_lcell_comb
-- Equation(s):
-- \recv|Decoder0~8_combout\ = (!\recv|Decoder0~0_combout\ & (!\recv|index\(2) & (!\recv|index\(0) & !\recv|index\(1))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \recv|Decoder0~0_combout\,
	datab => \recv|index\(2),
	datac => \recv|index\(0),
	datad => \recv|index\(1),
	combout => \recv|Decoder0~8_combout\);

-- Location: LCCOMB_X17_Y18_N26
\recv|data_temp[7]~7\ : cycloneive_lcell_comb
-- Equation(s):
-- \recv|data_temp[7]~7_combout\ = (\recv|Decoder0~8_combout\ & (\mosi~input_o\)) # (!\recv|Decoder0~8_combout\ & ((\recv|data_temp\(7))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \mosi~input_o\,
	datac => \recv|data_temp\(7),
	datad => \recv|Decoder0~8_combout\,
	combout => \recv|data_temp[7]~7_combout\);

-- Location: FF_X17_Y18_N27
\recv|data_temp[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \sck~input_o\,
	d => \recv|data_temp[7]~7_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \recv|data_temp\(7));

-- Location: LCCOMB_X17_Y18_N28
\recv|Decoder0~5\ : cycloneive_lcell_comb
-- Equation(s):
-- \recv|Decoder0~5_combout\ = (!\recv|Decoder0~0_combout\ & (!\recv|index\(2) & (!\recv|index\(0) & \recv|index\(1))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \recv|Decoder0~0_combout\,
	datab => \recv|index\(2),
	datac => \recv|index\(0),
	datad => \recv|index\(1),
	combout => \recv|Decoder0~5_combout\);

-- Location: LCCOMB_X17_Y18_N4
\recv|data_temp[5]~4\ : cycloneive_lcell_comb
-- Equation(s):
-- \recv|data_temp[5]~4_combout\ = (\recv|Decoder0~5_combout\ & (\mosi~input_o\)) # (!\recv|Decoder0~5_combout\ & ((\recv|data_temp\(5))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \mosi~input_o\,
	datac => \recv|data_temp\(5),
	datad => \recv|Decoder0~5_combout\,
	combout => \recv|data_temp[5]~4_combout\);

-- Location: FF_X17_Y18_N5
\recv|data_temp[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \sck~input_o\,
	d => \recv|data_temp[5]~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \recv|data_temp\(5));

-- Location: LCCOMB_X21_Y12_N0
\trns|index[0]~32\ : cycloneive_lcell_comb
-- Equation(s):
-- \trns|index[0]~32_combout\ = !\trns|index\(0)
-- \trns|index[0]~33\ = CARRY(!\trns|index\(0))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100110011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \trns|index\(0),
	combout => \trns|index[0]~32_combout\,
	cout => \trns|index[0]~33\);

-- Location: LCCOMB_X21_Y12_N2
\trns|index[1]~34\ : cycloneive_lcell_comb
-- Equation(s):
-- \trns|index[1]~34_combout\ = (\trns|index\(1) & (\trns|index[0]~33\ $ (GND))) # (!\trns|index\(1) & (!\trns|index[0]~33\ & VCC))
-- \trns|index[1]~35\ = CARRY((\trns|index\(1) & !\trns|index[0]~33\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \trns|index\(1),
	datad => VCC,
	cin => \trns|index[0]~33\,
	combout => \trns|index[1]~34_combout\,
	cout => \trns|index[1]~35\);

-- Location: FF_X21_Y12_N3
\trns|index[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_sck~input_o\,
	d => \trns|index[1]~34_combout\,
	asdata => \~GND~combout\,
	clrn => \ALT_INV_rst~input_o\,
	sload => \trns|LessThan0~2_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \trns|index\(1));

-- Location: LCCOMB_X21_Y12_N4
\trns|index[2]~36\ : cycloneive_lcell_comb
-- Equation(s):
-- \trns|index[2]~36_combout\ = (\trns|index\(2) & (!\trns|index[1]~35\)) # (!\trns|index\(2) & ((\trns|index[1]~35\) # (GND)))
-- \trns|index[2]~37\ = CARRY((!\trns|index[1]~35\) # (!\trns|index\(2)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \trns|index\(2),
	datad => VCC,
	cin => \trns|index[1]~35\,
	combout => \trns|index[2]~36_combout\,
	cout => \trns|index[2]~37\);

-- Location: FF_X21_Y12_N5
\trns|index[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_sck~input_o\,
	d => \trns|index[2]~36_combout\,
	asdata => \~GND~combout\,
	clrn => \ALT_INV_rst~input_o\,
	sload => \trns|LessThan0~2_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \trns|index\(2));

-- Location: LCCOMB_X21_Y12_N6
\trns|index[3]~38\ : cycloneive_lcell_comb
-- Equation(s):
-- \trns|index[3]~38_combout\ = (\trns|index\(3) & (\trns|index[2]~37\ & VCC)) # (!\trns|index\(3) & (!\trns|index[2]~37\))
-- \trns|index[3]~39\ = CARRY((!\trns|index\(3) & !\trns|index[2]~37\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100000101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \trns|index\(3),
	datad => VCC,
	cin => \trns|index[2]~37\,
	combout => \trns|index[3]~38_combout\,
	cout => \trns|index[3]~39\);

-- Location: FF_X21_Y12_N7
\trns|index[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_sck~input_o\,
	d => \trns|index[3]~38_combout\,
	asdata => \~GND~combout\,
	clrn => \ALT_INV_rst~input_o\,
	sload => \trns|LessThan0~2_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \trns|index\(3));

-- Location: LCCOMB_X21_Y12_N8
\trns|index[4]~40\ : cycloneive_lcell_comb
-- Equation(s):
-- \trns|index[4]~40_combout\ = (\trns|index\(4) & ((GND) # (!\trns|index[3]~39\))) # (!\trns|index\(4) & (\trns|index[3]~39\ $ (GND)))
-- \trns|index[4]~41\ = CARRY((\trns|index\(4)) # (!\trns|index[3]~39\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \trns|index\(4),
	datad => VCC,
	cin => \trns|index[3]~39\,
	combout => \trns|index[4]~40_combout\,
	cout => \trns|index[4]~41\);

-- Location: FF_X21_Y12_N9
\trns|index[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_sck~input_o\,
	d => \trns|index[4]~40_combout\,
	asdata => \~GND~combout\,
	clrn => \ALT_INV_rst~input_o\,
	sload => \trns|LessThan0~2_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \trns|index\(4));

-- Location: LCCOMB_X21_Y12_N10
\trns|index[5]~42\ : cycloneive_lcell_comb
-- Equation(s):
-- \trns|index[5]~42_combout\ = (\trns|index\(5) & (\trns|index[4]~41\ & VCC)) # (!\trns|index\(5) & (!\trns|index[4]~41\))
-- \trns|index[5]~43\ = CARRY((!\trns|index\(5) & !\trns|index[4]~41\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100000101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \trns|index\(5),
	datad => VCC,
	cin => \trns|index[4]~41\,
	combout => \trns|index[5]~42_combout\,
	cout => \trns|index[5]~43\);

-- Location: FF_X21_Y12_N11
\trns|index[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_sck~input_o\,
	d => \trns|index[5]~42_combout\,
	asdata => \~GND~combout\,
	clrn => \ALT_INV_rst~input_o\,
	sload => \trns|LessThan0~2_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \trns|index\(5));

-- Location: LCCOMB_X21_Y12_N12
\trns|index[6]~44\ : cycloneive_lcell_comb
-- Equation(s):
-- \trns|index[6]~44_combout\ = (\trns|index\(6) & ((GND) # (!\trns|index[5]~43\))) # (!\trns|index\(6) & (\trns|index[5]~43\ $ (GND)))
-- \trns|index[6]~45\ = CARRY((\trns|index\(6)) # (!\trns|index[5]~43\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101010101111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \trns|index\(6),
	datad => VCC,
	cin => \trns|index[5]~43\,
	combout => \trns|index[6]~44_combout\,
	cout => \trns|index[6]~45\);

-- Location: FF_X21_Y12_N13
\trns|index[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_sck~input_o\,
	d => \trns|index[6]~44_combout\,
	asdata => \~GND~combout\,
	clrn => \ALT_INV_rst~input_o\,
	sload => \trns|LessThan0~2_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \trns|index\(6));

-- Location: LCCOMB_X21_Y12_N14
\trns|index[7]~46\ : cycloneive_lcell_comb
-- Equation(s):
-- \trns|index[7]~46_combout\ = (\trns|index\(7) & (\trns|index[6]~45\ & VCC)) # (!\trns|index\(7) & (!\trns|index[6]~45\))
-- \trns|index[7]~47\ = CARRY((!\trns|index\(7) & !\trns|index[6]~45\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \trns|index\(7),
	datad => VCC,
	cin => \trns|index[6]~45\,
	combout => \trns|index[7]~46_combout\,
	cout => \trns|index[7]~47\);

-- Location: FF_X21_Y12_N15
\trns|index[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_sck~input_o\,
	d => \trns|index[7]~46_combout\,
	asdata => \~GND~combout\,
	clrn => \ALT_INV_rst~input_o\,
	sload => \trns|LessThan0~2_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \trns|index\(7));

-- Location: LCCOMB_X21_Y12_N16
\trns|index[8]~48\ : cycloneive_lcell_comb
-- Equation(s):
-- \trns|index[8]~48_combout\ = (\trns|index\(8) & ((GND) # (!\trns|index[7]~47\))) # (!\trns|index\(8) & (\trns|index[7]~47\ $ (GND)))
-- \trns|index[8]~49\ = CARRY((\trns|index\(8)) # (!\trns|index[7]~47\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \trns|index\(8),
	datad => VCC,
	cin => \trns|index[7]~47\,
	combout => \trns|index[8]~48_combout\,
	cout => \trns|index[8]~49\);

-- Location: FF_X21_Y12_N17
\trns|index[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_sck~input_o\,
	d => \trns|index[8]~48_combout\,
	asdata => \~GND~combout\,
	clrn => \ALT_INV_rst~input_o\,
	sload => \trns|LessThan0~2_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \trns|index\(8));

-- Location: LCCOMB_X21_Y12_N18
\trns|index[9]~50\ : cycloneive_lcell_comb
-- Equation(s):
-- \trns|index[9]~50_combout\ = (\trns|index\(9) & (\trns|index[8]~49\ & VCC)) # (!\trns|index\(9) & (!\trns|index[8]~49\))
-- \trns|index[9]~51\ = CARRY((!\trns|index\(9) & !\trns|index[8]~49\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \trns|index\(9),
	datad => VCC,
	cin => \trns|index[8]~49\,
	combout => \trns|index[9]~50_combout\,
	cout => \trns|index[9]~51\);

-- Location: FF_X21_Y12_N19
\trns|index[9]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_sck~input_o\,
	d => \trns|index[9]~50_combout\,
	asdata => \~GND~combout\,
	clrn => \ALT_INV_rst~input_o\,
	sload => \trns|LessThan0~2_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \trns|index\(9));

-- Location: LCCOMB_X21_Y12_N20
\trns|index[10]~52\ : cycloneive_lcell_comb
-- Equation(s):
-- \trns|index[10]~52_combout\ = (\trns|index\(10) & ((GND) # (!\trns|index[9]~51\))) # (!\trns|index\(10) & (\trns|index[9]~51\ $ (GND)))
-- \trns|index[10]~53\ = CARRY((\trns|index\(10)) # (!\trns|index[9]~51\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \trns|index\(10),
	datad => VCC,
	cin => \trns|index[9]~51\,
	combout => \trns|index[10]~52_combout\,
	cout => \trns|index[10]~53\);

-- Location: FF_X21_Y12_N21
\trns|index[10]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_sck~input_o\,
	d => \trns|index[10]~52_combout\,
	asdata => \~GND~combout\,
	clrn => \ALT_INV_rst~input_o\,
	sload => \trns|LessThan0~2_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \trns|index\(10));

-- Location: LCCOMB_X21_Y12_N22
\trns|index[11]~54\ : cycloneive_lcell_comb
-- Equation(s):
-- \trns|index[11]~54_combout\ = (\trns|index\(11) & (\trns|index[10]~53\ & VCC)) # (!\trns|index\(11) & (!\trns|index[10]~53\))
-- \trns|index[11]~55\ = CARRY((!\trns|index\(11) & !\trns|index[10]~53\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100000101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \trns|index\(11),
	datad => VCC,
	cin => \trns|index[10]~53\,
	combout => \trns|index[11]~54_combout\,
	cout => \trns|index[11]~55\);

-- Location: FF_X21_Y12_N23
\trns|index[11]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_sck~input_o\,
	d => \trns|index[11]~54_combout\,
	asdata => \~GND~combout\,
	clrn => \ALT_INV_rst~input_o\,
	sload => \trns|LessThan0~2_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \trns|index\(11));

-- Location: LCCOMB_X21_Y12_N24
\trns|index[12]~56\ : cycloneive_lcell_comb
-- Equation(s):
-- \trns|index[12]~56_combout\ = (\trns|index\(12) & ((GND) # (!\trns|index[11]~55\))) # (!\trns|index\(12) & (\trns|index[11]~55\ $ (GND)))
-- \trns|index[12]~57\ = CARRY((\trns|index\(12)) # (!\trns|index[11]~55\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \trns|index\(12),
	datad => VCC,
	cin => \trns|index[11]~55\,
	combout => \trns|index[12]~56_combout\,
	cout => \trns|index[12]~57\);

-- Location: FF_X21_Y12_N25
\trns|index[12]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_sck~input_o\,
	d => \trns|index[12]~56_combout\,
	asdata => \~GND~combout\,
	clrn => \ALT_INV_rst~input_o\,
	sload => \trns|LessThan0~2_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \trns|index\(12));

-- Location: LCCOMB_X21_Y12_N26
\trns|index[13]~58\ : cycloneive_lcell_comb
-- Equation(s):
-- \trns|index[13]~58_combout\ = (\trns|index\(13) & (\trns|index[12]~57\ & VCC)) # (!\trns|index\(13) & (!\trns|index[12]~57\))
-- \trns|index[13]~59\ = CARRY((!\trns|index\(13) & !\trns|index[12]~57\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100000101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \trns|index\(13),
	datad => VCC,
	cin => \trns|index[12]~57\,
	combout => \trns|index[13]~58_combout\,
	cout => \trns|index[13]~59\);

-- Location: FF_X21_Y12_N27
\trns|index[13]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_sck~input_o\,
	d => \trns|index[13]~58_combout\,
	asdata => \~GND~combout\,
	clrn => \ALT_INV_rst~input_o\,
	sload => \trns|LessThan0~2_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \trns|index\(13));

-- Location: LCCOMB_X21_Y12_N28
\trns|index[14]~60\ : cycloneive_lcell_comb
-- Equation(s):
-- \trns|index[14]~60_combout\ = (\trns|index\(14) & ((GND) # (!\trns|index[13]~59\))) # (!\trns|index\(14) & (\trns|index[13]~59\ $ (GND)))
-- \trns|index[14]~61\ = CARRY((\trns|index\(14)) # (!\trns|index[13]~59\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \trns|index\(14),
	datad => VCC,
	cin => \trns|index[13]~59\,
	combout => \trns|index[14]~60_combout\,
	cout => \trns|index[14]~61\);

-- Location: FF_X21_Y12_N29
\trns|index[14]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_sck~input_o\,
	d => \trns|index[14]~60_combout\,
	asdata => \~GND~combout\,
	clrn => \ALT_INV_rst~input_o\,
	sload => \trns|LessThan0~2_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \trns|index\(14));

-- Location: LCCOMB_X21_Y12_N30
\trns|index[15]~62\ : cycloneive_lcell_comb
-- Equation(s):
-- \trns|index[15]~62_combout\ = (\trns|index\(15) & (\trns|index[14]~61\ & VCC)) # (!\trns|index\(15) & (!\trns|index[14]~61\))
-- \trns|index[15]~63\ = CARRY((!\trns|index\(15) & !\trns|index[14]~61\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100000101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \trns|index\(15),
	datad => VCC,
	cin => \trns|index[14]~61\,
	combout => \trns|index[15]~62_combout\,
	cout => \trns|index[15]~63\);

-- Location: FF_X21_Y12_N31
\trns|index[15]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_sck~input_o\,
	d => \trns|index[15]~62_combout\,
	asdata => \~GND~combout\,
	clrn => \ALT_INV_rst~input_o\,
	sload => \trns|LessThan0~2_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \trns|index\(15));

-- Location: LCCOMB_X21_Y11_N0
\trns|index[16]~64\ : cycloneive_lcell_comb
-- Equation(s):
-- \trns|index[16]~64_combout\ = (\trns|index\(16) & ((GND) # (!\trns|index[15]~63\))) # (!\trns|index\(16) & (\trns|index[15]~63\ $ (GND)))
-- \trns|index[16]~65\ = CARRY((\trns|index\(16)) # (!\trns|index[15]~63\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \trns|index\(16),
	datad => VCC,
	cin => \trns|index[15]~63\,
	combout => \trns|index[16]~64_combout\,
	cout => \trns|index[16]~65\);

-- Location: FF_X21_Y11_N1
\trns|index[16]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_sck~input_o\,
	d => \trns|index[16]~64_combout\,
	asdata => \~GND~combout\,
	clrn => \ALT_INV_rst~input_o\,
	sload => \trns|LessThan0~2_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \trns|index\(16));

-- Location: LCCOMB_X21_Y11_N2
\trns|index[17]~66\ : cycloneive_lcell_comb
-- Equation(s):
-- \trns|index[17]~66_combout\ = (\trns|index\(17) & (\trns|index[16]~65\ & VCC)) # (!\trns|index\(17) & (!\trns|index[16]~65\))
-- \trns|index[17]~67\ = CARRY((!\trns|index\(17) & !\trns|index[16]~65\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \trns|index\(17),
	datad => VCC,
	cin => \trns|index[16]~65\,
	combout => \trns|index[17]~66_combout\,
	cout => \trns|index[17]~67\);

-- Location: FF_X21_Y11_N3
\trns|index[17]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_sck~input_o\,
	d => \trns|index[17]~66_combout\,
	asdata => \~GND~combout\,
	clrn => \ALT_INV_rst~input_o\,
	sload => \trns|LessThan0~2_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \trns|index\(17));

-- Location: LCCOMB_X21_Y11_N4
\trns|index[18]~68\ : cycloneive_lcell_comb
-- Equation(s):
-- \trns|index[18]~68_combout\ = (\trns|index\(18) & ((GND) # (!\trns|index[17]~67\))) # (!\trns|index\(18) & (\trns|index[17]~67\ $ (GND)))
-- \trns|index[18]~69\ = CARRY((\trns|index\(18)) # (!\trns|index[17]~67\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \trns|index\(18),
	datad => VCC,
	cin => \trns|index[17]~67\,
	combout => \trns|index[18]~68_combout\,
	cout => \trns|index[18]~69\);

-- Location: FF_X21_Y11_N5
\trns|index[18]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_sck~input_o\,
	d => \trns|index[18]~68_combout\,
	asdata => \~GND~combout\,
	clrn => \ALT_INV_rst~input_o\,
	sload => \trns|LessThan0~2_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \trns|index\(18));

-- Location: LCCOMB_X21_Y11_N6
\trns|index[19]~70\ : cycloneive_lcell_comb
-- Equation(s):
-- \trns|index[19]~70_combout\ = (\trns|index\(19) & (\trns|index[18]~69\ & VCC)) # (!\trns|index\(19) & (!\trns|index[18]~69\))
-- \trns|index[19]~71\ = CARRY((!\trns|index\(19) & !\trns|index[18]~69\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100000101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \trns|index\(19),
	datad => VCC,
	cin => \trns|index[18]~69\,
	combout => \trns|index[19]~70_combout\,
	cout => \trns|index[19]~71\);

-- Location: FF_X21_Y11_N7
\trns|index[19]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_sck~input_o\,
	d => \trns|index[19]~70_combout\,
	asdata => \~GND~combout\,
	clrn => \ALT_INV_rst~input_o\,
	sload => \trns|LessThan0~2_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \trns|index\(19));

-- Location: LCCOMB_X21_Y11_N8
\trns|index[20]~72\ : cycloneive_lcell_comb
-- Equation(s):
-- \trns|index[20]~72_combout\ = (\trns|index\(20) & ((GND) # (!\trns|index[19]~71\))) # (!\trns|index\(20) & (\trns|index[19]~71\ $ (GND)))
-- \trns|index[20]~73\ = CARRY((\trns|index\(20)) # (!\trns|index[19]~71\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \trns|index\(20),
	datad => VCC,
	cin => \trns|index[19]~71\,
	combout => \trns|index[20]~72_combout\,
	cout => \trns|index[20]~73\);

-- Location: FF_X21_Y11_N9
\trns|index[20]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_sck~input_o\,
	d => \trns|index[20]~72_combout\,
	asdata => \~GND~combout\,
	clrn => \ALT_INV_rst~input_o\,
	sload => \trns|LessThan0~2_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \trns|index\(20));

-- Location: LCCOMB_X21_Y11_N10
\trns|index[21]~74\ : cycloneive_lcell_comb
-- Equation(s):
-- \trns|index[21]~74_combout\ = (\trns|index\(21) & (\trns|index[20]~73\ & VCC)) # (!\trns|index\(21) & (!\trns|index[20]~73\))
-- \trns|index[21]~75\ = CARRY((!\trns|index\(21) & !\trns|index[20]~73\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100000101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \trns|index\(21),
	datad => VCC,
	cin => \trns|index[20]~73\,
	combout => \trns|index[21]~74_combout\,
	cout => \trns|index[21]~75\);

-- Location: FF_X21_Y11_N11
\trns|index[21]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_sck~input_o\,
	d => \trns|index[21]~74_combout\,
	asdata => \~GND~combout\,
	clrn => \ALT_INV_rst~input_o\,
	sload => \trns|LessThan0~2_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \trns|index\(21));

-- Location: LCCOMB_X21_Y11_N12
\trns|index[22]~76\ : cycloneive_lcell_comb
-- Equation(s):
-- \trns|index[22]~76_combout\ = (\trns|index\(22) & ((GND) # (!\trns|index[21]~75\))) # (!\trns|index\(22) & (\trns|index[21]~75\ $ (GND)))
-- \trns|index[22]~77\ = CARRY((\trns|index\(22)) # (!\trns|index[21]~75\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101010101111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \trns|index\(22),
	datad => VCC,
	cin => \trns|index[21]~75\,
	combout => \trns|index[22]~76_combout\,
	cout => \trns|index[22]~77\);

-- Location: FF_X21_Y11_N13
\trns|index[22]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_sck~input_o\,
	d => \trns|index[22]~76_combout\,
	asdata => \~GND~combout\,
	clrn => \ALT_INV_rst~input_o\,
	sload => \trns|LessThan0~2_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \trns|index\(22));

-- Location: LCCOMB_X21_Y11_N14
\trns|index[23]~78\ : cycloneive_lcell_comb
-- Equation(s):
-- \trns|index[23]~78_combout\ = (\trns|index\(23) & (\trns|index[22]~77\ & VCC)) # (!\trns|index\(23) & (!\trns|index[22]~77\))
-- \trns|index[23]~79\ = CARRY((!\trns|index\(23) & !\trns|index[22]~77\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \trns|index\(23),
	datad => VCC,
	cin => \trns|index[22]~77\,
	combout => \trns|index[23]~78_combout\,
	cout => \trns|index[23]~79\);

-- Location: FF_X21_Y11_N15
\trns|index[23]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_sck~input_o\,
	d => \trns|index[23]~78_combout\,
	asdata => \~GND~combout\,
	clrn => \ALT_INV_rst~input_o\,
	sload => \trns|LessThan0~2_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \trns|index\(23));

-- Location: LCCOMB_X21_Y11_N16
\trns|index[24]~80\ : cycloneive_lcell_comb
-- Equation(s):
-- \trns|index[24]~80_combout\ = (\trns|index\(24) & ((GND) # (!\trns|index[23]~79\))) # (!\trns|index\(24) & (\trns|index[23]~79\ $ (GND)))
-- \trns|index[24]~81\ = CARRY((\trns|index\(24)) # (!\trns|index[23]~79\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \trns|index\(24),
	datad => VCC,
	cin => \trns|index[23]~79\,
	combout => \trns|index[24]~80_combout\,
	cout => \trns|index[24]~81\);

-- Location: FF_X21_Y11_N17
\trns|index[24]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_sck~input_o\,
	d => \trns|index[24]~80_combout\,
	asdata => \~GND~combout\,
	clrn => \ALT_INV_rst~input_o\,
	sload => \trns|LessThan0~2_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \trns|index\(24));

-- Location: LCCOMB_X21_Y11_N18
\trns|index[25]~82\ : cycloneive_lcell_comb
-- Equation(s):
-- \trns|index[25]~82_combout\ = (\trns|index\(25) & (\trns|index[24]~81\ & VCC)) # (!\trns|index\(25) & (!\trns|index[24]~81\))
-- \trns|index[25]~83\ = CARRY((!\trns|index\(25) & !\trns|index[24]~81\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \trns|index\(25),
	datad => VCC,
	cin => \trns|index[24]~81\,
	combout => \trns|index[25]~82_combout\,
	cout => \trns|index[25]~83\);

-- Location: FF_X21_Y11_N19
\trns|index[25]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_sck~input_o\,
	d => \trns|index[25]~82_combout\,
	asdata => \~GND~combout\,
	clrn => \ALT_INV_rst~input_o\,
	sload => \trns|LessThan0~2_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \trns|index\(25));

-- Location: LCCOMB_X21_Y11_N20
\trns|index[26]~84\ : cycloneive_lcell_comb
-- Equation(s):
-- \trns|index[26]~84_combout\ = (\trns|index\(26) & ((GND) # (!\trns|index[25]~83\))) # (!\trns|index\(26) & (\trns|index[25]~83\ $ (GND)))
-- \trns|index[26]~85\ = CARRY((\trns|index\(26)) # (!\trns|index[25]~83\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \trns|index\(26),
	datad => VCC,
	cin => \trns|index[25]~83\,
	combout => \trns|index[26]~84_combout\,
	cout => \trns|index[26]~85\);

-- Location: FF_X21_Y11_N21
\trns|index[26]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_sck~input_o\,
	d => \trns|index[26]~84_combout\,
	asdata => \~GND~combout\,
	clrn => \ALT_INV_rst~input_o\,
	sload => \trns|LessThan0~2_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \trns|index\(26));

-- Location: LCCOMB_X21_Y11_N22
\trns|index[27]~86\ : cycloneive_lcell_comb
-- Equation(s):
-- \trns|index[27]~86_combout\ = (\trns|index\(27) & (\trns|index[26]~85\ & VCC)) # (!\trns|index\(27) & (!\trns|index[26]~85\))
-- \trns|index[27]~87\ = CARRY((!\trns|index\(27) & !\trns|index[26]~85\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100000101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \trns|index\(27),
	datad => VCC,
	cin => \trns|index[26]~85\,
	combout => \trns|index[27]~86_combout\,
	cout => \trns|index[27]~87\);

-- Location: FF_X21_Y11_N23
\trns|index[27]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_sck~input_o\,
	d => \trns|index[27]~86_combout\,
	asdata => \~GND~combout\,
	clrn => \ALT_INV_rst~input_o\,
	sload => \trns|LessThan0~2_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \trns|index\(27));

-- Location: LCCOMB_X21_Y11_N24
\trns|index[28]~88\ : cycloneive_lcell_comb
-- Equation(s):
-- \trns|index[28]~88_combout\ = (\trns|index\(28) & ((GND) # (!\trns|index[27]~87\))) # (!\trns|index\(28) & (\trns|index[27]~87\ $ (GND)))
-- \trns|index[28]~89\ = CARRY((\trns|index\(28)) # (!\trns|index[27]~87\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \trns|index\(28),
	datad => VCC,
	cin => \trns|index[27]~87\,
	combout => \trns|index[28]~88_combout\,
	cout => \trns|index[28]~89\);

-- Location: FF_X21_Y11_N25
\trns|index[28]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_sck~input_o\,
	d => \trns|index[28]~88_combout\,
	asdata => \~GND~combout\,
	clrn => \ALT_INV_rst~input_o\,
	sload => \trns|LessThan0~2_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \trns|index\(28));

-- Location: LCCOMB_X21_Y11_N26
\trns|index[29]~90\ : cycloneive_lcell_comb
-- Equation(s):
-- \trns|index[29]~90_combout\ = (\trns|index\(29) & (\trns|index[28]~89\ & VCC)) # (!\trns|index\(29) & (!\trns|index[28]~89\))
-- \trns|index[29]~91\ = CARRY((!\trns|index\(29) & !\trns|index[28]~89\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100000101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \trns|index\(29),
	datad => VCC,
	cin => \trns|index[28]~89\,
	combout => \trns|index[29]~90_combout\,
	cout => \trns|index[29]~91\);

-- Location: FF_X21_Y11_N27
\trns|index[29]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_sck~input_o\,
	d => \trns|index[29]~90_combout\,
	asdata => \~GND~combout\,
	clrn => \ALT_INV_rst~input_o\,
	sload => \trns|LessThan0~2_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \trns|index\(29));

-- Location: LCCOMB_X21_Y11_N28
\trns|index[30]~92\ : cycloneive_lcell_comb
-- Equation(s):
-- \trns|index[30]~92_combout\ = (\trns|index\(30) & ((GND) # (!\trns|index[29]~91\))) # (!\trns|index\(30) & (\trns|index[29]~91\ $ (GND)))
-- \trns|index[30]~93\ = CARRY((\trns|index\(30)) # (!\trns|index[29]~91\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \trns|index\(30),
	datad => VCC,
	cin => \trns|index[29]~91\,
	combout => \trns|index[30]~92_combout\,
	cout => \trns|index[30]~93\);

-- Location: FF_X21_Y11_N29
\trns|index[30]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_sck~input_o\,
	d => \trns|index[30]~92_combout\,
	asdata => \~GND~combout\,
	clrn => \ALT_INV_rst~input_o\,
	sload => \trns|LessThan0~2_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \trns|index\(30));

-- Location: LCCOMB_X21_Y11_N30
\trns|index[31]~94\ : cycloneive_lcell_comb
-- Equation(s):
-- \trns|index[31]~94_combout\ = \trns|index\(31) $ (!\trns|index[30]~93\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010110100101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \trns|index\(31),
	cin => \trns|index[30]~93\,
	combout => \trns|index[31]~94_combout\);

-- Location: FF_X21_Y11_N31
\trns|index[31]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_sck~input_o\,
	d => \trns|index[31]~94_combout\,
	asdata => \~GND~combout\,
	clrn => \ALT_INV_rst~input_o\,
	sload => \trns|LessThan0~2_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \trns|index\(31));

-- Location: LCCOMB_X22_Y12_N18
\busy~18\ : cycloneive_lcell_comb
-- Equation(s):
-- \busy~18_combout\ = (!\trns|index\(29) & (!\trns|index\(27) & (!\trns|index\(28) & !\trns|index\(30))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \trns|index\(29),
	datab => \trns|index\(27),
	datac => \trns|index\(28),
	datad => \trns|index\(30),
	combout => \busy~18_combout\);

-- Location: LCCOMB_X22_Y12_N8
\trns|LessThan0~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \trns|LessThan0~0_combout\ = (((!\busy~18_combout\) # (!\trns|index\(2))) # (!\trns|index\(1))) # (!\trns|index\(0))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111111111111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \trns|index\(0),
	datab => \trns|index\(1),
	datac => \trns|index\(2),
	datad => \busy~18_combout\,
	combout => \trns|LessThan0~0_combout\);

-- Location: LCCOMB_X22_Y12_N26
\busy~11\ : cycloneive_lcell_comb
-- Equation(s):
-- \busy~11_combout\ = (!\trns|index\(5) & (!\trns|index\(3) & (!\trns|index\(4) & !\trns|index\(6))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \trns|index\(5),
	datab => \trns|index\(3),
	datac => \trns|index\(4),
	datad => \trns|index\(6),
	combout => \busy~11_combout\);

-- Location: LCCOMB_X22_Y12_N20
\busy~12\ : cycloneive_lcell_comb
-- Equation(s):
-- \busy~12_combout\ = (!\trns|index\(7) & (!\trns|index\(9) & (!\trns|index\(8) & !\trns|index\(10))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \trns|index\(7),
	datab => \trns|index\(9),
	datac => \trns|index\(8),
	datad => \trns|index\(10),
	combout => \busy~12_combout\);

-- Location: LCCOMB_X22_Y12_N14
\busy~13\ : cycloneive_lcell_comb
-- Equation(s):
-- \busy~13_combout\ = (!\trns|index\(13) & (!\trns|index\(14) & (!\trns|index\(12) & !\trns|index\(11))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \trns|index\(13),
	datab => \trns|index\(14),
	datac => \trns|index\(12),
	datad => \trns|index\(11),
	combout => \busy~13_combout\);

-- Location: LCCOMB_X22_Y12_N0
\busy~14\ : cycloneive_lcell_comb
-- Equation(s):
-- \busy~14_combout\ = (!\trns|index\(17) & (!\trns|index\(15) & (!\trns|index\(18) & !\trns|index\(16))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \trns|index\(17),
	datab => \trns|index\(15),
	datac => \trns|index\(18),
	datad => \trns|index\(16),
	combout => \busy~14_combout\);

-- Location: LCCOMB_X22_Y12_N22
\busy~15\ : cycloneive_lcell_comb
-- Equation(s):
-- \busy~15_combout\ = (\busy~11_combout\ & (\busy~12_combout\ & (\busy~13_combout\ & \busy~14_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \busy~11_combout\,
	datab => \busy~12_combout\,
	datac => \busy~13_combout\,
	datad => \busy~14_combout\,
	combout => \busy~15_combout\);

-- Location: LCCOMB_X22_Y12_N4
\busy~16\ : cycloneive_lcell_comb
-- Equation(s):
-- \busy~16_combout\ = (!\trns|index\(20) & (!\trns|index\(19) & (!\trns|index\(21) & !\trns|index\(22))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \trns|index\(20),
	datab => \trns|index\(19),
	datac => \trns|index\(21),
	datad => \trns|index\(22),
	combout => \busy~16_combout\);

-- Location: LCCOMB_X22_Y11_N12
\busy~17\ : cycloneive_lcell_comb
-- Equation(s):
-- \busy~17_combout\ = (!\trns|index\(23) & (!\trns|index\(25) & (!\trns|index\(24) & !\trns|index\(26))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \trns|index\(23),
	datab => \trns|index\(25),
	datac => \trns|index\(24),
	datad => \trns|index\(26),
	combout => \busy~17_combout\);

-- Location: LCCOMB_X22_Y12_N10
\trns|LessThan0~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \trns|LessThan0~1_combout\ = (!\busy~17_combout\) # (!\busy~16_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111111111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \busy~16_combout\,
	datad => \busy~17_combout\,
	combout => \trns|LessThan0~1_combout\);

-- Location: LCCOMB_X22_Y12_N24
\trns|LessThan0~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \trns|LessThan0~2_combout\ = (\trns|index\(31)) # ((!\trns|LessThan0~0_combout\ & (\busy~15_combout\ & !\trns|LessThan0~1_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101010111010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \trns|index\(31),
	datab => \trns|LessThan0~0_combout\,
	datac => \busy~15_combout\,
	datad => \trns|LessThan0~1_combout\,
	combout => \trns|LessThan0~2_combout\);

-- Location: FF_X21_Y12_N1
\trns|index[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_sck~input_o\,
	d => \trns|index[0]~32_combout\,
	asdata => \~GND~combout\,
	clrn => \ALT_INV_rst~input_o\,
	sload => \trns|LessThan0~2_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \trns|index\(0));

-- Location: LCCOMB_X17_Y18_N20
\recv|Decoder0~7\ : cycloneive_lcell_comb
-- Equation(s):
-- \recv|Decoder0~7_combout\ = (!\recv|Decoder0~0_combout\ & (!\recv|index\(2) & (\recv|index\(0) & \recv|index\(1))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \recv|Decoder0~0_combout\,
	datab => \recv|index\(2),
	datac => \recv|index\(0),
	datad => \recv|index\(1),
	combout => \recv|Decoder0~7_combout\);

-- Location: LCCOMB_X17_Y18_N12
\recv|data_temp[4]~6\ : cycloneive_lcell_comb
-- Equation(s):
-- \recv|data_temp[4]~6_combout\ = (\recv|Decoder0~7_combout\ & (\mosi~input_o\)) # (!\recv|Decoder0~7_combout\ & ((\recv|data_temp\(4))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \mosi~input_o\,
	datac => \recv|data_temp\(4),
	datad => \recv|Decoder0~7_combout\,
	combout => \recv|data_temp[4]~6_combout\);

-- Location: FF_X17_Y18_N13
\recv|data_temp[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \sck~input_o\,
	d => \recv|data_temp[4]~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \recv|data_temp\(4));

-- Location: LCCOMB_X17_Y18_N10
\recv|Decoder0~6\ : cycloneive_lcell_comb
-- Equation(s):
-- \recv|Decoder0~6_combout\ = (!\recv|Decoder0~0_combout\ & (!\recv|index\(2) & (\recv|index\(0) & !\recv|index\(1))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \recv|Decoder0~0_combout\,
	datab => \recv|index\(2),
	datac => \recv|index\(0),
	datad => \recv|index\(1),
	combout => \recv|Decoder0~6_combout\);

-- Location: LCCOMB_X17_Y18_N22
\recv|data_temp[6]~5\ : cycloneive_lcell_comb
-- Equation(s):
-- \recv|data_temp[6]~5_combout\ = (\recv|Decoder0~6_combout\ & (\mosi~input_o\)) # (!\recv|Decoder0~6_combout\ & ((\recv|data_temp\(6))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \mosi~input_o\,
	datac => \recv|data_temp\(6),
	datad => \recv|Decoder0~6_combout\,
	combout => \recv|data_temp[6]~5_combout\);

-- Location: FF_X17_Y18_N23
\recv|data_temp[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \sck~input_o\,
	d => \recv|data_temp[6]~5_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \recv|data_temp\(6));

-- Location: LCCOMB_X18_Y18_N12
\trns|Mux0~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \trns|Mux0~2_combout\ = (\trns|index\(1) & (\recv|data_temp\(4) & (\trns|index\(0)))) # (!\trns|index\(1) & (((\recv|data_temp\(6)) # (!\trns|index\(0)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011001110000011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \recv|data_temp\(4),
	datab => \trns|index\(1),
	datac => \trns|index\(0),
	datad => \recv|data_temp\(6),
	combout => \trns|Mux0~2_combout\);

-- Location: LCCOMB_X18_Y18_N22
\trns|Mux0~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \trns|Mux0~3_combout\ = (\trns|index\(0) & (((\trns|Mux0~2_combout\)))) # (!\trns|index\(0) & ((\trns|Mux0~2_combout\ & (\recv|data_temp\(7))) # (!\trns|Mux0~2_combout\ & ((\recv|data_temp\(5))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \recv|data_temp\(7),
	datab => \recv|data_temp\(5),
	datac => \trns|index\(0),
	datad => \trns|Mux0~2_combout\,
	combout => \trns|Mux0~3_combout\);

-- Location: LCCOMB_X21_Y18_N16
\recv|Decoder0~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \recv|Decoder0~1_combout\ = (!\recv|Decoder0~0_combout\ & (\recv|index\(0) & (\recv|index\(2) & !\recv|index\(1))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000001000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \recv|Decoder0~0_combout\,
	datab => \recv|index\(0),
	datac => \recv|index\(2),
	datad => \recv|index\(1),
	combout => \recv|Decoder0~1_combout\);

-- Location: LCCOMB_X12_Y18_N8
\recv|data_temp[2]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \recv|data_temp[2]~0_combout\ = (\recv|Decoder0~1_combout\ & (\mosi~input_o\)) # (!\recv|Decoder0~1_combout\ & ((\recv|data_temp\(2))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \mosi~input_o\,
	datac => \recv|data_temp\(2),
	datad => \recv|Decoder0~1_combout\,
	combout => \recv|data_temp[2]~0_combout\);

-- Location: FF_X12_Y18_N9
\recv|data_temp[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \sck~input_o\,
	d => \recv|data_temp[2]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \recv|data_temp\(2));

-- Location: LCCOMB_X17_Y18_N2
\recv|Decoder0~4\ : cycloneive_lcell_comb
-- Equation(s):
-- \recv|Decoder0~4_combout\ = (!\recv|Decoder0~0_combout\ & (\recv|index\(2) & (!\recv|index\(0) & !\recv|index\(1))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \recv|Decoder0~0_combout\,
	datab => \recv|index\(2),
	datac => \recv|index\(0),
	datad => \recv|index\(1),
	combout => \recv|Decoder0~4_combout\);

-- Location: LCCOMB_X17_Y18_N30
\recv|data_temp[3]~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \recv|data_temp[3]~3_combout\ = (\recv|Decoder0~4_combout\ & (\mosi~input_o\)) # (!\recv|Decoder0~4_combout\ & ((\recv|data_temp\(3))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \mosi~input_o\,
	datac => \recv|data_temp\(3),
	datad => \recv|Decoder0~4_combout\,
	combout => \recv|data_temp[3]~3_combout\);

-- Location: FF_X17_Y18_N31
\recv|data_temp[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \sck~input_o\,
	d => \recv|data_temp[3]~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \recv|data_temp\(3));

-- Location: LCCOMB_X13_Y23_N2
\recv|Decoder0~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \recv|Decoder0~3_combout\ = (!\recv|Decoder0~0_combout\ & (\recv|index\(0) & (\recv|index\(1) & \recv|index\(2))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \recv|Decoder0~0_combout\,
	datab => \recv|index\(0),
	datac => \recv|index\(1),
	datad => \recv|index\(2),
	combout => \recv|Decoder0~3_combout\);

-- Location: LCCOMB_X13_Y23_N0
\recv|data_temp[0]~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \recv|data_temp[0]~2_combout\ = (\recv|Decoder0~3_combout\ & (\mosi~input_o\)) # (!\recv|Decoder0~3_combout\ & ((\recv|data_temp\(0))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \mosi~input_o\,
	datac => \recv|data_temp\(0),
	datad => \recv|Decoder0~3_combout\,
	combout => \recv|data_temp[0]~2_combout\);

-- Location: FF_X13_Y23_N1
\recv|data_temp[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \sck~input_o\,
	d => \recv|data_temp[0]~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \recv|data_temp\(0));

-- Location: LCCOMB_X17_Y18_N24
\recv|Decoder0~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \recv|Decoder0~2_combout\ = (!\recv|Decoder0~0_combout\ & (\recv|index\(2) & (!\recv|index\(0) & \recv|index\(1))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000010000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \recv|Decoder0~0_combout\,
	datab => \recv|index\(2),
	datac => \recv|index\(0),
	datad => \recv|index\(1),
	combout => \recv|Decoder0~2_combout\);

-- Location: LCCOMB_X17_Y18_N8
\recv|data_temp[1]~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \recv|data_temp[1]~1_combout\ = (\recv|Decoder0~2_combout\ & (\mosi~input_o\)) # (!\recv|Decoder0~2_combout\ & ((\recv|data_temp\(1))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \mosi~input_o\,
	datac => \recv|data_temp\(1),
	datad => \recv|Decoder0~2_combout\,
	combout => \recv|data_temp[1]~1_combout\);

-- Location: FF_X17_Y18_N9
\recv|data_temp[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \sck~input_o\,
	d => \recv|data_temp[1]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \recv|data_temp\(1));

-- Location: LCCOMB_X18_Y18_N0
\trns|Mux0~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \trns|Mux0~0_combout\ = (\trns|index\(1) & ((\trns|index\(0) & (\recv|data_temp\(0))) # (!\trns|index\(0) & ((\recv|data_temp\(1)))))) # (!\trns|index\(1) & (((!\trns|index\(0)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000111110000011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \recv|data_temp\(0),
	datab => \trns|index\(1),
	datac => \trns|index\(0),
	datad => \recv|data_temp\(1),
	combout => \trns|Mux0~0_combout\);

-- Location: LCCOMB_X18_Y18_N2
\trns|Mux0~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \trns|Mux0~1_combout\ = (\trns|index\(1) & (((\trns|Mux0~0_combout\)))) # (!\trns|index\(1) & ((\trns|Mux0~0_combout\ & ((\recv|data_temp\(3)))) # (!\trns|Mux0~0_combout\ & (\recv|data_temp\(2)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \recv|data_temp\(2),
	datab => \recv|data_temp\(3),
	datac => \trns|index\(1),
	datad => \trns|Mux0~0_combout\,
	combout => \trns|Mux0~1_combout\);

-- Location: LCCOMB_X18_Y18_N20
\trns|Mux0~4\ : cycloneive_lcell_comb
-- Equation(s):
-- \trns|Mux0~4_combout\ = (\trns|index\(2) & ((\trns|Mux0~1_combout\))) # (!\trns|index\(2) & (\trns|Mux0~3_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \trns|Mux0~3_combout\,
	datac => \trns|index\(2),
	datad => \trns|Mux0~1_combout\,
	combout => \trns|Mux0~4_combout\);

-- Location: LCCOMB_X21_Y18_N12
\busy~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \busy~0_combout\ = (\recv|index\(1)) # ((\recv|index\(2)) # ((\recv|index\(31)) # (\recv|index\(0))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111111110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \recv|index\(1),
	datab => \recv|index\(2),
	datac => \recv|index\(31),
	datad => \recv|index\(0),
	combout => \busy~0_combout\);

-- Location: LCCOMB_X22_Y12_N16
\busy~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \busy~1_combout\ = (\trns|index\(0)) # ((\trns|index\(1)) # ((\trns|index\(2)) # (\trns|index\(31))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111111110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \trns|index\(0),
	datab => \trns|index\(1),
	datac => \trns|index\(2),
	datad => \trns|index\(31),
	combout => \busy~1_combout\);

-- Location: LCCOMB_X22_Y12_N28
\busy~19\ : cycloneive_lcell_comb
-- Equation(s):
-- \busy~19_combout\ = (\busy~17_combout\ & (\busy~16_combout\ & (\busy~15_combout\ & \busy~18_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \busy~17_combout\,
	datab => \busy~16_combout\,
	datac => \busy~15_combout\,
	datad => \busy~18_combout\,
	combout => \busy~19_combout\);

-- Location: LCCOMB_X22_Y12_N30
\busy~20\ : cycloneive_lcell_comb
-- Equation(s):
-- \busy~20_combout\ = (\busy~0_combout\) # ((\busy~1_combout\) # ((!\busy~19_combout\) # (!\busy~10_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111111111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \busy~0_combout\,
	datab => \busy~1_combout\,
	datac => \busy~10_combout\,
	datad => \busy~19_combout\,
	combout => \busy~20_combout\);

ww_miso <= \miso~output_o\;

ww_busy <= \busy~output_o\;
END structure;


