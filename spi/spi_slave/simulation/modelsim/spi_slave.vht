-- Copyright (C) 1991-2013 Altera Corporation
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, Altera MegaCore Function License 
-- Agreement, or other applicable license agreement, including, 
-- without limitation, that your use is for the sole purpose of 
-- programming logic devices manufactured by Altera and sold by 
-- Altera or its authorized distributors.  Please refer to the 
-- applicable agreement for further details.

-- ***************************************************************************
-- This file contains a Vhdl test bench template that is freely editable to   
-- suit user's needs .Comments are provided in each section to help the user  
-- fill out necessary details.                                                
-- ***************************************************************************
-- Generated on "11/02/2020 11:41:40"
                                                            
-- Vhdl Test Bench template for design  :  spi_slave
-- 
-- Simulation tool : ModelSim-Altera (VHDL)
-- 

LIBRARY ieee;                                               
USE ieee.std_logic_1164.all;                                

ENTITY spi_slave_vhd_tst IS
END spi_slave_vhd_tst;
ARCHITECTURE spi_slave_arch OF spi_slave_vhd_tst IS
-- constants                                                 
-- signals                                                   
SIGNAL miso : STD_LOGIC;
SIGNAL mosi : STD_LOGIC;
SIGNAL p_data_out : STD_LOGIC_VECTOR(7 DOWNTO 0);
SIGNAL p_data_in : STD_LOGIC_VECTOR(7 DOWNTO 0);
SIGNAL sck : STD_LOGIC := '0';
SIGNAL ss : STD_LOGIC;
SIGNAL busy : STD_LOGIC;
signal rst : std_logic;
COMPONENT spi_slave
	PORT (
	miso : OUT STD_LOGIC;
	mosi : IN STD_LOGIC;
	p_data_out : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
	p_data_in : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
	sck, rst : IN STD_LOGIC;
	ss : IN STD_LOGIC;
	busy : OUT STD_LOGIC
	);
END COMPONENT;
BEGIN
	i1 : spi_slave
	PORT MAP (
-- list connections between master ports and signals
	miso => miso,
	mosi => mosi,
	p_data_out => p_data_out,
	p_data_in => p_data_in,
	sck => sck,
	ss => ss,
	busy => busy,
	rst => rst
	);
	
	
	
always : PROCESS                                              
BEGIN           

	rst <= '1';
	wait for 20 ns;
	rst <= '0';
	wait for 10 ns;
                                              
	p_data_in <= x"7B";
	mosi <= '1';
	wait for 5 ns;
	
	ss <= '1';
	wait for 5 ns;
	ss <= '0';
	wait for 16 ns;
	-- bit 0
	mosi <= '1';
	wait for 2 ns;
	-- bit 1
	sck <= not sck;
	wait for 1 ns;
	sck <= not sck;
	mosi <= '0';
	wait for 1 ns;
	-- bit 2
	sck <= not sck;
	wait for 1 ns;
	sck <= not sck;
	mosi <= '0';
	wait for 1 ns;
	-- bit 3
	sck <= not sck;
	wait for 1 ns;
	sck <= not sck;
	mosi <= '0';
	wait for 1 ns;
	-- bit 4
	sck <= not sck;
	wait for 1 ns;
	sck <= not sck;
	mosi <= '0';
	wait for 1 ns;
	-- bit 5
	sck <= not sck;
	wait for 1 ns;
	sck <= not sck;
	mosi <= '0';
	wait for 1 ns;
	-- bit 6
	sck <= not sck;
	wait for 1 ns;
	sck <= not sck;
	mosi <= '0';
	wait for 1 ns;
	-- bit 7
	sck <= not sck;
	wait for 1 ns;
	sck <= not sck;
	mosi <= '0';
	wait for 1 ns;
	sck <= not sck;
	wait for 1 ns;
	sck <= not sck;
	
	p_data_in <= x"CE";
	wait for 16 ns;
	-- bit 0
	mosi <= '1';
	wait for 2 ns;
	-- bit 1
	sck <= not sck;
	wait for 1 ns;
	sck <= not sck;
	mosi <= '0';
	wait for 1 ns;
	-- bit 2
	sck <= not sck;
	wait for 1 ns;
	sck <= not sck;
	mosi <= '1';
	wait for 1 ns;
	-- bit 3
	sck <= not sck;
	wait for 1 ns;
	sck <= not sck;
	mosi <= '0';
	wait for 1 ns;
	-- bit 4
	sck <= not sck;
	wait for 1 ns;
	sck <= not sck;
	mosi <= '1';
	wait for 1 ns;
	-- bit 5
	sck <= not sck;
	wait for 1 ns;
	sck <= not sck;
	mosi <= '0';
	wait for 1 ns;
	-- bit 6
	sck <= not sck;
	wait for 1 ns;
	sck <= not sck;
	mosi <= '1';
	wait for 1 ns;
	-- bit 7
	sck <= not sck;
	wait for 1 ns;
	sck <= not sck;
	mosi <= '0';
	wait for 1 ns;
	sck <= not sck;
	wait for 1 ns;
	sck <= not sck;
	wait for 16 ns;
	ss <= '1';

WAIT;                                                        
END PROCESS always;                                          
END spi_slave_arch;
