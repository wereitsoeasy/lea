library ieee;
use ieee.std_logic_1164.all;

entity data_bridge is
	port(
		clk, lea_busy, spi_busy, ss, rst : in std_logic;
		inst_out : out std_logic_vector(37 downto 0);
		lea_mem_in : in std_logic_vector(31 downto 0);
		spi_in : in std_logic_vector(7 downto 0);
		spi_out : out std_logic_vector(7 downto 0);
		lea_exec : out std_logic
	);
end data_bridge;

architecture data_bridge_arch of data_bridge is

	signal control_signals : std_logic_vector(6 downto 0);
	signal reg_addr_data_out : std_logic_vector(3 downto 0);
	signal cmd_data_out : std_logic_vector(7 downto 0);
	signal reg_wr_data_out, regs_wr_master : std_logic;
	signal unused_data_in : std_logic_vector(6 downto 0):= "0000000";
	signal unused_inst_out : std_logic_vector(1 downto 0);
	
	component db_regs
		port (
			rw, clk, lea_rw : in std_logic;
			write_addr, read_addr : in std_logic_vector(3 downto 0);
			data_in, status_data_in : in std_logic_vector(7 downto 0);
			data_out, command_out : out std_logic_vector(7 downto 0);
			inst_out : out std_logic_vector(39 downto 0);
			lea_mem_in : in std_logic_vector(31 downto 0)
		);
	end component;
	
	component db_control_logic
		port(
			control_signals : out std_logic_vector(6 downto 0);
			clk, ss, lea_busy, spi_busy, rst, reg_wr_in : in std_logic;
			cmd_in : in std_logic_vector(7 downto 0);
			reg_addr_in : in std_logic_vector(3 downto 0)
		);
	end component;
	
	component db_reg_single
		port(
			clk, clr, rw : in std_logic;
			data_in : in std_logic;
			data_out : out std_logic
		);
	end component;
	
	component db_reg
		generic(
			data_length : integer := 4
		);
		port(
			clk, clr, rw : in std_logic;
			data_in : in std_logic_vector(data_length-1 downto 0);
			data_out : out std_logic_vector(data_length-1 downto 0)
		);
	end component;
	
begin

	regs: db_regs
	port map(
		rw => regs_wr_master,
		clk => clk,
		lea_rw => control_signals(1),
		write_addr => reg_addr_data_out,
		read_addr => reg_addr_data_out,
		data_in => spi_in,
		status_data_in(0) => lea_busy,
		status_data_in(7 downto 1) => unused_data_in,
		data_out => spi_out,
		command_out => cmd_data_out,
		inst_out(39 downto 38) => unused_inst_out,
		inst_out(37 downto 0) => inst_out,
		lea_mem_in => lea_mem_in
	);
	
	reg_addr: db_reg
	generic map(
		data_length => 4
	)
	port map(
		clk => clk,
		clr => control_signals(4),
		rw => control_signals(5),
		data_in => spi_in(6 downto 3),
		data_out => reg_addr_data_out
	);
	
	reg_wr: db_reg_single
	port map(
		clk => clk,
		clr => control_signals(2),
		rw => control_signals(3),
		data_in => spi_in(7),
		data_out => reg_wr_data_out
	);
	
	control_logic: db_control_logic
	port map(
		control_signals => control_signals,
		clk => clk,
		ss => ss,
		lea_busy => lea_busy,
		spi_busy => spi_busy,
		rst => rst,
		cmd_in => cmd_data_out,
		reg_addr_in => reg_addr_data_out,
		reg_wr_in => reg_wr_data_out
	);

	regs_wr_master <= control_signals(6) and reg_wr_data_out;
	lea_exec <= control_signals(0);
		
end data_bridge_arch;