-- Copyright (C) 1991-2013 Altera Corporation
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, Altera MegaCore Function License 
-- Agreement, or other applicable license agreement, including, 
-- without limitation, that your use is for the sole purpose of 
-- programming logic devices manufactured by Altera and sold by 
-- Altera or its authorized distributors.  Please refer to the 
-- applicable agreement for further details.

-- ***************************************************************************
-- This file contains a Vhdl test bench template that is freely editable to   
-- suit user's needs .Comments are provided in each section to help the user  
-- fill out necessary details.                                                
-- ***************************************************************************
-- Generated on "11/09/2020 12:33:47"
                                                            
-- Vhdl Test Bench template for design  :  db_regs
-- 
-- Simulation tool : ModelSim-Altera (VHDL)
-- 

LIBRARY ieee;                                               
USE ieee.std_logic_1164.all;                                

ENTITY db_regs_vhd_tst IS
END db_regs_vhd_tst;
ARCHITECTURE db_regs_arch OF db_regs_vhd_tst IS
-- constants                                                 
-- signals                                                   
SIGNAL clk : STD_LOGIC := '0';
SIGNAL command_out : STD_LOGIC_VECTOR(7 DOWNTO 0);
SIGNAL data_in : STD_LOGIC_VECTOR(7 DOWNTO 0);
SIGNAL data_out : STD_LOGIC_VECTOR(7 DOWNTO 0);
SIGNAL inst_out : STD_LOGIC_VECTOR(39 DOWNTO 0);
SIGNAL lea_mem_in : STD_LOGIC_VECTOR(31 DOWNTO 0);
SIGNAL lea_rw : STD_LOGIC;
SIGNAL read_addr : STD_LOGIC_VECTOR(3 DOWNTO 0);
SIGNAL rw : STD_LOGIC;
SIGNAL status_data_in : STD_LOGIC_VECTOR(7 DOWNTO 0);
SIGNAL write_addr : STD_LOGIC_VECTOR(3 DOWNTO 0);
COMPONENT db_regs
	PORT (
	clk : IN STD_LOGIC;
	command_out : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
	data_in : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
	data_out : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
	inst_out : OUT STD_LOGIC_VECTOR(39 DOWNTO 0);
	lea_mem_in : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
	lea_rw : IN STD_LOGIC;
	read_addr : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
	rw : IN STD_LOGIC;
	status_data_in : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
	write_addr : IN STD_LOGIC_VECTOR(3 DOWNTO 0)
	);
END COMPONENT;
BEGIN
	i1 : db_regs
	PORT MAP (
-- list connections between master ports and signals
	clk => clk,
	command_out => command_out,
	data_in => data_in,
	data_out => data_out,
	inst_out => inst_out,
	lea_mem_in => lea_mem_in,
	lea_rw => lea_rw,
	read_addr => read_addr,
	rw => rw,
	status_data_in => status_data_in,
	write_addr => write_addr
	);                      
clk <= not clk after 1 ns;	
always : PROCESS                                              
BEGIN                                                         
	rw <= '1';
	lea_rw <= '1';
	data_in <= x"AB";
	write_addr <= x"0";
	read_addr <= x"0";
	lea_mem_in <= x"BEBEC07A";
	wait for 2 ns;
	rw <= '0';
	wait for 2 ns;
	read_addr <= x"6";
	wait for 2 ns;
	read_addr <= x"7";
	wait for 2 ns;
	read_addr <= x"8";
	wait for 2 ns;
	read_addr <= x"9";
	wait for 2 ns;
WAIT;                                                        
END PROCESS always;                                          
END db_regs_arch;
