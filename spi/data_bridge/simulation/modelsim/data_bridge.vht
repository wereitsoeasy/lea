-- Copyright (C) 1991-2013 Altera Corporation
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, Altera MegaCore Function License 
-- Agreement, or other applicable license agreement, including, 
-- without limitation, that your use is for the sole purpose of 
-- programming logic devices manufactured by Altera and sold by 
-- Altera or its authorized distributors.  Please refer to the 
-- applicable agreement for further details.

-- ***************************************************************************
-- This file contains a Vhdl test bench template that is freely editable to   
-- suit user's needs .Comments are provided in each section to help the user  
-- fill out necessary details.                                                
-- ***************************************************************************
-- Generated on "11/09/2020 15:24:07"
                                                            
-- Vhdl Test Bench template for design  :  data_bridge
-- 
-- Simulation tool : ModelSim-Altera (VHDL)
-- 

LIBRARY ieee;                                               
USE ieee.std_logic_1164.all;                                

ENTITY data_bridge_vhd_tst IS
END data_bridge_vhd_tst;
ARCHITECTURE data_bridge_arch OF data_bridge_vhd_tst IS
-- constants                                                 
-- signals                                                   
SIGNAL clk : STD_LOGIC:= '0';
SIGNAL inst_out : STD_LOGIC_VECTOR(37 DOWNTO 0);
SIGNAL lea_busy : STD_LOGIC;
SIGNAL lea_exec : STD_LOGIC;
SIGNAL lea_mem_in : STD_LOGIC_VECTOR(31 DOWNTO 0);
SIGNAL rst : STD_LOGIC;
SIGNAL spi_busy : STD_LOGIC;
SIGNAL spi_in : STD_LOGIC_VECTOR(7 DOWNTO 0);
SIGNAL spi_out : STD_LOGIC_VECTOR(7 DOWNTO 0);
SIGNAL ss : STD_LOGIC;
COMPONENT data_bridge
	PORT (
	clk : IN STD_LOGIC;
	inst_out : OUT STD_LOGIC_VECTOR(37 DOWNTO 0);
	lea_busy : IN STD_LOGIC;
	lea_exec : OUT STD_LOGIC;
	lea_mem_in : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
	rst : IN STD_LOGIC;
	spi_busy : IN STD_LOGIC;
	spi_in : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
	spi_out : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
	ss : IN STD_LOGIC
	);
END COMPONENT;
BEGIN
	i1 : data_bridge
	PORT MAP (
-- list connections between master ports and signals
	clk => clk,
	inst_out => inst_out,
	lea_busy => lea_busy,
	lea_exec => lea_exec,
	lea_mem_in => lea_mem_in,
	rst => rst,
	spi_busy => spi_busy,
	spi_in => spi_in,
	spi_out => spi_out,
	ss => ss
	);
	
	clk <= not clk after 1 ns;
	
always : PROCESS                   
BEGIN                                                         
	
	rst <= '1';
	wait for 3 ns;
	rst <= '0';
	wait for 3 ns;
	ss <= '1';
	wait for 3 ns;
	
	-- write idle instruction
	
	ss <= '0';
	wait for 3 ns;
	
	spi_busy <= '1';
	lea_busy <= '1';
	wait for 3 ns;
	
	spi_busy <= '0';
	spi_in <= x"80";
	wait for 3 ns;
	lea_mem_in <= x"BEBEC07A";
	wait for 10 ns;
	spi_busy <= '1';
	spi_in <= x"00";
	wait for 3 ns;
	spi_busy <= '0';
	wait for 3 ns;
	ss <= '1';
	wait for 6 ns;
	
	-- read command reg
	
	ss <= '0';
	wait for 3 ns;
	
	spi_busy <= '1';
	lea_busy <= '1';
	wait for 3 ns;
	
	spi_busy <= '0';
	spi_in <= x"00";
	wait for 3 ns;
	lea_mem_in <= x"BEBEC07A";
	wait for 10 ns;
	spi_busy <= '1';
	spi_in <= x"00";
	wait for 3 ns;
	spi_busy <= '0';
	wait for 3 ns;
	ss <= '1';
	wait for 6 ns;
	
	-- write 'write lea' instruction
	
	ss <= '0';
	wait for 3 ns;
	
	spi_busy <= '1';
	lea_busy <= '1';
	wait for 3 ns;
	
	spi_busy <= '0';
	spi_in <= x"80"; -- write cmd reg
	wait for 3 ns;
	lea_mem_in <= x"BEBEC07A";
	wait for 10 ns;
	spi_busy <= '1';
	spi_in <= x"02"; -- cmd reg data
	wait for 3 ns;
	spi_busy <= '0';
	wait for 3 ns;
	ss <= '1';
	wait for 6 ns;
	
	-- read lea reg 3
	
	ss <= '0';
	wait for 3 ns;
	
	spi_busy <= '1';
	lea_busy <= '1';
	wait for 3 ns;
	
	spi_busy <= '0';
	spi_in <= x"38"; -- write cmd reg
	wait for 3 ns;
	lea_mem_in <= x"BEBEC07A";
	wait for 10 ns;
	spi_busy <= '1';
	spi_in <= x"02"; -- cmd reg data
	wait for 3 ns;
	spi_busy <= '0';
	wait for 3 ns;
	ss <= '1';
	wait for 6 ns;
	
	-- write to inst reg 4
	
	ss <= '0';
	wait for 3 ns;
	
	spi_busy <= '1';
	lea_busy <= '1';
	wait for 3 ns;
	
	spi_busy <= '0';
	spi_in <= x"88"; -- write reg
	wait for 3 ns;
	wait for 10 ns;
	spi_busy <= '1';
	spi_in <= x"7B"; -- reg data
	wait for 3 ns;
	spi_busy <= '0';
	wait for 3 ns;
	ss <= '1';
	wait for 6 ns;
	
	-- read inst reg 4
	
	ss <= '0';
	wait for 3 ns;
	
	spi_busy <= '1';
	lea_busy <= '1';
	wait for 3 ns;
	
	spi_busy <= '0';
	spi_in <= x"08"; -- read reg
	wait for 3 ns;
	wait for 10 ns;
	spi_busy <= '1';
	spi_in <= x"00"; -- cmd reg data
	wait for 3 ns;
	spi_busy <= '0';
	wait for 3 ns;
	ss <= '1';
	wait for 6 ns;
	
	-- write to inst reg 3
	
	ss <= '0';
	wait for 3 ns;
	
	spi_busy <= '1';
	lea_busy <= '1';
	wait for 3 ns;
	
	spi_busy <= '0';
	spi_in <= x"90"; -- write reg
	wait for 3 ns;
	wait for 10 ns;
	spi_busy <= '1';
	spi_in <= x"9F"; -- reg data
	wait for 3 ns;
	spi_busy <= '0';
	wait for 3 ns;
	ss <= '1';
	wait for 6 ns;
	
	-- read inst reg 3
	
	ss <= '0';
	wait for 3 ns;
	
	spi_busy <= '1';
	lea_busy <= '1';
	wait for 3 ns;
	
	spi_busy <= '0';
	spi_in <= x"10"; -- read reg
	wait for 3 ns;
	wait for 10 ns;
	spi_busy <= '1';
	spi_in <= x"00"; -- cmd reg data
	wait for 3 ns;
	spi_busy <= '0';
	wait for 3 ns;
	ss <= '1';
	wait for 6 ns;

	-- write 'write lea' instruction
	
	ss <= '0';
	wait for 3 ns;
	
	spi_busy <= '1';
	lea_busy <= '1';
	wait for 3 ns;
	
	spi_busy <= '0';
	spi_in <= x"80"; -- write cmd reg
	wait for 3 ns;
	lea_mem_in <= x"778899FF";
	wait for 10 ns;
	spi_busy <= '1';
	spi_in <= x"02"; -- cmd reg data
	wait for 3 ns;
	spi_busy <= '0';
	wait for 3 ns;
	ss <= '1';
	wait for 6 ns;
	
	-- read lea reg 3
	
	ss <= '0';
	wait for 3 ns;
	
	spi_busy <= '1';
	lea_busy <= '1';
	wait for 3 ns;
	
	spi_busy <= '0';
	spi_in <= x"38"; -- read lea 3
	wait for 3 ns;
	wait for 10 ns;
	spi_busy <= '1';
	spi_in <= x"00"; -- 0x00
	wait for 3 ns;
	spi_busy <= '0';
	wait for 3 ns;
	ss <= '1';
	wait for 6 ns;
	
WAIT;                                                        
END PROCESS always;                                          
END data_bridge_arch;
