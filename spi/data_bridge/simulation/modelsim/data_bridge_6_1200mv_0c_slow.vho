-- Copyright (C) 1991-2013 Altera Corporation
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, Altera MegaCore Function License 
-- Agreement, or other applicable license agreement, including, 
-- without limitation, that your use is for the sole purpose of 
-- programming logic devices manufactured by Altera and sold by 
-- Altera or its authorized distributors.  Please refer to the 
-- applicable agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus II 32-bit"
-- VERSION "Version 13.1.0 Build 162 10/23/2013 SJ Web Edition"

-- DATE "11/22/2020 15:48:26"

-- 
-- Device: Altera EP4CGX22CF19C6 Package FBGA324
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL) only
-- 

LIBRARY ALTERA;
LIBRARY CYCLONEIV;
LIBRARY IEEE;
USE ALTERA.ALTERA_PRIMITIVES_COMPONENTS.ALL;
USE CYCLONEIV.CYCLONEIV_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	data_bridge IS
    PORT (
	clk : IN std_logic;
	lea_busy : IN std_logic;
	spi_busy : IN std_logic;
	ss : IN std_logic;
	rst : IN std_logic;
	inst_out : BUFFER std_logic_vector(37 DOWNTO 0);
	lea_mem_in : IN std_logic_vector(31 DOWNTO 0);
	spi_in : IN std_logic_vector(7 DOWNTO 0);
	spi_out : BUFFER std_logic_vector(7 DOWNTO 0);
	lea_exec : BUFFER std_logic
	);
END data_bridge;

-- Design Ports Information
-- inst_out[0]	=>  Location: PIN_L18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- inst_out[1]	=>  Location: PIN_F15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- inst_out[2]	=>  Location: PIN_D14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- inst_out[3]	=>  Location: PIN_R11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- inst_out[4]	=>  Location: PIN_A15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- inst_out[5]	=>  Location: PIN_M16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- inst_out[6]	=>  Location: PIN_E16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- inst_out[7]	=>  Location: PIN_A18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- inst_out[8]	=>  Location: PIN_D10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- inst_out[9]	=>  Location: PIN_A13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- inst_out[10]	=>  Location: PIN_D13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- inst_out[11]	=>  Location: PIN_T12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- inst_out[12]	=>  Location: PIN_C14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- inst_out[13]	=>  Location: PIN_E15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- inst_out[14]	=>  Location: PIN_D18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- inst_out[15]	=>  Location: PIN_E10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- inst_out[16]	=>  Location: PIN_T11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- inst_out[17]	=>  Location: PIN_R9,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- inst_out[18]	=>  Location: PIN_T9,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- inst_out[19]	=>  Location: PIN_N18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- inst_out[20]	=>  Location: PIN_D15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- inst_out[21]	=>  Location: PIN_F16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- inst_out[22]	=>  Location: PIN_A17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- inst_out[23]	=>  Location: PIN_A14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- inst_out[24]	=>  Location: PIN_F18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- inst_out[25]	=>  Location: PIN_B18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- inst_out[26]	=>  Location: PIN_R10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- inst_out[27]	=>  Location: PIN_P10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- inst_out[28]	=>  Location: PIN_C18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- inst_out[29]	=>  Location: PIN_A6,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- inst_out[30]	=>  Location: PIN_C10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- inst_out[31]	=>  Location: PIN_B10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- inst_out[32]	=>  Location: PIN_J17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- inst_out[33]	=>  Location: PIN_G17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- inst_out[34]	=>  Location: PIN_E18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- inst_out[35]	=>  Location: PIN_A11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- inst_out[36]	=>  Location: PIN_G18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- inst_out[37]	=>  Location: PIN_D16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- spi_out[0]	=>  Location: PIN_D9,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- spi_out[1]	=>  Location: PIN_C17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- spi_out[2]	=>  Location: PIN_E12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- spi_out[3]	=>  Location: PIN_L16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- spi_out[4]	=>  Location: PIN_V13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- spi_out[5]	=>  Location: PIN_H16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- spi_out[6]	=>  Location: PIN_V9,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- spi_out[7]	=>  Location: PIN_G15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- lea_exec	=>  Location: PIN_A8,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- spi_in[0]	=>  Location: PIN_V11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- clk	=>  Location: PIN_M10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- spi_in[1]	=>  Location: PIN_V12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- spi_in[2]	=>  Location: PIN_D12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- spi_in[3]	=>  Location: PIN_B13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- spi_in[4]	=>  Location: PIN_C11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- spi_in[5]	=>  Location: PIN_A10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- spi_in[6]	=>  Location: PIN_U12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- spi_in[7]	=>  Location: PIN_D11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- lea_mem_in[24]	=>  Location: PIN_C15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- lea_mem_in[16]	=>  Location: PIN_J16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- lea_busy	=>  Location: PIN_V15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- lea_mem_in[0]	=>  Location: PIN_D8,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- lea_mem_in[8]	=>  Location: PIN_B9,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- lea_mem_in[9]	=>  Location: PIN_U9,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- lea_mem_in[25]	=>  Location: PIN_C12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- lea_mem_in[17]	=>  Location: PIN_U13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- lea_mem_in[1]	=>  Location: PIN_M18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- lea_mem_in[10]	=>  Location: PIN_D6,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- lea_mem_in[26]	=>  Location: PIN_G16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- lea_mem_in[18]	=>  Location: PIN_M17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- lea_mem_in[2]	=>  Location: PIN_C8,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- lea_mem_in[11]	=>  Location: PIN_C9,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- lea_mem_in[27]	=>  Location: PIN_B15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- lea_mem_in[19]	=>  Location: PIN_A16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- lea_mem_in[3]	=>  Location: PIN_K15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- lea_mem_in[12]	=>  Location: PIN_U10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- lea_mem_in[28]	=>  Location: PIN_C13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- lea_mem_in[20]	=>  Location: PIN_N17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- lea_mem_in[4]	=>  Location: PIN_C7,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- lea_mem_in[13]	=>  Location: PIN_A7,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- lea_mem_in[29]	=>  Location: PIN_V14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- lea_mem_in[21]	=>  Location: PIN_B16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- lea_mem_in[5]	=>  Location: PIN_B6,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- lea_mem_in[14]	=>  Location: PIN_D7,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- lea_mem_in[30]	=>  Location: PIN_D17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- lea_mem_in[22]	=>  Location: PIN_F17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- lea_mem_in[6]	=>  Location: PIN_B7,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- lea_mem_in[15]	=>  Location: PIN_T10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- lea_mem_in[31]	=>  Location: PIN_P13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- lea_mem_in[23]	=>  Location: PIN_K16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- lea_mem_in[7]	=>  Location: PIN_A9,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ss	=>  Location: PIN_C16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- rst	=>  Location: PIN_M9,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- spi_busy	=>  Location: PIN_V10,	 I/O Standard: 2.5 V,	 Current Strength: Default


ARCHITECTURE structure OF data_bridge IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_clk : std_logic;
SIGNAL ww_lea_busy : std_logic;
SIGNAL ww_spi_busy : std_logic;
SIGNAL ww_ss : std_logic;
SIGNAL ww_rst : std_logic;
SIGNAL ww_inst_out : std_logic_vector(37 DOWNTO 0);
SIGNAL ww_lea_mem_in : std_logic_vector(31 DOWNTO 0);
SIGNAL ww_spi_in : std_logic_vector(7 DOWNTO 0);
SIGNAL ww_spi_out : std_logic_vector(7 DOWNTO 0);
SIGNAL ww_lea_exec : std_logic;
SIGNAL \rst~inputclkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \clk~inputclkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \inst_out[0]~output_o\ : std_logic;
SIGNAL \inst_out[1]~output_o\ : std_logic;
SIGNAL \inst_out[2]~output_o\ : std_logic;
SIGNAL \inst_out[3]~output_o\ : std_logic;
SIGNAL \inst_out[4]~output_o\ : std_logic;
SIGNAL \inst_out[5]~output_o\ : std_logic;
SIGNAL \inst_out[6]~output_o\ : std_logic;
SIGNAL \inst_out[7]~output_o\ : std_logic;
SIGNAL \inst_out[8]~output_o\ : std_logic;
SIGNAL \inst_out[9]~output_o\ : std_logic;
SIGNAL \inst_out[10]~output_o\ : std_logic;
SIGNAL \inst_out[11]~output_o\ : std_logic;
SIGNAL \inst_out[12]~output_o\ : std_logic;
SIGNAL \inst_out[13]~output_o\ : std_logic;
SIGNAL \inst_out[14]~output_o\ : std_logic;
SIGNAL \inst_out[15]~output_o\ : std_logic;
SIGNAL \inst_out[16]~output_o\ : std_logic;
SIGNAL \inst_out[17]~output_o\ : std_logic;
SIGNAL \inst_out[18]~output_o\ : std_logic;
SIGNAL \inst_out[19]~output_o\ : std_logic;
SIGNAL \inst_out[20]~output_o\ : std_logic;
SIGNAL \inst_out[21]~output_o\ : std_logic;
SIGNAL \inst_out[22]~output_o\ : std_logic;
SIGNAL \inst_out[23]~output_o\ : std_logic;
SIGNAL \inst_out[24]~output_o\ : std_logic;
SIGNAL \inst_out[25]~output_o\ : std_logic;
SIGNAL \inst_out[26]~output_o\ : std_logic;
SIGNAL \inst_out[27]~output_o\ : std_logic;
SIGNAL \inst_out[28]~output_o\ : std_logic;
SIGNAL \inst_out[29]~output_o\ : std_logic;
SIGNAL \inst_out[30]~output_o\ : std_logic;
SIGNAL \inst_out[31]~output_o\ : std_logic;
SIGNAL \inst_out[32]~output_o\ : std_logic;
SIGNAL \inst_out[33]~output_o\ : std_logic;
SIGNAL \inst_out[34]~output_o\ : std_logic;
SIGNAL \inst_out[35]~output_o\ : std_logic;
SIGNAL \inst_out[36]~output_o\ : std_logic;
SIGNAL \inst_out[37]~output_o\ : std_logic;
SIGNAL \spi_out[0]~output_o\ : std_logic;
SIGNAL \spi_out[1]~output_o\ : std_logic;
SIGNAL \spi_out[2]~output_o\ : std_logic;
SIGNAL \spi_out[3]~output_o\ : std_logic;
SIGNAL \spi_out[4]~output_o\ : std_logic;
SIGNAL \spi_out[5]~output_o\ : std_logic;
SIGNAL \spi_out[6]~output_o\ : std_logic;
SIGNAL \spi_out[7]~output_o\ : std_logic;
SIGNAL \lea_exec~output_o\ : std_logic;
SIGNAL \clk~input_o\ : std_logic;
SIGNAL \clk~inputclkctrl_outclk\ : std_logic;
SIGNAL \spi_in[0]~input_o\ : std_logic;
SIGNAL \spi_in[5]~input_o\ : std_logic;
SIGNAL \spi_busy~input_o\ : std_logic;
SIGNAL \spi_in[1]~input_o\ : std_logic;
SIGNAL \spi_in[3]~input_o\ : std_logic;
SIGNAL \spi_in[6]~input_o\ : std_logic;
SIGNAL \spi_in[4]~input_o\ : std_logic;
SIGNAL \regs|Decoder0~5_combout\ : std_logic;
SIGNAL \spi_in[7]~input_o\ : std_logic;
SIGNAL \reg_wr|mem~feeder_combout\ : std_logic;
SIGNAL \reg_wr|mem~q\ : std_logic;
SIGNAL \regs|dbr[5][0]~0_combout\ : std_logic;
SIGNAL \control_logic|Selector2~0_combout\ : std_logic;
SIGNAL \rst~input_o\ : std_logic;
SIGNAL \rst~inputclkctrl_outclk\ : std_logic;
SIGNAL \control_logic|state.s4~q\ : std_logic;
SIGNAL \control_logic|next_state.s5~0_combout\ : std_logic;
SIGNAL \control_logic|state.s5~q\ : std_logic;
SIGNAL \control_logic|next_state.s6~0_combout\ : std_logic;
SIGNAL \control_logic|state.s6~q\ : std_logic;
SIGNAL \regs|Decoder0~10_combout\ : std_logic;
SIGNAL \regs|dbr[0][1]~q\ : std_logic;
SIGNAL \regs|dbr[0][0]~q\ : std_logic;
SIGNAL \regs|dbr[0][7]~feeder_combout\ : std_logic;
SIGNAL \regs|dbr[0][7]~q\ : std_logic;
SIGNAL \regs|dbr[0][6]~q\ : std_logic;
SIGNAL \regs|dbr[0][4]~q\ : std_logic;
SIGNAL \spi_in[2]~input_o\ : std_logic;
SIGNAL \regs|dbr[0][2]~feeder_combout\ : std_logic;
SIGNAL \regs|dbr[0][2]~q\ : std_logic;
SIGNAL \regs|dbr[0][3]~q\ : std_logic;
SIGNAL \regs|dbr[0][5]~feeder_combout\ : std_logic;
SIGNAL \regs|dbr[0][5]~q\ : std_logic;
SIGNAL \control_logic|Equal2~2_combout\ : std_logic;
SIGNAL \control_logic|Equal2~4_combout\ : std_logic;
SIGNAL \ss~input_o\ : std_logic;
SIGNAL \control_logic|Selector3~0_combout\ : std_logic;
SIGNAL \control_logic|state.s7~q\ : std_logic;
SIGNAL \control_logic|process_1~0_combout\ : std_logic;
SIGNAL \control_logic|process_1~1_combout\ : std_logic;
SIGNAL \control_logic|next_state.s8~0_combout\ : std_logic;
SIGNAL \control_logic|next_state.s9~0_combout\ : std_logic;
SIGNAL \control_logic|state.s9~q\ : std_logic;
SIGNAL \control_logic|next_state.s8~1_combout\ : std_logic;
SIGNAL \control_logic|state.s8~q\ : std_logic;
SIGNAL \control_logic|Equal2~3_combout\ : std_logic;
SIGNAL \control_logic|Selector0~0_combout\ : std_logic;
SIGNAL \control_logic|Selector0~1_combout\ : std_logic;
SIGNAL \control_logic|Selector0~2_combout\ : std_logic;
SIGNAL \control_logic|state.s0~q\ : std_logic;
SIGNAL \control_logic|Selector1~0_combout\ : std_logic;
SIGNAL \control_logic|state.s1~q\ : std_logic;
SIGNAL \control_logic|next_state.s2~0_combout\ : std_logic;
SIGNAL \control_logic|state.s2~q\ : std_logic;
SIGNAL \control_logic|next_state.s3~0_combout\ : std_logic;
SIGNAL \control_logic|state.s3~q\ : std_logic;
SIGNAL \regs|Decoder0~3_combout\ : std_logic;
SIGNAL \regs|Decoder0~4_combout\ : std_logic;
SIGNAL \regs|dbr[5][0]~q\ : std_logic;
SIGNAL \regs|dbr[5][1]~feeder_combout\ : std_logic;
SIGNAL \regs|dbr[5][1]~q\ : std_logic;
SIGNAL \regs|dbr[5][2]~q\ : std_logic;
SIGNAL \regs|dbr[5][3]~feeder_combout\ : std_logic;
SIGNAL \regs|dbr[5][3]~q\ : std_logic;
SIGNAL \regs|dbr[5][4]~feeder_combout\ : std_logic;
SIGNAL \regs|dbr[5][4]~q\ : std_logic;
SIGNAL \regs|dbr[5][5]~feeder_combout\ : std_logic;
SIGNAL \regs|dbr[5][5]~q\ : std_logic;
SIGNAL \regs|dbr[5][6]~q\ : std_logic;
SIGNAL \regs|dbr[5][7]~q\ : std_logic;
SIGNAL \regs|Decoder0~6_combout\ : std_logic;
SIGNAL \regs|dbr[4][0]~q\ : std_logic;
SIGNAL \regs|dbr[4][1]~feeder_combout\ : std_logic;
SIGNAL \regs|dbr[4][1]~q\ : std_logic;
SIGNAL \regs|dbr[4][2]~q\ : std_logic;
SIGNAL \regs|dbr[4][3]~q\ : std_logic;
SIGNAL \regs|dbr[4][4]~feeder_combout\ : std_logic;
SIGNAL \regs|dbr[4][4]~q\ : std_logic;
SIGNAL \regs|dbr[4][5]~feeder_combout\ : std_logic;
SIGNAL \regs|dbr[4][5]~q\ : std_logic;
SIGNAL \regs|dbr[4][6]~feeder_combout\ : std_logic;
SIGNAL \regs|dbr[4][6]~q\ : std_logic;
SIGNAL \regs|dbr[4][7]~q\ : std_logic;
SIGNAL \regs|Decoder0~11_combout\ : std_logic;
SIGNAL \regs|dbr[3][0]~q\ : std_logic;
SIGNAL \regs|dbr[3][1]~q\ : std_logic;
SIGNAL \regs|dbr[3][2]~feeder_combout\ : std_logic;
SIGNAL \regs|dbr[3][2]~q\ : std_logic;
SIGNAL \regs|dbr[3][3]~feeder_combout\ : std_logic;
SIGNAL \regs|dbr[3][3]~q\ : std_logic;
SIGNAL \regs|dbr[3][4]~feeder_combout\ : std_logic;
SIGNAL \regs|dbr[3][4]~q\ : std_logic;
SIGNAL \regs|dbr[3][5]~q\ : std_logic;
SIGNAL \regs|dbr[3][6]~q\ : std_logic;
SIGNAL \regs|dbr[3][7]~q\ : std_logic;
SIGNAL \regs|Decoder0~7_combout\ : std_logic;
SIGNAL \regs|Decoder0~8_combout\ : std_logic;
SIGNAL \regs|dbr[2][0]~q\ : std_logic;
SIGNAL \regs|dbr[2][1]~q\ : std_logic;
SIGNAL \regs|dbr[2][2]~q\ : std_logic;
SIGNAL \regs|dbr[2][3]~q\ : std_logic;
SIGNAL \regs|dbr[2][4]~q\ : std_logic;
SIGNAL \regs|dbr[2][5]~q\ : std_logic;
SIGNAL \regs|dbr[2][6]~q\ : std_logic;
SIGNAL \regs|dbr[2][7]~feeder_combout\ : std_logic;
SIGNAL \regs|dbr[2][7]~q\ : std_logic;
SIGNAL \regs|Decoder0~9_combout\ : std_logic;
SIGNAL \regs|dbr[1][0]~q\ : std_logic;
SIGNAL \regs|dbr[1][1]~feeder_combout\ : std_logic;
SIGNAL \regs|dbr[1][1]~q\ : std_logic;
SIGNAL \regs|dbr[1][2]~q\ : std_logic;
SIGNAL \regs|dbr[1][3]~q\ : std_logic;
SIGNAL \regs|dbr[1][4]~q\ : std_logic;
SIGNAL \regs|dbr[1][5]~q\ : std_logic;
SIGNAL \lea_mem_in[0]~input_o\ : std_logic;
SIGNAL \regs|dbr[9][0]~feeder_combout\ : std_logic;
SIGNAL \regs|dbr[9][0]~q\ : std_logic;
SIGNAL \lea_mem_in[8]~input_o\ : std_logic;
SIGNAL \regs|dbr[8][0]~q\ : std_logic;
SIGNAL \regs|data_out[0]~2_combout\ : std_logic;
SIGNAL \lea_busy~input_o\ : std_logic;
SIGNAL \regs|dbr[10][0]~q\ : std_logic;
SIGNAL \regs|data_out[0]~3_combout\ : std_logic;
SIGNAL \lea_mem_in[24]~input_o\ : std_logic;
SIGNAL \regs|dbr[6][0]~q\ : std_logic;
SIGNAL \regs|Mux7~0_combout\ : std_logic;
SIGNAL \lea_mem_in[16]~input_o\ : std_logic;
SIGNAL \regs|dbr[7][0]~q\ : std_logic;
SIGNAL \regs|Mux7~1_combout\ : std_logic;
SIGNAL \regs|Mux7~2_combout\ : std_logic;
SIGNAL \regs|Mux7~3_combout\ : std_logic;
SIGNAL \regs|data_out[0]~0_combout\ : std_logic;
SIGNAL \regs|data_out[0]~1_combout\ : std_logic;
SIGNAL \regs|data_out[0]~4_combout\ : std_logic;
SIGNAL \regs|data_out[1]~6_combout\ : std_logic;
SIGNAL \regs|data_out[1]~5_combout\ : std_logic;
SIGNAL \lea_mem_in[9]~input_o\ : std_logic;
SIGNAL \regs|dbr[8][1]~q\ : std_logic;
SIGNAL \regs|data_out[1]~9_combout\ : std_logic;
SIGNAL \lea_mem_in[25]~input_o\ : std_logic;
SIGNAL \regs|dbr[6][1]~q\ : std_logic;
SIGNAL \regs|data_out[1]~7_combout\ : std_logic;
SIGNAL \lea_mem_in[17]~input_o\ : std_logic;
SIGNAL \regs|dbr[7][1]~q\ : std_logic;
SIGNAL \regs|data_out[1]~8_combout\ : std_logic;
SIGNAL \regs|Decoder0~2_combout\ : std_logic;
SIGNAL \regs|data_out[1]~10_combout\ : std_logic;
SIGNAL \regs|data_out[1]~11_combout\ : std_logic;
SIGNAL \regs|data_out[1]~12_combout\ : std_logic;
SIGNAL \lea_mem_in[1]~input_o\ : std_logic;
SIGNAL \regs|dbr[9][1]~q\ : std_logic;
SIGNAL \regs|data_out[1]~13_combout\ : std_logic;
SIGNAL \regs|data_out[1]~14_combout\ : std_logic;
SIGNAL \regs|data_out[2]~15_combout\ : std_logic;
SIGNAL \lea_mem_in[18]~input_o\ : std_logic;
SIGNAL \regs|dbr[7][2]~q\ : std_logic;
SIGNAL \lea_mem_in[26]~input_o\ : std_logic;
SIGNAL \regs|dbr[6][2]~feeder_combout\ : std_logic;
SIGNAL \regs|dbr[6][2]~q\ : std_logic;
SIGNAL \regs|data_out[2]~16_combout\ : std_logic;
SIGNAL \regs|data_out[2]~17_combout\ : std_logic;
SIGNAL \regs|data_out[2]~18_combout\ : std_logic;
SIGNAL \regs|data_out[2]~19_combout\ : std_logic;
SIGNAL \lea_mem_in[2]~input_o\ : std_logic;
SIGNAL \regs|dbr[9][2]~q\ : std_logic;
SIGNAL \lea_mem_in[10]~input_o\ : std_logic;
SIGNAL \regs|dbr[8][2]~feeder_combout\ : std_logic;
SIGNAL \regs|dbr[8][2]~q\ : std_logic;
SIGNAL \regs|data_out[2]~20_combout\ : std_logic;
SIGNAL \regs|data_out[2]~21_combout\ : std_logic;
SIGNAL \lea_mem_in[27]~input_o\ : std_logic;
SIGNAL \regs|dbr[6][3]~q\ : std_logic;
SIGNAL \regs|data_out[3]~22_combout\ : std_logic;
SIGNAL \lea_mem_in[19]~input_o\ : std_logic;
SIGNAL \regs|dbr[7][3]~q\ : std_logic;
SIGNAL \regs|data_out[3]~23_combout\ : std_logic;
SIGNAL \regs|data_out[3]~24_combout\ : std_logic;
SIGNAL \regs|data_out[3]~25_combout\ : std_logic;
SIGNAL \lea_mem_in[11]~input_o\ : std_logic;
SIGNAL \regs|dbr[8][3]~q\ : std_logic;
SIGNAL \regs|data_out[3]~26_combout\ : std_logic;
SIGNAL \lea_mem_in[3]~input_o\ : std_logic;
SIGNAL \regs|dbr[9][3]~q\ : std_logic;
SIGNAL \regs|data_out[3]~27_combout\ : std_logic;
SIGNAL \regs|data_out[3]~28_combout\ : std_logic;
SIGNAL \lea_mem_in[12]~input_o\ : std_logic;
SIGNAL \regs|dbr[8][4]~q\ : std_logic;
SIGNAL \lea_mem_in[4]~input_o\ : std_logic;
SIGNAL \regs|dbr[9][4]~q\ : std_logic;
SIGNAL \lea_mem_in[28]~input_o\ : std_logic;
SIGNAL \regs|dbr[6][4]~feeder_combout\ : std_logic;
SIGNAL \regs|dbr[6][4]~q\ : std_logic;
SIGNAL \lea_mem_in[20]~input_o\ : std_logic;
SIGNAL \regs|dbr[7][4]~q\ : std_logic;
SIGNAL \regs|data_out[4]~29_combout\ : std_logic;
SIGNAL \regs|data_out[4]~30_combout\ : std_logic;
SIGNAL \regs|data_out[4]~31_combout\ : std_logic;
SIGNAL \regs|data_out[4]~32_combout\ : std_logic;
SIGNAL \regs|data_out[4]~33_combout\ : std_logic;
SIGNAL \regs|data_out[4]~34_combout\ : std_logic;
SIGNAL \regs|data_out[4]~35_combout\ : std_logic;
SIGNAL \lea_mem_in[13]~input_o\ : std_logic;
SIGNAL \regs|dbr[8][5]~q\ : std_logic;
SIGNAL \regs|data_out[5]~38_combout\ : std_logic;
SIGNAL \lea_mem_in[29]~input_o\ : std_logic;
SIGNAL \regs|dbr[6][5]~q\ : std_logic;
SIGNAL \regs|data_out[5]~36_combout\ : std_logic;
SIGNAL \lea_mem_in[21]~input_o\ : std_logic;
SIGNAL \regs|dbr[7][5]~q\ : std_logic;
SIGNAL \regs|data_out[5]~37_combout\ : std_logic;
SIGNAL \regs|data_out[5]~39_combout\ : std_logic;
SIGNAL \regs|data_out[5]~40_combout\ : std_logic;
SIGNAL \lea_mem_in[5]~input_o\ : std_logic;
SIGNAL \regs|dbr[9][5]~q\ : std_logic;
SIGNAL \regs|data_out[5]~41_combout\ : std_logic;
SIGNAL \regs|data_out[5]~42_combout\ : std_logic;
SIGNAL \lea_mem_in[14]~input_o\ : std_logic;
SIGNAL \regs|dbr[8][6]~q\ : std_logic;
SIGNAL \lea_mem_in[6]~input_o\ : std_logic;
SIGNAL \regs|dbr[9][6]~q\ : std_logic;
SIGNAL \regs|data_out[6]~43_combout\ : std_logic;
SIGNAL \lea_mem_in[22]~input_o\ : std_logic;
SIGNAL \regs|dbr[7][6]~q\ : std_logic;
SIGNAL \lea_mem_in[30]~input_o\ : std_logic;
SIGNAL \regs|dbr[6][6]~q\ : std_logic;
SIGNAL \regs|data_out[6]~44_combout\ : std_logic;
SIGNAL \regs|dbr[1][6]~q\ : std_logic;
SIGNAL \regs|data_out[6]~45_combout\ : std_logic;
SIGNAL \regs|data_out[6]~46_combout\ : std_logic;
SIGNAL \regs|data_out[6]~47_combout\ : std_logic;
SIGNAL \regs|data_out[6]~48_combout\ : std_logic;
SIGNAL \regs|data_out[6]~49_combout\ : std_logic;
SIGNAL \lea_mem_in[15]~input_o\ : std_logic;
SIGNAL \regs|dbr[8][7]~q\ : std_logic;
SIGNAL \lea_mem_in[31]~input_o\ : std_logic;
SIGNAL \regs|dbr[6][7]~q\ : std_logic;
SIGNAL \regs|data_out[7]~50_combout\ : std_logic;
SIGNAL \lea_mem_in[23]~input_o\ : std_logic;
SIGNAL \regs|dbr[7][7]~q\ : std_logic;
SIGNAL \regs|data_out[7]~51_combout\ : std_logic;
SIGNAL \regs|dbr[1][7]~q\ : std_logic;
SIGNAL \regs|data_out[7]~52_combout\ : std_logic;
SIGNAL \regs|data_out[7]~53_combout\ : std_logic;
SIGNAL \regs|data_out[7]~54_combout\ : std_logic;
SIGNAL \lea_mem_in[7]~input_o\ : std_logic;
SIGNAL \regs|dbr[9][7]~q\ : std_logic;
SIGNAL \regs|data_out[7]~55_combout\ : std_logic;
SIGNAL \regs|data_out[7]~56_combout\ : std_logic;
SIGNAL \reg_addr|mem\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \ALT_INV_clk~inputclkctrl_outclk\ : std_logic;
SIGNAL \ALT_INV_rst~inputclkctrl_outclk\ : std_logic;

BEGIN

ww_clk <= clk;
ww_lea_busy <= lea_busy;
ww_spi_busy <= spi_busy;
ww_ss <= ss;
ww_rst <= rst;
inst_out <= ww_inst_out;
ww_lea_mem_in <= lea_mem_in;
ww_spi_in <= spi_in;
spi_out <= ww_spi_out;
lea_exec <= ww_lea_exec;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;

\rst~inputclkctrl_INCLK_bus\ <= (vcc & vcc & vcc & \rst~input_o\);

\clk~inputclkctrl_INCLK_bus\ <= (vcc & vcc & vcc & \clk~input_o\);
\ALT_INV_clk~inputclkctrl_outclk\ <= NOT \clk~inputclkctrl_outclk\;
\ALT_INV_rst~inputclkctrl_outclk\ <= NOT \rst~inputclkctrl_outclk\;

-- Location: IOOBUF_X52_Y19_N9
\inst_out[0]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \regs|dbr[5][0]~q\,
	devoe => ww_devoe,
	o => \inst_out[0]~output_o\);

-- Location: IOOBUF_X52_Y32_N2
\inst_out[1]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \regs|dbr[5][1]~q\,
	devoe => ww_devoe,
	o => \inst_out[1]~output_o\);

-- Location: IOOBUF_X43_Y41_N9
\inst_out[2]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \regs|dbr[5][2]~q\,
	devoe => ww_devoe,
	o => \inst_out[2]~output_o\);

-- Location: IOOBUF_X31_Y0_N23
\inst_out[3]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \regs|dbr[5][3]~q\,
	devoe => ww_devoe,
	o => \inst_out[3]~output_o\);

-- Location: IOOBUF_X34_Y41_N9
\inst_out[4]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \regs|dbr[5][4]~q\,
	devoe => ww_devoe,
	o => \inst_out[4]~output_o\);

-- Location: IOOBUF_X52_Y15_N2
\inst_out[5]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \regs|dbr[5][5]~q\,
	devoe => ww_devoe,
	o => \inst_out[5]~output_o\);

-- Location: IOOBUF_X52_Y32_N23
\inst_out[6]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \regs|dbr[5][6]~q\,
	devoe => ww_devoe,
	o => \inst_out[6]~output_o\);

-- Location: IOOBUF_X46_Y41_N23
\inst_out[7]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \regs|dbr[5][7]~q\,
	devoe => ww_devoe,
	o => \inst_out[7]~output_o\);

-- Location: IOOBUF_X29_Y41_N2
\inst_out[8]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \regs|dbr[4][0]~q\,
	devoe => ww_devoe,
	o => \inst_out[8]~output_o\);

-- Location: IOOBUF_X31_Y41_N16
\inst_out[9]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \regs|dbr[4][1]~q\,
	devoe => ww_devoe,
	o => \inst_out[9]~output_o\);

-- Location: IOOBUF_X41_Y41_N16
\inst_out[10]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \regs|dbr[4][2]~q\,
	devoe => ww_devoe,
	o => \inst_out[10]~output_o\);

-- Location: IOOBUF_X31_Y0_N9
\inst_out[11]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \regs|dbr[4][3]~q\,
	devoe => ww_devoe,
	o => \inst_out[11]~output_o\);

-- Location: IOOBUF_X43_Y41_N2
\inst_out[12]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \regs|dbr[4][4]~q\,
	devoe => ww_devoe,
	o => \inst_out[12]~output_o\);

-- Location: IOOBUF_X52_Y32_N9
\inst_out[13]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \regs|dbr[4][5]~q\,
	devoe => ww_devoe,
	o => \inst_out[13]~output_o\);

-- Location: IOOBUF_X52_Y31_N9
\inst_out[14]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \regs|dbr[4][6]~q\,
	devoe => ww_devoe,
	o => \inst_out[14]~output_o\);

-- Location: IOOBUF_X29_Y41_N9
\inst_out[15]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \regs|dbr[4][7]~q\,
	devoe => ww_devoe,
	o => \inst_out[15]~output_o\);

-- Location: IOOBUF_X31_Y0_N16
\inst_out[16]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \regs|dbr[3][0]~q\,
	devoe => ww_devoe,
	o => \inst_out[16]~output_o\);

-- Location: IOOBUF_X18_Y0_N9
\inst_out[17]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \regs|dbr[3][1]~q\,
	devoe => ww_devoe,
	o => \inst_out[17]~output_o\);

-- Location: IOOBUF_X18_Y0_N2
\inst_out[18]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \regs|dbr[3][2]~q\,
	devoe => ww_devoe,
	o => \inst_out[18]~output_o\);

-- Location: IOOBUF_X52_Y16_N9
\inst_out[19]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \regs|dbr[3][3]~q\,
	devoe => ww_devoe,
	o => \inst_out[19]~output_o\);

-- Location: IOOBUF_X46_Y41_N2
\inst_out[20]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \regs|dbr[3][4]~q\,
	devoe => ww_devoe,
	o => \inst_out[20]~output_o\);

-- Location: IOOBUF_X52_Y32_N16
\inst_out[21]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \regs|dbr[3][5]~q\,
	devoe => ww_devoe,
	o => \inst_out[21]~output_o\);

-- Location: IOOBUF_X46_Y41_N16
\inst_out[22]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \regs|dbr[3][6]~q\,
	devoe => ww_devoe,
	o => \inst_out[22]~output_o\);

-- Location: IOOBUF_X34_Y41_N2
\inst_out[23]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \regs|dbr[3][7]~q\,
	devoe => ww_devoe,
	o => \inst_out[23]~output_o\);

-- Location: IOOBUF_X52_Y30_N2
\inst_out[24]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \regs|dbr[2][0]~q\,
	devoe => ww_devoe,
	o => \inst_out[24]~output_o\);

-- Location: IOOBUF_X50_Y41_N9
\inst_out[25]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \regs|dbr[2][1]~q\,
	devoe => ww_devoe,
	o => \inst_out[25]~output_o\);

-- Location: IOOBUF_X25_Y0_N2
\inst_out[26]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \regs|dbr[2][2]~q\,
	devoe => ww_devoe,
	o => \inst_out[26]~output_o\);

-- Location: IOOBUF_X25_Y0_N9
\inst_out[27]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \regs|dbr[2][3]~q\,
	devoe => ww_devoe,
	o => \inst_out[27]~output_o\);

-- Location: IOOBUF_X50_Y41_N2
\inst_out[28]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \regs|dbr[2][4]~q\,
	devoe => ww_devoe,
	o => \inst_out[28]~output_o\);

-- Location: IOOBUF_X7_Y41_N2
\inst_out[29]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \regs|dbr[2][5]~q\,
	devoe => ww_devoe,
	o => \inst_out[29]~output_o\);

-- Location: IOOBUF_X25_Y41_N2
\inst_out[30]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \regs|dbr[2][6]~q\,
	devoe => ww_devoe,
	o => \inst_out[30]~output_o\);

-- Location: IOOBUF_X21_Y41_N9
\inst_out[31]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \regs|dbr[2][7]~q\,
	devoe => ww_devoe,
	o => \inst_out[31]~output_o\);

-- Location: IOOBUF_X52_Y23_N9
\inst_out[32]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \regs|dbr[1][0]~q\,
	devoe => ww_devoe,
	o => \inst_out[32]~output_o\);

-- Location: IOOBUF_X52_Y27_N9
\inst_out[33]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \regs|dbr[1][1]~q\,
	devoe => ww_devoe,
	o => \inst_out[33]~output_o\);

-- Location: IOOBUF_X52_Y30_N9
\inst_out[34]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \regs|dbr[1][2]~q\,
	devoe => ww_devoe,
	o => \inst_out[34]~output_o\);

-- Location: IOOBUF_X23_Y41_N9
\inst_out[35]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \regs|dbr[1][3]~q\,
	devoe => ww_devoe,
	o => \inst_out[35]~output_o\);

-- Location: IOOBUF_X52_Y25_N9
\inst_out[36]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \regs|dbr[1][4]~q\,
	devoe => ww_devoe,
	o => \inst_out[36]~output_o\);

-- Location: IOOBUF_X46_Y41_N9
\inst_out[37]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \regs|dbr[1][5]~q\,
	devoe => ww_devoe,
	o => \inst_out[37]~output_o\);

-- Location: IOOBUF_X18_Y41_N9
\spi_out[0]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \regs|data_out[0]~4_combout\,
	devoe => ww_devoe,
	o => \spi_out[0]~output_o\);

-- Location: IOOBUF_X48_Y41_N9
\spi_out[1]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \regs|data_out[1]~14_combout\,
	devoe => ww_devoe,
	o => \spi_out[1]~output_o\);

-- Location: IOOBUF_X41_Y41_N23
\spi_out[2]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \regs|data_out[2]~21_combout\,
	devoe => ww_devoe,
	o => \spi_out[2]~output_o\);

-- Location: IOOBUF_X52_Y13_N9
\spi_out[3]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \regs|data_out[3]~28_combout\,
	devoe => ww_devoe,
	o => \spi_out[3]~output_o\);

-- Location: IOOBUF_X29_Y0_N2
\spi_out[4]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \regs|data_out[4]~35_combout\,
	devoe => ww_devoe,
	o => \spi_out[4]~output_o\);

-- Location: IOOBUF_X52_Y28_N2
\spi_out[5]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \regs|data_out[5]~42_combout\,
	devoe => ww_devoe,
	o => \spi_out[5]~output_o\);

-- Location: IOOBUF_X21_Y0_N9
\spi_out[6]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \regs|data_out[6]~49_combout\,
	devoe => ww_devoe,
	o => \spi_out[6]~output_o\);

-- Location: IOOBUF_X52_Y28_N9
\spi_out[7]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \regs|data_out[7]~56_combout\,
	devoe => ww_devoe,
	o => \spi_out[7]~output_o\);

-- Location: IOOBUF_X16_Y41_N2
\lea_exec~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \control_logic|state.s8~q\,
	devoe => ww_devoe,
	o => \lea_exec~output_o\);

-- Location: IOIBUF_X27_Y0_N15
\clk~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_clk,
	o => \clk~input_o\);

-- Location: CLKCTRL_G17
\clk~inputclkctrl\ : cycloneiv_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \clk~inputclkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \clk~inputclkctrl_outclk\);

-- Location: IOIBUF_X27_Y0_N8
\spi_in[0]~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_spi_in(0),
	o => \spi_in[0]~input_o\);

-- Location: IOIBUF_X23_Y41_N1
\spi_in[5]~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_spi_in(5),
	o => \spi_in[5]~input_o\);

-- Location: IOIBUF_X21_Y0_N1
\spi_busy~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_spi_busy,
	o => \spi_busy~input_o\);

-- Location: IOIBUF_X27_Y0_N1
\spi_in[1]~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_spi_in(1),
	o => \spi_in[1]~input_o\);

-- Location: IOIBUF_X31_Y41_N22
\spi_in[3]~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_spi_in(3),
	o => \spi_in[3]~input_o\);

-- Location: FF_X26_Y34_N31
\reg_addr|mem[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	asdata => \spi_in[3]~input_o\,
	sload => VCC,
	ena => \control_logic|state.s3~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \reg_addr|mem\(0));

-- Location: IOIBUF_X31_Y0_N1
\spi_in[6]~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_spi_in(6),
	o => \spi_in[6]~input_o\);

-- Location: FF_X26_Y34_N21
\reg_addr|mem[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	asdata => \spi_in[6]~input_o\,
	sload => VCC,
	ena => \control_logic|state.s3~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \reg_addr|mem\(3));

-- Location: IOIBUF_X25_Y41_N8
\spi_in[4]~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_spi_in(4),
	o => \spi_in[4]~input_o\);

-- Location: FF_X26_Y34_N25
\reg_addr|mem[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	asdata => \spi_in[4]~input_o\,
	sload => VCC,
	ena => \control_logic|state.s3~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \reg_addr|mem\(1));

-- Location: LCCOMB_X25_Y34_N14
\regs|Decoder0~5\ : cycloneiv_lcell_comb
-- Equation(s):
-- \regs|Decoder0~5_combout\ = (!\reg_addr|mem\(0) & (!\reg_addr|mem\(3) & !\reg_addr|mem\(1)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000010001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \reg_addr|mem\(0),
	datab => \reg_addr|mem\(3),
	datad => \reg_addr|mem\(1),
	combout => \regs|Decoder0~5_combout\);

-- Location: IOIBUF_X31_Y41_N1
\spi_in[7]~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_spi_in(7),
	o => \spi_in[7]~input_o\);

-- Location: LCCOMB_X26_Y34_N14
\reg_wr|mem~feeder\ : cycloneiv_lcell_comb
-- Equation(s):
-- \reg_wr|mem~feeder_combout\ = \spi_in[7]~input_o\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \spi_in[7]~input_o\,
	combout => \reg_wr|mem~feeder_combout\);

-- Location: FF_X26_Y34_N15
\reg_wr|mem\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	d => \reg_wr|mem~feeder_combout\,
	clrn => \control_logic|state.s0~q\,
	ena => \control_logic|state.s3~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \reg_wr|mem~q\);

-- Location: LCCOMB_X26_Y34_N0
\regs|dbr[5][0]~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \regs|dbr[5][0]~0_combout\ = (!\reg_addr|mem\(3) & (\reg_wr|mem~q\ & ((!\reg_addr|mem\(1)) # (!\reg_addr|mem\(2)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \reg_addr|mem\(2),
	datab => \reg_addr|mem\(3),
	datac => \reg_wr|mem~q\,
	datad => \reg_addr|mem\(1),
	combout => \regs|dbr[5][0]~0_combout\);

-- Location: LCCOMB_X25_Y34_N10
\control_logic|Selector2~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \control_logic|Selector2~0_combout\ = (\control_logic|state.s3~q\) # ((!\spi_busy~input_o\ & \control_logic|state.s4~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \spi_busy~input_o\,
	datac => \control_logic|state.s4~q\,
	datad => \control_logic|state.s3~q\,
	combout => \control_logic|Selector2~0_combout\);

-- Location: IOIBUF_X27_Y0_N22
\rst~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_rst,
	o => \rst~input_o\);

-- Location: CLKCTRL_G19
\rst~inputclkctrl\ : cycloneiv_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \rst~inputclkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \rst~inputclkctrl_outclk\);

-- Location: FF_X25_Y34_N11
\control_logic|state.s4\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \control_logic|Selector2~0_combout\,
	clrn => \ALT_INV_rst~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \control_logic|state.s4~q\);

-- Location: LCCOMB_X25_Y34_N24
\control_logic|next_state.s5~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \control_logic|next_state.s5~0_combout\ = (\spi_busy~input_o\ & ((\control_logic|state.s5~q\) # (\control_logic|state.s4~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \spi_busy~input_o\,
	datac => \control_logic|state.s5~q\,
	datad => \control_logic|state.s4~q\,
	combout => \control_logic|next_state.s5~0_combout\);

-- Location: FF_X25_Y34_N25
\control_logic|state.s5\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \control_logic|next_state.s5~0_combout\,
	clrn => \ALT_INV_rst~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \control_logic|state.s5~q\);

-- Location: LCCOMB_X25_Y34_N2
\control_logic|next_state.s6~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \control_logic|next_state.s6~0_combout\ = (!\spi_busy~input_o\ & \control_logic|state.s5~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \spi_busy~input_o\,
	datad => \control_logic|state.s5~q\,
	combout => \control_logic|next_state.s6~0_combout\);

-- Location: FF_X25_Y34_N3
\control_logic|state.s6\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \control_logic|next_state.s6~0_combout\,
	clrn => \ALT_INV_rst~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \control_logic|state.s6~q\);

-- Location: LCCOMB_X25_Y34_N12
\regs|Decoder0~10\ : cycloneiv_lcell_comb
-- Equation(s):
-- \regs|Decoder0~10_combout\ = (!\reg_addr|mem\(2) & (\regs|Decoder0~5_combout\ & (\regs|dbr[5][0]~0_combout\ & \control_logic|state.s6~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \reg_addr|mem\(2),
	datab => \regs|Decoder0~5_combout\,
	datac => \regs|dbr[5][0]~0_combout\,
	datad => \control_logic|state.s6~q\,
	combout => \regs|Decoder0~10_combout\);

-- Location: FF_X24_Y34_N9
\regs|dbr[0][1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	asdata => \spi_in[1]~input_o\,
	sload => VCC,
	ena => \regs|Decoder0~10_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \regs|dbr[0][1]~q\);

-- Location: FF_X24_Y34_N1
\regs|dbr[0][0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	asdata => \spi_in[0]~input_o\,
	sload => VCC,
	ena => \regs|Decoder0~10_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \regs|dbr[0][0]~q\);

-- Location: LCCOMB_X24_Y34_N26
\regs|dbr[0][7]~feeder\ : cycloneiv_lcell_comb
-- Equation(s):
-- \regs|dbr[0][7]~feeder_combout\ = \spi_in[7]~input_o\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \spi_in[7]~input_o\,
	combout => \regs|dbr[0][7]~feeder_combout\);

-- Location: FF_X24_Y34_N27
\regs|dbr[0][7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	d => \regs|dbr[0][7]~feeder_combout\,
	ena => \regs|Decoder0~10_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \regs|dbr[0][7]~q\);

-- Location: FF_X24_Y34_N17
\regs|dbr[0][6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	asdata => \spi_in[6]~input_o\,
	sload => VCC,
	ena => \regs|Decoder0~10_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \regs|dbr[0][6]~q\);

-- Location: FF_X24_Y34_N13
\regs|dbr[0][4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	asdata => \spi_in[4]~input_o\,
	sload => VCC,
	ena => \regs|Decoder0~10_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \regs|dbr[0][4]~q\);

-- Location: IOIBUF_X31_Y41_N8
\spi_in[2]~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_spi_in(2),
	o => \spi_in[2]~input_o\);

-- Location: LCCOMB_X24_Y34_N14
\regs|dbr[0][2]~feeder\ : cycloneiv_lcell_comb
-- Equation(s):
-- \regs|dbr[0][2]~feeder_combout\ = \spi_in[2]~input_o\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \spi_in[2]~input_o\,
	combout => \regs|dbr[0][2]~feeder_combout\);

-- Location: FF_X24_Y34_N15
\regs|dbr[0][2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	d => \regs|dbr[0][2]~feeder_combout\,
	ena => \regs|Decoder0~10_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \regs|dbr[0][2]~q\);

-- Location: FF_X24_Y34_N11
\regs|dbr[0][3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	asdata => \spi_in[3]~input_o\,
	sload => VCC,
	ena => \regs|Decoder0~10_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \regs|dbr[0][3]~q\);

-- Location: LCCOMB_X24_Y34_N2
\regs|dbr[0][5]~feeder\ : cycloneiv_lcell_comb
-- Equation(s):
-- \regs|dbr[0][5]~feeder_combout\ = \spi_in[5]~input_o\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \spi_in[5]~input_o\,
	combout => \regs|dbr[0][5]~feeder_combout\);

-- Location: FF_X24_Y34_N3
\regs|dbr[0][5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	d => \regs|dbr[0][5]~feeder_combout\,
	ena => \regs|Decoder0~10_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \regs|dbr[0][5]~q\);

-- Location: LCCOMB_X24_Y34_N10
\control_logic|Equal2~2\ : cycloneiv_lcell_comb
-- Equation(s):
-- \control_logic|Equal2~2_combout\ = (!\regs|dbr[0][4]~q\ & (!\regs|dbr[0][2]~q\ & (!\regs|dbr[0][3]~q\ & !\regs|dbr[0][5]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \regs|dbr[0][4]~q\,
	datab => \regs|dbr[0][2]~q\,
	datac => \regs|dbr[0][3]~q\,
	datad => \regs|dbr[0][5]~q\,
	combout => \control_logic|Equal2~2_combout\);

-- Location: LCCOMB_X24_Y34_N16
\control_logic|Equal2~4\ : cycloneiv_lcell_comb
-- Equation(s):
-- \control_logic|Equal2~4_combout\ = (!\regs|dbr[0][7]~q\ & (!\regs|dbr[0][6]~q\ & \control_logic|Equal2~2_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000010100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \regs|dbr[0][7]~q\,
	datac => \regs|dbr[0][6]~q\,
	datad => \control_logic|Equal2~2_combout\,
	combout => \control_logic|Equal2~4_combout\);

-- Location: IOIBUF_X48_Y41_N1
\ss~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_ss,
	o => \ss~input_o\);

-- Location: LCCOMB_X25_Y34_N26
\control_logic|Selector3~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \control_logic|Selector3~0_combout\ = (\control_logic|state.s6~q\) # ((\control_logic|state.s7~q\ & !\ss~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \control_logic|state.s6~q\,
	datac => \control_logic|state.s7~q\,
	datad => \ss~input_o\,
	combout => \control_logic|Selector3~0_combout\);

-- Location: FF_X25_Y34_N27
\control_logic|state.s7\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \control_logic|Selector3~0_combout\,
	clrn => \ALT_INV_rst~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \control_logic|state.s7~q\);

-- Location: LCCOMB_X26_Y34_N28
\control_logic|process_1~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \control_logic|process_1~0_combout\ = (!\reg_addr|mem\(0) & !\reg_addr|mem\(3))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \reg_addr|mem\(0),
	datad => \reg_addr|mem\(3),
	combout => \control_logic|process_1~0_combout\);

-- Location: LCCOMB_X26_Y34_N2
\control_logic|process_1~1\ : cycloneiv_lcell_comb
-- Equation(s):
-- \control_logic|process_1~1_combout\ = (!\reg_addr|mem\(2) & (!\reg_addr|mem\(1) & (\reg_wr|mem~q\ & \control_logic|process_1~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \reg_addr|mem\(2),
	datab => \reg_addr|mem\(1),
	datac => \reg_wr|mem~q\,
	datad => \control_logic|process_1~0_combout\,
	combout => \control_logic|process_1~1_combout\);

-- Location: LCCOMB_X26_Y34_N4
\control_logic|next_state.s8~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \control_logic|next_state.s8~0_combout\ = (\control_logic|state.s7~q\ & (\ss~input_o\ & \control_logic|process_1~1_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000100000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \control_logic|state.s7~q\,
	datab => \ss~input_o\,
	datad => \control_logic|process_1~1_combout\,
	combout => \control_logic|next_state.s8~0_combout\);

-- Location: LCCOMB_X25_Y34_N30
\control_logic|next_state.s9~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \control_logic|next_state.s9~0_combout\ = (\regs|dbr[0][1]~q\ & (!\regs|dbr[0][0]~q\ & (\control_logic|Equal2~4_combout\ & \control_logic|next_state.s8~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \regs|dbr[0][1]~q\,
	datab => \regs|dbr[0][0]~q\,
	datac => \control_logic|Equal2~4_combout\,
	datad => \control_logic|next_state.s8~0_combout\,
	combout => \control_logic|next_state.s9~0_combout\);

-- Location: FF_X25_Y34_N31
\control_logic|state.s9\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \control_logic|next_state.s9~0_combout\,
	clrn => \ALT_INV_rst~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \control_logic|state.s9~q\);

-- Location: LCCOMB_X25_Y34_N8
\control_logic|next_state.s8~1\ : cycloneiv_lcell_comb
-- Equation(s):
-- \control_logic|next_state.s8~1_combout\ = (!\regs|dbr[0][1]~q\ & (\regs|dbr[0][0]~q\ & (\control_logic|Equal2~4_combout\ & \control_logic|next_state.s8~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \regs|dbr[0][1]~q\,
	datab => \regs|dbr[0][0]~q\,
	datac => \control_logic|Equal2~4_combout\,
	datad => \control_logic|next_state.s8~0_combout\,
	combout => \control_logic|next_state.s8~1_combout\);

-- Location: FF_X25_Y34_N9
\control_logic|state.s8\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \control_logic|next_state.s8~1_combout\,
	clrn => \ALT_INV_rst~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \control_logic|state.s8~q\);

-- Location: LCCOMB_X24_Y34_N28
\control_logic|Equal2~3\ : cycloneiv_lcell_comb
-- Equation(s):
-- \control_logic|Equal2~3_combout\ = (!\regs|dbr[0][7]~q\ & !\regs|dbr[0][6]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000001010101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \regs|dbr[0][7]~q\,
	datad => \regs|dbr[0][6]~q\,
	combout => \control_logic|Equal2~3_combout\);

-- Location: LCCOMB_X24_Y34_N0
\control_logic|Selector0~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \control_logic|Selector0~0_combout\ = ((\regs|dbr[0][1]~q\ $ (!\regs|dbr[0][0]~q\)) # (!\control_logic|Equal2~3_combout\)) # (!\control_logic|Equal2~2_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101011111111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \control_logic|Equal2~2_combout\,
	datab => \regs|dbr[0][1]~q\,
	datac => \regs|dbr[0][0]~q\,
	datad => \control_logic|Equal2~3_combout\,
	combout => \control_logic|Selector0~0_combout\);

-- Location: LCCOMB_X25_Y34_N28
\control_logic|Selector0~1\ : cycloneiv_lcell_comb
-- Equation(s):
-- \control_logic|Selector0~1_combout\ = ((\control_logic|state.s7~q\ & ((\control_logic|Selector0~0_combout\) # (!\control_logic|process_1~1_combout\)))) # (!\control_logic|state.s0~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101010111011101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \control_logic|state.s0~q\,
	datab => \control_logic|state.s7~q\,
	datac => \control_logic|Selector0~0_combout\,
	datad => \control_logic|process_1~1_combout\,
	combout => \control_logic|Selector0~1_combout\);

-- Location: LCCOMB_X25_Y34_N6
\control_logic|Selector0~2\ : cycloneiv_lcell_comb
-- Equation(s):
-- \control_logic|Selector0~2_combout\ = (!\control_logic|state.s9~q\ & (!\control_logic|state.s8~q\ & ((!\control_logic|Selector0~1_combout\) # (!\ss~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000100000101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \control_logic|state.s9~q\,
	datab => \ss~input_o\,
	datac => \control_logic|state.s8~q\,
	datad => \control_logic|Selector0~1_combout\,
	combout => \control_logic|Selector0~2_combout\);

-- Location: FF_X25_Y34_N7
\control_logic|state.s0\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \control_logic|Selector0~2_combout\,
	clrn => \ALT_INV_rst~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \control_logic|state.s0~q\);

-- Location: LCCOMB_X25_Y34_N18
\control_logic|Selector1~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \control_logic|Selector1~0_combout\ = (\control_logic|state.s0~q\ & (!\spi_busy~input_o\ & (\control_logic|state.s1~q\))) # (!\control_logic|state.s0~q\ & (((!\spi_busy~input_o\ & \control_logic|state.s1~q\)) # (!\ss~input_o\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000001110101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \control_logic|state.s0~q\,
	datab => \spi_busy~input_o\,
	datac => \control_logic|state.s1~q\,
	datad => \ss~input_o\,
	combout => \control_logic|Selector1~0_combout\);

-- Location: FF_X25_Y34_N19
\control_logic|state.s1\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \control_logic|Selector1~0_combout\,
	clrn => \ALT_INV_rst~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \control_logic|state.s1~q\);

-- Location: LCCOMB_X25_Y34_N20
\control_logic|next_state.s2~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \control_logic|next_state.s2~0_combout\ = (\spi_busy~input_o\ & ((\control_logic|state.s2~q\) # (\control_logic|state.s1~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \spi_busy~input_o\,
	datac => \control_logic|state.s2~q\,
	datad => \control_logic|state.s1~q\,
	combout => \control_logic|next_state.s2~0_combout\);

-- Location: FF_X25_Y34_N21
\control_logic|state.s2\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \control_logic|next_state.s2~0_combout\,
	clrn => \ALT_INV_rst~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \control_logic|state.s2~q\);

-- Location: LCCOMB_X25_Y34_N0
\control_logic|next_state.s3~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \control_logic|next_state.s3~0_combout\ = (!\spi_busy~input_o\ & \control_logic|state.s2~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \spi_busy~input_o\,
	datad => \control_logic|state.s2~q\,
	combout => \control_logic|next_state.s3~0_combout\);

-- Location: FF_X25_Y34_N1
\control_logic|state.s3\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \control_logic|next_state.s3~0_combout\,
	clrn => \ALT_INV_rst~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \control_logic|state.s3~q\);

-- Location: FF_X26_Y34_N23
\reg_addr|mem[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	asdata => \spi_in[5]~input_o\,
	sload => VCC,
	ena => \control_logic|state.s3~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \reg_addr|mem\(2));

-- Location: LCCOMB_X25_Y34_N16
\regs|Decoder0~3\ : cycloneiv_lcell_comb
-- Equation(s):
-- \regs|Decoder0~3_combout\ = (\reg_addr|mem\(0) & (!\reg_addr|mem\(3) & (\regs|dbr[5][0]~0_combout\ & \control_logic|state.s6~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \reg_addr|mem\(0),
	datab => \reg_addr|mem\(3),
	datac => \regs|dbr[5][0]~0_combout\,
	datad => \control_logic|state.s6~q\,
	combout => \regs|Decoder0~3_combout\);

-- Location: LCCOMB_X30_Y34_N30
\regs|Decoder0~4\ : cycloneiv_lcell_comb
-- Equation(s):
-- \regs|Decoder0~4_combout\ = (\reg_addr|mem\(2) & (!\reg_addr|mem\(1) & \regs|Decoder0~3_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010001000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \reg_addr|mem\(2),
	datab => \reg_addr|mem\(1),
	datad => \regs|Decoder0~3_combout\,
	combout => \regs|Decoder0~4_combout\);

-- Location: FF_X30_Y34_N1
\regs|dbr[5][0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	asdata => \spi_in[0]~input_o\,
	sload => VCC,
	ena => \regs|Decoder0~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \regs|dbr[5][0]~q\);

-- Location: LCCOMB_X30_Y34_N6
\regs|dbr[5][1]~feeder\ : cycloneiv_lcell_comb
-- Equation(s):
-- \regs|dbr[5][1]~feeder_combout\ = \spi_in[1]~input_o\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \spi_in[1]~input_o\,
	combout => \regs|dbr[5][1]~feeder_combout\);

-- Location: FF_X30_Y34_N7
\regs|dbr[5][1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	d => \regs|dbr[5][1]~feeder_combout\,
	ena => \regs|Decoder0~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \regs|dbr[5][1]~q\);

-- Location: FF_X30_Y34_N9
\regs|dbr[5][2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	asdata => \spi_in[2]~input_o\,
	sload => VCC,
	ena => \regs|Decoder0~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \regs|dbr[5][2]~q\);

-- Location: LCCOMB_X30_Y34_N18
\regs|dbr[5][3]~feeder\ : cycloneiv_lcell_comb
-- Equation(s):
-- \regs|dbr[5][3]~feeder_combout\ = \spi_in[3]~input_o\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \spi_in[3]~input_o\,
	combout => \regs|dbr[5][3]~feeder_combout\);

-- Location: FF_X30_Y34_N19
\regs|dbr[5][3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	d => \regs|dbr[5][3]~feeder_combout\,
	ena => \regs|Decoder0~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \regs|dbr[5][3]~q\);

-- Location: LCCOMB_X30_Y34_N16
\regs|dbr[5][4]~feeder\ : cycloneiv_lcell_comb
-- Equation(s):
-- \regs|dbr[5][4]~feeder_combout\ = \spi_in[4]~input_o\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \spi_in[4]~input_o\,
	combout => \regs|dbr[5][4]~feeder_combout\);

-- Location: FF_X30_Y34_N17
\regs|dbr[5][4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	d => \regs|dbr[5][4]~feeder_combout\,
	ena => \regs|Decoder0~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \regs|dbr[5][4]~q\);

-- Location: LCCOMB_X30_Y34_N26
\regs|dbr[5][5]~feeder\ : cycloneiv_lcell_comb
-- Equation(s):
-- \regs|dbr[5][5]~feeder_combout\ = \spi_in[5]~input_o\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \spi_in[5]~input_o\,
	combout => \regs|dbr[5][5]~feeder_combout\);

-- Location: FF_X30_Y34_N27
\regs|dbr[5][5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	d => \regs|dbr[5][5]~feeder_combout\,
	ena => \regs|Decoder0~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \regs|dbr[5][5]~q\);

-- Location: FF_X30_Y34_N13
\regs|dbr[5][6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	asdata => \spi_in[6]~input_o\,
	sload => VCC,
	ena => \regs|Decoder0~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \regs|dbr[5][6]~q\);

-- Location: FF_X30_Y34_N31
\regs|dbr[5][7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	asdata => \spi_in[7]~input_o\,
	sload => VCC,
	ena => \regs|Decoder0~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \regs|dbr[5][7]~q\);

-- Location: LCCOMB_X25_Y34_N22
\regs|Decoder0~6\ : cycloneiv_lcell_comb
-- Equation(s):
-- \regs|Decoder0~6_combout\ = (\reg_addr|mem\(2) & (\regs|Decoder0~5_combout\ & (\regs|dbr[5][0]~0_combout\ & \control_logic|state.s6~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \reg_addr|mem\(2),
	datab => \regs|Decoder0~5_combout\,
	datac => \regs|dbr[5][0]~0_combout\,
	datad => \control_logic|state.s6~q\,
	combout => \regs|Decoder0~6_combout\);

-- Location: FF_X29_Y34_N1
\regs|dbr[4][0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	asdata => \spi_in[0]~input_o\,
	sload => VCC,
	ena => \regs|Decoder0~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \regs|dbr[4][0]~q\);

-- Location: LCCOMB_X29_Y34_N10
\regs|dbr[4][1]~feeder\ : cycloneiv_lcell_comb
-- Equation(s):
-- \regs|dbr[4][1]~feeder_combout\ = \spi_in[1]~input_o\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \spi_in[1]~input_o\,
	combout => \regs|dbr[4][1]~feeder_combout\);

-- Location: FF_X29_Y34_N11
\regs|dbr[4][1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	d => \regs|dbr[4][1]~feeder_combout\,
	ena => \regs|Decoder0~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \regs|dbr[4][1]~q\);

-- Location: FF_X25_Y34_N5
\regs|dbr[4][2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	asdata => \spi_in[2]~input_o\,
	sload => VCC,
	ena => \regs|Decoder0~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \regs|dbr[4][2]~q\);

-- Location: FF_X25_Y34_N15
\regs|dbr[4][3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	asdata => \spi_in[3]~input_o\,
	sload => VCC,
	ena => \regs|Decoder0~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \regs|dbr[4][3]~q\);

-- Location: LCCOMB_X29_Y34_N12
\regs|dbr[4][4]~feeder\ : cycloneiv_lcell_comb
-- Equation(s):
-- \regs|dbr[4][4]~feeder_combout\ = \spi_in[4]~input_o\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \spi_in[4]~input_o\,
	combout => \regs|dbr[4][4]~feeder_combout\);

-- Location: FF_X29_Y34_N13
\regs|dbr[4][4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	d => \regs|dbr[4][4]~feeder_combout\,
	ena => \regs|Decoder0~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \regs|dbr[4][4]~q\);

-- Location: LCCOMB_X29_Y34_N14
\regs|dbr[4][5]~feeder\ : cycloneiv_lcell_comb
-- Equation(s):
-- \regs|dbr[4][5]~feeder_combout\ = \spi_in[5]~input_o\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \spi_in[5]~input_o\,
	combout => \regs|dbr[4][5]~feeder_combout\);

-- Location: FF_X29_Y34_N15
\regs|dbr[4][5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	d => \regs|dbr[4][5]~feeder_combout\,
	ena => \regs|Decoder0~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \regs|dbr[4][5]~q\);

-- Location: LCCOMB_X29_Y34_N4
\regs|dbr[4][6]~feeder\ : cycloneiv_lcell_comb
-- Equation(s):
-- \regs|dbr[4][6]~feeder_combout\ = \spi_in[6]~input_o\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \spi_in[6]~input_o\,
	combout => \regs|dbr[4][6]~feeder_combout\);

-- Location: FF_X29_Y34_N5
\regs|dbr[4][6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	d => \regs|dbr[4][6]~feeder_combout\,
	ena => \regs|Decoder0~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \regs|dbr[4][6]~q\);

-- Location: FF_X29_Y34_N23
\regs|dbr[4][7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	asdata => \spi_in[7]~input_o\,
	sload => VCC,
	ena => \regs|Decoder0~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \regs|dbr[4][7]~q\);

-- Location: LCCOMB_X27_Y34_N12
\regs|Decoder0~11\ : cycloneiv_lcell_comb
-- Equation(s):
-- \regs|Decoder0~11_combout\ = (!\reg_addr|mem\(2) & (\reg_addr|mem\(1) & \regs|Decoder0~3_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100010000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \reg_addr|mem\(2),
	datab => \reg_addr|mem\(1),
	datad => \regs|Decoder0~3_combout\,
	combout => \regs|Decoder0~11_combout\);

-- Location: FF_X27_Y34_N13
\regs|dbr[3][0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	asdata => \spi_in[0]~input_o\,
	sload => VCC,
	ena => \regs|Decoder0~11_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \regs|dbr[3][0]~q\);

-- Location: FF_X27_Y34_N23
\regs|dbr[3][1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	asdata => \spi_in[1]~input_o\,
	sload => VCC,
	ena => \regs|Decoder0~11_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \regs|dbr[3][1]~q\);

-- Location: LCCOMB_X27_Y34_N8
\regs|dbr[3][2]~feeder\ : cycloneiv_lcell_comb
-- Equation(s):
-- \regs|dbr[3][2]~feeder_combout\ = \spi_in[2]~input_o\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \spi_in[2]~input_o\,
	combout => \regs|dbr[3][2]~feeder_combout\);

-- Location: FF_X27_Y34_N9
\regs|dbr[3][2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	d => \regs|dbr[3][2]~feeder_combout\,
	ena => \regs|Decoder0~11_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \regs|dbr[3][2]~q\);

-- Location: LCCOMB_X27_Y34_N30
\regs|dbr[3][3]~feeder\ : cycloneiv_lcell_comb
-- Equation(s):
-- \regs|dbr[3][3]~feeder_combout\ = \spi_in[3]~input_o\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \spi_in[3]~input_o\,
	combout => \regs|dbr[3][3]~feeder_combout\);

-- Location: FF_X27_Y34_N31
\regs|dbr[3][3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	d => \regs|dbr[3][3]~feeder_combout\,
	ena => \regs|Decoder0~11_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \regs|dbr[3][3]~q\);

-- Location: LCCOMB_X27_Y34_N20
\regs|dbr[3][4]~feeder\ : cycloneiv_lcell_comb
-- Equation(s):
-- \regs|dbr[3][4]~feeder_combout\ = \spi_in[4]~input_o\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \spi_in[4]~input_o\,
	combout => \regs|dbr[3][4]~feeder_combout\);

-- Location: FF_X27_Y34_N21
\regs|dbr[3][4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	d => \regs|dbr[3][4]~feeder_combout\,
	ena => \regs|Decoder0~11_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \regs|dbr[3][4]~q\);

-- Location: FF_X27_Y34_N19
\regs|dbr[3][5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	asdata => \spi_in[5]~input_o\,
	sload => VCC,
	ena => \regs|Decoder0~11_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \regs|dbr[3][5]~q\);

-- Location: FF_X27_Y34_N29
\regs|dbr[3][6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	asdata => \spi_in[6]~input_o\,
	sload => VCC,
	ena => \regs|Decoder0~11_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \regs|dbr[3][6]~q\);

-- Location: FF_X27_Y34_N27
\regs|dbr[3][7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	asdata => \spi_in[7]~input_o\,
	sload => VCC,
	ena => \regs|Decoder0~11_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \regs|dbr[3][7]~q\);

-- Location: LCCOMB_X26_Y34_N24
\regs|Decoder0~7\ : cycloneiv_lcell_comb
-- Equation(s):
-- \regs|Decoder0~7_combout\ = (!\reg_addr|mem\(2) & (\reg_addr|mem\(1) & !\reg_addr|mem\(0)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \reg_addr|mem\(2),
	datac => \reg_addr|mem\(1),
	datad => \reg_addr|mem\(0),
	combout => \regs|Decoder0~7_combout\);

-- Location: LCCOMB_X26_Y34_N18
\regs|Decoder0~8\ : cycloneiv_lcell_comb
-- Equation(s):
-- \regs|Decoder0~8_combout\ = (!\reg_addr|mem\(3) & (\regs|dbr[5][0]~0_combout\ & (\regs|Decoder0~7_combout\ & \control_logic|state.s6~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \reg_addr|mem\(3),
	datab => \regs|dbr[5][0]~0_combout\,
	datac => \regs|Decoder0~7_combout\,
	datad => \control_logic|state.s6~q\,
	combout => \regs|Decoder0~8_combout\);

-- Location: FF_X27_Y34_N5
\regs|dbr[2][0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	asdata => \spi_in[0]~input_o\,
	sload => VCC,
	ena => \regs|Decoder0~8_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \regs|dbr[2][0]~q\);

-- Location: FF_X27_Y34_N3
\regs|dbr[2][1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	asdata => \spi_in[1]~input_o\,
	sload => VCC,
	ena => \regs|Decoder0~8_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \regs|dbr[2][1]~q\);

-- Location: FF_X27_Y34_N25
\regs|dbr[2][2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	asdata => \spi_in[2]~input_o\,
	sload => VCC,
	ena => \regs|Decoder0~8_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \regs|dbr[2][2]~q\);

-- Location: FF_X26_Y34_N5
\regs|dbr[2][3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	asdata => \spi_in[3]~input_o\,
	sload => VCC,
	ena => \regs|Decoder0~8_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \regs|dbr[2][3]~q\);

-- Location: FF_X26_Y34_N11
\regs|dbr[2][4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	asdata => \spi_in[4]~input_o\,
	sload => VCC,
	ena => \regs|Decoder0~8_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \regs|dbr[2][4]~q\);

-- Location: FF_X26_Y34_N9
\regs|dbr[2][5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	asdata => \spi_in[5]~input_o\,
	sload => VCC,
	ena => \regs|Decoder0~8_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \regs|dbr[2][5]~q\);

-- Location: FF_X26_Y34_N27
\regs|dbr[2][6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	asdata => \spi_in[6]~input_o\,
	sload => VCC,
	ena => \regs|Decoder0~8_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \regs|dbr[2][6]~q\);

-- Location: LCCOMB_X26_Y34_N16
\regs|dbr[2][7]~feeder\ : cycloneiv_lcell_comb
-- Equation(s):
-- \regs|dbr[2][7]~feeder_combout\ = \spi_in[7]~input_o\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \spi_in[7]~input_o\,
	combout => \regs|dbr[2][7]~feeder_combout\);

-- Location: FF_X26_Y34_N17
\regs|dbr[2][7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	d => \regs|dbr[2][7]~feeder_combout\,
	ena => \regs|Decoder0~8_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \regs|dbr[2][7]~q\);

-- Location: LCCOMB_X25_Y34_N4
\regs|Decoder0~9\ : cycloneiv_lcell_comb
-- Equation(s):
-- \regs|Decoder0~9_combout\ = (!\reg_addr|mem\(2) & (!\reg_addr|mem\(1) & \regs|Decoder0~3_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \reg_addr|mem\(2),
	datab => \reg_addr|mem\(1),
	datad => \regs|Decoder0~3_combout\,
	combout => \regs|Decoder0~9_combout\);

-- Location: FF_X28_Y34_N21
\regs|dbr[1][0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	asdata => \spi_in[0]~input_o\,
	sload => VCC,
	ena => \regs|Decoder0~9_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \regs|dbr[1][0]~q\);

-- Location: LCCOMB_X28_Y34_N26
\regs|dbr[1][1]~feeder\ : cycloneiv_lcell_comb
-- Equation(s):
-- \regs|dbr[1][1]~feeder_combout\ = \spi_in[1]~input_o\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \spi_in[1]~input_o\,
	combout => \regs|dbr[1][1]~feeder_combout\);

-- Location: FF_X28_Y34_N27
\regs|dbr[1][1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	d => \regs|dbr[1][1]~feeder_combout\,
	ena => \regs|Decoder0~9_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \regs|dbr[1][1]~q\);

-- Location: FF_X28_Y34_N5
\regs|dbr[1][2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	asdata => \spi_in[2]~input_o\,
	sload => VCC,
	ena => \regs|Decoder0~9_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \regs|dbr[1][2]~q\);

-- Location: FF_X28_Y34_N7
\regs|dbr[1][3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	asdata => \spi_in[3]~input_o\,
	sload => VCC,
	ena => \regs|Decoder0~9_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \regs|dbr[1][3]~q\);

-- Location: FF_X28_Y34_N9
\regs|dbr[1][4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	asdata => \spi_in[4]~input_o\,
	sload => VCC,
	ena => \regs|Decoder0~9_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \regs|dbr[1][4]~q\);

-- Location: FF_X28_Y34_N3
\regs|dbr[1][5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	asdata => \spi_in[5]~input_o\,
	sload => VCC,
	ena => \regs|Decoder0~9_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \regs|dbr[1][5]~q\);

-- Location: IOIBUF_X14_Y41_N1
\lea_mem_in[0]~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_lea_mem_in(0),
	o => \lea_mem_in[0]~input_o\);

-- Location: LCCOMB_X23_Y34_N8
\regs|dbr[9][0]~feeder\ : cycloneiv_lcell_comb
-- Equation(s):
-- \regs|dbr[9][0]~feeder_combout\ = \lea_mem_in[0]~input_o\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \lea_mem_in[0]~input_o\,
	combout => \regs|dbr[9][0]~feeder_combout\);

-- Location: FF_X23_Y34_N9
\regs|dbr[9][0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	d => \regs|dbr[9][0]~feeder_combout\,
	ena => \control_logic|state.s9~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \regs|dbr[9][0]~q\);

-- Location: IOIBUF_X21_Y41_N1
\lea_mem_in[8]~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_lea_mem_in(8),
	o => \lea_mem_in[8]~input_o\);

-- Location: FF_X24_Y34_N23
\regs|dbr[8][0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	asdata => \lea_mem_in[8]~input_o\,
	sload => VCC,
	ena => \control_logic|state.s9~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \regs|dbr[8][0]~q\);

-- Location: LCCOMB_X24_Y34_N22
\regs|data_out[0]~2\ : cycloneiv_lcell_comb
-- Equation(s):
-- \regs|data_out[0]~2_combout\ = (\reg_addr|mem\(0) & ((\regs|dbr[9][0]~q\) # ((\reg_addr|mem\(1))))) # (!\reg_addr|mem\(0) & (((\regs|dbr[8][0]~q\ & !\reg_addr|mem\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110010111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \regs|dbr[9][0]~q\,
	datab => \reg_addr|mem\(0),
	datac => \regs|dbr[8][0]~q\,
	datad => \reg_addr|mem\(1),
	combout => \regs|data_out[0]~2_combout\);

-- Location: IOIBUF_X34_Y0_N1
\lea_busy~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_lea_busy,
	o => \lea_busy~input_o\);

-- Location: FF_X29_Y34_N17
\regs|dbr[10][0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	asdata => \lea_busy~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \regs|dbr[10][0]~q\);

-- Location: LCCOMB_X29_Y34_N16
\regs|data_out[0]~3\ : cycloneiv_lcell_comb
-- Equation(s):
-- \regs|data_out[0]~3_combout\ = (!\reg_addr|mem\(2) & ((\regs|data_out[0]~2_combout\ & ((!\reg_addr|mem\(1)))) # (!\regs|data_out[0]~2_combout\ & (\regs|dbr[10][0]~q\ & \reg_addr|mem\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000001000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \reg_addr|mem\(2),
	datab => \regs|data_out[0]~2_combout\,
	datac => \regs|dbr[10][0]~q\,
	datad => \reg_addr|mem\(1),
	combout => \regs|data_out[0]~3_combout\);

-- Location: IOIBUF_X41_Y41_N8
\lea_mem_in[24]~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_lea_mem_in(24),
	o => \lea_mem_in[24]~input_o\);

-- Location: FF_X30_Y34_N29
\regs|dbr[6][0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	asdata => \lea_mem_in[24]~input_o\,
	sload => VCC,
	ena => \control_logic|state.s9~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \regs|dbr[6][0]~q\);

-- Location: LCCOMB_X30_Y34_N28
\regs|Mux7~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \regs|Mux7~0_combout\ = (\reg_addr|mem\(0) & (\reg_addr|mem\(1))) # (!\reg_addr|mem\(0) & ((\reg_addr|mem\(1) & (\regs|dbr[6][0]~q\)) # (!\reg_addr|mem\(1) & ((\regs|dbr[4][0]~q\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101100111001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \reg_addr|mem\(0),
	datab => \reg_addr|mem\(1),
	datac => \regs|dbr[6][0]~q\,
	datad => \regs|dbr[4][0]~q\,
	combout => \regs|Mux7~0_combout\);

-- Location: IOIBUF_X52_Y23_N1
\lea_mem_in[16]~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_lea_mem_in(16),
	o => \lea_mem_in[16]~input_o\);

-- Location: FF_X30_Y34_N15
\regs|dbr[7][0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	asdata => \lea_mem_in[16]~input_o\,
	sload => VCC,
	ena => \control_logic|state.s9~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \regs|dbr[7][0]~q\);

-- Location: LCCOMB_X30_Y34_N14
\regs|Mux7~1\ : cycloneiv_lcell_comb
-- Equation(s):
-- \regs|Mux7~1_combout\ = (\reg_addr|mem\(0) & ((\regs|Mux7~0_combout\ & (\regs|dbr[7][0]~q\)) # (!\regs|Mux7~0_combout\ & ((\regs|dbr[5][0]~q\))))) # (!\reg_addr|mem\(0) & (\regs|Mux7~0_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110011011000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \reg_addr|mem\(0),
	datab => \regs|Mux7~0_combout\,
	datac => \regs|dbr[7][0]~q\,
	datad => \regs|dbr[5][0]~q\,
	combout => \regs|Mux7~1_combout\);

-- Location: LCCOMB_X28_Y34_N20
\regs|Mux7~2\ : cycloneiv_lcell_comb
-- Equation(s):
-- \regs|Mux7~2_combout\ = (\reg_addr|mem\(1) & (\reg_addr|mem\(0))) # (!\reg_addr|mem\(1) & ((\reg_addr|mem\(0) & (\regs|dbr[1][0]~q\)) # (!\reg_addr|mem\(0) & ((\regs|dbr[0][0]~q\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101100111001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \reg_addr|mem\(1),
	datab => \reg_addr|mem\(0),
	datac => \regs|dbr[1][0]~q\,
	datad => \regs|dbr[0][0]~q\,
	combout => \regs|Mux7~2_combout\);

-- Location: LCCOMB_X27_Y34_N4
\regs|Mux7~3\ : cycloneiv_lcell_comb
-- Equation(s):
-- \regs|Mux7~3_combout\ = (\regs|Mux7~2_combout\ & (((\regs|dbr[3][0]~q\)) # (!\reg_addr|mem\(1)))) # (!\regs|Mux7~2_combout\ & (\reg_addr|mem\(1) & (\regs|dbr[2][0]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110101001100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \regs|Mux7~2_combout\,
	datab => \reg_addr|mem\(1),
	datac => \regs|dbr[2][0]~q\,
	datad => \regs|dbr[3][0]~q\,
	combout => \regs|Mux7~3_combout\);

-- Location: LCCOMB_X26_Y34_N22
\regs|data_out[0]~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \regs|data_out[0]~0_combout\ = (!\reg_addr|mem\(3) & ((\reg_addr|mem\(2) & (\regs|Mux7~1_combout\)) # (!\reg_addr|mem\(2) & ((\regs|Mux7~3_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100010101000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \reg_addr|mem\(3),
	datab => \regs|Mux7~1_combout\,
	datac => \reg_addr|mem\(2),
	datad => \regs|Mux7~3_combout\,
	combout => \regs|data_out[0]~0_combout\);

-- Location: LCCOMB_X26_Y34_N30
\regs|data_out[0]~1\ : cycloneiv_lcell_comb
-- Equation(s):
-- \regs|data_out[0]~1_combout\ = (\regs|dbr[0][0]~q\ & ((\reg_addr|mem\(2)) # ((\reg_addr|mem\(1) & \reg_addr|mem\(0)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110101000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \reg_addr|mem\(2),
	datab => \reg_addr|mem\(1),
	datac => \reg_addr|mem\(0),
	datad => \regs|dbr[0][0]~q\,
	combout => \regs|data_out[0]~1_combout\);

-- Location: LCCOMB_X26_Y34_N20
\regs|data_out[0]~4\ : cycloneiv_lcell_comb
-- Equation(s):
-- \regs|data_out[0]~4_combout\ = (\regs|data_out[0]~0_combout\) # ((\reg_addr|mem\(3) & ((\regs|data_out[0]~3_combout\) # (\regs|data_out[0]~1_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110011101100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \regs|data_out[0]~3_combout\,
	datab => \regs|data_out[0]~0_combout\,
	datac => \reg_addr|mem\(3),
	datad => \regs|data_out[0]~1_combout\,
	combout => \regs|data_out[0]~4_combout\);

-- Location: LCCOMB_X26_Y34_N12
\regs|data_out[1]~6\ : cycloneiv_lcell_comb
-- Equation(s):
-- \regs|data_out[1]~6_combout\ = (\reg_addr|mem\(3) & (!\reg_addr|mem\(2) & ((!\reg_addr|mem\(1)) # (!\reg_addr|mem\(0)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000001000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \reg_addr|mem\(3),
	datab => \reg_addr|mem\(0),
	datac => \reg_addr|mem\(2),
	datad => \reg_addr|mem\(1),
	combout => \regs|data_out[1]~6_combout\);

-- Location: LCCOMB_X26_Y34_N6
\regs|data_out[1]~5\ : cycloneiv_lcell_comb
-- Equation(s):
-- \regs|data_out[1]~5_combout\ = (\reg_addr|mem\(3) & ((\reg_addr|mem\(0)) # ((\reg_addr|mem\(2))))) # (!\reg_addr|mem\(3) & (!\reg_addr|mem\(0) & (!\reg_addr|mem\(2) & !\reg_addr|mem\(1))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010100010101001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \reg_addr|mem\(3),
	datab => \reg_addr|mem\(0),
	datac => \reg_addr|mem\(2),
	datad => \reg_addr|mem\(1),
	combout => \regs|data_out[1]~5_combout\);

-- Location: IOIBUF_X16_Y0_N8
\lea_mem_in[9]~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_lea_mem_in(9),
	o => \lea_mem_in[9]~input_o\);

-- Location: FF_X24_Y34_N31
\regs|dbr[8][1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	asdata => \lea_mem_in[9]~input_o\,
	sload => VCC,
	ena => \control_logic|state.s9~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \regs|dbr[8][1]~q\);

-- Location: LCCOMB_X27_Y34_N14
\regs|data_out[1]~9\ : cycloneiv_lcell_comb
-- Equation(s):
-- \regs|data_out[1]~9_combout\ = (\reg_addr|mem\(2)) # ((\reg_addr|mem\(1) & \reg_addr|mem\(0)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \reg_addr|mem\(2),
	datab => \reg_addr|mem\(1),
	datad => \reg_addr|mem\(0),
	combout => \regs|data_out[1]~9_combout\);

-- Location: IOIBUF_X36_Y41_N1
\lea_mem_in[25]~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_lea_mem_in(25),
	o => \lea_mem_in[25]~input_o\);

-- Location: FF_X28_Y34_N25
\regs|dbr[6][1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	asdata => \lea_mem_in[25]~input_o\,
	sload => VCC,
	ena => \control_logic|state.s9~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \regs|dbr[6][1]~q\);

-- Location: LCCOMB_X28_Y34_N24
\regs|data_out[1]~7\ : cycloneiv_lcell_comb
-- Equation(s):
-- \regs|data_out[1]~7_combout\ = (\reg_addr|mem\(1) & (((\regs|dbr[6][1]~q\) # (\reg_addr|mem\(0))))) # (!\reg_addr|mem\(1) & (\regs|dbr[4][1]~q\ & ((!\reg_addr|mem\(0)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101011100100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \reg_addr|mem\(1),
	datab => \regs|dbr[4][1]~q\,
	datac => \regs|dbr[6][1]~q\,
	datad => \reg_addr|mem\(0),
	combout => \regs|data_out[1]~7_combout\);

-- Location: IOIBUF_X29_Y0_N8
\lea_mem_in[17]~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_lea_mem_in(17),
	o => \lea_mem_in[17]~input_o\);

-- Location: FF_X28_Y34_N11
\regs|dbr[7][1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	asdata => \lea_mem_in[17]~input_o\,
	sload => VCC,
	ena => \control_logic|state.s9~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \regs|dbr[7][1]~q\);

-- Location: LCCOMB_X28_Y34_N10
\regs|data_out[1]~8\ : cycloneiv_lcell_comb
-- Equation(s):
-- \regs|data_out[1]~8_combout\ = (\regs|data_out[1]~7_combout\ & (((\regs|dbr[7][1]~q\)) # (!\reg_addr|mem\(0)))) # (!\regs|data_out[1]~7_combout\ & (\reg_addr|mem\(0) & ((\regs|dbr[5][1]~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110011010100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \regs|data_out[1]~7_combout\,
	datab => \reg_addr|mem\(0),
	datac => \regs|dbr[7][1]~q\,
	datad => \regs|dbr[5][1]~q\,
	combout => \regs|data_out[1]~8_combout\);

-- Location: LCCOMB_X27_Y34_N28
\regs|Decoder0~2\ : cycloneiv_lcell_comb
-- Equation(s):
-- \regs|Decoder0~2_combout\ = (!\reg_addr|mem\(2) & \reg_addr|mem\(1))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \reg_addr|mem\(2),
	datad => \reg_addr|mem\(1),
	combout => \regs|Decoder0~2_combout\);

-- Location: LCCOMB_X27_Y34_N2
\regs|data_out[1]~10\ : cycloneiv_lcell_comb
-- Equation(s):
-- \regs|data_out[1]~10_combout\ = (\regs|Decoder0~2_combout\ & ((\regs|data_out[1]~9_combout\) # ((\regs|dbr[2][1]~q\)))) # (!\regs|Decoder0~2_combout\ & (!\regs|data_out[1]~9_combout\ & ((\regs|dbr[1][1]~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011100110101000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \regs|Decoder0~2_combout\,
	datab => \regs|data_out[1]~9_combout\,
	datac => \regs|dbr[2][1]~q\,
	datad => \regs|dbr[1][1]~q\,
	combout => \regs|data_out[1]~10_combout\);

-- Location: LCCOMB_X27_Y34_N0
\regs|data_out[1]~11\ : cycloneiv_lcell_comb
-- Equation(s):
-- \regs|data_out[1]~11_combout\ = (\regs|data_out[1]~9_combout\ & ((\regs|data_out[1]~10_combout\ & (\regs|dbr[3][1]~q\)) # (!\regs|data_out[1]~10_combout\ & ((\regs|data_out[1]~8_combout\))))) # (!\regs|data_out[1]~9_combout\ & 
-- (((\regs|data_out[1]~10_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101111000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \regs|dbr[3][1]~q\,
	datab => \regs|data_out[1]~9_combout\,
	datac => \regs|data_out[1]~8_combout\,
	datad => \regs|data_out[1]~10_combout\,
	combout => \regs|data_out[1]~11_combout\);

-- Location: LCCOMB_X24_Y34_N30
\regs|data_out[1]~12\ : cycloneiv_lcell_comb
-- Equation(s):
-- \regs|data_out[1]~12_combout\ = (\regs|data_out[1]~6_combout\ & ((\regs|data_out[1]~5_combout\) # ((\regs|dbr[8][1]~q\)))) # (!\regs|data_out[1]~6_combout\ & (!\regs|data_out[1]~5_combout\ & ((\regs|data_out[1]~11_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011100110101000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \regs|data_out[1]~6_combout\,
	datab => \regs|data_out[1]~5_combout\,
	datac => \regs|dbr[8][1]~q\,
	datad => \regs|data_out[1]~11_combout\,
	combout => \regs|data_out[1]~12_combout\);

-- Location: IOIBUF_X52_Y19_N1
\lea_mem_in[1]~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_lea_mem_in(1),
	o => \lea_mem_in[1]~input_o\);

-- Location: FF_X24_Y34_N29
\regs|dbr[9][1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	asdata => \lea_mem_in[1]~input_o\,
	sload => VCC,
	ena => \control_logic|state.s9~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \regs|dbr[9][1]~q\);

-- Location: LCCOMB_X24_Y34_N8
\regs|data_out[1]~13\ : cycloneiv_lcell_comb
-- Equation(s):
-- \regs|data_out[1]~13_combout\ = (\regs|data_out[1]~12_combout\ & ((\regs|dbr[9][1]~q\) # ((!\regs|data_out[1]~5_combout\)))) # (!\regs|data_out[1]~12_combout\ & (((\regs|dbr[0][1]~q\ & \regs|data_out[1]~5_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101100010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \regs|data_out[1]~12_combout\,
	datab => \regs|dbr[9][1]~q\,
	datac => \regs|dbr[0][1]~q\,
	datad => \regs|data_out[1]~5_combout\,
	combout => \regs|data_out[1]~13_combout\);

-- Location: LCCOMB_X29_Y34_N6
\regs|data_out[1]~14\ : cycloneiv_lcell_comb
-- Equation(s):
-- \regs|data_out[1]~14_combout\ = (\regs|data_out[1]~13_combout\ & ((!\reg_addr|mem\(1)) # (!\regs|data_out[1]~6_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010001010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \regs|data_out[1]~13_combout\,
	datab => \regs|data_out[1]~6_combout\,
	datad => \reg_addr|mem\(1),
	combout => \regs|data_out[1]~14_combout\);

-- Location: LCCOMB_X29_Y34_N24
\regs|data_out[2]~15\ : cycloneiv_lcell_comb
-- Equation(s):
-- \regs|data_out[2]~15_combout\ = (\reg_addr|mem\(1) & (((\reg_addr|mem\(0))))) # (!\reg_addr|mem\(1) & ((\reg_addr|mem\(0) & ((\regs|dbr[5][2]~q\))) # (!\reg_addr|mem\(0) & (\regs|dbr[4][2]~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111001011000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \regs|dbr[4][2]~q\,
	datab => \reg_addr|mem\(1),
	datac => \reg_addr|mem\(0),
	datad => \regs|dbr[5][2]~q\,
	combout => \regs|data_out[2]~15_combout\);

-- Location: IOIBUF_X52_Y15_N8
\lea_mem_in[18]~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_lea_mem_in(18),
	o => \lea_mem_in[18]~input_o\);

-- Location: FF_X28_Y34_N31
\regs|dbr[7][2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	asdata => \lea_mem_in[18]~input_o\,
	sload => VCC,
	ena => \control_logic|state.s9~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \regs|dbr[7][2]~q\);

-- Location: IOIBUF_X52_Y27_N1
\lea_mem_in[26]~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_lea_mem_in(26),
	o => \lea_mem_in[26]~input_o\);

-- Location: LCCOMB_X28_Y34_N28
\regs|dbr[6][2]~feeder\ : cycloneiv_lcell_comb
-- Equation(s):
-- \regs|dbr[6][2]~feeder_combout\ = \lea_mem_in[26]~input_o\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \lea_mem_in[26]~input_o\,
	combout => \regs|dbr[6][2]~feeder_combout\);

-- Location: FF_X28_Y34_N29
\regs|dbr[6][2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	d => \regs|dbr[6][2]~feeder_combout\,
	ena => \control_logic|state.s9~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \regs|dbr[6][2]~q\);

-- Location: LCCOMB_X28_Y34_N30
\regs|data_out[2]~16\ : cycloneiv_lcell_comb
-- Equation(s):
-- \regs|data_out[2]~16_combout\ = (\reg_addr|mem\(1) & ((\regs|data_out[2]~15_combout\ & (\regs|dbr[7][2]~q\)) # (!\regs|data_out[2]~15_combout\ & ((\regs|dbr[6][2]~q\))))) # (!\reg_addr|mem\(1) & (\regs|data_out[2]~15_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110011011000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \reg_addr|mem\(1),
	datab => \regs|data_out[2]~15_combout\,
	datac => \regs|dbr[7][2]~q\,
	datad => \regs|dbr[6][2]~q\,
	combout => \regs|data_out[2]~16_combout\);

-- Location: LCCOMB_X28_Y34_N4
\regs|data_out[2]~17\ : cycloneiv_lcell_comb
-- Equation(s):
-- \regs|data_out[2]~17_combout\ = (\regs|Decoder0~2_combout\ & (((\regs|data_out[1]~9_combout\)))) # (!\regs|Decoder0~2_combout\ & ((\regs|data_out[1]~9_combout\ & (\regs|data_out[2]~16_combout\)) # (!\regs|data_out[1]~9_combout\ & ((\regs|dbr[1][2]~q\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \regs|Decoder0~2_combout\,
	datab => \regs|data_out[2]~16_combout\,
	datac => \regs|dbr[1][2]~q\,
	datad => \regs|data_out[1]~9_combout\,
	combout => \regs|data_out[2]~17_combout\);

-- Location: LCCOMB_X27_Y34_N24
\regs|data_out[2]~18\ : cycloneiv_lcell_comb
-- Equation(s):
-- \regs|data_out[2]~18_combout\ = (\regs|data_out[2]~17_combout\ & ((\regs|dbr[3][2]~q\) # ((!\regs|Decoder0~2_combout\)))) # (!\regs|data_out[2]~17_combout\ & (((\regs|dbr[2][2]~q\ & \regs|Decoder0~2_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101100010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \regs|data_out[2]~17_combout\,
	datab => \regs|dbr[3][2]~q\,
	datac => \regs|dbr[2][2]~q\,
	datad => \regs|Decoder0~2_combout\,
	combout => \regs|data_out[2]~18_combout\);

-- Location: LCCOMB_X24_Y34_N20
\regs|data_out[2]~19\ : cycloneiv_lcell_comb
-- Equation(s):
-- \regs|data_out[2]~19_combout\ = (\regs|data_out[1]~6_combout\ & (((\regs|data_out[1]~5_combout\)))) # (!\regs|data_out[1]~6_combout\ & ((\regs|data_out[1]~5_combout\ & ((\regs|dbr[0][2]~q\))) # (!\regs|data_out[1]~5_combout\ & 
-- (\regs|data_out[2]~18_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101001000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \regs|data_out[1]~6_combout\,
	datab => \regs|data_out[2]~18_combout\,
	datac => \regs|dbr[0][2]~q\,
	datad => \regs|data_out[1]~5_combout\,
	combout => \regs|data_out[2]~19_combout\);

-- Location: IOIBUF_X14_Y41_N8
\lea_mem_in[2]~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_lea_mem_in(2),
	o => \lea_mem_in[2]~input_o\);

-- Location: FF_X23_Y34_N5
\regs|dbr[9][2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	asdata => \lea_mem_in[2]~input_o\,
	sload => VCC,
	ena => \control_logic|state.s9~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \regs|dbr[9][2]~q\);

-- Location: IOIBUF_X7_Y41_N22
\lea_mem_in[10]~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_lea_mem_in(10),
	o => \lea_mem_in[10]~input_o\);

-- Location: LCCOMB_X23_Y34_N18
\regs|dbr[8][2]~feeder\ : cycloneiv_lcell_comb
-- Equation(s):
-- \regs|dbr[8][2]~feeder_combout\ = \lea_mem_in[10]~input_o\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \lea_mem_in[10]~input_o\,
	combout => \regs|dbr[8][2]~feeder_combout\);

-- Location: FF_X23_Y34_N19
\regs|dbr[8][2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	d => \regs|dbr[8][2]~feeder_combout\,
	ena => \control_logic|state.s9~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \regs|dbr[8][2]~q\);

-- Location: LCCOMB_X23_Y34_N4
\regs|data_out[2]~20\ : cycloneiv_lcell_comb
-- Equation(s):
-- \regs|data_out[2]~20_combout\ = (\regs|data_out[2]~19_combout\ & (((\regs|dbr[9][2]~q\)) # (!\regs|data_out[1]~6_combout\))) # (!\regs|data_out[2]~19_combout\ & (\regs|data_out[1]~6_combout\ & ((\regs|dbr[8][2]~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110011010100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \regs|data_out[2]~19_combout\,
	datab => \regs|data_out[1]~6_combout\,
	datac => \regs|dbr[9][2]~q\,
	datad => \regs|dbr[8][2]~q\,
	combout => \regs|data_out[2]~20_combout\);

-- Location: LCCOMB_X29_Y34_N26
\regs|data_out[2]~21\ : cycloneiv_lcell_comb
-- Equation(s):
-- \regs|data_out[2]~21_combout\ = (\regs|data_out[2]~20_combout\ & ((!\regs|data_out[1]~6_combout\) # (!\reg_addr|mem\(1))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010101000101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \regs|data_out[2]~20_combout\,
	datab => \reg_addr|mem\(1),
	datac => \regs|data_out[1]~6_combout\,
	combout => \regs|data_out[2]~21_combout\);

-- Location: IOIBUF_X41_Y41_N1
\lea_mem_in[27]~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_lea_mem_in(27),
	o => \lea_mem_in[27]~input_o\);

-- Location: FF_X30_Y34_N25
\regs|dbr[6][3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	asdata => \lea_mem_in[27]~input_o\,
	sload => VCC,
	ena => \control_logic|state.s9~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \regs|dbr[6][3]~q\);

-- Location: LCCOMB_X30_Y34_N24
\regs|data_out[3]~22\ : cycloneiv_lcell_comb
-- Equation(s):
-- \regs|data_out[3]~22_combout\ = (\reg_addr|mem\(0) & (\reg_addr|mem\(1))) # (!\reg_addr|mem\(0) & ((\reg_addr|mem\(1) & (\regs|dbr[6][3]~q\)) # (!\reg_addr|mem\(1) & ((\regs|dbr[4][3]~q\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101100111001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \reg_addr|mem\(0),
	datab => \reg_addr|mem\(1),
	datac => \regs|dbr[6][3]~q\,
	datad => \regs|dbr[4][3]~q\,
	combout => \regs|data_out[3]~22_combout\);

-- Location: IOIBUF_X38_Y41_N1
\lea_mem_in[19]~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_lea_mem_in(19),
	o => \lea_mem_in[19]~input_o\);

-- Location: FF_X30_Y34_N3
\regs|dbr[7][3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	asdata => \lea_mem_in[19]~input_o\,
	sload => VCC,
	ena => \control_logic|state.s9~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \regs|dbr[7][3]~q\);

-- Location: LCCOMB_X30_Y34_N2
\regs|data_out[3]~23\ : cycloneiv_lcell_comb
-- Equation(s):
-- \regs|data_out[3]~23_combout\ = (\reg_addr|mem\(0) & ((\regs|data_out[3]~22_combout\ & (\regs|dbr[7][3]~q\)) # (!\regs|data_out[3]~22_combout\ & ((\regs|dbr[5][3]~q\))))) # (!\reg_addr|mem\(0) & (\regs|data_out[3]~22_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110011011000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \reg_addr|mem\(0),
	datab => \regs|data_out[3]~22_combout\,
	datac => \regs|dbr[7][3]~q\,
	datad => \regs|dbr[5][3]~q\,
	combout => \regs|data_out[3]~23_combout\);

-- Location: LCCOMB_X28_Y34_N6
\regs|data_out[3]~24\ : cycloneiv_lcell_comb
-- Equation(s):
-- \regs|data_out[3]~24_combout\ = (\regs|Decoder0~2_combout\ & ((\regs|dbr[2][3]~q\) # ((\regs|data_out[1]~9_combout\)))) # (!\regs|Decoder0~2_combout\ & (((\regs|dbr[1][3]~q\ & !\regs|data_out[1]~9_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101011011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \regs|Decoder0~2_combout\,
	datab => \regs|dbr[2][3]~q\,
	datac => \regs|dbr[1][3]~q\,
	datad => \regs|data_out[1]~9_combout\,
	combout => \regs|data_out[3]~24_combout\);

-- Location: LCCOMB_X27_Y34_N10
\regs|data_out[3]~25\ : cycloneiv_lcell_comb
-- Equation(s):
-- \regs|data_out[3]~25_combout\ = (\regs|data_out[1]~9_combout\ & ((\regs|data_out[3]~24_combout\ & (\regs|dbr[3][3]~q\)) # (!\regs|data_out[3]~24_combout\ & ((\regs|data_out[3]~23_combout\))))) # (!\regs|data_out[1]~9_combout\ & 
-- (((\regs|data_out[3]~24_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101111000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \regs|dbr[3][3]~q\,
	datab => \regs|data_out[1]~9_combout\,
	datac => \regs|data_out[3]~23_combout\,
	datad => \regs|data_out[3]~24_combout\,
	combout => \regs|data_out[3]~25_combout\);

-- Location: IOIBUF_X18_Y41_N1
\lea_mem_in[11]~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_lea_mem_in(11),
	o => \lea_mem_in[11]~input_o\);

-- Location: FF_X24_Y34_N5
\regs|dbr[8][3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	asdata => \lea_mem_in[11]~input_o\,
	sload => VCC,
	ena => \control_logic|state.s9~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \regs|dbr[8][3]~q\);

-- Location: LCCOMB_X24_Y34_N4
\regs|data_out[3]~26\ : cycloneiv_lcell_comb
-- Equation(s):
-- \regs|data_out[3]~26_combout\ = (\regs|data_out[1]~6_combout\ & (((\regs|dbr[8][3]~q\) # (\regs|data_out[1]~5_combout\)))) # (!\regs|data_out[1]~6_combout\ & (\regs|data_out[3]~25_combout\ & ((!\regs|data_out[1]~5_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101011100100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \regs|data_out[1]~6_combout\,
	datab => \regs|data_out[3]~25_combout\,
	datac => \regs|dbr[8][3]~q\,
	datad => \regs|data_out[1]~5_combout\,
	combout => \regs|data_out[3]~26_combout\);

-- Location: IOIBUF_X52_Y18_N1
\lea_mem_in[3]~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_lea_mem_in(3),
	o => \lea_mem_in[3]~input_o\);

-- Location: FF_X24_Y34_N7
\regs|dbr[9][3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	asdata => \lea_mem_in[3]~input_o\,
	sload => VCC,
	ena => \control_logic|state.s9~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \regs|dbr[9][3]~q\);

-- Location: LCCOMB_X24_Y34_N6
\regs|data_out[3]~27\ : cycloneiv_lcell_comb
-- Equation(s):
-- \regs|data_out[3]~27_combout\ = (\regs|data_out[3]~26_combout\ & (((\regs|dbr[9][3]~q\) # (!\regs|data_out[1]~5_combout\)))) # (!\regs|data_out[3]~26_combout\ & (\regs|dbr[0][3]~q\ & ((\regs|data_out[1]~5_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110001011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \regs|dbr[0][3]~q\,
	datab => \regs|data_out[3]~26_combout\,
	datac => \regs|dbr[9][3]~q\,
	datad => \regs|data_out[1]~5_combout\,
	combout => \regs|data_out[3]~27_combout\);

-- Location: LCCOMB_X29_Y34_N20
\regs|data_out[3]~28\ : cycloneiv_lcell_comb
-- Equation(s):
-- \regs|data_out[3]~28_combout\ = (\regs|data_out[3]~27_combout\ & ((!\reg_addr|mem\(1)) # (!\regs|data_out[1]~6_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000101010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \regs|data_out[3]~27_combout\,
	datac => \regs|data_out[1]~6_combout\,
	datad => \reg_addr|mem\(1),
	combout => \regs|data_out[3]~28_combout\);

-- Location: IOIBUF_X23_Y0_N1
\lea_mem_in[12]~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_lea_mem_in(12),
	o => \lea_mem_in[12]~input_o\);

-- Location: FF_X23_Y34_N27
\regs|dbr[8][4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	asdata => \lea_mem_in[12]~input_o\,
	sload => VCC,
	ena => \control_logic|state.s9~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \regs|dbr[8][4]~q\);

-- Location: IOIBUF_X10_Y41_N1
\lea_mem_in[4]~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_lea_mem_in(4),
	o => \lea_mem_in[4]~input_o\);

-- Location: FF_X23_Y34_N13
\regs|dbr[9][4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	asdata => \lea_mem_in[4]~input_o\,
	sload => VCC,
	ena => \control_logic|state.s9~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \regs|dbr[9][4]~q\);

-- Location: IOIBUF_X36_Y41_N8
\lea_mem_in[28]~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_lea_mem_in(28),
	o => \lea_mem_in[28]~input_o\);

-- Location: LCCOMB_X28_Y34_N16
\regs|dbr[6][4]~feeder\ : cycloneiv_lcell_comb
-- Equation(s):
-- \regs|dbr[6][4]~feeder_combout\ = \lea_mem_in[28]~input_o\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \lea_mem_in[28]~input_o\,
	combout => \regs|dbr[6][4]~feeder_combout\);

-- Location: FF_X28_Y34_N17
\regs|dbr[6][4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	d => \regs|dbr[6][4]~feeder_combout\,
	ena => \control_logic|state.s9~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \regs|dbr[6][4]~q\);

-- Location: IOIBUF_X52_Y16_N1
\lea_mem_in[20]~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_lea_mem_in(20),
	o => \lea_mem_in[20]~input_o\);

-- Location: FF_X28_Y34_N15
\regs|dbr[7][4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	asdata => \lea_mem_in[20]~input_o\,
	sload => VCC,
	ena => \control_logic|state.s9~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \regs|dbr[7][4]~q\);

-- Location: LCCOMB_X29_Y34_N2
\regs|data_out[4]~29\ : cycloneiv_lcell_comb
-- Equation(s):
-- \regs|data_out[4]~29_combout\ = (\reg_addr|mem\(0) & ((\regs|dbr[5][4]~q\) # ((\reg_addr|mem\(1))))) # (!\reg_addr|mem\(0) & (((\regs|dbr[4][4]~q\ & !\reg_addr|mem\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110010111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \regs|dbr[5][4]~q\,
	datab => \reg_addr|mem\(0),
	datac => \regs|dbr[4][4]~q\,
	datad => \reg_addr|mem\(1),
	combout => \regs|data_out[4]~29_combout\);

-- Location: LCCOMB_X28_Y34_N14
\regs|data_out[4]~30\ : cycloneiv_lcell_comb
-- Equation(s):
-- \regs|data_out[4]~30_combout\ = (\reg_addr|mem\(1) & ((\regs|data_out[4]~29_combout\ & ((\regs|dbr[7][4]~q\))) # (!\regs|data_out[4]~29_combout\ & (\regs|dbr[6][4]~q\)))) # (!\reg_addr|mem\(1) & (((\regs|data_out[4]~29_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111010110001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \reg_addr|mem\(1),
	datab => \regs|dbr[6][4]~q\,
	datac => \regs|dbr[7][4]~q\,
	datad => \regs|data_out[4]~29_combout\,
	combout => \regs|data_out[4]~30_combout\);

-- Location: LCCOMB_X28_Y34_N8
\regs|data_out[4]~31\ : cycloneiv_lcell_comb
-- Equation(s):
-- \regs|data_out[4]~31_combout\ = (\regs|Decoder0~2_combout\ & (((\regs|data_out[1]~9_combout\)))) # (!\regs|Decoder0~2_combout\ & ((\regs|data_out[1]~9_combout\ & (\regs|data_out[4]~30_combout\)) # (!\regs|data_out[1]~9_combout\ & ((\regs|dbr[1][4]~q\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \regs|Decoder0~2_combout\,
	datab => \regs|data_out[4]~30_combout\,
	datac => \regs|dbr[1][4]~q\,
	datad => \regs|data_out[1]~9_combout\,
	combout => \regs|data_out[4]~31_combout\);

-- Location: LCCOMB_X27_Y34_N16
\regs|data_out[4]~32\ : cycloneiv_lcell_comb
-- Equation(s):
-- \regs|data_out[4]~32_combout\ = (\regs|data_out[4]~31_combout\ & ((\regs|dbr[3][4]~q\) # ((!\regs|Decoder0~2_combout\)))) # (!\regs|data_out[4]~31_combout\ & (((\regs|dbr[2][4]~q\ & \regs|Decoder0~2_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010110011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \regs|dbr[3][4]~q\,
	datab => \regs|dbr[2][4]~q\,
	datac => \regs|data_out[4]~31_combout\,
	datad => \regs|Decoder0~2_combout\,
	combout => \regs|data_out[4]~32_combout\);

-- Location: LCCOMB_X24_Y34_N12
\regs|data_out[4]~33\ : cycloneiv_lcell_comb
-- Equation(s):
-- \regs|data_out[4]~33_combout\ = (\regs|data_out[1]~6_combout\ & (\regs|data_out[1]~5_combout\)) # (!\regs|data_out[1]~6_combout\ & ((\regs|data_out[1]~5_combout\ & (\regs|dbr[0][4]~q\)) # (!\regs|data_out[1]~5_combout\ & 
-- ((\regs|data_out[4]~32_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101100111001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \regs|data_out[1]~6_combout\,
	datab => \regs|data_out[1]~5_combout\,
	datac => \regs|dbr[0][4]~q\,
	datad => \regs|data_out[4]~32_combout\,
	combout => \regs|data_out[4]~33_combout\);

-- Location: LCCOMB_X23_Y34_N12
\regs|data_out[4]~34\ : cycloneiv_lcell_comb
-- Equation(s):
-- \regs|data_out[4]~34_combout\ = (\regs|data_out[1]~6_combout\ & ((\regs|data_out[4]~33_combout\ & ((\regs|dbr[9][4]~q\))) # (!\regs|data_out[4]~33_combout\ & (\regs|dbr[8][4]~q\)))) # (!\regs|data_out[1]~6_combout\ & (((\regs|data_out[4]~33_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111001110001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \regs|dbr[8][4]~q\,
	datab => \regs|data_out[1]~6_combout\,
	datac => \regs|dbr[9][4]~q\,
	datad => \regs|data_out[4]~33_combout\,
	combout => \regs|data_out[4]~34_combout\);

-- Location: LCCOMB_X29_Y34_N8
\regs|data_out[4]~35\ : cycloneiv_lcell_comb
-- Equation(s):
-- \regs|data_out[4]~35_combout\ = (\regs|data_out[4]~34_combout\ & ((!\reg_addr|mem\(1)) # (!\regs|data_out[1]~6_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \regs|data_out[1]~6_combout\,
	datac => \regs|data_out[4]~34_combout\,
	datad => \reg_addr|mem\(1),
	combout => \regs|data_out[4]~35_combout\);

-- Location: IOIBUF_X12_Y41_N1
\lea_mem_in[13]~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_lea_mem_in(13),
	o => \lea_mem_in[13]~input_o\);

-- Location: FF_X23_Y34_N3
\regs|dbr[8][5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	asdata => \lea_mem_in[13]~input_o\,
	sload => VCC,
	ena => \control_logic|state.s9~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \regs|dbr[8][5]~q\);

-- Location: LCCOMB_X28_Y34_N2
\regs|data_out[5]~38\ : cycloneiv_lcell_comb
-- Equation(s):
-- \regs|data_out[5]~38_combout\ = (\regs|data_out[1]~9_combout\ & (((\regs|Decoder0~2_combout\)))) # (!\regs|data_out[1]~9_combout\ & ((\regs|Decoder0~2_combout\ & (\regs|dbr[2][5]~q\)) # (!\regs|Decoder0~2_combout\ & ((\regs|dbr[1][5]~q\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \regs|data_out[1]~9_combout\,
	datab => \regs|dbr[2][5]~q\,
	datac => \regs|dbr[1][5]~q\,
	datad => \regs|Decoder0~2_combout\,
	combout => \regs|data_out[5]~38_combout\);

-- Location: IOIBUF_X34_Y0_N8
\lea_mem_in[29]~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_lea_mem_in(29),
	o => \lea_mem_in[29]~input_o\);

-- Location: FF_X30_Y34_N5
\regs|dbr[6][5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	asdata => \lea_mem_in[29]~input_o\,
	sload => VCC,
	ena => \control_logic|state.s9~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \regs|dbr[6][5]~q\);

-- Location: LCCOMB_X30_Y34_N4
\regs|data_out[5]~36\ : cycloneiv_lcell_comb
-- Equation(s):
-- \regs|data_out[5]~36_combout\ = (\reg_addr|mem\(0) & (\reg_addr|mem\(1))) # (!\reg_addr|mem\(0) & ((\reg_addr|mem\(1) & (\regs|dbr[6][5]~q\)) # (!\reg_addr|mem\(1) & ((\regs|dbr[4][5]~q\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101100111001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \reg_addr|mem\(0),
	datab => \reg_addr|mem\(1),
	datac => \regs|dbr[6][5]~q\,
	datad => \regs|dbr[4][5]~q\,
	combout => \regs|data_out[5]~36_combout\);

-- Location: IOIBUF_X38_Y41_N8
\lea_mem_in[21]~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_lea_mem_in(21),
	o => \lea_mem_in[21]~input_o\);

-- Location: FF_X30_Y34_N23
\regs|dbr[7][5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	asdata => \lea_mem_in[21]~input_o\,
	sload => VCC,
	ena => \control_logic|state.s9~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \regs|dbr[7][5]~q\);

-- Location: LCCOMB_X30_Y34_N22
\regs|data_out[5]~37\ : cycloneiv_lcell_comb
-- Equation(s):
-- \regs|data_out[5]~37_combout\ = (\reg_addr|mem\(0) & ((\regs|data_out[5]~36_combout\ & (\regs|dbr[7][5]~q\)) # (!\regs|data_out[5]~36_combout\ & ((\regs|dbr[5][5]~q\))))) # (!\reg_addr|mem\(0) & (\regs|data_out[5]~36_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110011011000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \reg_addr|mem\(0),
	datab => \regs|data_out[5]~36_combout\,
	datac => \regs|dbr[7][5]~q\,
	datad => \regs|dbr[5][5]~q\,
	combout => \regs|data_out[5]~37_combout\);

-- Location: LCCOMB_X27_Y34_N18
\regs|data_out[5]~39\ : cycloneiv_lcell_comb
-- Equation(s):
-- \regs|data_out[5]~39_combout\ = (\regs|data_out[5]~38_combout\ & (((\regs|dbr[3][5]~q\)) # (!\regs|data_out[1]~9_combout\))) # (!\regs|data_out[5]~38_combout\ & (\regs|data_out[1]~9_combout\ & ((\regs|data_out[5]~37_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110011010100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \regs|data_out[5]~38_combout\,
	datab => \regs|data_out[1]~9_combout\,
	datac => \regs|dbr[3][5]~q\,
	datad => \regs|data_out[5]~37_combout\,
	combout => \regs|data_out[5]~39_combout\);

-- Location: LCCOMB_X23_Y34_N2
\regs|data_out[5]~40\ : cycloneiv_lcell_comb
-- Equation(s):
-- \regs|data_out[5]~40_combout\ = (\regs|data_out[1]~5_combout\ & (\regs|data_out[1]~6_combout\)) # (!\regs|data_out[1]~5_combout\ & ((\regs|data_out[1]~6_combout\ & (\regs|dbr[8][5]~q\)) # (!\regs|data_out[1]~6_combout\ & 
-- ((\regs|data_out[5]~39_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101100111001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \regs|data_out[1]~5_combout\,
	datab => \regs|data_out[1]~6_combout\,
	datac => \regs|dbr[8][5]~q\,
	datad => \regs|data_out[5]~39_combout\,
	combout => \regs|data_out[5]~40_combout\);

-- Location: IOIBUF_X7_Y41_N8
\lea_mem_in[5]~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_lea_mem_in(5),
	o => \lea_mem_in[5]~input_o\);

-- Location: FF_X23_Y34_N1
\regs|dbr[9][5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	asdata => \lea_mem_in[5]~input_o\,
	sload => VCC,
	ena => \control_logic|state.s9~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \regs|dbr[9][5]~q\);

-- Location: LCCOMB_X23_Y34_N0
\regs|data_out[5]~41\ : cycloneiv_lcell_comb
-- Equation(s):
-- \regs|data_out[5]~41_combout\ = (\regs|data_out[1]~5_combout\ & ((\regs|data_out[5]~40_combout\ & (\regs|dbr[9][5]~q\)) # (!\regs|data_out[5]~40_combout\ & ((\regs|dbr[0][5]~q\))))) # (!\regs|data_out[1]~5_combout\ & (\regs|data_out[5]~40_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110011011000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \regs|data_out[1]~5_combout\,
	datab => \regs|data_out[5]~40_combout\,
	datac => \regs|dbr[9][5]~q\,
	datad => \regs|dbr[0][5]~q\,
	combout => \regs|data_out[5]~41_combout\);

-- Location: LCCOMB_X29_Y34_N18
\regs|data_out[5]~42\ : cycloneiv_lcell_comb
-- Equation(s):
-- \regs|data_out[5]~42_combout\ = (\regs|data_out[5]~41_combout\ & ((!\regs|data_out[1]~6_combout\) # (!\reg_addr|mem\(1))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010101000101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \regs|data_out[5]~41_combout\,
	datab => \reg_addr|mem\(1),
	datac => \regs|data_out[1]~6_combout\,
	combout => \regs|data_out[5]~42_combout\);

-- Location: IOIBUF_X10_Y41_N8
\lea_mem_in[14]~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_lea_mem_in(14),
	o => \lea_mem_in[14]~input_o\);

-- Location: FF_X23_Y34_N11
\regs|dbr[8][6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	asdata => \lea_mem_in[14]~input_o\,
	sload => VCC,
	ena => \control_logic|state.s9~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \regs|dbr[8][6]~q\);

-- Location: IOIBUF_X12_Y41_N8
\lea_mem_in[6]~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_lea_mem_in(6),
	o => \lea_mem_in[6]~input_o\);

-- Location: FF_X23_Y34_N23
\regs|dbr[9][6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	asdata => \lea_mem_in[6]~input_o\,
	sload => VCC,
	ena => \control_logic|state.s9~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \regs|dbr[9][6]~q\);

-- Location: LCCOMB_X29_Y34_N28
\regs|data_out[6]~43\ : cycloneiv_lcell_comb
-- Equation(s):
-- \regs|data_out[6]~43_combout\ = (\reg_addr|mem\(0) & (((\regs|dbr[5][6]~q\) # (\reg_addr|mem\(1))))) # (!\reg_addr|mem\(0) & (\regs|dbr[4][6]~q\ & ((!\reg_addr|mem\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \regs|dbr[4][6]~q\,
	datab => \regs|dbr[5][6]~q\,
	datac => \reg_addr|mem\(0),
	datad => \reg_addr|mem\(1),
	combout => \regs|data_out[6]~43_combout\);

-- Location: IOIBUF_X52_Y25_N1
\lea_mem_in[22]~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_lea_mem_in(22),
	o => \lea_mem_in[22]~input_o\);

-- Location: FF_X28_Y34_N23
\regs|dbr[7][6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	asdata => \lea_mem_in[22]~input_o\,
	sload => VCC,
	ena => \control_logic|state.s9~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \regs|dbr[7][6]~q\);

-- Location: IOIBUF_X52_Y31_N1
\lea_mem_in[30]~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_lea_mem_in(30),
	o => \lea_mem_in[30]~input_o\);

-- Location: FF_X28_Y34_N13
\regs|dbr[6][6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	asdata => \lea_mem_in[30]~input_o\,
	sload => VCC,
	ena => \control_logic|state.s9~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \regs|dbr[6][6]~q\);

-- Location: LCCOMB_X28_Y34_N22
\regs|data_out[6]~44\ : cycloneiv_lcell_comb
-- Equation(s):
-- \regs|data_out[6]~44_combout\ = (\reg_addr|mem\(1) & ((\regs|data_out[6]~43_combout\ & (\regs|dbr[7][6]~q\)) # (!\regs|data_out[6]~43_combout\ & ((\regs|dbr[6][6]~q\))))) # (!\reg_addr|mem\(1) & (\regs|data_out[6]~43_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110011011000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \reg_addr|mem\(1),
	datab => \regs|data_out[6]~43_combout\,
	datac => \regs|dbr[7][6]~q\,
	datad => \regs|dbr[6][6]~q\,
	combout => \regs|data_out[6]~44_combout\);

-- Location: FF_X28_Y34_N1
\regs|dbr[1][6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	asdata => \spi_in[6]~input_o\,
	sload => VCC,
	ena => \regs|Decoder0~9_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \regs|dbr[1][6]~q\);

-- Location: LCCOMB_X28_Y34_N0
\regs|data_out[6]~45\ : cycloneiv_lcell_comb
-- Equation(s):
-- \regs|data_out[6]~45_combout\ = (\regs|Decoder0~2_combout\ & (((\regs|data_out[1]~9_combout\)))) # (!\regs|Decoder0~2_combout\ & ((\regs|data_out[1]~9_combout\ & (\regs|data_out[6]~44_combout\)) # (!\regs|data_out[1]~9_combout\ & ((\regs|dbr[1][6]~q\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \regs|Decoder0~2_combout\,
	datab => \regs|data_out[6]~44_combout\,
	datac => \regs|dbr[1][6]~q\,
	datad => \regs|data_out[1]~9_combout\,
	combout => \regs|data_out[6]~45_combout\);

-- Location: LCCOMB_X27_Y34_N6
\regs|data_out[6]~46\ : cycloneiv_lcell_comb
-- Equation(s):
-- \regs|data_out[6]~46_combout\ = (\regs|data_out[6]~45_combout\ & (((\regs|dbr[3][6]~q\) # (!\regs|Decoder0~2_combout\)))) # (!\regs|data_out[6]~45_combout\ & (\regs|dbr[2][6]~q\ & ((\regs|Decoder0~2_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100101011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \regs|dbr[2][6]~q\,
	datab => \regs|dbr[3][6]~q\,
	datac => \regs|data_out[6]~45_combout\,
	datad => \regs|Decoder0~2_combout\,
	combout => \regs|data_out[6]~46_combout\);

-- Location: LCCOMB_X23_Y34_N20
\regs|data_out[6]~47\ : cycloneiv_lcell_comb
-- Equation(s):
-- \regs|data_out[6]~47_combout\ = (\regs|data_out[1]~5_combout\ & ((\regs|data_out[1]~6_combout\) # ((\regs|dbr[0][6]~q\)))) # (!\regs|data_out[1]~5_combout\ & (!\regs|data_out[1]~6_combout\ & ((\regs|data_out[6]~46_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011100110101000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \regs|data_out[1]~5_combout\,
	datab => \regs|data_out[1]~6_combout\,
	datac => \regs|dbr[0][6]~q\,
	datad => \regs|data_out[6]~46_combout\,
	combout => \regs|data_out[6]~47_combout\);

-- Location: LCCOMB_X23_Y34_N22
\regs|data_out[6]~48\ : cycloneiv_lcell_comb
-- Equation(s):
-- \regs|data_out[6]~48_combout\ = (\regs|data_out[1]~6_combout\ & ((\regs|data_out[6]~47_combout\ & ((\regs|dbr[9][6]~q\))) # (!\regs|data_out[6]~47_combout\ & (\regs|dbr[8][6]~q\)))) # (!\regs|data_out[1]~6_combout\ & (((\regs|data_out[6]~47_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111001110001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \regs|dbr[8][6]~q\,
	datab => \regs|data_out[1]~6_combout\,
	datac => \regs|dbr[9][6]~q\,
	datad => \regs|data_out[6]~47_combout\,
	combout => \regs|data_out[6]~48_combout\);

-- Location: LCCOMB_X26_Y34_N26
\regs|data_out[6]~49\ : cycloneiv_lcell_comb
-- Equation(s):
-- \regs|data_out[6]~49_combout\ = (\regs|data_out[6]~48_combout\ & ((!\regs|data_out[1]~6_combout\) # (!\reg_addr|mem\(1))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010001010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \regs|data_out[6]~48_combout\,
	datab => \reg_addr|mem\(1),
	datad => \regs|data_out[1]~6_combout\,
	combout => \regs|data_out[6]~49_combout\);

-- Location: IOIBUF_X23_Y0_N8
\lea_mem_in[15]~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_lea_mem_in(15),
	o => \lea_mem_in[15]~input_o\);

-- Location: FF_X24_Y34_N25
\regs|dbr[8][7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	asdata => \lea_mem_in[15]~input_o\,
	sload => VCC,
	ena => \control_logic|state.s9~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \regs|dbr[8][7]~q\);

-- Location: IOIBUF_X38_Y0_N1
\lea_mem_in[31]~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_lea_mem_in(31),
	o => \lea_mem_in[31]~input_o\);

-- Location: FF_X30_Y34_N21
\regs|dbr[6][7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	asdata => \lea_mem_in[31]~input_o\,
	sload => VCC,
	ena => \control_logic|state.s9~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \regs|dbr[6][7]~q\);

-- Location: LCCOMB_X30_Y34_N20
\regs|data_out[7]~50\ : cycloneiv_lcell_comb
-- Equation(s):
-- \regs|data_out[7]~50_combout\ = (\reg_addr|mem\(0) & (\reg_addr|mem\(1))) # (!\reg_addr|mem\(0) & ((\reg_addr|mem\(1) & (\regs|dbr[6][7]~q\)) # (!\reg_addr|mem\(1) & ((\regs|dbr[4][7]~q\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101100111001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \reg_addr|mem\(0),
	datab => \reg_addr|mem\(1),
	datac => \regs|dbr[6][7]~q\,
	datad => \regs|dbr[4][7]~q\,
	combout => \regs|data_out[7]~50_combout\);

-- Location: IOIBUF_X52_Y18_N8
\lea_mem_in[23]~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_lea_mem_in(23),
	o => \lea_mem_in[23]~input_o\);

-- Location: FF_X30_Y34_N11
\regs|dbr[7][7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	asdata => \lea_mem_in[23]~input_o\,
	sload => VCC,
	ena => \control_logic|state.s9~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \regs|dbr[7][7]~q\);

-- Location: LCCOMB_X30_Y34_N10
\regs|data_out[7]~51\ : cycloneiv_lcell_comb
-- Equation(s):
-- \regs|data_out[7]~51_combout\ = (\reg_addr|mem\(0) & ((\regs|data_out[7]~50_combout\ & (\regs|dbr[7][7]~q\)) # (!\regs|data_out[7]~50_combout\ & ((\regs|dbr[5][7]~q\))))) # (!\reg_addr|mem\(0) & (\regs|data_out[7]~50_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110011011000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \reg_addr|mem\(0),
	datab => \regs|data_out[7]~50_combout\,
	datac => \regs|dbr[7][7]~q\,
	datad => \regs|dbr[5][7]~q\,
	combout => \regs|data_out[7]~51_combout\);

-- Location: FF_X28_Y34_N19
\regs|dbr[1][7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	asdata => \spi_in[7]~input_o\,
	sload => VCC,
	ena => \regs|Decoder0~9_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \regs|dbr[1][7]~q\);

-- Location: LCCOMB_X28_Y34_N18
\regs|data_out[7]~52\ : cycloneiv_lcell_comb
-- Equation(s):
-- \regs|data_out[7]~52_combout\ = (\regs|Decoder0~2_combout\ & ((\regs|dbr[2][7]~q\) # ((\regs|data_out[1]~9_combout\)))) # (!\regs|Decoder0~2_combout\ & (((\regs|dbr[1][7]~q\ & !\regs|data_out[1]~9_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101011011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \regs|Decoder0~2_combout\,
	datab => \regs|dbr[2][7]~q\,
	datac => \regs|dbr[1][7]~q\,
	datad => \regs|data_out[1]~9_combout\,
	combout => \regs|data_out[7]~52_combout\);

-- Location: LCCOMB_X27_Y34_N26
\regs|data_out[7]~53\ : cycloneiv_lcell_comb
-- Equation(s):
-- \regs|data_out[7]~53_combout\ = (\regs|data_out[1]~9_combout\ & ((\regs|data_out[7]~52_combout\ & ((\regs|dbr[3][7]~q\))) # (!\regs|data_out[7]~52_combout\ & (\regs|data_out[7]~51_combout\)))) # (!\regs|data_out[1]~9_combout\ & 
-- (((\regs|data_out[7]~52_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111001110001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \regs|data_out[7]~51_combout\,
	datab => \regs|data_out[1]~9_combout\,
	datac => \regs|dbr[3][7]~q\,
	datad => \regs|data_out[7]~52_combout\,
	combout => \regs|data_out[7]~53_combout\);

-- Location: LCCOMB_X24_Y34_N24
\regs|data_out[7]~54\ : cycloneiv_lcell_comb
-- Equation(s):
-- \regs|data_out[7]~54_combout\ = (\regs|data_out[1]~6_combout\ & ((\regs|data_out[1]~5_combout\) # ((\regs|dbr[8][7]~q\)))) # (!\regs|data_out[1]~6_combout\ & (!\regs|data_out[1]~5_combout\ & ((\regs|data_out[7]~53_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011100110101000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \regs|data_out[1]~6_combout\,
	datab => \regs|data_out[1]~5_combout\,
	datac => \regs|dbr[8][7]~q\,
	datad => \regs|data_out[7]~53_combout\,
	combout => \regs|data_out[7]~54_combout\);

-- Location: IOIBUF_X16_Y41_N8
\lea_mem_in[7]~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_lea_mem_in(7),
	o => \lea_mem_in[7]~input_o\);

-- Location: FF_X24_Y34_N19
\regs|dbr[9][7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk~inputclkctrl_outclk\,
	asdata => \lea_mem_in[7]~input_o\,
	sload => VCC,
	ena => \control_logic|state.s9~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \regs|dbr[9][7]~q\);

-- Location: LCCOMB_X24_Y34_N18
\regs|data_out[7]~55\ : cycloneiv_lcell_comb
-- Equation(s):
-- \regs|data_out[7]~55_combout\ = (\regs|data_out[7]~54_combout\ & (((\regs|dbr[9][7]~q\) # (!\regs|data_out[1]~5_combout\)))) # (!\regs|data_out[7]~54_combout\ & (\regs|dbr[0][7]~q\ & ((\regs|data_out[1]~5_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110010010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \regs|data_out[7]~54_combout\,
	datab => \regs|dbr[0][7]~q\,
	datac => \regs|dbr[9][7]~q\,
	datad => \regs|data_out[1]~5_combout\,
	combout => \regs|data_out[7]~55_combout\);

-- Location: LCCOMB_X29_Y34_N30
\regs|data_out[7]~56\ : cycloneiv_lcell_comb
-- Equation(s):
-- \regs|data_out[7]~56_combout\ = (\regs|data_out[7]~55_combout\ & ((!\regs|data_out[1]~6_combout\) # (!\reg_addr|mem\(1))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010101000101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \regs|data_out[7]~55_combout\,
	datab => \reg_addr|mem\(1),
	datac => \regs|data_out[1]~6_combout\,
	combout => \regs|data_out[7]~56_combout\);

ww_inst_out(0) <= \inst_out[0]~output_o\;

ww_inst_out(1) <= \inst_out[1]~output_o\;

ww_inst_out(2) <= \inst_out[2]~output_o\;

ww_inst_out(3) <= \inst_out[3]~output_o\;

ww_inst_out(4) <= \inst_out[4]~output_o\;

ww_inst_out(5) <= \inst_out[5]~output_o\;

ww_inst_out(6) <= \inst_out[6]~output_o\;

ww_inst_out(7) <= \inst_out[7]~output_o\;

ww_inst_out(8) <= \inst_out[8]~output_o\;

ww_inst_out(9) <= \inst_out[9]~output_o\;

ww_inst_out(10) <= \inst_out[10]~output_o\;

ww_inst_out(11) <= \inst_out[11]~output_o\;

ww_inst_out(12) <= \inst_out[12]~output_o\;

ww_inst_out(13) <= \inst_out[13]~output_o\;

ww_inst_out(14) <= \inst_out[14]~output_o\;

ww_inst_out(15) <= \inst_out[15]~output_o\;

ww_inst_out(16) <= \inst_out[16]~output_o\;

ww_inst_out(17) <= \inst_out[17]~output_o\;

ww_inst_out(18) <= \inst_out[18]~output_o\;

ww_inst_out(19) <= \inst_out[19]~output_o\;

ww_inst_out(20) <= \inst_out[20]~output_o\;

ww_inst_out(21) <= \inst_out[21]~output_o\;

ww_inst_out(22) <= \inst_out[22]~output_o\;

ww_inst_out(23) <= \inst_out[23]~output_o\;

ww_inst_out(24) <= \inst_out[24]~output_o\;

ww_inst_out(25) <= \inst_out[25]~output_o\;

ww_inst_out(26) <= \inst_out[26]~output_o\;

ww_inst_out(27) <= \inst_out[27]~output_o\;

ww_inst_out(28) <= \inst_out[28]~output_o\;

ww_inst_out(29) <= \inst_out[29]~output_o\;

ww_inst_out(30) <= \inst_out[30]~output_o\;

ww_inst_out(31) <= \inst_out[31]~output_o\;

ww_inst_out(32) <= \inst_out[32]~output_o\;

ww_inst_out(33) <= \inst_out[33]~output_o\;

ww_inst_out(34) <= \inst_out[34]~output_o\;

ww_inst_out(35) <= \inst_out[35]~output_o\;

ww_inst_out(36) <= \inst_out[36]~output_o\;

ww_inst_out(37) <= \inst_out[37]~output_o\;

ww_spi_out(0) <= \spi_out[0]~output_o\;

ww_spi_out(1) <= \spi_out[1]~output_o\;

ww_spi_out(2) <= \spi_out[2]~output_o\;

ww_spi_out(3) <= \spi_out[3]~output_o\;

ww_spi_out(4) <= \spi_out[4]~output_o\;

ww_spi_out(5) <= \spi_out[5]~output_o\;

ww_spi_out(6) <= \spi_out[6]~output_o\;

ww_spi_out(7) <= \spi_out[7]~output_o\;

ww_lea_exec <= \lea_exec~output_o\;
END structure;


