library ieee;
use ieee.std_logic_1164.all;

entity transmitter is
	generic (
		data_length : integer := 8
	);
	port(
		sck : in std_logic;
      ss : in std_logic;
		data : in std_logic_vector (data_length-1 downto 0);
		miso : out std_logic;
		busy : out std_logic
	);
end transmitter;

architecture behavioral of transmitter is

signal index  : integer := data_length-1;

begin

	process(ss, index)
	begin
		if (ss = '1') then 
			miso <= 'Z';
		else
			miso <= data(index);
		end if;
	end process;

	process(sck, ss)
	begin
		if falling_edge(sck) and ss = '0' then
			if (index > 0) then 
				index <= index - 1;
			else 
				index <= data_length-1;
			end if;
		end if;
	end process;	

	process(index)
	begin
		if (index = data_length - 1) then
			busy <= '0';
		else
			busy <= '1';
		end if;
	end process;
	
end behavioral;
