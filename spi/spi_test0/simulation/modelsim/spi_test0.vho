-- Copyright (C) 1991-2013 Altera Corporation
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, Altera MegaCore Function License 
-- Agreement, or other applicable license agreement, including, 
-- without limitation, that your use is for the sole purpose of 
-- programming logic devices manufactured by Altera and sold by 
-- Altera or its authorized distributors.  Please refer to the 
-- applicable agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus II 32-bit"
-- VERSION "Version 13.1.0 Build 162 10/23/2013 SJ Web Edition"

-- DATE "11/10/2020 10:45:31"

-- 
-- Device: Altera EP4CE10F17C8 Package FBGA256
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL) only
-- 

LIBRARY ALTERA;
LIBRARY CYCLONEIVE;
LIBRARY IEEE;
USE ALTERA.ALTERA_PRIMITIVES_COMPONENTS.ALL;
USE CYCLONEIVE.CYCLONEIVE_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	spi_slave IS
    PORT (
	sck : IN std_logic;
	ss : IN std_logic;
	mosi : IN std_logic;
	miso : BUFFER std_logic;
	busy : BUFFER std_logic
	);
END spi_slave;

-- Design Ports Information
-- miso	=>  Location: PIN_D6,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- busy	=>  Location: PIN_B7,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ss	=>  Location: PIN_C8,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- sck	=>  Location: PIN_E8,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- mosi	=>  Location: PIN_E7,	 I/O Standard: 2.5 V,	 Current Strength: Default


ARCHITECTURE structure OF spi_slave IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_sck : std_logic;
SIGNAL ww_ss : std_logic;
SIGNAL ww_mosi : std_logic;
SIGNAL ww_miso : std_logic;
SIGNAL ww_busy : std_logic;
SIGNAL \miso~output_o\ : std_logic;
SIGNAL \busy~output_o\ : std_logic;
SIGNAL \sck~input_o\ : std_logic;
SIGNAL \trns|Add0~0_combout\ : std_logic;
SIGNAL \trns|Add0~2_combout\ : std_logic;
SIGNAL \ss~input_o\ : std_logic;
SIGNAL \trns|Add0~1\ : std_logic;
SIGNAL \trns|Add0~4\ : std_logic;
SIGNAL \trns|Add0~7\ : std_logic;
SIGNAL \trns|Add0~9_combout\ : std_logic;
SIGNAL \trns|Add0~68_combout\ : std_logic;
SIGNAL \trns|Add0~10\ : std_logic;
SIGNAL \trns|Add0~11_combout\ : std_logic;
SIGNAL \trns|Add0~69_combout\ : std_logic;
SIGNAL \trns|Add0~12\ : std_logic;
SIGNAL \trns|Add0~13_combout\ : std_logic;
SIGNAL \trns|Add0~70_combout\ : std_logic;
SIGNAL \trns|Add0~14\ : std_logic;
SIGNAL \trns|Add0~15_combout\ : std_logic;
SIGNAL \trns|Add0~71_combout\ : std_logic;
SIGNAL \trns|Add0~16\ : std_logic;
SIGNAL \trns|Add0~17_combout\ : std_logic;
SIGNAL \trns|Add0~72_combout\ : std_logic;
SIGNAL \trns|Add0~18\ : std_logic;
SIGNAL \trns|Add0~19_combout\ : std_logic;
SIGNAL \trns|Add0~73_combout\ : std_logic;
SIGNAL \trns|Add0~20\ : std_logic;
SIGNAL \trns|Add0~21_combout\ : std_logic;
SIGNAL \trns|Add0~74_combout\ : std_logic;
SIGNAL \trns|Add0~22\ : std_logic;
SIGNAL \trns|Add0~23_combout\ : std_logic;
SIGNAL \trns|Add0~75_combout\ : std_logic;
SIGNAL \trns|Add0~24\ : std_logic;
SIGNAL \trns|Add0~25_combout\ : std_logic;
SIGNAL \trns|Add0~76_combout\ : std_logic;
SIGNAL \trns|Add0~26\ : std_logic;
SIGNAL \trns|Add0~27_combout\ : std_logic;
SIGNAL \trns|Add0~77_combout\ : std_logic;
SIGNAL \trns|Add0~28\ : std_logic;
SIGNAL \trns|Add0~29_combout\ : std_logic;
SIGNAL \trns|Add0~78_combout\ : std_logic;
SIGNAL \trns|Add0~30\ : std_logic;
SIGNAL \trns|Add0~31_combout\ : std_logic;
SIGNAL \trns|Add0~79_combout\ : std_logic;
SIGNAL \trns|Add0~32\ : std_logic;
SIGNAL \trns|Add0~33_combout\ : std_logic;
SIGNAL \trns|Add0~80_combout\ : std_logic;
SIGNAL \trns|Add0~34\ : std_logic;
SIGNAL \trns|Add0~35_combout\ : std_logic;
SIGNAL \trns|Add0~81_combout\ : std_logic;
SIGNAL \trns|Add0~36\ : std_logic;
SIGNAL \trns|Add0~37_combout\ : std_logic;
SIGNAL \trns|Add0~82_combout\ : std_logic;
SIGNAL \trns|Add0~38\ : std_logic;
SIGNAL \trns|Add0~39_combout\ : std_logic;
SIGNAL \trns|Add0~83_combout\ : std_logic;
SIGNAL \trns|Add0~40\ : std_logic;
SIGNAL \trns|Add0~41_combout\ : std_logic;
SIGNAL \trns|Add0~84_combout\ : std_logic;
SIGNAL \trns|Add0~42\ : std_logic;
SIGNAL \trns|Add0~43_combout\ : std_logic;
SIGNAL \trns|Add0~85_combout\ : std_logic;
SIGNAL \trns|Add0~44\ : std_logic;
SIGNAL \trns|Add0~45_combout\ : std_logic;
SIGNAL \trns|Add0~86_combout\ : std_logic;
SIGNAL \trns|Add0~46\ : std_logic;
SIGNAL \trns|Add0~47_combout\ : std_logic;
SIGNAL \trns|Add0~87_combout\ : std_logic;
SIGNAL \trns|Add0~48\ : std_logic;
SIGNAL \trns|Add0~49_combout\ : std_logic;
SIGNAL \trns|Add0~88_combout\ : std_logic;
SIGNAL \trns|Add0~50\ : std_logic;
SIGNAL \trns|Add0~51_combout\ : std_logic;
SIGNAL \trns|Add0~89_combout\ : std_logic;
SIGNAL \trns|Add0~52\ : std_logic;
SIGNAL \trns|Add0~53_combout\ : std_logic;
SIGNAL \trns|Add0~90_combout\ : std_logic;
SIGNAL \trns|Add0~54\ : std_logic;
SIGNAL \trns|Add0~55_combout\ : std_logic;
SIGNAL \trns|Add0~91_combout\ : std_logic;
SIGNAL \trns|Add0~56\ : std_logic;
SIGNAL \trns|Add0~57_combout\ : std_logic;
SIGNAL \trns|Add0~92_combout\ : std_logic;
SIGNAL \trns|Add0~58\ : std_logic;
SIGNAL \trns|Add0~59_combout\ : std_logic;
SIGNAL \trns|Add0~93_combout\ : std_logic;
SIGNAL \trns|Add0~60\ : std_logic;
SIGNAL \trns|Add0~61_combout\ : std_logic;
SIGNAL \trns|Add0~94_combout\ : std_logic;
SIGNAL \trns|Add0~62\ : std_logic;
SIGNAL \trns|Add0~63_combout\ : std_logic;
SIGNAL \trns|Add0~95_combout\ : std_logic;
SIGNAL \trns|Add0~64\ : std_logic;
SIGNAL \trns|Add0~65_combout\ : std_logic;
SIGNAL \trns|Add0~67_combout\ : std_logic;
SIGNAL \trns|Add0~3_combout\ : std_logic;
SIGNAL \trns|Add0~5_combout\ : std_logic;
SIGNAL \busy~17_combout\ : std_logic;
SIGNAL \busy~16_combout\ : std_logic;
SIGNAL \busy~18_combout\ : std_logic;
SIGNAL \busy~14_combout\ : std_logic;
SIGNAL \busy~13_combout\ : std_logic;
SIGNAL \busy~12_combout\ : std_logic;
SIGNAL \busy~11_combout\ : std_logic;
SIGNAL \busy~15_combout\ : std_logic;
SIGNAL \busy~19_combout\ : std_logic;
SIGNAL \trns|LessThan0~0_combout\ : std_logic;
SIGNAL \trns|Add0~6_combout\ : std_logic;
SIGNAL \trns|Add0~8_combout\ : std_logic;
SIGNAL \mosi~input_o\ : std_logic;
SIGNAL \recv|Add0~0_combout\ : std_logic;
SIGNAL \recv|Add0~2_combout\ : std_logic;
SIGNAL \recv|Add0~7\ : std_logic;
SIGNAL \recv|Add0~9_combout\ : std_logic;
SIGNAL \recv|Add0~68_combout\ : std_logic;
SIGNAL \recv|Add0~10\ : std_logic;
SIGNAL \recv|Add0~11_combout\ : std_logic;
SIGNAL \recv|Add0~69_combout\ : std_logic;
SIGNAL \recv|Add0~12\ : std_logic;
SIGNAL \recv|Add0~13_combout\ : std_logic;
SIGNAL \recv|Add0~70_combout\ : std_logic;
SIGNAL \recv|Add0~14\ : std_logic;
SIGNAL \recv|Add0~15_combout\ : std_logic;
SIGNAL \recv|Add0~71_combout\ : std_logic;
SIGNAL \recv|Add0~16\ : std_logic;
SIGNAL \recv|Add0~17_combout\ : std_logic;
SIGNAL \recv|Add0~72_combout\ : std_logic;
SIGNAL \recv|Add0~18\ : std_logic;
SIGNAL \recv|Add0~19_combout\ : std_logic;
SIGNAL \recv|Add0~73_combout\ : std_logic;
SIGNAL \recv|Add0~20\ : std_logic;
SIGNAL \recv|Add0~21_combout\ : std_logic;
SIGNAL \recv|Add0~74_combout\ : std_logic;
SIGNAL \recv|Add0~22\ : std_logic;
SIGNAL \recv|Add0~23_combout\ : std_logic;
SIGNAL \recv|Add0~75_combout\ : std_logic;
SIGNAL \recv|Add0~24\ : std_logic;
SIGNAL \recv|Add0~25_combout\ : std_logic;
SIGNAL \recv|Add0~76_combout\ : std_logic;
SIGNAL \recv|Add0~26\ : std_logic;
SIGNAL \recv|Add0~27_combout\ : std_logic;
SIGNAL \recv|Add0~77_combout\ : std_logic;
SIGNAL \recv|Add0~28\ : std_logic;
SIGNAL \recv|Add0~29_combout\ : std_logic;
SIGNAL \recv|Add0~78_combout\ : std_logic;
SIGNAL \recv|Add0~30\ : std_logic;
SIGNAL \recv|Add0~31_combout\ : std_logic;
SIGNAL \recv|Add0~79_combout\ : std_logic;
SIGNAL \recv|Add0~32\ : std_logic;
SIGNAL \recv|Add0~33_combout\ : std_logic;
SIGNAL \recv|Add0~80_combout\ : std_logic;
SIGNAL \recv|Add0~34\ : std_logic;
SIGNAL \recv|Add0~35_combout\ : std_logic;
SIGNAL \recv|Add0~81_combout\ : std_logic;
SIGNAL \recv|Add0~36\ : std_logic;
SIGNAL \recv|Add0~37_combout\ : std_logic;
SIGNAL \recv|Add0~82_combout\ : std_logic;
SIGNAL \recv|Add0~38\ : std_logic;
SIGNAL \recv|Add0~39_combout\ : std_logic;
SIGNAL \recv|Add0~83_combout\ : std_logic;
SIGNAL \recv|Add0~40\ : std_logic;
SIGNAL \recv|Add0~41_combout\ : std_logic;
SIGNAL \recv|Add0~84_combout\ : std_logic;
SIGNAL \recv|Add0~42\ : std_logic;
SIGNAL \recv|Add0~43_combout\ : std_logic;
SIGNAL \recv|Add0~85_combout\ : std_logic;
SIGNAL \recv|Add0~44\ : std_logic;
SIGNAL \recv|Add0~45_combout\ : std_logic;
SIGNAL \recv|Add0~86_combout\ : std_logic;
SIGNAL \recv|Add0~46\ : std_logic;
SIGNAL \recv|Add0~47_combout\ : std_logic;
SIGNAL \recv|Add0~87_combout\ : std_logic;
SIGNAL \busy~7_combout\ : std_logic;
SIGNAL \recv|Add0~48\ : std_logic;
SIGNAL \recv|Add0~49_combout\ : std_logic;
SIGNAL \recv|Add0~88_combout\ : std_logic;
SIGNAL \recv|Add0~50\ : std_logic;
SIGNAL \recv|Add0~51_combout\ : std_logic;
SIGNAL \recv|Add0~89_combout\ : std_logic;
SIGNAL \recv|Add0~52\ : std_logic;
SIGNAL \recv|Add0~53_combout\ : std_logic;
SIGNAL \recv|Add0~90_combout\ : std_logic;
SIGNAL \recv|Add0~54\ : std_logic;
SIGNAL \recv|Add0~55_combout\ : std_logic;
SIGNAL \recv|Add0~91_combout\ : std_logic;
SIGNAL \busy~8_combout\ : std_logic;
SIGNAL \recv|Add0~56\ : std_logic;
SIGNAL \recv|Add0~57_combout\ : std_logic;
SIGNAL \recv|Add0~92_combout\ : std_logic;
SIGNAL \recv|Add0~58\ : std_logic;
SIGNAL \recv|Add0~59_combout\ : std_logic;
SIGNAL \recv|Add0~93_combout\ : std_logic;
SIGNAL \recv|Add0~60\ : std_logic;
SIGNAL \recv|Add0~61_combout\ : std_logic;
SIGNAL \recv|Add0~94_combout\ : std_logic;
SIGNAL \recv|Add0~62\ : std_logic;
SIGNAL \recv|Add0~63_combout\ : std_logic;
SIGNAL \recv|Add0~95_combout\ : std_logic;
SIGNAL \busy~9_combout\ : std_logic;
SIGNAL \busy~3_combout\ : std_logic;
SIGNAL \busy~4_combout\ : std_logic;
SIGNAL \busy~2_combout\ : std_logic;
SIGNAL \busy~5_combout\ : std_logic;
SIGNAL \busy~6_combout\ : std_logic;
SIGNAL \busy~10_combout\ : std_logic;
SIGNAL \recv|LessThan0~0_combout\ : std_logic;
SIGNAL \recv|Add0~64\ : std_logic;
SIGNAL \recv|Add0~65_combout\ : std_logic;
SIGNAL \recv|Add0~67_combout\ : std_logic;
SIGNAL \recv|Add0~1\ : std_logic;
SIGNAL \recv|Add0~3_combout\ : std_logic;
SIGNAL \recv|Add0~5_combout\ : std_logic;
SIGNAL \recv|Add0~4\ : std_logic;
SIGNAL \recv|Add0~6_combout\ : std_logic;
SIGNAL \recv|Add0~8_combout\ : std_logic;
SIGNAL \recv|Decoder0~7_combout\ : std_logic;
SIGNAL \recv|Decoder0~4_combout\ : std_logic;
SIGNAL \recv|Decoder0~6_combout\ : std_logic;
SIGNAL \recv|Decoder0~5_combout\ : std_logic;
SIGNAL \trns|Mux0~2_combout\ : std_logic;
SIGNAL \trns|Mux0~3_combout\ : std_logic;
SIGNAL \recv|Decoder0~0_combout\ : std_logic;
SIGNAL \recv|Decoder0~3_combout\ : std_logic;
SIGNAL \recv|Decoder0~1_combout\ : std_logic;
SIGNAL \recv|Decoder0~2_combout\ : std_logic;
SIGNAL \trns|Mux0~0_combout\ : std_logic;
SIGNAL \trns|Mux0~1_combout\ : std_logic;
SIGNAL \trns|Mux0~4_combout\ : std_logic;
SIGNAL \busy~0_combout\ : std_logic;
SIGNAL \busy~1_combout\ : std_logic;
SIGNAL \busy~20_combout\ : std_logic;
SIGNAL \trns|index\ : std_logic_vector(31 DOWNTO 0);
SIGNAL \recv|data_temp\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \recv|index\ : std_logic_vector(31 DOWNTO 0);
SIGNAL \ALT_INV_sck~input_o\ : std_logic;
SIGNAL \ALT_INV_ss~input_o\ : std_logic;

BEGIN

ww_sck <= sck;
ww_ss <= ss;
ww_mosi <= mosi;
miso <= ww_miso;
busy <= ww_busy;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;
\ALT_INV_sck~input_o\ <= NOT \sck~input_o\;
\ALT_INV_ss~input_o\ <= NOT \ss~input_o\;

-- Location: IOOBUF_X3_Y24_N9
\miso~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \trns|Mux0~4_combout\,
	oe => \ALT_INV_ss~input_o\,
	devoe => ww_devoe,
	o => \miso~output_o\);

-- Location: IOOBUF_X11_Y24_N9
\busy~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \busy~20_combout\,
	devoe => ww_devoe,
	o => \busy~output_o\);

-- Location: IOIBUF_X13_Y24_N15
\sck~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_sck,
	o => \sck~input_o\);

-- Location: LCCOMB_X11_Y21_N0
\trns|Add0~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \trns|Add0~0_combout\ = \trns|index\(0) $ (GND)
-- \trns|Add0~1\ = CARRY(!\trns|index\(0))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110000110011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \trns|index\(0),
	datad => VCC,
	combout => \trns|Add0~0_combout\,
	cout => \trns|Add0~1\);

-- Location: LCCOMB_X13_Y21_N20
\trns|Add0~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \trns|Add0~2_combout\ = (!\trns|index\(31) & (\trns|LessThan0~0_combout\ & !\trns|Add0~0_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \trns|index\(31),
	datac => \trns|LessThan0~0_combout\,
	datad => \trns|Add0~0_combout\,
	combout => \trns|Add0~2_combout\);

-- Location: IOIBUF_X13_Y24_N1
\ss~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_ss,
	o => \ss~input_o\);

-- Location: FF_X13_Y21_N21
\trns|index[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_sck~input_o\,
	d => \trns|Add0~2_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \trns|index\(0));

-- Location: LCCOMB_X11_Y21_N2
\trns|Add0~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \trns|Add0~3_combout\ = (\trns|index\(1) & (!\trns|Add0~1\)) # (!\trns|index\(1) & (\trns|Add0~1\ & VCC))
-- \trns|Add0~4\ = CARRY((\trns|index\(1) & !\trns|Add0~1\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101000001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \trns|index\(1),
	datad => VCC,
	cin => \trns|Add0~1\,
	combout => \trns|Add0~3_combout\,
	cout => \trns|Add0~4\);

-- Location: LCCOMB_X11_Y21_N4
\trns|Add0~6\ : cycloneive_lcell_comb
-- Equation(s):
-- \trns|Add0~6_combout\ = (\trns|index\(2) & (\trns|Add0~4\ $ (GND))) # (!\trns|index\(2) & ((GND) # (!\trns|Add0~4\)))
-- \trns|Add0~7\ = CARRY((!\trns|Add0~4\) # (!\trns|index\(2)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010101011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \trns|index\(2),
	datad => VCC,
	cin => \trns|Add0~4\,
	combout => \trns|Add0~6_combout\,
	cout => \trns|Add0~7\);

-- Location: LCCOMB_X11_Y21_N6
\trns|Add0~9\ : cycloneive_lcell_comb
-- Equation(s):
-- \trns|Add0~9_combout\ = (\trns|index\(3) & (\trns|Add0~7\ & VCC)) # (!\trns|index\(3) & (!\trns|Add0~7\))
-- \trns|Add0~10\ = CARRY((!\trns|index\(3) & !\trns|Add0~7\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \trns|index\(3),
	datad => VCC,
	cin => \trns|Add0~7\,
	combout => \trns|Add0~9_combout\,
	cout => \trns|Add0~10\);

-- Location: LCCOMB_X12_Y21_N12
\trns|Add0~68\ : cycloneive_lcell_comb
-- Equation(s):
-- \trns|Add0~68_combout\ = (\trns|LessThan0~0_combout\ & (!\trns|index\(31) & \trns|Add0~9_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010000000100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \trns|LessThan0~0_combout\,
	datab => \trns|index\(31),
	datac => \trns|Add0~9_combout\,
	combout => \trns|Add0~68_combout\);

-- Location: FF_X12_Y21_N13
\trns|index[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_sck~input_o\,
	d => \trns|Add0~68_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \trns|index\(3));

-- Location: LCCOMB_X11_Y21_N8
\trns|Add0~11\ : cycloneive_lcell_comb
-- Equation(s):
-- \trns|Add0~11_combout\ = (\trns|index\(4) & ((GND) # (!\trns|Add0~10\))) # (!\trns|index\(4) & (\trns|Add0~10\ $ (GND)))
-- \trns|Add0~12\ = CARRY((\trns|index\(4)) # (!\trns|Add0~10\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \trns|index\(4),
	datad => VCC,
	cin => \trns|Add0~10\,
	combout => \trns|Add0~11_combout\,
	cout => \trns|Add0~12\);

-- Location: LCCOMB_X12_Y21_N22
\trns|Add0~69\ : cycloneive_lcell_comb
-- Equation(s):
-- \trns|Add0~69_combout\ = (!\trns|index\(31) & (\trns|LessThan0~0_combout\ & \trns|Add0~11_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \trns|index\(31),
	datac => \trns|LessThan0~0_combout\,
	datad => \trns|Add0~11_combout\,
	combout => \trns|Add0~69_combout\);

-- Location: FF_X12_Y21_N23
\trns|index[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_sck~input_o\,
	d => \trns|Add0~69_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \trns|index\(4));

-- Location: LCCOMB_X11_Y21_N10
\trns|Add0~13\ : cycloneive_lcell_comb
-- Equation(s):
-- \trns|Add0~13_combout\ = (\trns|index\(5) & (\trns|Add0~12\ & VCC)) # (!\trns|index\(5) & (!\trns|Add0~12\))
-- \trns|Add0~14\ = CARRY((!\trns|index\(5) & !\trns|Add0~12\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \trns|index\(5),
	datad => VCC,
	cin => \trns|Add0~12\,
	combout => \trns|Add0~13_combout\,
	cout => \trns|Add0~14\);

-- Location: LCCOMB_X13_Y21_N4
\trns|Add0~70\ : cycloneive_lcell_comb
-- Equation(s):
-- \trns|Add0~70_combout\ = (!\trns|index\(31) & (\trns|LessThan0~0_combout\ & \trns|Add0~13_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \trns|index\(31),
	datac => \trns|LessThan0~0_combout\,
	datad => \trns|Add0~13_combout\,
	combout => \trns|Add0~70_combout\);

-- Location: FF_X13_Y21_N5
\trns|index[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_sck~input_o\,
	d => \trns|Add0~70_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \trns|index\(5));

-- Location: LCCOMB_X11_Y21_N12
\trns|Add0~15\ : cycloneive_lcell_comb
-- Equation(s):
-- \trns|Add0~15_combout\ = (\trns|index\(6) & ((GND) # (!\trns|Add0~14\))) # (!\trns|index\(6) & (\trns|Add0~14\ $ (GND)))
-- \trns|Add0~16\ = CARRY((\trns|index\(6)) # (!\trns|Add0~14\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101010101111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \trns|index\(6),
	datad => VCC,
	cin => \trns|Add0~14\,
	combout => \trns|Add0~15_combout\,
	cout => \trns|Add0~16\);

-- Location: LCCOMB_X13_Y21_N22
\trns|Add0~71\ : cycloneive_lcell_comb
-- Equation(s):
-- \trns|Add0~71_combout\ = (!\trns|index\(31) & (\trns|LessThan0~0_combout\ & \trns|Add0~15_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \trns|index\(31),
	datac => \trns|LessThan0~0_combout\,
	datad => \trns|Add0~15_combout\,
	combout => \trns|Add0~71_combout\);

-- Location: FF_X13_Y21_N23
\trns|index[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_sck~input_o\,
	d => \trns|Add0~71_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \trns|index\(6));

-- Location: LCCOMB_X11_Y21_N14
\trns|Add0~17\ : cycloneive_lcell_comb
-- Equation(s):
-- \trns|Add0~17_combout\ = (\trns|index\(7) & (\trns|Add0~16\ & VCC)) # (!\trns|index\(7) & (!\trns|Add0~16\))
-- \trns|Add0~18\ = CARRY((!\trns|index\(7) & !\trns|Add0~16\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \trns|index\(7),
	datad => VCC,
	cin => \trns|Add0~16\,
	combout => \trns|Add0~17_combout\,
	cout => \trns|Add0~18\);

-- Location: LCCOMB_X12_Y21_N0
\trns|Add0~72\ : cycloneive_lcell_comb
-- Equation(s):
-- \trns|Add0~72_combout\ = (!\trns|index\(31) & (\trns|LessThan0~0_combout\ & \trns|Add0~17_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \trns|index\(31),
	datac => \trns|LessThan0~0_combout\,
	datad => \trns|Add0~17_combout\,
	combout => \trns|Add0~72_combout\);

-- Location: FF_X12_Y21_N1
\trns|index[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_sck~input_o\,
	d => \trns|Add0~72_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \trns|index\(7));

-- Location: LCCOMB_X11_Y21_N16
\trns|Add0~19\ : cycloneive_lcell_comb
-- Equation(s):
-- \trns|Add0~19_combout\ = (\trns|index\(8) & ((GND) # (!\trns|Add0~18\))) # (!\trns|index\(8) & (\trns|Add0~18\ $ (GND)))
-- \trns|Add0~20\ = CARRY((\trns|index\(8)) # (!\trns|Add0~18\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \trns|index\(8),
	datad => VCC,
	cin => \trns|Add0~18\,
	combout => \trns|Add0~19_combout\,
	cout => \trns|Add0~20\);

-- Location: LCCOMB_X12_Y21_N10
\trns|Add0~73\ : cycloneive_lcell_comb
-- Equation(s):
-- \trns|Add0~73_combout\ = (\trns|Add0~19_combout\ & (!\trns|index\(31) & \trns|LessThan0~0_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010000000100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \trns|Add0~19_combout\,
	datab => \trns|index\(31),
	datac => \trns|LessThan0~0_combout\,
	combout => \trns|Add0~73_combout\);

-- Location: FF_X12_Y21_N11
\trns|index[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_sck~input_o\,
	d => \trns|Add0~73_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \trns|index\(8));

-- Location: LCCOMB_X11_Y21_N18
\trns|Add0~21\ : cycloneive_lcell_comb
-- Equation(s):
-- \trns|Add0~21_combout\ = (\trns|index\(9) & (\trns|Add0~20\ & VCC)) # (!\trns|index\(9) & (!\trns|Add0~20\))
-- \trns|Add0~22\ = CARRY((!\trns|index\(9) & !\trns|Add0~20\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \trns|index\(9),
	datad => VCC,
	cin => \trns|Add0~20\,
	combout => \trns|Add0~21_combout\,
	cout => \trns|Add0~22\);

-- Location: LCCOMB_X12_Y21_N8
\trns|Add0~74\ : cycloneive_lcell_comb
-- Equation(s):
-- \trns|Add0~74_combout\ = (!\trns|index\(31) & (\trns|LessThan0~0_combout\ & \trns|Add0~21_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \trns|index\(31),
	datac => \trns|LessThan0~0_combout\,
	datad => \trns|Add0~21_combout\,
	combout => \trns|Add0~74_combout\);

-- Location: FF_X12_Y21_N9
\trns|index[9]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_sck~input_o\,
	d => \trns|Add0~74_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \trns|index\(9));

-- Location: LCCOMB_X11_Y21_N20
\trns|Add0~23\ : cycloneive_lcell_comb
-- Equation(s):
-- \trns|Add0~23_combout\ = (\trns|index\(10) & ((GND) # (!\trns|Add0~22\))) # (!\trns|index\(10) & (\trns|Add0~22\ $ (GND)))
-- \trns|Add0~24\ = CARRY((\trns|index\(10)) # (!\trns|Add0~22\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \trns|index\(10),
	datad => VCC,
	cin => \trns|Add0~22\,
	combout => \trns|Add0~23_combout\,
	cout => \trns|Add0~24\);

-- Location: LCCOMB_X12_Y21_N18
\trns|Add0~75\ : cycloneive_lcell_comb
-- Equation(s):
-- \trns|Add0~75_combout\ = (!\trns|index\(31) & (\trns|LessThan0~0_combout\ & \trns|Add0~23_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \trns|index\(31),
	datac => \trns|LessThan0~0_combout\,
	datad => \trns|Add0~23_combout\,
	combout => \trns|Add0~75_combout\);

-- Location: FF_X12_Y21_N19
\trns|index[10]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_sck~input_o\,
	d => \trns|Add0~75_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \trns|index\(10));

-- Location: LCCOMB_X11_Y21_N22
\trns|Add0~25\ : cycloneive_lcell_comb
-- Equation(s):
-- \trns|Add0~25_combout\ = (\trns|index\(11) & (\trns|Add0~24\ & VCC)) # (!\trns|index\(11) & (!\trns|Add0~24\))
-- \trns|Add0~26\ = CARRY((!\trns|index\(11) & !\trns|Add0~24\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100000101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \trns|index\(11),
	datad => VCC,
	cin => \trns|Add0~24\,
	combout => \trns|Add0~25_combout\,
	cout => \trns|Add0~26\);

-- Location: LCCOMB_X12_Y21_N4
\trns|Add0~76\ : cycloneive_lcell_comb
-- Equation(s):
-- \trns|Add0~76_combout\ = (!\trns|index\(31) & (\trns|LessThan0~0_combout\ & \trns|Add0~25_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \trns|index\(31),
	datac => \trns|LessThan0~0_combout\,
	datad => \trns|Add0~25_combout\,
	combout => \trns|Add0~76_combout\);

-- Location: FF_X12_Y21_N5
\trns|index[11]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_sck~input_o\,
	d => \trns|Add0~76_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \trns|index\(11));

-- Location: LCCOMB_X11_Y21_N24
\trns|Add0~27\ : cycloneive_lcell_comb
-- Equation(s):
-- \trns|Add0~27_combout\ = (\trns|index\(12) & ((GND) # (!\trns|Add0~26\))) # (!\trns|index\(12) & (\trns|Add0~26\ $ (GND)))
-- \trns|Add0~28\ = CARRY((\trns|index\(12)) # (!\trns|Add0~26\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \trns|index\(12),
	datad => VCC,
	cin => \trns|Add0~26\,
	combout => \trns|Add0~27_combout\,
	cout => \trns|Add0~28\);

-- Location: LCCOMB_X12_Y21_N14
\trns|Add0~77\ : cycloneive_lcell_comb
-- Equation(s):
-- \trns|Add0~77_combout\ = (!\trns|index\(31) & (\trns|LessThan0~0_combout\ & \trns|Add0~27_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \trns|index\(31),
	datac => \trns|LessThan0~0_combout\,
	datad => \trns|Add0~27_combout\,
	combout => \trns|Add0~77_combout\);

-- Location: FF_X12_Y21_N15
\trns|index[12]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_sck~input_o\,
	d => \trns|Add0~77_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \trns|index\(12));

-- Location: LCCOMB_X11_Y21_N26
\trns|Add0~29\ : cycloneive_lcell_comb
-- Equation(s):
-- \trns|Add0~29_combout\ = (\trns|index\(13) & (\trns|Add0~28\ & VCC)) # (!\trns|index\(13) & (!\trns|Add0~28\))
-- \trns|Add0~30\ = CARRY((!\trns|index\(13) & !\trns|Add0~28\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \trns|index\(13),
	datad => VCC,
	cin => \trns|Add0~28\,
	combout => \trns|Add0~29_combout\,
	cout => \trns|Add0~30\);

-- Location: LCCOMB_X12_Y21_N24
\trns|Add0~78\ : cycloneive_lcell_comb
-- Equation(s):
-- \trns|Add0~78_combout\ = (\trns|LessThan0~0_combout\ & (!\trns|index\(31) & \trns|Add0~29_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010000000100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \trns|LessThan0~0_combout\,
	datab => \trns|index\(31),
	datac => \trns|Add0~29_combout\,
	combout => \trns|Add0~78_combout\);

-- Location: FF_X12_Y21_N25
\trns|index[13]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_sck~input_o\,
	d => \trns|Add0~78_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \trns|index\(13));

-- Location: LCCOMB_X11_Y21_N28
\trns|Add0~31\ : cycloneive_lcell_comb
-- Equation(s):
-- \trns|Add0~31_combout\ = (\trns|index\(14) & ((GND) # (!\trns|Add0~30\))) # (!\trns|index\(14) & (\trns|Add0~30\ $ (GND)))
-- \trns|Add0~32\ = CARRY((\trns|index\(14)) # (!\trns|Add0~30\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101010101111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \trns|index\(14),
	datad => VCC,
	cin => \trns|Add0~30\,
	combout => \trns|Add0~31_combout\,
	cout => \trns|Add0~32\);

-- Location: LCCOMB_X12_Y21_N6
\trns|Add0~79\ : cycloneive_lcell_comb
-- Equation(s):
-- \trns|Add0~79_combout\ = (!\trns|index\(31) & (\trns|LessThan0~0_combout\ & \trns|Add0~31_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \trns|index\(31),
	datac => \trns|LessThan0~0_combout\,
	datad => \trns|Add0~31_combout\,
	combout => \trns|Add0~79_combout\);

-- Location: FF_X12_Y21_N7
\trns|index[14]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_sck~input_o\,
	d => \trns|Add0~79_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \trns|index\(14));

-- Location: LCCOMB_X11_Y21_N30
\trns|Add0~33\ : cycloneive_lcell_comb
-- Equation(s):
-- \trns|Add0~33_combout\ = (\trns|index\(15) & (\trns|Add0~32\ & VCC)) # (!\trns|index\(15) & (!\trns|Add0~32\))
-- \trns|Add0~34\ = CARRY((!\trns|index\(15) & !\trns|Add0~32\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \trns|index\(15),
	datad => VCC,
	cin => \trns|Add0~32\,
	combout => \trns|Add0~33_combout\,
	cout => \trns|Add0~34\);

-- Location: LCCOMB_X12_Y21_N28
\trns|Add0~80\ : cycloneive_lcell_comb
-- Equation(s):
-- \trns|Add0~80_combout\ = (!\trns|index\(31) & (\trns|LessThan0~0_combout\ & \trns|Add0~33_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \trns|index\(31),
	datac => \trns|LessThan0~0_combout\,
	datad => \trns|Add0~33_combout\,
	combout => \trns|Add0~80_combout\);

-- Location: FF_X12_Y21_N29
\trns|index[15]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_sck~input_o\,
	d => \trns|Add0~80_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \trns|index\(15));

-- Location: LCCOMB_X11_Y20_N0
\trns|Add0~35\ : cycloneive_lcell_comb
-- Equation(s):
-- \trns|Add0~35_combout\ = (\trns|index\(16) & ((GND) # (!\trns|Add0~34\))) # (!\trns|index\(16) & (\trns|Add0~34\ $ (GND)))
-- \trns|Add0~36\ = CARRY((\trns|index\(16)) # (!\trns|Add0~34\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \trns|index\(16),
	datad => VCC,
	cin => \trns|Add0~34\,
	combout => \trns|Add0~35_combout\,
	cout => \trns|Add0~36\);

-- Location: LCCOMB_X12_Y21_N30
\trns|Add0~81\ : cycloneive_lcell_comb
-- Equation(s):
-- \trns|Add0~81_combout\ = (!\trns|index\(31) & (\trns|LessThan0~0_combout\ & \trns|Add0~35_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \trns|index\(31),
	datac => \trns|LessThan0~0_combout\,
	datad => \trns|Add0~35_combout\,
	combout => \trns|Add0~81_combout\);

-- Location: FF_X12_Y21_N31
\trns|index[16]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_sck~input_o\,
	d => \trns|Add0~81_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \trns|index\(16));

-- Location: LCCOMB_X11_Y20_N2
\trns|Add0~37\ : cycloneive_lcell_comb
-- Equation(s):
-- \trns|Add0~37_combout\ = (\trns|index\(17) & (\trns|Add0~36\ & VCC)) # (!\trns|index\(17) & (!\trns|Add0~36\))
-- \trns|Add0~38\ = CARRY((!\trns|index\(17) & !\trns|Add0~36\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100000101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \trns|index\(17),
	datad => VCC,
	cin => \trns|Add0~36\,
	combout => \trns|Add0~37_combout\,
	cout => \trns|Add0~38\);

-- Location: LCCOMB_X12_Y21_N16
\trns|Add0~82\ : cycloneive_lcell_comb
-- Equation(s):
-- \trns|Add0~82_combout\ = (\trns|LessThan0~0_combout\ & (!\trns|index\(31) & \trns|Add0~37_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010000000100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \trns|LessThan0~0_combout\,
	datab => \trns|index\(31),
	datac => \trns|Add0~37_combout\,
	combout => \trns|Add0~82_combout\);

-- Location: FF_X12_Y21_N17
\trns|index[17]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_sck~input_o\,
	d => \trns|Add0~82_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \trns|index\(17));

-- Location: LCCOMB_X11_Y20_N4
\trns|Add0~39\ : cycloneive_lcell_comb
-- Equation(s):
-- \trns|Add0~39_combout\ = (\trns|index\(18) & ((GND) # (!\trns|Add0~38\))) # (!\trns|index\(18) & (\trns|Add0~38\ $ (GND)))
-- \trns|Add0~40\ = CARRY((\trns|index\(18)) # (!\trns|Add0~38\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101010101111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \trns|index\(18),
	datad => VCC,
	cin => \trns|Add0~38\,
	combout => \trns|Add0~39_combout\,
	cout => \trns|Add0~40\);

-- Location: LCCOMB_X12_Y20_N2
\trns|Add0~83\ : cycloneive_lcell_comb
-- Equation(s):
-- \trns|Add0~83_combout\ = (\trns|LessThan0~0_combout\ & (!\trns|index\(31) & \trns|Add0~39_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010000000100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \trns|LessThan0~0_combout\,
	datab => \trns|index\(31),
	datac => \trns|Add0~39_combout\,
	combout => \trns|Add0~83_combout\);

-- Location: FF_X12_Y20_N3
\trns|index[18]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_sck~input_o\,
	d => \trns|Add0~83_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \trns|index\(18));

-- Location: LCCOMB_X11_Y20_N6
\trns|Add0~41\ : cycloneive_lcell_comb
-- Equation(s):
-- \trns|Add0~41_combout\ = (\trns|index\(19) & (\trns|Add0~40\ & VCC)) # (!\trns|index\(19) & (!\trns|Add0~40\))
-- \trns|Add0~42\ = CARRY((!\trns|index\(19) & !\trns|Add0~40\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \trns|index\(19),
	datad => VCC,
	cin => \trns|Add0~40\,
	combout => \trns|Add0~41_combout\,
	cout => \trns|Add0~42\);

-- Location: LCCOMB_X12_Y20_N12
\trns|Add0~84\ : cycloneive_lcell_comb
-- Equation(s):
-- \trns|Add0~84_combout\ = (\trns|LessThan0~0_combout\ & (!\trns|index\(31) & \trns|Add0~41_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010000000100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \trns|LessThan0~0_combout\,
	datab => \trns|index\(31),
	datac => \trns|Add0~41_combout\,
	combout => \trns|Add0~84_combout\);

-- Location: FF_X12_Y20_N13
\trns|index[19]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_sck~input_o\,
	d => \trns|Add0~84_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \trns|index\(19));

-- Location: LCCOMB_X11_Y20_N8
\trns|Add0~43\ : cycloneive_lcell_comb
-- Equation(s):
-- \trns|Add0~43_combout\ = (\trns|index\(20) & ((GND) # (!\trns|Add0~42\))) # (!\trns|index\(20) & (\trns|Add0~42\ $ (GND)))
-- \trns|Add0~44\ = CARRY((\trns|index\(20)) # (!\trns|Add0~42\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \trns|index\(20),
	datad => VCC,
	cin => \trns|Add0~42\,
	combout => \trns|Add0~43_combout\,
	cout => \trns|Add0~44\);

-- Location: LCCOMB_X12_Y20_N22
\trns|Add0~85\ : cycloneive_lcell_comb
-- Equation(s):
-- \trns|Add0~85_combout\ = (!\trns|index\(31) & (\trns|LessThan0~0_combout\ & \trns|Add0~43_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \trns|index\(31),
	datac => \trns|LessThan0~0_combout\,
	datad => \trns|Add0~43_combout\,
	combout => \trns|Add0~85_combout\);

-- Location: FF_X12_Y20_N23
\trns|index[20]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_sck~input_o\,
	d => \trns|Add0~85_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \trns|index\(20));

-- Location: LCCOMB_X11_Y20_N10
\trns|Add0~45\ : cycloneive_lcell_comb
-- Equation(s):
-- \trns|Add0~45_combout\ = (\trns|index\(21) & (\trns|Add0~44\ & VCC)) # (!\trns|index\(21) & (!\trns|Add0~44\))
-- \trns|Add0~46\ = CARRY((!\trns|index\(21) & !\trns|Add0~44\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \trns|index\(21),
	datad => VCC,
	cin => \trns|Add0~44\,
	combout => \trns|Add0~45_combout\,
	cout => \trns|Add0~46\);

-- Location: LCCOMB_X12_Y20_N16
\trns|Add0~86\ : cycloneive_lcell_comb
-- Equation(s):
-- \trns|Add0~86_combout\ = (!\trns|index\(31) & (\trns|LessThan0~0_combout\ & \trns|Add0~45_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \trns|index\(31),
	datac => \trns|LessThan0~0_combout\,
	datad => \trns|Add0~45_combout\,
	combout => \trns|Add0~86_combout\);

-- Location: FF_X12_Y20_N17
\trns|index[21]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_sck~input_o\,
	d => \trns|Add0~86_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \trns|index\(21));

-- Location: LCCOMB_X11_Y20_N12
\trns|Add0~47\ : cycloneive_lcell_comb
-- Equation(s):
-- \trns|Add0~47_combout\ = (\trns|index\(22) & ((GND) # (!\trns|Add0~46\))) # (!\trns|index\(22) & (\trns|Add0~46\ $ (GND)))
-- \trns|Add0~48\ = CARRY((\trns|index\(22)) # (!\trns|Add0~46\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101010101111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \trns|index\(22),
	datad => VCC,
	cin => \trns|Add0~46\,
	combout => \trns|Add0~47_combout\,
	cout => \trns|Add0~48\);

-- Location: LCCOMB_X12_Y20_N30
\trns|Add0~87\ : cycloneive_lcell_comb
-- Equation(s):
-- \trns|Add0~87_combout\ = (!\trns|index\(31) & (\trns|LessThan0~0_combout\ & \trns|Add0~47_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \trns|index\(31),
	datac => \trns|LessThan0~0_combout\,
	datad => \trns|Add0~47_combout\,
	combout => \trns|Add0~87_combout\);

-- Location: FF_X12_Y20_N31
\trns|index[22]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_sck~input_o\,
	d => \trns|Add0~87_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \trns|index\(22));

-- Location: LCCOMB_X11_Y20_N14
\trns|Add0~49\ : cycloneive_lcell_comb
-- Equation(s):
-- \trns|Add0~49_combout\ = (\trns|index\(23) & (\trns|Add0~48\ & VCC)) # (!\trns|index\(23) & (!\trns|Add0~48\))
-- \trns|Add0~50\ = CARRY((!\trns|index\(23) & !\trns|Add0~48\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \trns|index\(23),
	datad => VCC,
	cin => \trns|Add0~48\,
	combout => \trns|Add0~49_combout\,
	cout => \trns|Add0~50\);

-- Location: LCCOMB_X12_Y20_N14
\trns|Add0~88\ : cycloneive_lcell_comb
-- Equation(s):
-- \trns|Add0~88_combout\ = (!\trns|index\(31) & (\trns|LessThan0~0_combout\ & \trns|Add0~49_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \trns|index\(31),
	datac => \trns|LessThan0~0_combout\,
	datad => \trns|Add0~49_combout\,
	combout => \trns|Add0~88_combout\);

-- Location: FF_X12_Y20_N15
\trns|index[23]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_sck~input_o\,
	d => \trns|Add0~88_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \trns|index\(23));

-- Location: LCCOMB_X11_Y20_N16
\trns|Add0~51\ : cycloneive_lcell_comb
-- Equation(s):
-- \trns|Add0~51_combout\ = (\trns|index\(24) & ((GND) # (!\trns|Add0~50\))) # (!\trns|index\(24) & (\trns|Add0~50\ $ (GND)))
-- \trns|Add0~52\ = CARRY((\trns|index\(24)) # (!\trns|Add0~50\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101010101111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \trns|index\(24),
	datad => VCC,
	cin => \trns|Add0~50\,
	combout => \trns|Add0~51_combout\,
	cout => \trns|Add0~52\);

-- Location: LCCOMB_X12_Y20_N20
\trns|Add0~89\ : cycloneive_lcell_comb
-- Equation(s):
-- \trns|Add0~89_combout\ = (\trns|LessThan0~0_combout\ & (!\trns|index\(31) & \trns|Add0~51_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010000000100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \trns|LessThan0~0_combout\,
	datab => \trns|index\(31),
	datac => \trns|Add0~51_combout\,
	combout => \trns|Add0~89_combout\);

-- Location: FF_X12_Y20_N21
\trns|index[24]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_sck~input_o\,
	d => \trns|Add0~89_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \trns|index\(24));

-- Location: LCCOMB_X11_Y20_N18
\trns|Add0~53\ : cycloneive_lcell_comb
-- Equation(s):
-- \trns|Add0~53_combout\ = (\trns|index\(25) & (\trns|Add0~52\ & VCC)) # (!\trns|index\(25) & (!\trns|Add0~52\))
-- \trns|Add0~54\ = CARRY((!\trns|index\(25) & !\trns|Add0~52\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100000101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \trns|index\(25),
	datad => VCC,
	cin => \trns|Add0~52\,
	combout => \trns|Add0~53_combout\,
	cout => \trns|Add0~54\);

-- Location: LCCOMB_X12_Y20_N26
\trns|Add0~90\ : cycloneive_lcell_comb
-- Equation(s):
-- \trns|Add0~90_combout\ = (!\trns|index\(31) & (\trns|LessThan0~0_combout\ & \trns|Add0~53_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \trns|index\(31),
	datac => \trns|LessThan0~0_combout\,
	datad => \trns|Add0~53_combout\,
	combout => \trns|Add0~90_combout\);

-- Location: FF_X12_Y20_N27
\trns|index[25]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_sck~input_o\,
	d => \trns|Add0~90_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \trns|index\(25));

-- Location: LCCOMB_X11_Y20_N20
\trns|Add0~55\ : cycloneive_lcell_comb
-- Equation(s):
-- \trns|Add0~55_combout\ = (\trns|index\(26) & ((GND) # (!\trns|Add0~54\))) # (!\trns|index\(26) & (\trns|Add0~54\ $ (GND)))
-- \trns|Add0~56\ = CARRY((\trns|index\(26)) # (!\trns|Add0~54\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \trns|index\(26),
	datad => VCC,
	cin => \trns|Add0~54\,
	combout => \trns|Add0~55_combout\,
	cout => \trns|Add0~56\);

-- Location: LCCOMB_X12_Y20_N24
\trns|Add0~91\ : cycloneive_lcell_comb
-- Equation(s):
-- \trns|Add0~91_combout\ = (!\trns|index\(31) & (\trns|LessThan0~0_combout\ & \trns|Add0~55_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \trns|index\(31),
	datac => \trns|LessThan0~0_combout\,
	datad => \trns|Add0~55_combout\,
	combout => \trns|Add0~91_combout\);

-- Location: FF_X12_Y20_N25
\trns|index[26]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_sck~input_o\,
	d => \trns|Add0~91_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \trns|index\(26));

-- Location: LCCOMB_X11_Y20_N22
\trns|Add0~57\ : cycloneive_lcell_comb
-- Equation(s):
-- \trns|Add0~57_combout\ = (\trns|index\(27) & (\trns|Add0~56\ & VCC)) # (!\trns|index\(27) & (!\trns|Add0~56\))
-- \trns|Add0~58\ = CARRY((!\trns|index\(27) & !\trns|Add0~56\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \trns|index\(27),
	datad => VCC,
	cin => \trns|Add0~56\,
	combout => \trns|Add0~57_combout\,
	cout => \trns|Add0~58\);

-- Location: LCCOMB_X12_Y20_N18
\trns|Add0~92\ : cycloneive_lcell_comb
-- Equation(s):
-- \trns|Add0~92_combout\ = (!\trns|index\(31) & (\trns|LessThan0~0_combout\ & \trns|Add0~57_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \trns|index\(31),
	datac => \trns|LessThan0~0_combout\,
	datad => \trns|Add0~57_combout\,
	combout => \trns|Add0~92_combout\);

-- Location: FF_X12_Y20_N19
\trns|index[27]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_sck~input_o\,
	d => \trns|Add0~92_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \trns|index\(27));

-- Location: LCCOMB_X11_Y20_N24
\trns|Add0~59\ : cycloneive_lcell_comb
-- Equation(s):
-- \trns|Add0~59_combout\ = (\trns|index\(28) & ((GND) # (!\trns|Add0~58\))) # (!\trns|index\(28) & (\trns|Add0~58\ $ (GND)))
-- \trns|Add0~60\ = CARRY((\trns|index\(28)) # (!\trns|Add0~58\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \trns|index\(28),
	datad => VCC,
	cin => \trns|Add0~58\,
	combout => \trns|Add0~59_combout\,
	cout => \trns|Add0~60\);

-- Location: LCCOMB_X12_Y20_N0
\trns|Add0~93\ : cycloneive_lcell_comb
-- Equation(s):
-- \trns|Add0~93_combout\ = (!\trns|index\(31) & (\trns|LessThan0~0_combout\ & \trns|Add0~59_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \trns|index\(31),
	datac => \trns|LessThan0~0_combout\,
	datad => \trns|Add0~59_combout\,
	combout => \trns|Add0~93_combout\);

-- Location: FF_X12_Y20_N1
\trns|index[28]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_sck~input_o\,
	d => \trns|Add0~93_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \trns|index\(28));

-- Location: LCCOMB_X11_Y20_N26
\trns|Add0~61\ : cycloneive_lcell_comb
-- Equation(s):
-- \trns|Add0~61_combout\ = (\trns|index\(29) & (\trns|Add0~60\ & VCC)) # (!\trns|index\(29) & (!\trns|Add0~60\))
-- \trns|Add0~62\ = CARRY((!\trns|index\(29) & !\trns|Add0~60\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \trns|index\(29),
	datad => VCC,
	cin => \trns|Add0~60\,
	combout => \trns|Add0~61_combout\,
	cout => \trns|Add0~62\);

-- Location: LCCOMB_X12_Y20_N10
\trns|Add0~94\ : cycloneive_lcell_comb
-- Equation(s):
-- \trns|Add0~94_combout\ = (\trns|LessThan0~0_combout\ & (!\trns|index\(31) & \trns|Add0~61_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010000000100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \trns|LessThan0~0_combout\,
	datab => \trns|index\(31),
	datac => \trns|Add0~61_combout\,
	combout => \trns|Add0~94_combout\);

-- Location: FF_X12_Y20_N11
\trns|index[29]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_sck~input_o\,
	d => \trns|Add0~94_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \trns|index\(29));

-- Location: LCCOMB_X11_Y20_N28
\trns|Add0~63\ : cycloneive_lcell_comb
-- Equation(s):
-- \trns|Add0~63_combout\ = (\trns|index\(30) & ((GND) # (!\trns|Add0~62\))) # (!\trns|index\(30) & (\trns|Add0~62\ $ (GND)))
-- \trns|Add0~64\ = CARRY((\trns|index\(30)) # (!\trns|Add0~62\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \trns|index\(30),
	datad => VCC,
	cin => \trns|Add0~62\,
	combout => \trns|Add0~63_combout\,
	cout => \trns|Add0~64\);

-- Location: LCCOMB_X12_Y20_N4
\trns|Add0~95\ : cycloneive_lcell_comb
-- Equation(s):
-- \trns|Add0~95_combout\ = (!\trns|index\(31) & (\trns|LessThan0~0_combout\ & \trns|Add0~63_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \trns|index\(31),
	datac => \trns|LessThan0~0_combout\,
	datad => \trns|Add0~63_combout\,
	combout => \trns|Add0~95_combout\);

-- Location: FF_X12_Y20_N5
\trns|index[30]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_sck~input_o\,
	d => \trns|Add0~95_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \trns|index\(30));

-- Location: LCCOMB_X11_Y20_N30
\trns|Add0~65\ : cycloneive_lcell_comb
-- Equation(s):
-- \trns|Add0~65_combout\ = \trns|Add0~64\ $ (!\trns|index\(31))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datad => \trns|index\(31),
	cin => \trns|Add0~64\,
	combout => \trns|Add0~65_combout\);

-- Location: LCCOMB_X12_Y20_N28
\trns|Add0~67\ : cycloneive_lcell_comb
-- Equation(s):
-- \trns|Add0~67_combout\ = (\trns|LessThan0~0_combout\ & (!\trns|index\(31) & \trns|Add0~65_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000101000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \trns|LessThan0~0_combout\,
	datac => \trns|index\(31),
	datad => \trns|Add0~65_combout\,
	combout => \trns|Add0~67_combout\);

-- Location: FF_X12_Y20_N29
\trns|index[31]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_sck~input_o\,
	d => \trns|Add0~67_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \trns|index\(31));

-- Location: LCCOMB_X12_Y21_N20
\trns|Add0~5\ : cycloneive_lcell_comb
-- Equation(s):
-- \trns|Add0~5_combout\ = (!\trns|index\(31) & (\trns|LessThan0~0_combout\ & !\trns|Add0~3_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \trns|index\(31),
	datac => \trns|LessThan0~0_combout\,
	datad => \trns|Add0~3_combout\,
	combout => \trns|Add0~5_combout\);

-- Location: FF_X12_Y21_N21
\trns|index[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_sck~input_o\,
	d => \trns|Add0~5_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \trns|index\(1));

-- Location: LCCOMB_X12_Y21_N26
\busy~17\ : cycloneive_lcell_comb
-- Equation(s):
-- \busy~17_combout\ = (!\trns|index\(26) & (!\trns|index\(25) & (!\trns|index\(24) & !\trns|index\(23))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \trns|index\(26),
	datab => \trns|index\(25),
	datac => \trns|index\(24),
	datad => \trns|index\(23),
	combout => \busy~17_combout\);

-- Location: LCCOMB_X12_Y20_N8
\busy~16\ : cycloneive_lcell_comb
-- Equation(s):
-- \busy~16_combout\ = (!\trns|index\(20) & (!\trns|index\(21) & (!\trns|index\(22) & !\trns|index\(19))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \trns|index\(20),
	datab => \trns|index\(21),
	datac => \trns|index\(22),
	datad => \trns|index\(19),
	combout => \busy~16_combout\);

-- Location: LCCOMB_X12_Y20_N6
\busy~18\ : cycloneive_lcell_comb
-- Equation(s):
-- \busy~18_combout\ = (!\trns|index\(29) & (!\trns|index\(27) & (!\trns|index\(30) & !\trns|index\(28))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \trns|index\(29),
	datab => \trns|index\(27),
	datac => \trns|index\(30),
	datad => \trns|index\(28),
	combout => \busy~18_combout\);

-- Location: LCCOMB_X13_Y21_N26
\busy~14\ : cycloneive_lcell_comb
-- Equation(s):
-- \busy~14_combout\ = (!\trns|index\(18) & (!\trns|index\(16) & (!\trns|index\(17) & !\trns|index\(15))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \trns|index\(18),
	datab => \trns|index\(16),
	datac => \trns|index\(17),
	datad => \trns|index\(15),
	combout => \busy~14_combout\);

-- Location: LCCOMB_X13_Y21_N16
\busy~13\ : cycloneive_lcell_comb
-- Equation(s):
-- \busy~13_combout\ = (!\trns|index\(12) & (!\trns|index\(11) & (!\trns|index\(14) & !\trns|index\(13))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \trns|index\(12),
	datab => \trns|index\(11),
	datac => \trns|index\(14),
	datad => \trns|index\(13),
	combout => \busy~13_combout\);

-- Location: LCCOMB_X13_Y21_N14
\busy~12\ : cycloneive_lcell_comb
-- Equation(s):
-- \busy~12_combout\ = (!\trns|index\(7) & (!\trns|index\(10) & (!\trns|index\(9) & !\trns|index\(8))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \trns|index\(7),
	datab => \trns|index\(10),
	datac => \trns|index\(9),
	datad => \trns|index\(8),
	combout => \busy~12_combout\);

-- Location: LCCOMB_X13_Y21_N24
\busy~11\ : cycloneive_lcell_comb
-- Equation(s):
-- \busy~11_combout\ = (!\trns|index\(3) & (!\trns|index\(5) & (!\trns|index\(6) & !\trns|index\(4))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \trns|index\(3),
	datab => \trns|index\(5),
	datac => \trns|index\(6),
	datad => \trns|index\(4),
	combout => \busy~11_combout\);

-- Location: LCCOMB_X13_Y21_N12
\busy~15\ : cycloneive_lcell_comb
-- Equation(s):
-- \busy~15_combout\ = (\busy~14_combout\ & (\busy~13_combout\ & (\busy~12_combout\ & \busy~11_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \busy~14_combout\,
	datab => \busy~13_combout\,
	datac => \busy~12_combout\,
	datad => \busy~11_combout\,
	combout => \busy~15_combout\);

-- Location: LCCOMB_X13_Y21_N10
\busy~19\ : cycloneive_lcell_comb
-- Equation(s):
-- \busy~19_combout\ = (\busy~17_combout\ & (\busy~16_combout\ & (\busy~18_combout\ & \busy~15_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \busy~17_combout\,
	datab => \busy~16_combout\,
	datac => \busy~18_combout\,
	datad => \busy~15_combout\,
	combout => \busy~19_combout\);

-- Location: LCCOMB_X13_Y21_N30
\trns|LessThan0~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \trns|LessThan0~0_combout\ = (((!\busy~19_combout\) # (!\trns|index\(0))) # (!\trns|index\(1))) # (!\trns|index\(2))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111111111111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \trns|index\(2),
	datab => \trns|index\(1),
	datac => \trns|index\(0),
	datad => \busy~19_combout\,
	combout => \trns|LessThan0~0_combout\);

-- Location: LCCOMB_X12_Y21_N2
\trns|Add0~8\ : cycloneive_lcell_comb
-- Equation(s):
-- \trns|Add0~8_combout\ = (\trns|LessThan0~0_combout\ & (!\trns|index\(31) & !\trns|Add0~6_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000001000000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \trns|LessThan0~0_combout\,
	datab => \trns|index\(31),
	datac => \trns|Add0~6_combout\,
	combout => \trns|Add0~8_combout\);

-- Location: FF_X12_Y21_N3
\trns|index[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_sck~input_o\,
	d => \trns|Add0~8_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \trns|index\(2));

-- Location: IOIBUF_X7_Y24_N1
\mosi~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_mosi,
	o => \mosi~input_o\);

-- Location: LCCOMB_X13_Y23_N0
\recv|Add0~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \recv|Add0~0_combout\ = \recv|index\(0) $ (GND)
-- \recv|Add0~1\ = CARRY(!\recv|index\(0))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101001010101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \recv|index\(0),
	datad => VCC,
	combout => \recv|Add0~0_combout\,
	cout => \recv|Add0~1\);

-- Location: LCCOMB_X14_Y23_N30
\recv|Add0~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \recv|Add0~2_combout\ = (!\recv|Add0~0_combout\ & (!\recv|index\(31) & \recv|LessThan0~0_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000000010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \recv|Add0~0_combout\,
	datab => \recv|index\(31),
	datac => \recv|LessThan0~0_combout\,
	combout => \recv|Add0~2_combout\);

-- Location: FF_X14_Y23_N31
\recv|index[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \sck~input_o\,
	d => \recv|Add0~2_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \recv|index\(0));

-- Location: LCCOMB_X13_Y23_N4
\recv|Add0~6\ : cycloneive_lcell_comb
-- Equation(s):
-- \recv|Add0~6_combout\ = (\recv|index\(2) & (\recv|Add0~4\ $ (GND))) # (!\recv|index\(2) & ((GND) # (!\recv|Add0~4\)))
-- \recv|Add0~7\ = CARRY((!\recv|Add0~4\) # (!\recv|index\(2)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \recv|index\(2),
	datad => VCC,
	cin => \recv|Add0~4\,
	combout => \recv|Add0~6_combout\,
	cout => \recv|Add0~7\);

-- Location: LCCOMB_X13_Y23_N6
\recv|Add0~9\ : cycloneive_lcell_comb
-- Equation(s):
-- \recv|Add0~9_combout\ = (\recv|index\(3) & (\recv|Add0~7\ & VCC)) # (!\recv|index\(3) & (!\recv|Add0~7\))
-- \recv|Add0~10\ = CARRY((!\recv|index\(3) & !\recv|Add0~7\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100000101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \recv|index\(3),
	datad => VCC,
	cin => \recv|Add0~7\,
	combout => \recv|Add0~9_combout\,
	cout => \recv|Add0~10\);

-- Location: LCCOMB_X12_Y23_N0
\recv|Add0~68\ : cycloneive_lcell_comb
-- Equation(s):
-- \recv|Add0~68_combout\ = (\recv|Add0~9_combout\ & (!\recv|index\(31) & \recv|LessThan0~0_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000101000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \recv|Add0~9_combout\,
	datac => \recv|index\(31),
	datad => \recv|LessThan0~0_combout\,
	combout => \recv|Add0~68_combout\);

-- Location: FF_X12_Y23_N1
\recv|index[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \sck~input_o\,
	d => \recv|Add0~68_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \recv|index\(3));

-- Location: LCCOMB_X13_Y23_N8
\recv|Add0~11\ : cycloneive_lcell_comb
-- Equation(s):
-- \recv|Add0~11_combout\ = (\recv|index\(4) & ((GND) # (!\recv|Add0~10\))) # (!\recv|index\(4) & (\recv|Add0~10\ $ (GND)))
-- \recv|Add0~12\ = CARRY((\recv|index\(4)) # (!\recv|Add0~10\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101010101111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \recv|index\(4),
	datad => VCC,
	cin => \recv|Add0~10\,
	combout => \recv|Add0~11_combout\,
	cout => \recv|Add0~12\);

-- Location: LCCOMB_X12_Y23_N14
\recv|Add0~69\ : cycloneive_lcell_comb
-- Equation(s):
-- \recv|Add0~69_combout\ = (!\recv|index\(31) & (\recv|Add0~11_combout\ & \recv|LessThan0~0_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \recv|index\(31),
	datac => \recv|Add0~11_combout\,
	datad => \recv|LessThan0~0_combout\,
	combout => \recv|Add0~69_combout\);

-- Location: FF_X12_Y23_N15
\recv|index[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \sck~input_o\,
	d => \recv|Add0~69_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \recv|index\(4));

-- Location: LCCOMB_X13_Y23_N10
\recv|Add0~13\ : cycloneive_lcell_comb
-- Equation(s):
-- \recv|Add0~13_combout\ = (\recv|index\(5) & (\recv|Add0~12\ & VCC)) # (!\recv|index\(5) & (!\recv|Add0~12\))
-- \recv|Add0~14\ = CARRY((!\recv|index\(5) & !\recv|Add0~12\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100000101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \recv|index\(5),
	datad => VCC,
	cin => \recv|Add0~12\,
	combout => \recv|Add0~13_combout\,
	cout => \recv|Add0~14\);

-- Location: LCCOMB_X12_Y23_N16
\recv|Add0~70\ : cycloneive_lcell_comb
-- Equation(s):
-- \recv|Add0~70_combout\ = (!\recv|index\(31) & (\recv|Add0~13_combout\ & \recv|LessThan0~0_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \recv|index\(31),
	datac => \recv|Add0~13_combout\,
	datad => \recv|LessThan0~0_combout\,
	combout => \recv|Add0~70_combout\);

-- Location: FF_X12_Y23_N17
\recv|index[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \sck~input_o\,
	d => \recv|Add0~70_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \recv|index\(5));

-- Location: LCCOMB_X13_Y23_N12
\recv|Add0~15\ : cycloneive_lcell_comb
-- Equation(s):
-- \recv|Add0~15_combout\ = (\recv|index\(6) & ((GND) # (!\recv|Add0~14\))) # (!\recv|index\(6) & (\recv|Add0~14\ $ (GND)))
-- \recv|Add0~16\ = CARRY((\recv|index\(6)) # (!\recv|Add0~14\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101010101111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \recv|index\(6),
	datad => VCC,
	cin => \recv|Add0~14\,
	combout => \recv|Add0~15_combout\,
	cout => \recv|Add0~16\);

-- Location: LCCOMB_X12_Y23_N10
\recv|Add0~71\ : cycloneive_lcell_comb
-- Equation(s):
-- \recv|Add0~71_combout\ = (!\recv|index\(31) & (\recv|Add0~15_combout\ & \recv|LessThan0~0_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \recv|index\(31),
	datac => \recv|Add0~15_combout\,
	datad => \recv|LessThan0~0_combout\,
	combout => \recv|Add0~71_combout\);

-- Location: FF_X12_Y23_N11
\recv|index[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \sck~input_o\,
	d => \recv|Add0~71_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \recv|index\(6));

-- Location: LCCOMB_X13_Y23_N14
\recv|Add0~17\ : cycloneive_lcell_comb
-- Equation(s):
-- \recv|Add0~17_combout\ = (\recv|index\(7) & (\recv|Add0~16\ & VCC)) # (!\recv|index\(7) & (!\recv|Add0~16\))
-- \recv|Add0~18\ = CARRY((!\recv|index\(7) & !\recv|Add0~16\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100000101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \recv|index\(7),
	datad => VCC,
	cin => \recv|Add0~16\,
	combout => \recv|Add0~17_combout\,
	cout => \recv|Add0~18\);

-- Location: LCCOMB_X12_Y23_N26
\recv|Add0~72\ : cycloneive_lcell_comb
-- Equation(s):
-- \recv|Add0~72_combout\ = (!\recv|index\(31) & (\recv|Add0~17_combout\ & \recv|LessThan0~0_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \recv|index\(31),
	datac => \recv|Add0~17_combout\,
	datad => \recv|LessThan0~0_combout\,
	combout => \recv|Add0~72_combout\);

-- Location: FF_X12_Y23_N27
\recv|index[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \sck~input_o\,
	d => \recv|Add0~72_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \recv|index\(7));

-- Location: LCCOMB_X13_Y23_N16
\recv|Add0~19\ : cycloneive_lcell_comb
-- Equation(s):
-- \recv|Add0~19_combout\ = (\recv|index\(8) & ((GND) # (!\recv|Add0~18\))) # (!\recv|index\(8) & (\recv|Add0~18\ $ (GND)))
-- \recv|Add0~20\ = CARRY((\recv|index\(8)) # (!\recv|Add0~18\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \recv|index\(8),
	datad => VCC,
	cin => \recv|Add0~18\,
	combout => \recv|Add0~19_combout\,
	cout => \recv|Add0~20\);

-- Location: LCCOMB_X12_Y23_N20
\recv|Add0~73\ : cycloneive_lcell_comb
-- Equation(s):
-- \recv|Add0~73_combout\ = (\recv|Add0~19_combout\ & (!\recv|index\(31) & \recv|LessThan0~0_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \recv|Add0~19_combout\,
	datac => \recv|index\(31),
	datad => \recv|LessThan0~0_combout\,
	combout => \recv|Add0~73_combout\);

-- Location: FF_X12_Y23_N21
\recv|index[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \sck~input_o\,
	d => \recv|Add0~73_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \recv|index\(8));

-- Location: LCCOMB_X13_Y23_N18
\recv|Add0~21\ : cycloneive_lcell_comb
-- Equation(s):
-- \recv|Add0~21_combout\ = (\recv|index\(9) & (\recv|Add0~20\ & VCC)) # (!\recv|index\(9) & (!\recv|Add0~20\))
-- \recv|Add0~22\ = CARRY((!\recv|index\(9) & !\recv|Add0~20\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \recv|index\(9),
	datad => VCC,
	cin => \recv|Add0~20\,
	combout => \recv|Add0~21_combout\,
	cout => \recv|Add0~22\);

-- Location: LCCOMB_X12_Y23_N6
\recv|Add0~74\ : cycloneive_lcell_comb
-- Equation(s):
-- \recv|Add0~74_combout\ = (\recv|Add0~21_combout\ & (!\recv|index\(31) & \recv|LessThan0~0_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \recv|Add0~21_combout\,
	datac => \recv|index\(31),
	datad => \recv|LessThan0~0_combout\,
	combout => \recv|Add0~74_combout\);

-- Location: FF_X12_Y23_N7
\recv|index[9]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \sck~input_o\,
	d => \recv|Add0~74_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \recv|index\(9));

-- Location: LCCOMB_X13_Y23_N20
\recv|Add0~23\ : cycloneive_lcell_comb
-- Equation(s):
-- \recv|Add0~23_combout\ = (\recv|index\(10) & ((GND) # (!\recv|Add0~22\))) # (!\recv|index\(10) & (\recv|Add0~22\ $ (GND)))
-- \recv|Add0~24\ = CARRY((\recv|index\(10)) # (!\recv|Add0~22\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \recv|index\(10),
	datad => VCC,
	cin => \recv|Add0~22\,
	combout => \recv|Add0~23_combout\,
	cout => \recv|Add0~24\);

-- Location: LCCOMB_X12_Y23_N28
\recv|Add0~75\ : cycloneive_lcell_comb
-- Equation(s):
-- \recv|Add0~75_combout\ = (!\recv|index\(31) & (\recv|Add0~23_combout\ & \recv|LessThan0~0_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \recv|index\(31),
	datac => \recv|Add0~23_combout\,
	datad => \recv|LessThan0~0_combout\,
	combout => \recv|Add0~75_combout\);

-- Location: FF_X12_Y23_N29
\recv|index[10]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \sck~input_o\,
	d => \recv|Add0~75_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \recv|index\(10));

-- Location: LCCOMB_X13_Y23_N22
\recv|Add0~25\ : cycloneive_lcell_comb
-- Equation(s):
-- \recv|Add0~25_combout\ = (\recv|index\(11) & (\recv|Add0~24\ & VCC)) # (!\recv|index\(11) & (!\recv|Add0~24\))
-- \recv|Add0~26\ = CARRY((!\recv|index\(11) & !\recv|Add0~24\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \recv|index\(11),
	datad => VCC,
	cin => \recv|Add0~24\,
	combout => \recv|Add0~25_combout\,
	cout => \recv|Add0~26\);

-- Location: LCCOMB_X14_Y22_N20
\recv|Add0~76\ : cycloneive_lcell_comb
-- Equation(s):
-- \recv|Add0~76_combout\ = (!\recv|index\(31) & (\recv|Add0~25_combout\ & \recv|LessThan0~0_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100000001000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \recv|index\(31),
	datab => \recv|Add0~25_combout\,
	datac => \recv|LessThan0~0_combout\,
	combout => \recv|Add0~76_combout\);

-- Location: FF_X14_Y22_N21
\recv|index[11]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \sck~input_o\,
	d => \recv|Add0~76_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \recv|index\(11));

-- Location: LCCOMB_X13_Y23_N24
\recv|Add0~27\ : cycloneive_lcell_comb
-- Equation(s):
-- \recv|Add0~27_combout\ = (\recv|index\(12) & ((GND) # (!\recv|Add0~26\))) # (!\recv|index\(12) & (\recv|Add0~26\ $ (GND)))
-- \recv|Add0~28\ = CARRY((\recv|index\(12)) # (!\recv|Add0~26\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \recv|index\(12),
	datad => VCC,
	cin => \recv|Add0~26\,
	combout => \recv|Add0~27_combout\,
	cout => \recv|Add0~28\);

-- Location: LCCOMB_X14_Y22_N22
\recv|Add0~77\ : cycloneive_lcell_comb
-- Equation(s):
-- \recv|Add0~77_combout\ = (!\recv|index\(31) & (\recv|LessThan0~0_combout\ & \recv|Add0~27_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \recv|index\(31),
	datac => \recv|LessThan0~0_combout\,
	datad => \recv|Add0~27_combout\,
	combout => \recv|Add0~77_combout\);

-- Location: FF_X14_Y22_N23
\recv|index[12]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \sck~input_o\,
	d => \recv|Add0~77_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \recv|index\(12));

-- Location: LCCOMB_X13_Y23_N26
\recv|Add0~29\ : cycloneive_lcell_comb
-- Equation(s):
-- \recv|Add0~29_combout\ = (\recv|index\(13) & (\recv|Add0~28\ & VCC)) # (!\recv|index\(13) & (!\recv|Add0~28\))
-- \recv|Add0~30\ = CARRY((!\recv|index\(13) & !\recv|Add0~28\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \recv|index\(13),
	datad => VCC,
	cin => \recv|Add0~28\,
	combout => \recv|Add0~29_combout\,
	cout => \recv|Add0~30\);

-- Location: LCCOMB_X14_Y22_N24
\recv|Add0~78\ : cycloneive_lcell_comb
-- Equation(s):
-- \recv|Add0~78_combout\ = (!\recv|index\(31) & (\recv|LessThan0~0_combout\ & \recv|Add0~29_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100000001000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \recv|index\(31),
	datab => \recv|LessThan0~0_combout\,
	datac => \recv|Add0~29_combout\,
	combout => \recv|Add0~78_combout\);

-- Location: FF_X14_Y22_N25
\recv|index[13]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \sck~input_o\,
	d => \recv|Add0~78_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \recv|index\(13));

-- Location: LCCOMB_X13_Y23_N28
\recv|Add0~31\ : cycloneive_lcell_comb
-- Equation(s):
-- \recv|Add0~31_combout\ = (\recv|index\(14) & ((GND) # (!\recv|Add0~30\))) # (!\recv|index\(14) & (\recv|Add0~30\ $ (GND)))
-- \recv|Add0~32\ = CARRY((\recv|index\(14)) # (!\recv|Add0~30\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101010101111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \recv|index\(14),
	datad => VCC,
	cin => \recv|Add0~30\,
	combout => \recv|Add0~31_combout\,
	cout => \recv|Add0~32\);

-- Location: LCCOMB_X14_Y22_N2
\recv|Add0~79\ : cycloneive_lcell_comb
-- Equation(s):
-- \recv|Add0~79_combout\ = (!\recv|index\(31) & (\recv|Add0~31_combout\ & \recv|LessThan0~0_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100000001000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \recv|index\(31),
	datab => \recv|Add0~31_combout\,
	datac => \recv|LessThan0~0_combout\,
	combout => \recv|Add0~79_combout\);

-- Location: FF_X14_Y22_N3
\recv|index[14]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \sck~input_o\,
	d => \recv|Add0~79_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \recv|index\(14));

-- Location: LCCOMB_X13_Y23_N30
\recv|Add0~33\ : cycloneive_lcell_comb
-- Equation(s):
-- \recv|Add0~33_combout\ = (\recv|index\(15) & (\recv|Add0~32\ & VCC)) # (!\recv|index\(15) & (!\recv|Add0~32\))
-- \recv|Add0~34\ = CARRY((!\recv|index\(15) & !\recv|Add0~32\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \recv|index\(15),
	datad => VCC,
	cin => \recv|Add0~32\,
	combout => \recv|Add0~33_combout\,
	cout => \recv|Add0~34\);

-- Location: LCCOMB_X12_Y23_N24
\recv|Add0~80\ : cycloneive_lcell_comb
-- Equation(s):
-- \recv|Add0~80_combout\ = (!\recv|index\(31) & (\recv|Add0~33_combout\ & \recv|LessThan0~0_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \recv|index\(31),
	datac => \recv|Add0~33_combout\,
	datad => \recv|LessThan0~0_combout\,
	combout => \recv|Add0~80_combout\);

-- Location: FF_X12_Y23_N25
\recv|index[15]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \sck~input_o\,
	d => \recv|Add0~80_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \recv|index\(15));

-- Location: LCCOMB_X13_Y22_N0
\recv|Add0~35\ : cycloneive_lcell_comb
-- Equation(s):
-- \recv|Add0~35_combout\ = (\recv|index\(16) & ((GND) # (!\recv|Add0~34\))) # (!\recv|index\(16) & (\recv|Add0~34\ $ (GND)))
-- \recv|Add0~36\ = CARRY((\recv|index\(16)) # (!\recv|Add0~34\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101010101111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \recv|index\(16),
	datad => VCC,
	cin => \recv|Add0~34\,
	combout => \recv|Add0~35_combout\,
	cout => \recv|Add0~36\);

-- Location: LCCOMB_X12_Y23_N18
\recv|Add0~81\ : cycloneive_lcell_comb
-- Equation(s):
-- \recv|Add0~81_combout\ = (!\recv|index\(31) & (\recv|Add0~35_combout\ & \recv|LessThan0~0_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \recv|index\(31),
	datac => \recv|Add0~35_combout\,
	datad => \recv|LessThan0~0_combout\,
	combout => \recv|Add0~81_combout\);

-- Location: FF_X12_Y23_N19
\recv|index[16]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \sck~input_o\,
	d => \recv|Add0~81_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \recv|index\(16));

-- Location: LCCOMB_X13_Y22_N2
\recv|Add0~37\ : cycloneive_lcell_comb
-- Equation(s):
-- \recv|Add0~37_combout\ = (\recv|index\(17) & (\recv|Add0~36\ & VCC)) # (!\recv|index\(17) & (!\recv|Add0~36\))
-- \recv|Add0~38\ = CARRY((!\recv|index\(17) & !\recv|Add0~36\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100000101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \recv|index\(17),
	datad => VCC,
	cin => \recv|Add0~36\,
	combout => \recv|Add0~37_combout\,
	cout => \recv|Add0~38\);

-- Location: LCCOMB_X12_Y23_N8
\recv|Add0~82\ : cycloneive_lcell_comb
-- Equation(s):
-- \recv|Add0~82_combout\ = (\recv|LessThan0~0_combout\ & (!\recv|index\(31) & \recv|Add0~37_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \recv|LessThan0~0_combout\,
	datac => \recv|index\(31),
	datad => \recv|Add0~37_combout\,
	combout => \recv|Add0~82_combout\);

-- Location: FF_X12_Y23_N9
\recv|index[17]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \sck~input_o\,
	d => \recv|Add0~82_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \recv|index\(17));

-- Location: LCCOMB_X13_Y22_N4
\recv|Add0~39\ : cycloneive_lcell_comb
-- Equation(s):
-- \recv|Add0~39_combout\ = (\recv|index\(18) & ((GND) # (!\recv|Add0~38\))) # (!\recv|index\(18) & (\recv|Add0~38\ $ (GND)))
-- \recv|Add0~40\ = CARRY((\recv|index\(18)) # (!\recv|Add0~38\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \recv|index\(18),
	datad => VCC,
	cin => \recv|Add0~38\,
	combout => \recv|Add0~39_combout\,
	cout => \recv|Add0~40\);

-- Location: LCCOMB_X12_Y23_N22
\recv|Add0~83\ : cycloneive_lcell_comb
-- Equation(s):
-- \recv|Add0~83_combout\ = (\recv|LessThan0~0_combout\ & (!\recv|index\(31) & \recv|Add0~39_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \recv|LessThan0~0_combout\,
	datac => \recv|index\(31),
	datad => \recv|Add0~39_combout\,
	combout => \recv|Add0~83_combout\);

-- Location: FF_X12_Y23_N23
\recv|index[18]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \sck~input_o\,
	d => \recv|Add0~83_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \recv|index\(18));

-- Location: LCCOMB_X13_Y22_N6
\recv|Add0~41\ : cycloneive_lcell_comb
-- Equation(s):
-- \recv|Add0~41_combout\ = (\recv|index\(19) & (\recv|Add0~40\ & VCC)) # (!\recv|index\(19) & (!\recv|Add0~40\))
-- \recv|Add0~42\ = CARRY((!\recv|index\(19) & !\recv|Add0~40\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \recv|index\(19),
	datad => VCC,
	cin => \recv|Add0~40\,
	combout => \recv|Add0~41_combout\,
	cout => \recv|Add0~42\);

-- Location: LCCOMB_X14_Y22_N0
\recv|Add0~84\ : cycloneive_lcell_comb
-- Equation(s):
-- \recv|Add0~84_combout\ = (!\recv|index\(31) & (\recv|Add0~41_combout\ & \recv|LessThan0~0_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100000001000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \recv|index\(31),
	datab => \recv|Add0~41_combout\,
	datac => \recv|LessThan0~0_combout\,
	combout => \recv|Add0~84_combout\);

-- Location: FF_X14_Y22_N1
\recv|index[19]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \sck~input_o\,
	d => \recv|Add0~84_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \recv|index\(19));

-- Location: LCCOMB_X13_Y22_N8
\recv|Add0~43\ : cycloneive_lcell_comb
-- Equation(s):
-- \recv|Add0~43_combout\ = (\recv|index\(20) & ((GND) # (!\recv|Add0~42\))) # (!\recv|index\(20) & (\recv|Add0~42\ $ (GND)))
-- \recv|Add0~44\ = CARRY((\recv|index\(20)) # (!\recv|Add0~42\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101010101111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \recv|index\(20),
	datad => VCC,
	cin => \recv|Add0~42\,
	combout => \recv|Add0~43_combout\,
	cout => \recv|Add0~44\);

-- Location: LCCOMB_X14_Y22_N6
\recv|Add0~85\ : cycloneive_lcell_comb
-- Equation(s):
-- \recv|Add0~85_combout\ = (!\recv|index\(31) & (\recv|LessThan0~0_combout\ & \recv|Add0~43_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \recv|index\(31),
	datac => \recv|LessThan0~0_combout\,
	datad => \recv|Add0~43_combout\,
	combout => \recv|Add0~85_combout\);

-- Location: FF_X14_Y22_N7
\recv|index[20]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \sck~input_o\,
	d => \recv|Add0~85_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \recv|index\(20));

-- Location: LCCOMB_X13_Y22_N10
\recv|Add0~45\ : cycloneive_lcell_comb
-- Equation(s):
-- \recv|Add0~45_combout\ = (\recv|index\(21) & (\recv|Add0~44\ & VCC)) # (!\recv|index\(21) & (!\recv|Add0~44\))
-- \recv|Add0~46\ = CARRY((!\recv|index\(21) & !\recv|Add0~44\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100000101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \recv|index\(21),
	datad => VCC,
	cin => \recv|Add0~44\,
	combout => \recv|Add0~45_combout\,
	cout => \recv|Add0~46\);

-- Location: LCCOMB_X14_Y22_N4
\recv|Add0~86\ : cycloneive_lcell_comb
-- Equation(s):
-- \recv|Add0~86_combout\ = (!\recv|index\(31) & (\recv|LessThan0~0_combout\ & \recv|Add0~45_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \recv|index\(31),
	datac => \recv|LessThan0~0_combout\,
	datad => \recv|Add0~45_combout\,
	combout => \recv|Add0~86_combout\);

-- Location: FF_X14_Y22_N5
\recv|index[21]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \sck~input_o\,
	d => \recv|Add0~86_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \recv|index\(21));

-- Location: LCCOMB_X13_Y22_N12
\recv|Add0~47\ : cycloneive_lcell_comb
-- Equation(s):
-- \recv|Add0~47_combout\ = (\recv|index\(22) & ((GND) # (!\recv|Add0~46\))) # (!\recv|index\(22) & (\recv|Add0~46\ $ (GND)))
-- \recv|Add0~48\ = CARRY((\recv|index\(22)) # (!\recv|Add0~46\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \recv|index\(22),
	datad => VCC,
	cin => \recv|Add0~46\,
	combout => \recv|Add0~47_combout\,
	cout => \recv|Add0~48\);

-- Location: LCCOMB_X14_Y22_N26
\recv|Add0~87\ : cycloneive_lcell_comb
-- Equation(s):
-- \recv|Add0~87_combout\ = (!\recv|index\(31) & (\recv|LessThan0~0_combout\ & \recv|Add0~47_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \recv|index\(31),
	datac => \recv|LessThan0~0_combout\,
	datad => \recv|Add0~47_combout\,
	combout => \recv|Add0~87_combout\);

-- Location: FF_X14_Y22_N27
\recv|index[22]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \sck~input_o\,
	d => \recv|Add0~87_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \recv|index\(22));

-- Location: LCCOMB_X14_Y22_N8
\busy~7\ : cycloneive_lcell_comb
-- Equation(s):
-- \busy~7_combout\ = (!\recv|index\(22) & (!\recv|index\(19) & (!\recv|index\(21) & !\recv|index\(20))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \recv|index\(22),
	datab => \recv|index\(19),
	datac => \recv|index\(21),
	datad => \recv|index\(20),
	combout => \busy~7_combout\);

-- Location: LCCOMB_X13_Y22_N14
\recv|Add0~49\ : cycloneive_lcell_comb
-- Equation(s):
-- \recv|Add0~49_combout\ = (\recv|index\(23) & (\recv|Add0~48\ & VCC)) # (!\recv|index\(23) & (!\recv|Add0~48\))
-- \recv|Add0~50\ = CARRY((!\recv|index\(23) & !\recv|Add0~48\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100000101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \recv|index\(23),
	datad => VCC,
	cin => \recv|Add0~48\,
	combout => \recv|Add0~49_combout\,
	cout => \recv|Add0~50\);

-- Location: LCCOMB_X12_Y22_N16
\recv|Add0~88\ : cycloneive_lcell_comb
-- Equation(s):
-- \recv|Add0~88_combout\ = (!\recv|index\(31) & (\recv|Add0~49_combout\ & \recv|LessThan0~0_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \recv|index\(31),
	datac => \recv|Add0~49_combout\,
	datad => \recv|LessThan0~0_combout\,
	combout => \recv|Add0~88_combout\);

-- Location: FF_X12_Y22_N17
\recv|index[23]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \sck~input_o\,
	d => \recv|Add0~88_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \recv|index\(23));

-- Location: LCCOMB_X13_Y22_N16
\recv|Add0~51\ : cycloneive_lcell_comb
-- Equation(s):
-- \recv|Add0~51_combout\ = (\recv|index\(24) & ((GND) # (!\recv|Add0~50\))) # (!\recv|index\(24) & (\recv|Add0~50\ $ (GND)))
-- \recv|Add0~52\ = CARRY((\recv|index\(24)) # (!\recv|Add0~50\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \recv|index\(24),
	datad => VCC,
	cin => \recv|Add0~50\,
	combout => \recv|Add0~51_combout\,
	cout => \recv|Add0~52\);

-- Location: LCCOMB_X12_Y22_N6
\recv|Add0~89\ : cycloneive_lcell_comb
-- Equation(s):
-- \recv|Add0~89_combout\ = (\recv|LessThan0~0_combout\ & (!\recv|index\(31) & \recv|Add0~51_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \recv|LessThan0~0_combout\,
	datac => \recv|index\(31),
	datad => \recv|Add0~51_combout\,
	combout => \recv|Add0~89_combout\);

-- Location: FF_X12_Y22_N7
\recv|index[24]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \sck~input_o\,
	d => \recv|Add0~89_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \recv|index\(24));

-- Location: LCCOMB_X13_Y22_N18
\recv|Add0~53\ : cycloneive_lcell_comb
-- Equation(s):
-- \recv|Add0~53_combout\ = (\recv|index\(25) & (\recv|Add0~52\ & VCC)) # (!\recv|index\(25) & (!\recv|Add0~52\))
-- \recv|Add0~54\ = CARRY((!\recv|index\(25) & !\recv|Add0~52\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \recv|index\(25),
	datad => VCC,
	cin => \recv|Add0~52\,
	combout => \recv|Add0~53_combout\,
	cout => \recv|Add0~54\);

-- Location: LCCOMB_X12_Y22_N20
\recv|Add0~90\ : cycloneive_lcell_comb
-- Equation(s):
-- \recv|Add0~90_combout\ = (\recv|LessThan0~0_combout\ & (!\recv|index\(31) & \recv|Add0~53_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \recv|LessThan0~0_combout\,
	datac => \recv|index\(31),
	datad => \recv|Add0~53_combout\,
	combout => \recv|Add0~90_combout\);

-- Location: FF_X12_Y22_N21
\recv|index[25]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \sck~input_o\,
	d => \recv|Add0~90_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \recv|index\(25));

-- Location: LCCOMB_X13_Y22_N20
\recv|Add0~55\ : cycloneive_lcell_comb
-- Equation(s):
-- \recv|Add0~55_combout\ = (\recv|index\(26) & ((GND) # (!\recv|Add0~54\))) # (!\recv|index\(26) & (\recv|Add0~54\ $ (GND)))
-- \recv|Add0~56\ = CARRY((\recv|index\(26)) # (!\recv|Add0~54\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \recv|index\(26),
	datad => VCC,
	cin => \recv|Add0~54\,
	combout => \recv|Add0~55_combout\,
	cout => \recv|Add0~56\);

-- Location: LCCOMB_X12_Y22_N18
\recv|Add0~91\ : cycloneive_lcell_comb
-- Equation(s):
-- \recv|Add0~91_combout\ = (!\recv|index\(31) & (\recv|Add0~55_combout\ & \recv|LessThan0~0_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \recv|index\(31),
	datac => \recv|Add0~55_combout\,
	datad => \recv|LessThan0~0_combout\,
	combout => \recv|Add0~91_combout\);

-- Location: FF_X12_Y22_N19
\recv|index[26]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \sck~input_o\,
	d => \recv|Add0~91_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \recv|index\(26));

-- Location: LCCOMB_X12_Y22_N28
\busy~8\ : cycloneive_lcell_comb
-- Equation(s):
-- \busy~8_combout\ = (!\recv|index\(24) & (!\recv|index\(26) & (!\recv|index\(25) & !\recv|index\(23))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \recv|index\(24),
	datab => \recv|index\(26),
	datac => \recv|index\(25),
	datad => \recv|index\(23),
	combout => \busy~8_combout\);

-- Location: LCCOMB_X13_Y22_N22
\recv|Add0~57\ : cycloneive_lcell_comb
-- Equation(s):
-- \recv|Add0~57_combout\ = (\recv|index\(27) & (\recv|Add0~56\ & VCC)) # (!\recv|index\(27) & (!\recv|Add0~56\))
-- \recv|Add0~58\ = CARRY((!\recv|index\(27) & !\recv|Add0~56\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100000101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \recv|index\(27),
	datad => VCC,
	cin => \recv|Add0~56\,
	combout => \recv|Add0~57_combout\,
	cout => \recv|Add0~58\);

-- Location: LCCOMB_X12_Y22_N26
\recv|Add0~92\ : cycloneive_lcell_comb
-- Equation(s):
-- \recv|Add0~92_combout\ = (\recv|LessThan0~0_combout\ & (!\recv|index\(31) & \recv|Add0~57_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \recv|LessThan0~0_combout\,
	datac => \recv|index\(31),
	datad => \recv|Add0~57_combout\,
	combout => \recv|Add0~92_combout\);

-- Location: FF_X12_Y22_N27
\recv|index[27]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \sck~input_o\,
	d => \recv|Add0~92_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \recv|index\(27));

-- Location: LCCOMB_X13_Y22_N24
\recv|Add0~59\ : cycloneive_lcell_comb
-- Equation(s):
-- \recv|Add0~59_combout\ = (\recv|index\(28) & ((GND) # (!\recv|Add0~58\))) # (!\recv|index\(28) & (\recv|Add0~58\ $ (GND)))
-- \recv|Add0~60\ = CARRY((\recv|index\(28)) # (!\recv|Add0~58\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \recv|index\(28),
	datad => VCC,
	cin => \recv|Add0~58\,
	combout => \recv|Add0~59_combout\,
	cout => \recv|Add0~60\);

-- Location: LCCOMB_X12_Y22_N24
\recv|Add0~93\ : cycloneive_lcell_comb
-- Equation(s):
-- \recv|Add0~93_combout\ = (\recv|LessThan0~0_combout\ & (!\recv|index\(31) & \recv|Add0~59_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \recv|LessThan0~0_combout\,
	datac => \recv|index\(31),
	datad => \recv|Add0~59_combout\,
	combout => \recv|Add0~93_combout\);

-- Location: FF_X12_Y22_N25
\recv|index[28]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \sck~input_o\,
	d => \recv|Add0~93_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \recv|index\(28));

-- Location: LCCOMB_X13_Y22_N26
\recv|Add0~61\ : cycloneive_lcell_comb
-- Equation(s):
-- \recv|Add0~61_combout\ = (\recv|index\(29) & (\recv|Add0~60\ & VCC)) # (!\recv|index\(29) & (!\recv|Add0~60\))
-- \recv|Add0~62\ = CARRY((!\recv|index\(29) & !\recv|Add0~60\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100000101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \recv|index\(29),
	datad => VCC,
	cin => \recv|Add0~60\,
	combout => \recv|Add0~61_combout\,
	cout => \recv|Add0~62\);

-- Location: LCCOMB_X12_Y22_N10
\recv|Add0~94\ : cycloneive_lcell_comb
-- Equation(s):
-- \recv|Add0~94_combout\ = (\recv|LessThan0~0_combout\ & (!\recv|index\(31) & \recv|Add0~61_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \recv|LessThan0~0_combout\,
	datac => \recv|index\(31),
	datad => \recv|Add0~61_combout\,
	combout => \recv|Add0~94_combout\);

-- Location: FF_X12_Y22_N11
\recv|index[29]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \sck~input_o\,
	d => \recv|Add0~94_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \recv|index\(29));

-- Location: LCCOMB_X13_Y22_N28
\recv|Add0~63\ : cycloneive_lcell_comb
-- Equation(s):
-- \recv|Add0~63_combout\ = (\recv|index\(30) & ((GND) # (!\recv|Add0~62\))) # (!\recv|index\(30) & (\recv|Add0~62\ $ (GND)))
-- \recv|Add0~64\ = CARRY((\recv|index\(30)) # (!\recv|Add0~62\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \recv|index\(30),
	datad => VCC,
	cin => \recv|Add0~62\,
	combout => \recv|Add0~63_combout\,
	cout => \recv|Add0~64\);

-- Location: LCCOMB_X12_Y22_N4
\recv|Add0~95\ : cycloneive_lcell_comb
-- Equation(s):
-- \recv|Add0~95_combout\ = (\recv|LessThan0~0_combout\ & (!\recv|index\(31) & \recv|Add0~63_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \recv|LessThan0~0_combout\,
	datac => \recv|index\(31),
	datad => \recv|Add0~63_combout\,
	combout => \recv|Add0~95_combout\);

-- Location: FF_X12_Y22_N5
\recv|index[30]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \sck~input_o\,
	d => \recv|Add0~95_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \recv|index\(30));

-- Location: LCCOMB_X12_Y22_N14
\busy~9\ : cycloneive_lcell_comb
-- Equation(s):
-- \busy~9_combout\ = (!\recv|index\(27) & (!\recv|index\(28) & (!\recv|index\(30) & !\recv|index\(29))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \recv|index\(27),
	datab => \recv|index\(28),
	datac => \recv|index\(30),
	datad => \recv|index\(29),
	combout => \busy~9_combout\);

-- Location: LCCOMB_X12_Y23_N30
\busy~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \busy~3_combout\ = (!\recv|index\(9) & (!\recv|index\(8) & (!\recv|index\(7) & !\recv|index\(10))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \recv|index\(9),
	datab => \recv|index\(8),
	datac => \recv|index\(7),
	datad => \recv|index\(10),
	combout => \busy~3_combout\);

-- Location: LCCOMB_X14_Y22_N28
\busy~4\ : cycloneive_lcell_comb
-- Equation(s):
-- \busy~4_combout\ = (!\recv|index\(12) & (!\recv|index\(14) & (!\recv|index\(11) & !\recv|index\(13))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \recv|index\(12),
	datab => \recv|index\(14),
	datac => \recv|index\(11),
	datad => \recv|index\(13),
	combout => \busy~4_combout\);

-- Location: LCCOMB_X12_Y23_N4
\busy~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \busy~2_combout\ = (!\recv|index\(6) & (!\recv|index\(5) & (!\recv|index\(4) & !\recv|index\(3))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \recv|index\(6),
	datab => \recv|index\(5),
	datac => \recv|index\(4),
	datad => \recv|index\(3),
	combout => \busy~2_combout\);

-- Location: LCCOMB_X12_Y23_N12
\busy~5\ : cycloneive_lcell_comb
-- Equation(s):
-- \busy~5_combout\ = (!\recv|index\(18) & (!\recv|index\(15) & (!\recv|index\(17) & !\recv|index\(16))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \recv|index\(18),
	datab => \recv|index\(15),
	datac => \recv|index\(17),
	datad => \recv|index\(16),
	combout => \busy~5_combout\);

-- Location: LCCOMB_X14_Y22_N14
\busy~6\ : cycloneive_lcell_comb
-- Equation(s):
-- \busy~6_combout\ = (\busy~3_combout\ & (\busy~4_combout\ & (\busy~2_combout\ & \busy~5_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \busy~3_combout\,
	datab => \busy~4_combout\,
	datac => \busy~2_combout\,
	datad => \busy~5_combout\,
	combout => \busy~6_combout\);

-- Location: LCCOMB_X12_Y22_N12
\busy~10\ : cycloneive_lcell_comb
-- Equation(s):
-- \busy~10_combout\ = (\busy~7_combout\ & (\busy~8_combout\ & (\busy~9_combout\ & \busy~6_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \busy~7_combout\,
	datab => \busy~8_combout\,
	datac => \busy~9_combout\,
	datad => \busy~6_combout\,
	combout => \busy~10_combout\);

-- Location: LCCOMB_X12_Y22_N0
\recv|LessThan0~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \recv|LessThan0~0_combout\ = (((!\busy~10_combout\) # (!\recv|index\(2))) # (!\recv|index\(1))) # (!\recv|index\(0))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111111111111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \recv|index\(0),
	datab => \recv|index\(1),
	datac => \recv|index\(2),
	datad => \busy~10_combout\,
	combout => \recv|LessThan0~0_combout\);

-- Location: LCCOMB_X13_Y22_N30
\recv|Add0~65\ : cycloneive_lcell_comb
-- Equation(s):
-- \recv|Add0~65_combout\ = \recv|Add0~64\ $ (!\recv|index\(31))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datad => \recv|index\(31),
	cin => \recv|Add0~64\,
	combout => \recv|Add0~65_combout\);

-- Location: LCCOMB_X12_Y22_N8
\recv|Add0~67\ : cycloneive_lcell_comb
-- Equation(s):
-- \recv|Add0~67_combout\ = (\recv|LessThan0~0_combout\ & (!\recv|index\(31) & \recv|Add0~65_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \recv|LessThan0~0_combout\,
	datac => \recv|index\(31),
	datad => \recv|Add0~65_combout\,
	combout => \recv|Add0~67_combout\);

-- Location: FF_X12_Y22_N9
\recv|index[31]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \sck~input_o\,
	d => \recv|Add0~67_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \recv|index\(31));

-- Location: LCCOMB_X13_Y23_N2
\recv|Add0~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \recv|Add0~3_combout\ = (\recv|index\(1) & (!\recv|Add0~1\)) # (!\recv|index\(1) & (\recv|Add0~1\ & VCC))
-- \recv|Add0~4\ = CARRY((\recv|index\(1) & !\recv|Add0~1\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101000001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \recv|index\(1),
	datad => VCC,
	cin => \recv|Add0~1\,
	combout => \recv|Add0~3_combout\,
	cout => \recv|Add0~4\);

-- Location: LCCOMB_X14_Y23_N20
\recv|Add0~5\ : cycloneive_lcell_comb
-- Equation(s):
-- \recv|Add0~5_combout\ = (!\recv|index\(31) & (\recv|LessThan0~0_combout\ & !\recv|Add0~3_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \recv|index\(31),
	datac => \recv|LessThan0~0_combout\,
	datad => \recv|Add0~3_combout\,
	combout => \recv|Add0~5_combout\);

-- Location: FF_X14_Y23_N21
\recv|index[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \sck~input_o\,
	d => \recv|Add0~5_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \recv|index\(1));

-- Location: LCCOMB_X14_Y23_N24
\recv|Add0~8\ : cycloneive_lcell_comb
-- Equation(s):
-- \recv|Add0~8_combout\ = (!\recv|Add0~6_combout\ & (\recv|LessThan0~0_combout\ & !\recv|index\(31)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \recv|Add0~6_combout\,
	datac => \recv|LessThan0~0_combout\,
	datad => \recv|index\(31),
	combout => \recv|Add0~8_combout\);

-- Location: FF_X14_Y23_N25
\recv|index[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \sck~input_o\,
	d => \recv|Add0~8_combout\,
	ena => \ALT_INV_ss~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \recv|index\(2));

-- Location: LCCOMB_X14_Y23_N12
\recv|Decoder0~7\ : cycloneive_lcell_comb
-- Equation(s):
-- \recv|Decoder0~7_combout\ = (!\ss~input_o\ & (!\recv|index\(2) & (!\recv|index\(0) & !\recv|index\(1))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \ss~input_o\,
	datab => \recv|index\(2),
	datac => \recv|index\(0),
	datad => \recv|index\(1),
	combout => \recv|Decoder0~7_combout\);

-- Location: LCCOMB_X14_Y23_N14
\recv|data_temp[7]\ : cycloneive_lcell_comb
-- Equation(s):
-- \recv|data_temp\(7) = (\recv|Decoder0~7_combout\ & (\mosi~input_o\)) # (!\recv|Decoder0~7_combout\ & ((\recv|data_temp\(7))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \mosi~input_o\,
	datac => \recv|data_temp\(7),
	datad => \recv|Decoder0~7_combout\,
	combout => \recv|data_temp\(7));

-- Location: LCCOMB_X14_Y23_N10
\recv|Decoder0~4\ : cycloneive_lcell_comb
-- Equation(s):
-- \recv|Decoder0~4_combout\ = (!\ss~input_o\ & (!\recv|index\(2) & (!\recv|index\(0) & \recv|index\(1))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \ss~input_o\,
	datab => \recv|index\(2),
	datac => \recv|index\(0),
	datad => \recv|index\(1),
	combout => \recv|Decoder0~4_combout\);

-- Location: LCCOMB_X14_Y23_N22
\recv|data_temp[5]\ : cycloneive_lcell_comb
-- Equation(s):
-- \recv|data_temp\(5) = (\recv|Decoder0~4_combout\ & (\mosi~input_o\)) # (!\recv|Decoder0~4_combout\ & ((\recv|data_temp\(5))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \mosi~input_o\,
	datac => \recv|data_temp\(5),
	datad => \recv|Decoder0~4_combout\,
	combout => \recv|data_temp\(5));

-- Location: LCCOMB_X14_Y23_N28
\recv|Decoder0~6\ : cycloneive_lcell_comb
-- Equation(s):
-- \recv|Decoder0~6_combout\ = (!\ss~input_o\ & (!\recv|index\(2) & (\recv|index\(0) & \recv|index\(1))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \ss~input_o\,
	datab => \recv|index\(2),
	datac => \recv|index\(0),
	datad => \recv|index\(1),
	combout => \recv|Decoder0~6_combout\);

-- Location: LCCOMB_X14_Y23_N2
\recv|data_temp[4]\ : cycloneive_lcell_comb
-- Equation(s):
-- \recv|data_temp\(4) = (\recv|Decoder0~6_combout\ & (\mosi~input_o\)) # (!\recv|Decoder0~6_combout\ & ((\recv|data_temp\(4))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \mosi~input_o\,
	datab => \recv|data_temp\(4),
	datad => \recv|Decoder0~6_combout\,
	combout => \recv|data_temp\(4));

-- Location: LCCOMB_X14_Y23_N6
\recv|Decoder0~5\ : cycloneive_lcell_comb
-- Equation(s):
-- \recv|Decoder0~5_combout\ = (!\ss~input_o\ & (!\recv|index\(2) & (\recv|index\(0) & !\recv|index\(1))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \ss~input_o\,
	datab => \recv|index\(2),
	datac => \recv|index\(0),
	datad => \recv|index\(1),
	combout => \recv|Decoder0~5_combout\);

-- Location: LCCOMB_X16_Y23_N16
\recv|data_temp[6]\ : cycloneive_lcell_comb
-- Equation(s):
-- \recv|data_temp\(6) = (\recv|Decoder0~5_combout\ & (\mosi~input_o\)) # (!\recv|Decoder0~5_combout\ & ((\recv|data_temp\(6))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010111110100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \mosi~input_o\,
	datac => \recv|Decoder0~5_combout\,
	datad => \recv|data_temp\(6),
	combout => \recv|data_temp\(6));

-- Location: LCCOMB_X16_Y23_N8
\trns|Mux0~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \trns|Mux0~2_combout\ = (\trns|index\(1) & (\trns|index\(0) & (\recv|data_temp\(4)))) # (!\trns|index\(1) & (((\recv|data_temp\(6))) # (!\trns|index\(0))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101010110010001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \trns|index\(1),
	datab => \trns|index\(0),
	datac => \recv|data_temp\(4),
	datad => \recv|data_temp\(6),
	combout => \trns|Mux0~2_combout\);

-- Location: LCCOMB_X16_Y23_N22
\trns|Mux0~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \trns|Mux0~3_combout\ = (\trns|Mux0~2_combout\ & ((\recv|data_temp\(7)) # ((\trns|index\(0))))) # (!\trns|Mux0~2_combout\ & (((\recv|data_temp\(5) & !\trns|index\(0)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000010101100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \recv|data_temp\(7),
	datab => \recv|data_temp\(5),
	datac => \trns|Mux0~2_combout\,
	datad => \trns|index\(0),
	combout => \trns|Mux0~3_combout\);

-- Location: LCCOMB_X14_Y23_N0
\recv|Decoder0~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \recv|Decoder0~0_combout\ = (!\ss~input_o\ & (!\recv|index\(1) & (\recv|index\(0) & \recv|index\(2))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \ss~input_o\,
	datab => \recv|index\(1),
	datac => \recv|index\(0),
	datad => \recv|index\(2),
	combout => \recv|Decoder0~0_combout\);

-- Location: LCCOMB_X14_Y23_N4
\recv|data_temp[2]\ : cycloneive_lcell_comb
-- Equation(s):
-- \recv|data_temp\(2) = (\recv|Decoder0~0_combout\ & (\mosi~input_o\)) # (!\recv|Decoder0~0_combout\ & ((\recv|data_temp\(2))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \mosi~input_o\,
	datac => \recv|data_temp\(2),
	datad => \recv|Decoder0~0_combout\,
	combout => \recv|data_temp\(2));

-- Location: LCCOMB_X14_Y23_N26
\recv|Decoder0~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \recv|Decoder0~3_combout\ = (!\ss~input_o\ & (\recv|index\(2) & (!\recv|index\(0) & !\recv|index\(1))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \ss~input_o\,
	datab => \recv|index\(2),
	datac => \recv|index\(0),
	datad => \recv|index\(1),
	combout => \recv|Decoder0~3_combout\);

-- Location: LCCOMB_X16_Y23_N18
\recv|data_temp[3]\ : cycloneive_lcell_comb
-- Equation(s):
-- \recv|data_temp\(3) = (\recv|Decoder0~3_combout\ & (\mosi~input_o\)) # (!\recv|Decoder0~3_combout\ & ((\recv|data_temp\(3))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010111110100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \mosi~input_o\,
	datac => \recv|Decoder0~3_combout\,
	datad => \recv|data_temp\(3),
	combout => \recv|data_temp\(3));

-- Location: LCCOMB_X14_Y23_N18
\recv|Decoder0~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \recv|Decoder0~1_combout\ = (!\ss~input_o\ & (\recv|index\(1) & (!\recv|index\(0) & \recv|index\(2))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000010000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \ss~input_o\,
	datab => \recv|index\(1),
	datac => \recv|index\(0),
	datad => \recv|index\(2),
	combout => \recv|Decoder0~1_combout\);

-- Location: LCCOMB_X16_Y23_N12
\recv|data_temp[1]\ : cycloneive_lcell_comb
-- Equation(s):
-- \recv|data_temp\(1) = (\recv|Decoder0~1_combout\ & ((\mosi~input_o\))) # (!\recv|Decoder0~1_combout\ & (\recv|data_temp\(1)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \recv|data_temp\(1),
	datac => \mosi~input_o\,
	datad => \recv|Decoder0~1_combout\,
	combout => \recv|data_temp\(1));

-- Location: LCCOMB_X14_Y23_N16
\recv|Decoder0~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \recv|Decoder0~2_combout\ = (!\ss~input_o\ & (\recv|index\(2) & (\recv|index\(0) & \recv|index\(1))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \ss~input_o\,
	datab => \recv|index\(2),
	datac => \recv|index\(0),
	datad => \recv|index\(1),
	combout => \recv|Decoder0~2_combout\);

-- Location: LCCOMB_X14_Y23_N8
\recv|data_temp[0]\ : cycloneive_lcell_comb
-- Equation(s):
-- \recv|data_temp\(0) = (\recv|Decoder0~2_combout\ & (\mosi~input_o\)) # (!\recv|Decoder0~2_combout\ & ((\recv|data_temp\(0))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \mosi~input_o\,
	datac => \recv|data_temp\(0),
	datad => \recv|Decoder0~2_combout\,
	combout => \recv|data_temp\(0));

-- Location: LCCOMB_X16_Y23_N0
\trns|Mux0~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \trns|Mux0~0_combout\ = (\trns|index\(0) & (((\recv|data_temp\(0) & \trns|index\(1))))) # (!\trns|index\(0) & ((\recv|data_temp\(1)) # ((!\trns|index\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110001000110011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \recv|data_temp\(1),
	datab => \trns|index\(0),
	datac => \recv|data_temp\(0),
	datad => \trns|index\(1),
	combout => \trns|Mux0~0_combout\);

-- Location: LCCOMB_X16_Y23_N2
\trns|Mux0~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \trns|Mux0~1_combout\ = (\trns|index\(1) & (((\trns|Mux0~0_combout\)))) # (!\trns|index\(1) & ((\trns|Mux0~0_combout\ & ((\recv|data_temp\(3)))) # (!\trns|Mux0~0_combout\ & (\recv|data_temp\(2)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \recv|data_temp\(2),
	datab => \recv|data_temp\(3),
	datac => \trns|index\(1),
	datad => \trns|Mux0~0_combout\,
	combout => \trns|Mux0~1_combout\);

-- Location: LCCOMB_X13_Y21_N28
\trns|Mux0~4\ : cycloneive_lcell_comb
-- Equation(s):
-- \trns|Mux0~4_combout\ = (\trns|index\(2) & ((\trns|Mux0~1_combout\))) # (!\trns|index\(2) & (\trns|Mux0~3_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \trns|index\(2),
	datac => \trns|Mux0~3_combout\,
	datad => \trns|Mux0~1_combout\,
	combout => \trns|Mux0~4_combout\);

-- Location: LCCOMB_X12_Y22_N22
\busy~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \busy~0_combout\ = (\recv|index\(2)) # ((\recv|index\(31)) # ((\recv|index\(0)) # (\recv|index\(1))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111111110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \recv|index\(2),
	datab => \recv|index\(31),
	datac => \recv|index\(0),
	datad => \recv|index\(1),
	combout => \busy~0_combout\);

-- Location: LCCOMB_X13_Y21_N18
\busy~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \busy~1_combout\ = (\trns|index\(2)) # ((\trns|index\(0)) # ((\trns|index\(31)) # (\trns|index\(1))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111111110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \trns|index\(2),
	datab => \trns|index\(0),
	datac => \trns|index\(31),
	datad => \trns|index\(1),
	combout => \busy~1_combout\);

-- Location: LCCOMB_X12_Y22_N2
\busy~20\ : cycloneive_lcell_comb
-- Equation(s):
-- \busy~20_combout\ = (((\busy~0_combout\) # (\busy~1_combout\)) # (!\busy~19_combout\)) # (!\busy~10_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111110111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \busy~10_combout\,
	datab => \busy~19_combout\,
	datac => \busy~0_combout\,
	datad => \busy~1_combout\,
	combout => \busy~20_combout\);

ww_miso <= \miso~output_o\;

ww_busy <= \busy~output_o\;
END structure;


