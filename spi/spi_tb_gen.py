# 2 bytes in hex value

def hex_to_bin(hex_val):
  return bin(int(hex_val, 16))[2:].zfill(8)

def gen_serial_tb(bin_val):
  print('-- bit 0')
  print('mosi <= \''+bin_val[0]+'\';')
  print('wait for 2 ns;')
  bin_val = bin_val[1:]
  for i in range(7):
    # rising edge
    print('-- bit %d'%(i+1))
    print('sck <= not sck;')
    print('wait for 1 ns;')
    # falling edge
    print('sck <= not sck;')
    print('mosi <= \''+bin_val[i]+'\';')
    print('wait for 1 ns;')
  # rising edge
  print('sck <= not sck;')
  print('wait for 1 ns;')
  # falling edge
  print('sck <= not sck;')
  
# accepts byte values in hex only
def gen_ss_wrapper(addr_byte, data_byte):
  print('ss <= \'1\';')
  print('wait for 5 ns;')
  print('ss <= \'0\';')
  print('wait for 16 ns;')
  gen_serial_tb(hex_to_bin(addr_byte))
  print('wait for 16 ns;')
  gen_serial_tb(hex_to_bin(data_byte))
  print('wait for 16 ns;')
  print('ss <= \'1\';')

gen_ss_wrapper('80', 'aa')
