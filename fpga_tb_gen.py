class fpga_tb_gen:

    # addrs
    cmd_addr = 0x0
    inst_addrs = [0x1, 0x2, 0x3, 0x4, 0x5]
    lea_addrs = [0x6, 0x7, 0x8, 0x9]
    status_addr = 0xa

    # db cmds
    db_idle_cmd = 0x0
    db_lea_exec_cmd = 0x1
    db_write_lea_mem_cmd = 0x2

    def __init__(self):
        self.gen_reset_tb()
        
    def gen_reset_tb(self):
        print('-- reset')
        print('rst <= \'1\';')
        print('wait for 6 ns;')
        print('rst <= \'0\';')
        print('wait for 6 ns;')
        
    def hex_to_bin(self, hex_val):
        return bin(int(hex_val, 16))[2:].zfill(8)
        
    def gen_serial_tb(self, bin_val):
        print('-- bit 0')
        print('mosi <= \''+bin_val[0]+'\';')
        print('wait for 2 ns;')
        bin_val = bin_val[1:]
        for i in range(7):
            # rising edge
            print('-- bit %d'%(i+1))
            print('sck <= not sck;')
            print('wait for 1 ns;')
            # falling edge
            print('sck <= not sck;')
            print('mosi <= \''+bin_val[i]+'\';')
            print('wait for 1 ns;')
        # rising edge
        print('sck <= not sck;')
        print('wait for 1 ns;')
        # falling edge
        print('sck <= not sck;')
                
    # accepts byte values in hex string only
    # this is the equivalent of two transactions
    def gen_ss_wrapper(self, addr_byte, data_byte):
        print('--addr: %s' % (addr_byte))
        print('ss <= \'1\';')
        print('wait for 5 ns;')
        print('ss <= \'0\';')
        print('wait for 16 ns;')
        self.gen_serial_tb(self.hex_to_bin(addr_byte))
        print('wait for 16 ns;')
        self.gen_serial_tb(self.hex_to_bin(data_byte))
        print('wait for 16 ns;')
        print('ss <= \'1\';')

    def format_addr_byte(self, addr, rw):
        return (rw << 7) | (addr << 3)

    # accepts two ints
    def gen_write_reg_tb(self, addr, val):
        formatted_addr = self.format_addr_byte(addr, 1)
        self.gen_ss_wrapper(hex(formatted_addr)[2:].zfill(2), hex(val)[2:].zfill(2))

    # accepts one int
    def gen_read_reg_tb(self, addr):
        formatted_addr = self.format_addr_byte(addr, 0)
        self.gen_ss_wrapper(hex(formatted_addr)[2:].zfill(2), hex(0)[2:].zfill(2))

    # accepts a 40 bit hex string
    def write_inst_regs(self, lea_inst):
        print('-- writing instruction %s' % (lea_inst))
        for i in range(5):
            self.gen_write_reg_tb(self.inst_addrs[i], int(lea_inst[2*i:(2*i)+2], 16))
