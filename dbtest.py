import lea_spi_wrapper as lsw
import time

wrapper = lsw.lea_spi_wrapper()

while(True):
    wrapper.write_reg(wrapper.cmd_addr, 0x00)
    readval = wrapper.read_reg(wrapper.cmd_addr)
    print(readval)
    wrapper.write_reg(wrapper.cmd_addr, 0x01)
    readval = wrapper.read_reg(wrapper.cmd_addr)
    print(readval)
