-- reset
rst <= '1';
wait for 6 ns;
rst <= '0';
wait for 6 ns;
-- reset
rst <= '1';
wait for 6 ns;
rst <= '0';
wait for 6 ns;
-- writing instruction 1800000000
--addr: 88
ss <= '1';
wait for 5 ns;
ss <= '0';
wait for 16 ns;
-- bit 0
mosi <= '1';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
-- bit 0
mosi <= '0';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
ss <= '1';
--addr: 90
ss <= '1';
wait for 5 ns;
ss <= '0';
wait for 16 ns;
-- bit 0
mosi <= '1';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
-- bit 0
mosi <= '0';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
ss <= '1';
--addr: 98
ss <= '1';
wait for 5 ns;
ss <= '0';
wait for 16 ns;
-- bit 0
mosi <= '1';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
-- bit 0
mosi <= '0';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
ss <= '1';
--addr: a0
ss <= '1';
wait for 5 ns;
ss <= '0';
wait for 16 ns;
-- bit 0
mosi <= '1';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
-- bit 0
mosi <= '0';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
ss <= '1';
--addr: a8
ss <= '1';
wait for 5 ns;
ss <= '0';
wait for 16 ns;
-- bit 0
mosi <= '1';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
-- bit 0
mosi <= '0';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
ss <= '1';
--addr: 80
ss <= '1';
wait for 5 ns;
ss <= '0';
wait for 16 ns;
-- bit 0
mosi <= '1';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
-- bit 0
mosi <= '0';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
ss <= '1';
--addr: 80
ss <= '1';
wait for 5 ns;
ss <= '0';
wait for 16 ns;
-- bit 0
mosi <= '1';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
-- bit 0
mosi <= '0';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
ss <= '1';
--addr: 80
ss <= '1';
wait for 5 ns;
ss <= '0';
wait for 16 ns;
-- bit 0
mosi <= '1';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
-- bit 0
mosi <= '0';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
ss <= '1';
