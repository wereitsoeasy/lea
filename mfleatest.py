import lea_mfrc522_wrapper as lmw

wrapper = lmw.lea_mfrc522_wrapper()

p = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
k = wrapper.spi_wrapper.format_bytes(bytearray.fromhex('9C9C82DA4441ADF032A412F10629C92E'))
iv = wrapper.spi_wrapper.format_bytes(bytearray.fromhex('28B9629CD9D92C2C8BF6A1F01F7DCCE5')) 
wrapper.write_tag(p, k, iv)
print(wrapper.read_tag(k,iv))

