import fpga_tb_gen as ftb

def gen_inst_vect(opc, rw, addr, data_word):
    inst_vect = '00'
    inst_vect += opc
    inst_vect += rw
    inst_vect += addr
    inst_vect = hex(int(inst_vect, 2))[2:].zfill(2)
    inst_vect += data_word
    return inst_vect

def split_word(w):
    ret_arr = []
    for i in range(4):
        ret_arr.append(w[(i*8):(i*8)+8])
    return ret_arr

def exec_lea_write(opc_string, w_list):
    for i in range(4):
        # write the k block
        tb_gen.write_inst_regs(gen_inst_vect(opc_string, '1', bin(3-i)[2:].zfill(2), w_list[i]))
        tb_gen.gen_write_reg_tb(tb_gen.cmd_addr, 0x01)

tb_gen = ftb.fpga_tb_gen()
tb_gen.gen_reset_tb()
k = split_word('3c2d1e0f78695a4bb4a59687f0e1d2c3')
p = split_word('13121110171615141b1a19181f1e1d1c')
exec_lea_write('000', k)
exec_lea_write('001', p)
tb_gen.write_inst_regs(gen_inst_vect('011', '0', '00', '00000000'))
tb_gen.gen_write_reg_tb(tb_gen.cmd_addr, 0x01)
tb_gen.write_inst_regs(gen_inst_vect('010', '0', '11', '00000000'))
tb_gen.gen_write_reg_tb(tb_gen.cmd_addr, 0x01)
tb_gen.gen_write_reg_tb(tb_gen.cmd_addr, 0x02)
tb_gen.gen_read_reg_tb(tb_gen.lea_addrs[2])
# read the status register
#tb_gen.gen_read_reg_tb(tb_gen.status_addr)
