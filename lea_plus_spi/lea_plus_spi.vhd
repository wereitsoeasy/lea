library ieee;
use ieee.std_logic_1164.all;

entity lea_plus_spi is
	port(
		mosi, ss, sck, clk, rst: in std_logic;
		miso : out std_logic
	);
end lea_plus_spi;

architecture lea_plus_spi_arch of lea_plus_spi is

	signal spi_in, spi_out : std_logic_vector(7 downto 0);
	signal spi_busy, divd_clk, lea_busy, lea_exec,  sf_ss0, sf_ss1, sf_sb0, sf_sb1, sf_rst0, sf_rst1: std_logic;
	signal sf_ss2, sf_sb2, sf_rst2, sf_ss3, sf_sb3, sf_rst3, sf_ss4, sf_sb4, sf_rst4 : std_logic;
	signal lea_mem_out : std_logic_vector(31 downto 0);
	signal lea_inst_in : std_logic_vector(37 downto 0);
	
	component lea
		port(
			inst_in : in std_logic_vector(37 downto 0);
			clk, rst, exec : in std_logic;
			busy : out std_logic;
			mem_out : out std_logic_vector(31 downto 0)
		);
	end component;
	
	component data_bridge
		port(
			clk, lea_busy, spi_busy, ss, rst : in std_logic;
			inst_out : out std_logic_vector(37 downto 0);
			lea_mem_in : in std_logic_vector(31 downto 0);
			spi_in : in std_logic_vector(7 downto 0);
			spi_out : out std_logic_vector(7 downto 0);
			lea_exec : out std_logic
		);
	end component;

	component clk_div
		port(
			clk, rst : in std_logic;
			clk_out : out std_logic
		);
	end component;
	
	component spi_slave
		port(
			sck, rst : in std_logic;
			ss : in std_logic;
			mosi : in std_logic;
			miso : out std_logic;
			p_data_in : in std_logic_vector (7 downto 0);
			p_data_out : out std_logic_vector (7 downto 0);
			busy : out std_logic
		);
	end component;
	
begin
	
	l: lea
	port map(
		inst_in => lea_inst_in,
		clk => divd_clk,
		rst => sf_rst4,
		exec => lea_exec, 
		busy => lea_busy,
		mem_out => lea_mem_out
	);
	
	db: data_bridge
	port map(
		clk => divd_clk,  
		lea_busy => lea_busy,
		spi_busy => sf_sb4,
		ss => sf_ss4, 
		rst => sf_rst4,
		inst_out => lea_inst_in,
		lea_mem_in => lea_mem_out,
		spi_in =>spi_out,
		spi_out => spi_in,
		lea_exec => lea_exec
	);
	
	ssl: spi_slave
	port map(
		sck => sck,
		ss => ss,
		rst => rst,
		mosi => mosi,
		miso => miso,
		p_data_in => spi_in,
		p_data_out => spi_out,
		busy => spi_busy
	);

	cd: clk_div
	port map(
		clk => clk,
		rst => rst,
		clk_out => divd_clk
	);
	
	process(clk)
	begin
		if rising_edge(clk) then
			sf_ss0 <= ss;
			sf_ss1 <= sf_ss0;
			sf_ss2 <= sf_ss1;
			sf_ss3 <= sf_ss2;
			sf_ss4 <= sf_ss3;
			
			sf_sb0 <= spi_busy;
			sf_sb1 <= sf_sb0;
			sf_sb2 <= sf_sb1;
			sf_sb3 <= sf_sb2;
			sf_sb4 <= sf_sb3;
			
			sf_rst0 <= rst;
			sf_rst1 <= sf_rst0;
			sf_rst2 <= sf_rst1;
			sf_rst3 <= sf_rst2;
			sf_rst4 <= sf_rst3;
		end if;
	end process;
	
end lea_plus_spi_arch;