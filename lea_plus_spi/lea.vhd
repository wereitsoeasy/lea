

library ieee;
use ieee.std_logic_1164.all;

entity lea is
	port(
		inst_in : in std_logic_vector(37 downto 0);
		clk, rst, exec : in std_logic;
		busy : out std_logic;
		mem_out : out std_logic_vector(31 downto 0)
	);
end lea;

architecture lea_arch of lea is
	
	signal rot_dirs : std_logic_vector(1 downto 0) := "10";
	
	signal control_signals : std_logic_vector(17 downto 0);
	signal k_p_data_out, p_p_data_out, c_p_data_out, p_p_data_in, t_out, ks_out, x_out : std_logic_vector(127 downto 0);
	signal i_out : std_logic_vector(4 downto 0);
	signal mem_addr: std_logic_vector(1 downto 0);
	signal mem_data_out: std_logic_vector(31 downto 0);
	signal lt_24_flag : std_logic;
	
	component control_unit
		port ( 
			clk, rst, lt_24_flag, exec : in std_logic;
			inst_in : in std_logic_vector(37 downto 0);
			control_signals : out std_logic_vector(17 downto 0);
			busy: out std_logic;
			mem_data_out: out std_logic_vector(31 downto 0);
			mem_addr: out std_logic_vector(1 downto 0)
		);
	end component;
	
	component mem_block
		port(
			p_p_data_in, c_p_data_in : std_logic_vector(127 downto 0);
			data_in : in std_logic_vector(31 downto 0);
			clk, k_pi, k_rw, p_pi, p_rw, c_pi, c_rw : in std_logic;
			addr, mem_out_sel : in std_logic_vector(1 downto 0);
			k_p_data_out, p_p_data_out, c_p_data_out : out std_logic_vector(127 downto 0);
			mem_out : out std_logic_vector(31 downto 0)
		);
	end component;
	
	component ks_block 
		port(
			k_data_in : in std_logic_vector(127 downto 0);
			i_in : in std_logic_vector(4 downto 0);
			t_rw, t_clk, ks_end_mux, t_mux :in std_logic;
			rot_dirs : in std_logic_vector(1 downto 0);
			t_out, ks_out : out std_logic_vector(127 downto 0)
		);
	end component;
	
	component i_block 
		port(
			i_mux_sel, i_clr, i_clk, i_rw : in std_logic;
			lt_24_flag : out std_logic;
			i_data_out : out std_logic_vector(4 downto 0)
		);
	end component;
	
	component crypt_block
		port (
			p_mem_in, c_mem_in, ks_in, t_in : in std_logic_vector(127 downto 0);
			x_rw, x_clk, enc_end_mux : std_logic;
			x_mux : in std_logic_vector(1 downto 0);
			x_out : out std_logic_vector(127 downto 0);
			rot_dirs : in std_logic_vector(1 downto 0)
		);
	end component;

begin

	mb: mem_block
	port map(
		clk => clk,
		p_p_data_in => x_out,
		c_p_data_in => x_out,
		data_in => mem_data_out,
		k_pi => control_signals(5), 
		k_rw => control_signals(8), 
		p_pi => control_signals(4), 
		p_rw => control_signals(7), 
		c_pi => control_signals(3),
		c_rw => control_signals(6),
		addr => mem_addr,
		mem_out_sel => inst_in(36 downto 35),
		k_p_data_out => k_p_data_out,
		p_p_data_out => p_p_data_out,
		c_p_data_out => c_p_data_out,
		mem_out => mem_out
	);
	
	ksb: ks_block
	port map(
		k_data_in => k_p_data_out,
		i_in => i_out,
		t_rw => control_signals(11),
		t_clk => clk,
		ks_end_mux => control_signals(13),
		t_mux => control_signals(15),
		rot_dirs => rot_dirs,
		t_out => t_out,
		ks_out => ks_out
	);
	
	cb: crypt_block
	port map(
		p_mem_in => p_p_data_out,
		c_mem_in => c_p_data_out,
		ks_in => ks_out,
		t_in => t_out,
		x_rw => control_signals(12),
		x_clk => clk,
		enc_end_mux => control_signals(14),
		x_mux => control_signals(17 downto 16),
		x_out => x_out,
		rot_dirs => rot_dirs
	);
	
	ib: i_block 
	port map(
		i_mux_sel => control_signals(0),
		i_clr => control_signals(1), 
		i_clk => clk,
		i_rw => control_signals(2),
		lt_24_flag => lt_24_flag,
		i_data_out => i_out
	);
	
	cu: control_unit
	port map(
		clk => clk, 
		rst => rst,
		exec => exec,
		lt_24_flag => lt_24_flag,
		inst_in => inst_in,
		control_signals => control_signals,
		busy => busy,
		mem_data_out => mem_data_out,
		mem_addr => mem_addr
	);	
	
end lea_arch;