library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;

entity control_unit is
	port ( 
		clk, rst, lt_24_flag, exec : in std_logic;
		inst_in : in std_logic_vector(37 downto 0);
		control_signals : out std_logic_vector(17 downto 0);
		busy: out std_logic;
		mem_data_out: out std_logic_vector(31 downto 0);
		mem_addr: out std_logic_vector(1 downto 0)
	);
end control_unit;

architecture Behavioral of control_unit is    

    type states is (s0, s1, s2, s3, s4, s5, s6, s7, s8, s9, s10, s11, s12, s13, s14);
    signal state, next_state : states;
	 
begin

    -- registro de estados
	 
    process(clk, rst)
    begin
        if (clk'event and clk='1') then
            if (rst='1') then 
                state <= s0;
            else 
                state <= next_state;
            end if;
         end if;
    end process;

	 -- next state
	 
	 process(state, inst_in, lt_24_flag, exec)
	 begin
		case (state) is
			when s0 =>
				if exec = '1' then
					if (inst_in(37 downto 35) = "000" and inst_in(34) = '0') then 
						next_state <= s8;
					elsif (inst_in(37 downto 35) = "000" and inst_in(34) = '1') then 
						next_state <= s9;
					elsif (inst_in(37 downto 35) = "001" and inst_in(34) = '0') then 
						next_state <= s10;
					elsif (inst_in(37 downto 35) = "001" and inst_in(34) = '1') then 
						next_state <= s11;
					elsif (inst_in(37 downto 35) = "010" and inst_in(34) = '0') then 
						next_state <= s12;
					elsif (inst_in(37 downto 35) = "010" and inst_in(34) = '1') then 
						next_state <= s13;
					elsif (inst_in(37 downto 35) = "011") then
						next_state <= s1;
					elsif (inst_in(37 downto 35) = "100") then
						next_state <= s4;
					else 
						next_state <= s0;
					end if;
				else
					next_state <= s0;
				end if;
			when s1 =>
				next_state <= s2;
			when s2 => 
				if (lt_24_flag = '1') then
					next_state <= s2;
				else 
					next_state <= s3;
				end if;
			when s3 =>
				next_state <= s0;
			when s4 =>
				next_state <= s5;
			when s5 => 
				if (lt_24_flag = '1') then
					next_state <= s5;
				else  
					next_state <= s14;
				end if;
			when s14 =>
				next_state <= s6;
			when s6 => 
				if (lt_24_flag = '1') then
					next_state <= s6;
				else  
					next_state <= s7;
				end if;
			when s7 =>
				next_state <= s0;
			when others => 
				next_state <= s0;
		end case;
	 end process;
	 
	 --output
	 
	 process(state, inst_in)
	 begin
		case(state) is
			when s0 => 
				control_signals <= "000000001000111000";
				busy <= '0';
				mem_data_out <= inst_in(31 downto 0);
				mem_addr <= inst_in(33 downto 32);
			when s1 => 
				control_signals <= "011001100000000010" ;
				busy <= '1';
				mem_data_out <= inst_in(31 downto 0);
				mem_addr <= inst_in(33 downto 32);
			when s2 => 
				control_signals <= "000001100000000100" ;
				busy <= '1';
				mem_data_out <= inst_in(31 downto 0);
				mem_addr <= inst_in(33 downto 32);
			when s3 => 
				control_signals <= "000000000001000000" ;
				busy <= '1';
				mem_data_out <= inst_in(31 downto 0);
				mem_addr <= inst_in(33 downto 32);
			when s4 => 
				control_signals <= "101001100000000010" ;
				busy <= '1';
				mem_data_out <= inst_in(31 downto 0);
				mem_addr <= inst_in(33 downto 32);
			when s5 => 
				control_signals <= "000000100000000100" ;
				busy <= '1';
				mem_data_out <= inst_in(31 downto 0);
				mem_addr <= inst_in(33 downto 32);
			when s6 => 
				control_signals <= "000111100000000101" ;
				busy <= '1';
				mem_data_out <= inst_in(31 downto 0);
				mem_addr <= inst_in(33 downto 32);
			when s7 => 
				control_signals <= "000000000010000000" ;
				busy <= '1';
				mem_data_out <= inst_in(31 downto 0);
				mem_addr <= inst_in(33 downto 32);
			when s8 => 
				control_signals <= "000000000000111000";
				busy <= '1';
				mem_data_out <= inst_in(31 downto 0);
				mem_addr <= inst_in(33 downto 32);
			when s9 =>
				control_signals <= "000000000100111000";
				busy <= '1';
				mem_data_out <= inst_in(31 downto 0);
				mem_addr <= inst_in(33 downto 32);
			when s10 =>
				control_signals <= "000000001000111000";
				busy <= '1';
				mem_data_out <= inst_in(31 downto 0);
				mem_addr <= inst_in(33 downto 32);
			when s11 =>
				control_signals <= "000000001010111000";
				busy <= '1';
				mem_data_out <= inst_in(31 downto 0);
				mem_addr <= inst_in(33 downto 32);
			when s12 =>
				control_signals <= "000000010000111000";
				busy <= '1';
				mem_data_out <= inst_in(31 downto 0);
				mem_addr <= inst_in(33 downto 32);
			when s13 =>
				control_signals <= "000000010001111000";
				busy <= '1';
				mem_data_out <= inst_in(31 downto 0);
				mem_addr <= inst_in(33 downto 32);
			when s14 =>
				control_signals <= "000000000000000101";
				busy <= '1';
				mem_data_out <= inst_in(31 downto 0);
				mem_addr <= inst_in(33 downto 32);
			when others => 
				control_signals <= "000000000000000000";
				busy <= '0';
				mem_data_out <= inst_in(31 downto 0);
				mem_addr <= inst_in(33 downto 32);
		end case;
	 end process;
	 
end Behavioral;


