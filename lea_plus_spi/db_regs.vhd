library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
	
entity db_regs is
	port (
		rw, clk, lea_rw : in std_logic;
		write_addr, read_addr : in std_logic_vector(3 downto 0);
		data_in, status_data_in : in std_logic_vector(7 downto 0);
		data_out, command_out : out std_logic_vector(7 downto 0);
		inst_out : out std_logic_vector(39 downto 0);
		lea_mem_in : in std_logic_vector(31 downto 0)
	);
end db_regs;

architecture behavioral of db_regs is

	type regs is array (0 to 10) of std_logic_vector(7 downto 0);
	signal dbr : regs;
	
begin

	reg_write: 
	process(clk, rw, status_data_in, write_addr, lea_rw, lea_mem_in)
	begin	
		--command and inst registers
		if falling_edge(clk) then
			if rw = '1' then
				if to_integer(unsigned(write_addr)) < 6 then
					dbr(to_integer(unsigned(write_addr))) <= data_in;
				end if;
			end if;
			
			dbr(10) <= status_data_in;
			
			if lea_rw = '1' then
				dbr(6) <= lea_mem_in(31 downto 24);
				dbr(7) <= lea_mem_in(23 downto 16);
				dbr(8) <= lea_mem_in(15 downto 8);
				dbr(9) <= lea_mem_in(7 downto 0);
			end if;
		end if;
	end process;
	
	
	--individual read	
	process(read_addr, dbr)
	begin 
		if to_integer(unsigned(read_addr)) <= 10 then 
			data_out <= dbr(to_integer(unsigned(read_addr)));
		else
			data_out <= dbr(0);
		end if;
	end process;
	
	--parallel read
	command_out <= dbr(0);
	inst_out(39 downto 32) <= dbr(1);
	inst_out(31 downto 24) <= dbr(2);
	inst_out(23 downto 16) <= dbr(3);
	inst_out(15 downto 8) <= dbr(4);
	inst_out(7 downto 0) <= dbr(5);
	
end behavioral;