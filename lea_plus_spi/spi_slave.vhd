library ieee;
use ieee.std_logic_1164.all;

entity spi_slave is
	port(
		sck, rst : in std_logic;
		ss : in std_logic;
		mosi : in std_logic;
		miso : out std_logic;
		p_data_in : in std_logic_vector (7 downto 0);
		p_data_out : out std_logic_vector (7 downto 0);
		busy : out std_logic
	);
end spi_slave;

architecture spi_slave_arch of spi_slave is

	signal trns_busy, recv_busy : std_logic;
	signal circle : std_logic_vector (7 downto 0);

	component receiver
		generic(
			data_length : integer := 8
		);
		port(
			sck, rst : in std_logic;
			ss : in std_logic;
			mosi : in std_logic;
			data : out std_logic_vector (7 downto 0);
			busy : out std_logic
		);
	end component; 
	
	component transmitter
		generic (
			data_length : integer := 8
		);
		port(
			sck, rst : in std_logic;
			ss : in std_logic;
			data : in std_logic_vector (data_length-1 downto 0);
			miso : out std_logic;
			busy : out std_logic
		);
	end component; 
	
begin

	recv: receiver
	generic map(
		data_length => 8
	)
	port map(
		rst => rst,
		sck => sck,
		ss => ss,
		mosi => mosi,
		data => p_data_out,
		busy => recv_busy
	);
	
	trns: transmitter
	generic map(
		data_length => 8
	)
	port map(
		rst => rst,
		sck => sck,
		ss => ss,
		data => p_data_in,
		miso => miso,
		busy => trns_busy
	);
	
	busy <= trns_busy or recv_busy;

end spi_slave_arch;