library ieee;
use ieee.std_logic_1164.all;

entity db_reg is
	generic(
		data_length : integer := 4
	);
	port(
		clk, clr, rw : in std_logic;
		data_in : in std_logic_vector(data_length-1 downto 0);
		data_out : out std_logic_vector(data_length-1 downto 0)
	);
end db_reg;

architecture behavioral of db_reg is

	signal mem : std_logic_vector(data_length-1 downto 0);
	
begin

	reg: process(clk, rw, data_in, clr) is
	begin
		if clr = '1' then
			mem <= (others => '0');
		elsif(falling_edge(clk) and rw = '1') then
			mem <= data_in;
		end if;
	end process;

	data_out <= mem;

end behavioral;