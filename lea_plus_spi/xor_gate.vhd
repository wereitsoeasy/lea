library ieee;
use ieee.std_logic_1164.all; 
use ieee.numeric_std.all;

entity xor_gate is
 
	port (
		opa, opb : in std_logic_vector(31 downto 0);
		result : out std_logic_vector(31 downto 0)
	);
 
end xor_gate;

architecture xor_gate_arch of xor_gate is
begin
	
	result <= (opa xor opb);
	
end xor_gate_arch;