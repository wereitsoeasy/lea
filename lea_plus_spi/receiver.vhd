library ieee;
use ieee.std_logic_1164.all;

entity receiver is
	generic(
		data_length : integer := 8
	);
	port(
		sck, rst : in std_logic;
		ss : in std_logic;
		mosi : in std_logic;
		data : out std_logic_vector (7 downto 0);
		busy : out std_logic
	);
end receiver;

architecture behavioral of receiver is

signal data_temp : std_logic_vector (data_length-1 downto 0);
signal index     : integer := data_length-1;

begin

	process(sck, ss, rst, mosi, index)
	begin
		if rst = '1' then
			index <= data_length-1;
		elsif ss = '0' then
			if rising_edge(sck) then
				if (index > 0) then 
					index <= index - 1;
				else
					index <= data_length - 1;
				end if;
				data_temp(index) <= mosi;
			end if;
		end if;
	end process;	

	process(index)
	begin
		if (index = data_length - 1) then
			busy <= '0';
		else
			busy <= '1';
		end if;
	end process;

	data <= data_temp;
	
end behavioral;
