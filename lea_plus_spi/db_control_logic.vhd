library ieee;
use ieee.std_logic_1164.all;

entity db_control_logic is
	port(
		control_signals : out std_logic_vector(6 downto 0);
		clk, ss, lea_busy, spi_busy, rst, reg_wr_in : in std_logic;
		cmd_in : in std_logic_vector(7 downto 0);
		reg_addr_in : in std_logic_vector(3 downto 0)
	);
end db_control_logic;

architecture behavioral of db_control_logic is
	
	type states is (s0, s1, s2, s3, s4, s5, s6, s7, s8, s9);
	signal state, next_state : states;

begin
	
	 -- registro de estados
	 
    process(clk, rst)
    begin
		if (rst='1') then 
			state <= s0;
      elsif (clk'event and clk='1') then
			state <= next_state;
		end if;
    end process;

	 -- next state
	 
	 process(state, cmd_in, spi_busy, ss, reg_addr_in, reg_wr_in)
	 begin
		case (state) is
			when s0 =>
				if ss = '0' then
					next_state <= s1;
				else
					next_state <= s0;
				end if;
			when s1 =>
				if spi_busy = '1' then
					next_state <= s2;
				else
					next_state <= s1;
				end if;
			when s2 =>
				if spi_busy = '0' then
					next_state <= s3;
				else
					next_state <= s2;
				end if;
			when s3 =>
				next_state <= s4;
			when s4 =>
				if spi_busy = '1' then
					next_state <= s5;
				else
					next_state <= s4;
				end if;
			when s5 =>
				if spi_busy = '0' then
					next_state <= s6;
				else
					next_state <= s5;
				end if;
			when s6 =>
				next_state <= s7;
			when s7 =>
				if ss = '1' then
					if reg_addr_in = x"0" and reg_wr_in = '1' then
						if cmd_in = x"00" then
							next_state <= s0;
						elsif cmd_in = x"01" then
							next_state <= s8;
						elsif cmd_in = x"02" then
							next_state <= s9;
						else
							next_state <= s0;
						end if;
					else
						next_state <= s0;
					end if;
				else
					next_state <= s7;
				end if;
			when s8 =>
				next_state <= s0;
			when s9 =>
				next_state <= s0;
			when others =>
				next_state <= s0;
		end case;
	 end process;
	 
	 --output
	 
	 process(state)
	 begin
		case(state) is
			when s0 => 
				control_signals <= "0000100";
			when s3 =>
				control_signals <= "0101000";
			when s6 =>
				control_signals <= "1000000";
			when s8 => 
				control_signals <= "0000001";
			when s9 =>
				control_signals <= "0000010";
			when others => 
				control_signals <= "0000000";
		end case;
	 end process;
	 
	
end behavioral;
