library ieee;
use ieee.std_logic_1164.all;
use ieee.math_real.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;

entity delta_mem is

  port (
	 addr 	 : in  std_logic_vector(2 downto 0);
	 data_out : out std_logic_vector(31 downto 0)
	 );
	 
end delta_mem;
 
architecture delta_mem_arch of delta_mem is

type rom_memory is array (0 to 7) of std_logic_vector (31 downto 0);

signal rom : rom_memory := ("11000011111011111110100111011011",
									 "01000100011000100110101100000010",
									 "01111001111000100111110010001010",
									 "01111000110111110011000011101100",
									 "01110001010111101010010010011110",
									 "11000111100001011101101000001010",
									 "11100000010011101111001000101010",
									 "11100101110001000000100101010111");
									 
begin

	data_out <= rom(conv_integer(addr));

end delta_mem_arch;