library ieee;
use ieee.std_logic_1164.all; 
use ieee.numeric_std.all;

entity ks_rol is

	port(
		i : in std_logic_vector(4 downto 0);
		d : in std_logic_vector(31 downto 0);
		rot_dir : in std_logic;
		o : out std_logic_vector(127 downto 0)
	);

end ks_rol;

architecture ks_rol_arch of ks_rol is

	signal modadd0_to_rol1 : std_logic_vector(4 downto 0);
	signal modadd1_to_rol2 : std_logic_vector(4 downto 0);
	signal modadd2_to_rol3 : std_logic_vector(4 downto 0);
	signal rot_const : std_logic_vector(14 downto 0);

	component ks_rol_rot_const
		port(
			data : out std_logic_vector(14 downto 0)
		);
	end component;
	
	component mod_add
		generic(
		n : integer := 32
	);
	port (
		opa, opb : in std_logic_vector(n-1 downto 0);
		result: out std_logic_vector(n-1 downto 0)
	);
	end component;
	
	component rot_component
		port (
			n      : in std_logic_vector(4 downto 0);
			op     : in std_logic_vector(31 downto 0);
			lr     : in std_logic;
			result : out std_logic_vector(31 downto 0)
		);
	end component;

begin

	krrc0: ks_rol_rot_const
	port map(
		data(14 downto 10) => rot_const(14 downto 10),
		data(9 downto 5) => rot_const(9 downto 5),
		data(4 downto 0) => rot_const(4 downto 0)
	);
	
	modadd0: mod_add
	generic map(
		n => 5
	)
	port map(
		opa => i,
		opb => rot_const(14 downto 10),
		result => modadd0_to_rol1
	);
	
	modadd1: mod_add
	generic map(
		n => 5
	)
	port map(
		opa => i,
		opb => rot_const(9 downto 5),
		result => modadd1_to_rol2
	);
	
	modadd2:  mod_add
	generic map(
		n => 5
	)
	port map(
		opa => i,
		opb => rot_const(4 downto 0),
		result => modadd2_to_rol3
	);
	
	rol0: rot_component
	port map(
		n => i,
		op => d,
		lr => rot_dir,
		result => o(127 downto 96)
	);
	
	rol1: rot_component
	port map(
		n => modadd0_to_rol1,
		op => d,
		lr => rot_dir,
		result => o(95 downto 64)
	);
	
	rol2: rot_component
	port map(
		n => modadd1_to_rol2,
		op => d,
		lr => rot_dir,
		result => o(63 downto 32)
	);
	
	rol3: rot_component
	port map(
		n => modadd2_to_rol3,
		op => d,
		lr => rot_dir,
		result => o(31 downto 0)
	);

end ks_rol_arch;