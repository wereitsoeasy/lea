library ieee;
use ieee.std_logic_1164.all; 
use ieee.numeric_std.all;

entity ks_round is
	
	port (
		d : in std_logic_vector(127 downto 0);
		t : in std_logic_vector(127 downto 0);
		rot_dir : in std_logic;
		rot_const : in std_logic_vector(19 downto 0);
		ot : out std_logic_vector(127 downto 0)
	);
 
end ks_round;

architecture ks_round_arch of ks_round is

	component ks_round_block
		port (
			d : in std_logic_vector(31 downto 0);
			t : in std_logic_vector(31 downto 0);
			p_ot : out std_logic_vector(31 downto 0);
			rot_dir : in std_logic;
			rot_const_in : in std_logic_vector(4 downto 0)
		);
	end component;

begin

	krb0: ks_round_block
	port map(
		d => d(127 downto 96),
		t => t(127 downto 96),
		p_ot => ot(127 downto 96),
		rot_dir => rot_dir,
		rot_const_in => rot_const(19 downto 15)
	);
	
	
	krb1: ks_round_block
	port map(
		d => d(95 downto 64),
		t => t(95 downto 64),
		p_ot => ot(95 downto 64),
		rot_dir => rot_dir,
		rot_const_in => rot_const(14 downto 10)
	);
	
	krb2: ks_round_block
	port map(
		d => d(63 downto 32),
		t => t(63 downto 32),
		p_ot => ot(63 downto 32),
		rot_dir => rot_dir,
		rot_const_in => rot_const(9 downto 5)
	);
	
	krb3: ks_round_block
	port map(
		d => d(31 downto 0),
		t => t(31 downto 0),
		p_ot => ot(31 downto 0),
		rot_dir => rot_dir,
		rot_const_in => rot_const(4 downto 0)
	);

end ks_round_arch;