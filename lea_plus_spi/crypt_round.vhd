library ieee;
use ieee.std_logic_1164.all; 
use ieee.numeric_std.all;

entity crypt_round is
	port(
		x : in std_logic_vector(127 downto 0);
		t : in std_logic_vector(127 downto 0);
		rot_dirs : in std_logic_vector(1 downto 0);
		rot_const : in std_logic_vector(14 downto 0);
		ox : out std_logic_vector(127 downto 0)
	);
end crypt_round;

architecture crypt_round_arch of crypt_round is

	component crypt_round_block 
	port (
		x0 : in std_logic_vector(31 downto 0);
		x1 : in std_logic_vector(31 downto 0);
		t0 : in std_logic_vector(31 downto 0);
		t1 : in std_logic_vector(31 downto 0);
		p_ox : out std_logic_vector(31 downto 0);
		lr : in std_logic;
		rot_const_in : in std_logic_vector(4 downto 0)
	);
	end component;

begin
	
	crb0: crypt_round_block port map(
		x0 => x(127 downto 96),
		t0 => t(127 downto 96),
		x1 => x(95 downto 64),
		t1 => t(95 downto 64),
		p_ox => ox(127 downto 96),
		lr => rot_dirs(0),
		rot_const_in => rot_const(14 downto 10)
	);
	
	crb1: crypt_round_block port map(
		x0 => x(95 downto 64),
		t0 => t(63 downto 32),
		x1 => x(63 downto 32),
		t1 => t(95 downto 64),
		p_ox => ox(95 downto 64),
		lr => rot_dirs(1),
		rot_const_in => rot_const(9 downto 5)
	);
	
	crb2: crypt_round_block port map(
		x0 => x(63 downto 32),
		t0 => t(31 downto 0),
		x1 => x(31 downto 0),
		t1 => t(95 downto 64),
		p_ox => ox(63 downto 32),
		lr => rot_dirs(1),
		rot_const_in => rot_const(4 downto 0)
	);
	
	ox(31 downto 0) <= x(127 downto 96);

end crypt_round_arch;