library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity clk_div is
	port(
		clk, rst : in std_logic;
		clk_out : out std_logic
	);
end clk_div;

architecture behavioral of clk_div is

	signal clk_temp : unsigned (5 downto 0);

begin
	
	process(clk, rst) 
	begin
		if rst = '1' then
			clk_temp <= "000000";
		elsif rising_edge(clk) then
			clk_temp <= clk_temp + 1;
		end if;
	end process;
	clk_out <= clk;
end behavioral;