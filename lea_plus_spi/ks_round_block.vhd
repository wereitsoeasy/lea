library ieee;
use ieee.std_logic_1164.all; 
use ieee.numeric_std.all;

entity ks_round_block is
	
	port (
		d : in std_logic_vector(31 downto 0);
		t : in std_logic_vector(31 downto 0);
		p_ot : out std_logic_vector(31 downto 0);
		rot_dir : in std_logic;
		rot_const_in : in std_logic_vector(4 downto 0)
	);
 
end ks_round_block;

architecture ks_round_block_arch of ks_round_block is

	signal modadd_to_rol : std_logic_vector(31 downto 0);
	
	component rot_component
	port (
		n      : in std_logic_vector(4 downto 0);
		op     : in std_logic_vector(31 downto 0);
		lr     : in std_logic;
		result : out std_logic_vector(31 downto 0)
	);
	end component;
	
	component mod_add
	generic(
		n : integer := 32
	);
	port (
		opa, opb : in std_logic_vector(n-1 downto 0);
		result: out std_logic_vector(n-1 downto 0)
	);
	end component;

begin
	
	modadd0: mod_add 
	port map(
		opa => t,
		opb => d,
		result => modadd_to_rol
	);
	
	rol0: rot_component
	port map(
		n => rot_const_in,
		op => modadd_to_rol,
		lr => rot_dir,
		result => p_ot
	);

end ks_round_block_arch;