library ieee;
use ieee.std_logic_1164.all;
use ieee.math_real.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;


entity crypt_rot_const is

  port (
	 data	: out std_logic_vector(14 downto 0)
	);
	 
end crypt_rot_const;
 
architecture crypt_rot_const_arch of crypt_rot_const is

type rom_memory is array (2 downto 0) of std_logic_vector (4 downto 0);

signal rom : rom_memory := ("01001",
									 "00101",
									 "00011");
									 
begin
	
	data(14 downto 10) <= rom(2);
	data(9 downto 5) <= rom(1);
	data(4 downto 0) <= rom(0);

end crypt_rot_const_arch;