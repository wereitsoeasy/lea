library ieee;
use ieee.std_logic_1164.all; 
use ieee.numeric_std.all;

entity crypt_round_block is

	port (
		x0 : in std_logic_vector(31 downto 0);
		x1 : in std_logic_vector(31 downto 0);
		t0 : in std_logic_vector(31 downto 0);
		t1 : in std_logic_vector(31 downto 0);
		p_ox : out std_logic_vector(31 downto 0);
		lr : in std_logic;
		rot_const_in : in std_logic_vector(4 downto 0)
	);
	
end crypt_round_block;

architecture crypt_round_block_arch of crypt_round_block is
	
	signal xor0_to_modadd : std_logic_vector(31 downto 0);
	signal xor1_to_modadd : std_logic_vector(31 downto 0);
	signal modadd_to_rol : std_logic_vector(31 downto 0);
	
	component xor_gate 
	port (
		opa, opb : in std_logic_vector(31 downto 0);
		result : out std_logic_vector(31 downto 0)
	);
	end component;
	
	component mod_add 
	port (
		opa, opb : in std_logic_vector(31 downto 0);
		result: out std_logic_vector(31 downto 0)
	);
	end component;
	
	component rot_component 
	port (
		n      : in std_logic_vector(4 downto 0);
		op     : in std_logic_vector(31 downto 0);
		lr     : in std_logic;
		result : out std_logic_vector(31 downto 0)
	);
	end component;

begin
	
	xor0: xor_gate port map(
		opa => x0,
		opb => t0,
		result => xor0_to_modadd
	);
	
	xor1: xor_gate port map(
		opa => x1,
		opb => t1,
		result => xor1_to_modadd
	);
	
	modadd0: mod_add port map(
		opa => xor0_to_modadd,
		opb => xor1_to_modadd,
		result => modadd_to_rol 
	);
	
	rot0: rot_component 
	port map(
		n => rot_const_in,
		op => modadd_to_rol,
		lr => lr,
		result => p_ox
	);

end crypt_round_block_arch;