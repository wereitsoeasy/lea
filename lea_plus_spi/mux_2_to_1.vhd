library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.numeric_bit.all;
use ieee.std_logic_unsigned.all;

entity mux_2_to_1 is

	generic (
		n : integer
	);
	port (
		a: in  std_logic_vector(n-1 downto 0);
		b: in  std_logic_vector(n-1 downto 0);
		s: in  std_logic;
		c: out std_logic_vector(n-1 downto 0)
	);
 
end mux_2_to_1;

architecture mux_2_to_1_arch of mux_2_to_1 is
begin

	c <= b when s = '1' else a;
	
end mux_2_to_1_arch;