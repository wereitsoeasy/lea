transcript on
if {[file exists rtl_work]} {
	vdel -lib rtl_work -all
}
vlib rtl_work
vmap work rtl_work

vcom -93 -work work {/home/mauricio/lea/lea_plus_spi/spi_slave.vhd}
vcom -93 -work work {/home/mauricio/lea/lea_plus_spi/inv_ks_round_block.vhd}
vcom -93 -work work {/home/mauricio/lea/lea_plus_spi/ks_round.vhd}
vcom -93 -work work {/home/mauricio/lea/lea_plus_spi/clk_div.vhd}
vcom -93 -work work {/home/mauricio/lea/lea_plus_spi/mem_block.vhd}
vcom -93 -work work {/home/mauricio/lea/lea_plus_spi/receiver.vhd}
vcom -93 -work work {/home/mauricio/lea/lea_plus_spi/mux_2_to_1.vhd}
vcom -93 -work work {/home/mauricio/lea/lea_plus_spi/inv_ks_round.vhd}
vcom -93 -work work {/home/mauricio/lea/lea_plus_spi/ks_block.vhd}
vcom -93 -work work {/home/mauricio/lea/lea_plus_spi/xor_gate.vhd}
vcom -93 -work work {/home/mauricio/lea/lea_plus_spi/data_bridge.vhd}
vcom -93 -work work {/home/mauricio/lea/lea_plus_spi/KPCmem.vhd}
vcom -93 -work work {/home/mauricio/lea/lea_plus_spi/crypt_block.vhd}
vcom -93 -work work {/home/mauricio/lea/lea_plus_spi/control_unit.vhd}
vcom -93 -work work {/home/mauricio/lea/lea_plus_spi/ks_rot_const.vhd}
vcom -93 -work work {/home/mauricio/lea/lea_plus_spi/mod_add.vhd}
vcom -93 -work work {/home/mauricio/lea/lea_plus_spi/decrypt_round.vhd}
vcom -93 -work work {/home/mauricio/lea/lea_plus_spi/transmitter.vhd}
vcom -93 -work work {/home/mauricio/lea/lea_plus_spi/delta_mem.vhd}
vcom -93 -work work {/home/mauricio/lea/lea_plus_spi/db_reg_single.vhd}
vcom -93 -work work {/home/mauricio/lea/lea_plus_spi/i_block.vhd}
vcom -93 -work work {/home/mauricio/lea/lea_plus_spi/mux_3_to_1.vhd}
vcom -93 -work work {/home/mauricio/lea/lea_plus_spi/crypt_rot_const.vhd}
vcom -93 -work work {/home/mauricio/lea/lea_plus_spi/ks_rol_rot_const.vhd}
vcom -93 -work work {/home/mauricio/lea/lea_plus_spi/i_reg.vhd}
vcom -93 -work work {/home/mauricio/lea/lea_plus_spi/comp_flag.vhd}
vcom -93 -work work {/home/mauricio/lea/lea_plus_spi/ks_rol.vhd}
vcom -93 -work work {/home/mauricio/lea/lea_plus_spi/rot_component.vhd}
vcom -93 -work work {/home/mauricio/lea/lea_plus_spi/mod_sub.vhd}
vcom -93 -work work {/home/mauricio/lea/lea_plus_spi/db_regs.vhd}
vcom -93 -work work {/home/mauricio/lea/lea_plus_spi/crypt_round_block.vhd}
vcom -93 -work work {/home/mauricio/lea/lea_plus_spi/lea.vhd}
vcom -93 -work work {/home/mauricio/lea/lea_plus_spi/decrypt_round_block.vhd}
vcom -93 -work work {/home/mauricio/lea/lea_plus_spi/XT.vhd}
vcom -93 -work work {/home/mauricio/lea/lea_plus_spi/ks_round_block.vhd}
vcom -93 -work work {/home/mauricio/lea/lea_plus_spi/db_reg.vhd}
vcom -93 -work work {/home/mauricio/lea/lea_plus_spi/crypt_round.vhd}
vcom -93 -work work {/home/mauricio/lea/lea_plus_spi/db_control_logic.vhd}
vcom -93 -work work {/home/mauricio/lea/lea_plus_spi/lea_plus_spi.vhd}

