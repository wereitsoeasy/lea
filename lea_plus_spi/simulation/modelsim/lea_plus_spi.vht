-- Copyright (C) 1991-2013 Altera Corporation
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, Altera MegaCore Function License 
-- Agreement, or other applicable license agreement, including, 
-- without limitation, that your use is for the sole purpose of 
-- programming logic devices manufactured by Altera and sold by 
-- Altera or its authorized distributors.  Please refer to the 
-- applicable agreement for further details.

-- ***************************************************************************
-- This file contains a Vhdl test bench template that is freely editable to   
-- suit user's needs .Comments are provided in each section to help the user  
-- fill out necessary details.                                                
-- ***************************************************************************
-- Generated on "11/12/2020 18:51:05"
                                                            
-- Vhdl Test Bench template for design  :  lea_plus_spi
-- 
-- Simulation tool : ModelSim-Altera (VHDL)
-- 

LIBRARY ieee;                                               
USE ieee.std_logic_1164.all;                                

ENTITY lea_plus_spi_vhd_tst IS
END lea_plus_spi_vhd_tst;
ARCHITECTURE lea_plus_spi_arch OF lea_plus_spi_vhd_tst IS
-- constants                                                 
-- signals                                                   
SIGNAL clk : STD_LOGIC := '0';
SIGNAL miso : STD_LOGIC;
SIGNAL mosi : STD_LOGIC;
SIGNAL rst : STD_LOGIC;
SIGNAL sck : STD_LOGIC := '0';
SIGNAL ss : STD_LOGIC;
COMPONENT lea_plus_spi
	PORT (
	clk : IN STD_LOGIC;
	miso : BUFFER STD_LOGIC;
	mosi : IN STD_LOGIC;
	rst : IN STD_LOGIC;
	sck : IN STD_LOGIC;
	ss : IN STD_LOGIC
	);
END COMPONENT;
BEGIN
	i1 : lea_plus_spi
	PORT MAP (
-- list connections between master ports and signals
	clk => clk,
	miso => miso,
	mosi => mosi,
	rst => rst,
	sck => sck,
	ss => ss
	);

	clk <= not clk after 1 ps;
process
begin

	-- reset
rst <= '1';
wait for 6 ns;
rst <= '0';
wait for 6 ns;
-- reset
rst <= '1';
wait for 6 ns;
rst <= '0';
wait for 6 ns;
-- writing instruction 073c2d1e0f
--addr: 88
ss <= '1';
wait for 5 ns;
ss <= '0';
wait for 16 ns;
-- bit 0
mosi <= '1';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
-- bit 0
mosi <= '0';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
ss <= '1';
--addr: 90
ss <= '1';
wait for 5 ns;
ss <= '0';
wait for 16 ns;
-- bit 0
mosi <= '1';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
-- bit 0
mosi <= '0';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
ss <= '1';
--addr: 98
ss <= '1';
wait for 5 ns;
ss <= '0';
wait for 16 ns;
-- bit 0
mosi <= '1';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
-- bit 0
mosi <= '0';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
ss <= '1';
--addr: a0
ss <= '1';
wait for 5 ns;
ss <= '0';
wait for 16 ns;
-- bit 0
mosi <= '1';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
-- bit 0
mosi <= '0';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
ss <= '1';
--addr: a8
ss <= '1';
wait for 5 ns;
ss <= '0';
wait for 16 ns;
-- bit 0
mosi <= '1';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
-- bit 0
mosi <= '0';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
ss <= '1';
--addr: 80
ss <= '1';
wait for 5 ns;
ss <= '0';
wait for 16 ns;
-- bit 0
mosi <= '1';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
-- bit 0
mosi <= '0';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
ss <= '1';
-- writing instruction 0678695a4b
--addr: 88
ss <= '1';
wait for 5 ns;
ss <= '0';
wait for 16 ns;
-- bit 0
mosi <= '1';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
-- bit 0
mosi <= '0';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
ss <= '1';
--addr: 90
ss <= '1';
wait for 5 ns;
ss <= '0';
wait for 16 ns;
-- bit 0
mosi <= '1';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
-- bit 0
mosi <= '0';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
ss <= '1';
--addr: 98
ss <= '1';
wait for 5 ns;
ss <= '0';
wait for 16 ns;
-- bit 0
mosi <= '1';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
-- bit 0
mosi <= '0';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
ss <= '1';
--addr: a0
ss <= '1';
wait for 5 ns;
ss <= '0';
wait for 16 ns;
-- bit 0
mosi <= '1';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
-- bit 0
mosi <= '0';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
ss <= '1';
--addr: a8
ss <= '1';
wait for 5 ns;
ss <= '0';
wait for 16 ns;
-- bit 0
mosi <= '1';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
-- bit 0
mosi <= '0';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
ss <= '1';
--addr: 80
ss <= '1';
wait for 5 ns;
ss <= '0';
wait for 16 ns;
-- bit 0
mosi <= '1';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
-- bit 0
mosi <= '0';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
ss <= '1';
-- writing instruction 05b4a59687
--addr: 88
ss <= '1';
wait for 5 ns;
ss <= '0';
wait for 16 ns;
-- bit 0
mosi <= '1';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
-- bit 0
mosi <= '0';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
ss <= '1';
--addr: 90
ss <= '1';
wait for 5 ns;
ss <= '0';
wait for 16 ns;
-- bit 0
mosi <= '1';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
-- bit 0
mosi <= '1';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
ss <= '1';
--addr: 98
ss <= '1';
wait for 5 ns;
ss <= '0';
wait for 16 ns;
-- bit 0
mosi <= '1';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
-- bit 0
mosi <= '1';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
ss <= '1';
--addr: a0
ss <= '1';
wait for 5 ns;
ss <= '0';
wait for 16 ns;
-- bit 0
mosi <= '1';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
-- bit 0
mosi <= '1';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
ss <= '1';
--addr: a8
ss <= '1';
wait for 5 ns;
ss <= '0';
wait for 16 ns;
-- bit 0
mosi <= '1';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
-- bit 0
mosi <= '1';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
ss <= '1';
--addr: 80
ss <= '1';
wait for 5 ns;
ss <= '0';
wait for 16 ns;
-- bit 0
mosi <= '1';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
-- bit 0
mosi <= '0';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
ss <= '1';
-- writing instruction 04f0e1d2c3
--addr: 88
ss <= '1';
wait for 5 ns;
ss <= '0';
wait for 16 ns;
-- bit 0
mosi <= '1';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
-- bit 0
mosi <= '0';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
ss <= '1';
--addr: 90
ss <= '1';
wait for 5 ns;
ss <= '0';
wait for 16 ns;
-- bit 0
mosi <= '1';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
-- bit 0
mosi <= '1';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
ss <= '1';
--addr: 98
ss <= '1';
wait for 5 ns;
ss <= '0';
wait for 16 ns;
-- bit 0
mosi <= '1';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
-- bit 0
mosi <= '1';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
ss <= '1';
--addr: a0
ss <= '1';
wait for 5 ns;
ss <= '0';
wait for 16 ns;
-- bit 0
mosi <= '1';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
-- bit 0
mosi <= '1';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
ss <= '1';
--addr: a8
ss <= '1';
wait for 5 ns;
ss <= '0';
wait for 16 ns;
-- bit 0
mosi <= '1';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
-- bit 0
mosi <= '1';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
ss <= '1';
--addr: 80
ss <= '1';
wait for 5 ns;
ss <= '0';
wait for 16 ns;
-- bit 0
mosi <= '1';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
-- bit 0
mosi <= '0';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
ss <= '1';
-- writing instruction 0f13121110
--addr: 88
ss <= '1';
wait for 5 ns;
ss <= '0';
wait for 16 ns;
-- bit 0
mosi <= '1';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
-- bit 0
mosi <= '0';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
ss <= '1';
--addr: 90
ss <= '1';
wait for 5 ns;
ss <= '0';
wait for 16 ns;
-- bit 0
mosi <= '1';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
-- bit 0
mosi <= '0';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
ss <= '1';
--addr: 98
ss <= '1';
wait for 5 ns;
ss <= '0';
wait for 16 ns;
-- bit 0
mosi <= '1';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
-- bit 0
mosi <= '0';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
ss <= '1';
--addr: a0
ss <= '1';
wait for 5 ns;
ss <= '0';
wait for 16 ns;
-- bit 0
mosi <= '1';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
-- bit 0
mosi <= '0';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
ss <= '1';
--addr: a8
ss <= '1';
wait for 5 ns;
ss <= '0';
wait for 16 ns;
-- bit 0
mosi <= '1';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
-- bit 0
mosi <= '0';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
ss <= '1';
--addr: 80
ss <= '1';
wait for 5 ns;
ss <= '0';
wait for 16 ns;
-- bit 0
mosi <= '1';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
-- bit 0
mosi <= '0';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
ss <= '1';
-- writing instruction 0e17161514
--addr: 88
ss <= '1';
wait for 5 ns;
ss <= '0';
wait for 16 ns;
-- bit 0
mosi <= '1';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
-- bit 0
mosi <= '0';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
ss <= '1';
--addr: 90
ss <= '1';
wait for 5 ns;
ss <= '0';
wait for 16 ns;
-- bit 0
mosi <= '1';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
-- bit 0
mosi <= '0';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
ss <= '1';
--addr: 98
ss <= '1';
wait for 5 ns;
ss <= '0';
wait for 16 ns;
-- bit 0
mosi <= '1';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
-- bit 0
mosi <= '0';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
ss <= '1';
--addr: a0
ss <= '1';
wait for 5 ns;
ss <= '0';
wait for 16 ns;
-- bit 0
mosi <= '1';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
-- bit 0
mosi <= '0';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
ss <= '1';
--addr: a8
ss <= '1';
wait for 5 ns;
ss <= '0';
wait for 16 ns;
-- bit 0
mosi <= '1';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
-- bit 0
mosi <= '0';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
ss <= '1';
--addr: 80
ss <= '1';
wait for 5 ns;
ss <= '0';
wait for 16 ns;
-- bit 0
mosi <= '1';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
-- bit 0
mosi <= '0';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
ss <= '1';
-- writing instruction 0d1b1a1918
--addr: 88
ss <= '1';
wait for 5 ns;
ss <= '0';
wait for 16 ns;
-- bit 0
mosi <= '1';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
-- bit 0
mosi <= '0';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
ss <= '1';
--addr: 90
ss <= '1';
wait for 5 ns;
ss <= '0';
wait for 16 ns;
-- bit 0
mosi <= '1';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
-- bit 0
mosi <= '0';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
ss <= '1';
--addr: 98
ss <= '1';
wait for 5 ns;
ss <= '0';
wait for 16 ns;
-- bit 0
mosi <= '1';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
-- bit 0
mosi <= '0';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
ss <= '1';
--addr: a0
ss <= '1';
wait for 5 ns;
ss <= '0';
wait for 16 ns;
-- bit 0
mosi <= '1';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
-- bit 0
mosi <= '0';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
ss <= '1';
--addr: a8
ss <= '1';
wait for 5 ns;
ss <= '0';
wait for 16 ns;
-- bit 0
mosi <= '1';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
-- bit 0
mosi <= '0';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
ss <= '1';
--addr: 80
ss <= '1';
wait for 5 ns;
ss <= '0';
wait for 16 ns;
-- bit 0
mosi <= '1';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
-- bit 0
mosi <= '0';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
ss <= '1';
-- writing instruction 0c1f1e1d1c
--addr: 88
ss <= '1';
wait for 5 ns;
ss <= '0';
wait for 16 ns;
-- bit 0
mosi <= '1';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
-- bit 0
mosi <= '0';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
ss <= '1';
--addr: 90
ss <= '1';
wait for 5 ns;
ss <= '0';
wait for 16 ns;
-- bit 0
mosi <= '1';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
-- bit 0
mosi <= '0';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
ss <= '1';
--addr: 98
ss <= '1';
wait for 5 ns;
ss <= '0';
wait for 16 ns;
-- bit 0
mosi <= '1';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
-- bit 0
mosi <= '0';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
ss <= '1';
--addr: a0
ss <= '1';
wait for 5 ns;
ss <= '0';
wait for 16 ns;
-- bit 0
mosi <= '1';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
-- bit 0
mosi <= '0';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
ss <= '1';
--addr: a8
ss <= '1';
wait for 5 ns;
ss <= '0';
wait for 16 ns;
-- bit 0
mosi <= '1';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
-- bit 0
mosi <= '0';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
ss <= '1';
--addr: 80
ss <= '1';
wait for 5 ns;
ss <= '0';
wait for 16 ns;
-- bit 0
mosi <= '1';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
-- bit 0
mosi <= '0';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
ss <= '1';
-- writing instruction 1800000000
--addr: 88
ss <= '1';
wait for 5 ns;
ss <= '0';
wait for 16 ns;
-- bit 0
mosi <= '1';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
-- bit 0
mosi <= '0';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
ss <= '1';
--addr: 90
ss <= '1';
wait for 5 ns;
ss <= '0';
wait for 16 ns;
-- bit 0
mosi <= '1';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
-- bit 0
mosi <= '0';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
ss <= '1';
--addr: 98
ss <= '1';
wait for 5 ns;
ss <= '0';
wait for 16 ns;
-- bit 0
mosi <= '1';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
-- bit 0
mosi <= '0';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
ss <= '1';
--addr: a0
ss <= '1';
wait for 5 ns;
ss <= '0';
wait for 16 ns;
-- bit 0
mosi <= '1';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
-- bit 0
mosi <= '0';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
ss <= '1';
--addr: a8
ss <= '1';
wait for 5 ns;
ss <= '0';
wait for 16 ns;
-- bit 0
mosi <= '1';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
-- bit 0
mosi <= '0';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
ss <= '1';
--addr: 80
ss <= '1';
wait for 5 ns;
ss <= '0';
wait for 16 ns;
-- bit 0
mosi <= '1';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
-- bit 0
mosi <= '0';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
ss <= '1';
-- writing instruction 1300000000
--addr: 88
ss <= '1';
wait for 5 ns;
ss <= '0';
wait for 16 ns;
-- bit 0
mosi <= '1';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
-- bit 0
mosi <= '0';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
ss <= '1';
--addr: 90
ss <= '1';
wait for 5 ns;
ss <= '0';
wait for 16 ns;
-- bit 0
mosi <= '1';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
-- bit 0
mosi <= '0';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
ss <= '1';
--addr: 98
ss <= '1';
wait for 5 ns;
ss <= '0';
wait for 16 ns;
-- bit 0
mosi <= '1';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
-- bit 0
mosi <= '0';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
ss <= '1';
--addr: a0
ss <= '1';
wait for 5 ns;
ss <= '0';
wait for 16 ns;
-- bit 0
mosi <= '1';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
-- bit 0
mosi <= '0';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
ss <= '1';
--addr: a8
ss <= '1';
wait for 5 ns;
ss <= '0';
wait for 16 ns;
-- bit 0
mosi <= '1';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
-- bit 0
mosi <= '0';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
ss <= '1';
--addr: 80
ss <= '1';
wait for 5 ns;
ss <= '0';
wait for 16 ns;
-- bit 0
mosi <= '1';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
-- bit 0
mosi <= '0';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
ss <= '1';
--addr: 80
ss <= '1';
wait for 5 ns;
ss <= '0';
wait for 16 ns;
-- bit 0
mosi <= '1';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
-- bit 0
mosi <= '0';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
ss <= '1';
--addr: 40
ss <= '1';
wait for 5 ns;
ss <= '0';
wait for 16 ns;
-- bit 0
mosi <= '0';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '1';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
-- bit 0
mosi <= '0';
wait for 2 ns;
-- bit 1
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 2
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 3
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 4
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 5
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 6
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
-- bit 7
sck <= not sck;
wait for 1 ns;
sck <= not sck;
mosi <= '0';
wait for 1 ns;
sck <= not sck;
wait for 1 ns;
sck <= not sck;
wait for 16 ns;
ss <= '1';


wait;
end process;
end lea_plus_spi_arch;