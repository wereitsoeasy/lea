library ieee;
use ieee.std_logic_1164.all;
use ieee.math_real.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;


entity ks_rot_const is

  port (
	 data								: out std_logic_vector(19 downto 0)
	);
	 
end ks_rot_const;
 
architecture ks_rot_const_arch of ks_rot_const is

type rom_memory is array (3 downto 0) of std_logic_vector (4 downto 0);

signal rom : rom_memory := ("00001",
									 "00011",
									 "00110",
									 "01011");
									 
begin

	data(19 downto 15) <= rom(3);
	data(14 downto 10) <= rom(2);
	data(9 downto 5) <= rom(1);
	data(4 downto 0) <= rom(0);
	
end ks_rot_const_arch;