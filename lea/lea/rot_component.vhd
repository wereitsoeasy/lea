library ieee;
use ieee.std_logic_1164.all; 
use ieee.numeric_std.all;

entity rot_component is
	
	port (
		n      : in std_logic_vector(4 downto 0);
		op     : in std_logic_vector(31 downto 0);
		lr     : in std_logic;
		result : out std_logic_vector(31 downto 0)
	);
 
end rot_component;

architecture rot_arch of rot_component is
begin

	process(op, n, lr)
	begin
		if (lr = '1') then
			result <= std_logic_vector(unsigned(op) ror to_integer(unsigned(n)));
		else
			result <= std_logic_vector(unsigned(op) rol to_integer(unsigned(n)));
		end if;
	end process;
	
end rot_arch;