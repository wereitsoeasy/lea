library ieee;
use ieee.std_logic_1164.all; 
use ieee.numeric_std.all;

entity XT is 

	port(
		data_in : in std_logic_vector(127 downto 0);
		clk, rw : in std_logic;
		data_out : out std_logic_vector(127 downto 0)
	);

end XT;

architecture XT_behavioral of XT is

	signal mem : std_logic_vector(127 downto 0);

begin

	reg: process(clk, rw, data_in) is
	begin
		if(FALLING_EDGE(clk) and rw = '1') then
			mem <= data_in;
		end if;
	end process;

	data_out <= mem;
	
end XT_behavioral;