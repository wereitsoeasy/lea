library ieee;
use ieee.std_logic_1164.all; 
use ieee.numeric_std.all;

entity crypt_block is
	
	port (
		p_mem_in, c_mem_in, ks_in, t_in : in std_logic_vector(127 downto 0);
		x_rw, x_clk, enc_end_mux : std_logic;
		x_mux : in std_logic_vector(1 downto 0);
		x_out : out std_logic_vector(127 downto 0);
		rot_dirs : in std_logic_vector(1 downto 0)
	);
 
end crypt_block;

architecture crypt_block_arch of crypt_block is
	
	signal enc_mux_out, x_data_in, x_data_out, crypt_round_out, decrypt_round_out : std_logic_vector(127 downto 0);
	signal rot_const : std_logic_vector(14 downto 0);
	
	component mux_2_to_1
		generic (
			n : integer := 128
		);
		port (
			a: in  std_logic_vector(n-1 downto 0);
			b: in  std_logic_vector(n-1 downto 0);
			s: in  std_logic;
			c: out std_logic_vector(n-1 downto 0)
		);
	end component;

	component mux_3_to_1
		generic (
			n : integer := 128
		);
		port (
			a: in  std_logic_vector(n-1 downto 0);
			b: in  std_logic_vector(n-1 downto 0);
			c: in  std_logic_vector(n-1 downto 0);
			s: in  std_logic_vector(1 downto 0);
			d: out std_logic_vector(n-1 downto 0)
		);
	end component;
		
	component XT 
		port(
			data_in : in std_logic_vector(127 downto 0);
			clk, rw : in std_logic;
			data_out : out std_logic_vector(127 downto 0)
		);
	end component;
	
	component crypt_rot_const
		port (
			data	: out std_logic_vector(14 downto 0)
		);
	end component;
	
	component crypt_round
		port(
			x : in std_logic_vector(127 downto 0);
			t : in std_logic_vector(127 downto 0);
			rot_dirs : in std_logic_vector(1 downto 0);
			rot_const : in std_logic_vector(14 downto 0);
			ox : out std_logic_vector(127 downto 0)
		);
	end component;

	component decrypt_round 
		port(
			x : in std_logic_vector(127 downto 0);
			t : in std_logic_vector(127 downto 0);
			rot_dirs : in std_logic_vector(1 downto 0);
			rot_const : in std_logic_vector(14 downto 0);
			ox : out std_logic_vector(127 downto 0)
		);
	end component;
	
begin

	m31: mux_3_to_1
	generic map(
		n => 128
	)
	port map(
		a => enc_mux_out,
		b => p_mem_in,
		c => c_mem_in,
		s => x_mux,
		d => x_data_in
	);
	
	x_reg: XT
	port map(
		data_in => x_data_in,
		rw => x_rw,
		clk => x_clk,
		data_out => x_data_out
	);
	
	cr: crypt_round
	port map(
		x => x_data_out,
		t => ks_in,
		rot_dirs => rot_dirs,
		rot_const => rot_const,
		ox => crypt_round_out 
	);
	
	dcr: decrypt_round
	port map(
		x => x_data_out,
		t => t_in,
		rot_dirs => rot_dirs,
		rot_const => rot_const,
		ox => decrypt_round_out 
	);
	
	m21: mux_2_to_1
	generic map(
		n => 128
	)
	port map(
		a => crypt_round_out,
		b => decrypt_round_out,
		s => enc_end_mux,
		c => enc_mux_out
	);
	
	crc: crypt_rot_const
	port map(
		data => rot_const
	);
	
	x_out <= x_data_out;
	
end crypt_block_arch;