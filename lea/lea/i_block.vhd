library ieee;
use ieee.std_logic_1164.all; 
use ieee.numeric_std.all;

entity i_block is
	port(
		i_mux_sel, i_clr, i_clk, i_rw : in std_logic;
		lt_24_flag : out std_logic;
		i_data_out : out std_logic_vector(4 downto 0)
	);
end i_block;


architecture i_block_arch of i_block is
	
	signal one_mem : std_logic_vector(4 downto 0) := "00001";
	signal i_out, mux_out, mod_add_out, mod_sub_out : std_logic_vector(4 downto 0);

	component mod_add
		generic(
			n : integer
		);
		port (
			opa, opb : in std_logic_vector(n-1 downto 0);
			result: out std_logic_vector(n-1 downto 0)
		);
	end component;

	component mod_sub
		generic(
			n : integer
		);
		port (
			opa, opb : in std_logic_vector(n-1 downto 0);
			result: out std_logic_vector(n-1 downto 0)
		);
	end component;
	
	component mux_2_to_1
		generic (
			n : integer
		);
		port (
			a: in  std_logic_vector(n-1 downto 0);
			b: in  std_logic_vector(n-1 downto 0);
			s: in  std_logic;
			c: out std_logic_vector(n-1 downto 0)
		);
	end component;

	component i_reg
		port(
			data_in : in std_logic_vector(4 downto 0);
			clk, rw, clr : in std_logic;
			data_out : out std_logic_vector(4 downto 0)
		);
	end component;
	
	component comp_flag
		port(
			data_in : in std_logic_vector(4 downto 0);
			lt_24 : out std_logic
		);
	end component;

begin 	

	mod_sub0: mod_sub
	generic map(
		n => 5
	)
	port map(
		opa => i_out,
		opb => one_mem,
		result => mod_sub_out
	);
	
	mod_add0: mod_add
	generic map(
		n => 5
	)
	port map(
		opa => i_out,
		opb => one_mem,
		result => mod_add_out
	);
	
	mux0: mux_2_to_1
	generic map(
		n => 5
	)
	port map(
		a => mod_add_out,
		b => mod_sub_out,
		s => i_mux_sel,
		c => mux_out
	);
	
	ir: i_reg
	port map(
		data_in => mux_out,
		clk => i_clk,
		clr => i_clr,
		rw => i_rw,
		data_out => i_out
	);
	
	cf0: comp_flag
	port map(
		data_in => i_out,
		lt_24 => lt_24_flag
	);
	
	i_data_out <= i_out;

end i_block_arch;