library ieee;
use ieee.std_logic_1164.all;

entity ks_block is
	
	port(
		k_data_in : in std_logic_vector(127 downto 0);
		i_in : in std_logic_vector(4 downto 0);
		t_rw, t_clk, ks_end_mux, t_mux :in std_logic;
		rot_dirs : in std_logic_vector(1 downto 0);
		t_out, ks_out : out std_logic_vector(127 downto 0)
	);

end ks_block;

architecture ks_block_arch of ks_block is

	signal delta_mem_out : std_logic_vector(31 downto 0);
	signal ks_rol_out, t_data_in, t_data_out, ks_round_out, inv_ks_round_out, ks_mux_out : std_logic_vector(127 downto 0);
	signal ks_rot_sig : std_logic_vector(19 downto 0);

	component mux_2_to_1
		generic (
			n : integer := 128
		);
		port (
			a: in  std_logic_vector(n-1 downto 0);
			b: in  std_logic_vector(n-1 downto 0);
			s: in  std_logic;
			c: out std_logic_vector(n-1 downto 0)
		);
	end component;
	
	component XT 
		port(
			data_in : in std_logic_vector(127 downto 0);
			clk, rw : in std_logic;
			data_out : out std_logic_vector(127 downto 0)
		);
	end component;
	
	component delta_mem
		port (
			addr 	 : in  std_logic_vector(2 downto 0);
			data_out : out std_logic_vector(31 downto 0)
		);
	end component;
	
	component ks_rol
		port(
			i : in std_logic_vector(4 downto 0);
			d : in std_logic_vector(31 downto 0);
			rot_dir : in std_logic;
			o : out std_logic_vector(127 downto 0)
		);
	end component;
	
	component ks_round
		port (
			d : in std_logic_vector(127 downto 0);
			t : in std_logic_vector(127 downto 0);
			rot_dir : in std_logic;
			rot_const : in std_logic_vector(19 downto 0);
			ot : out std_logic_vector(127 downto 0)
		);
	end component;
	
	component inv_ks_round
		port (
			d : in std_logic_vector(127 downto 0);
			t : in std_logic_vector(127 downto 0);
			rot_dir : in std_logic;
			rot_const : in std_logic_vector(19 downto 0);
			ot : out std_logic_vector(127 downto 0)
		);
	end component;
	
	component ks_rot_const
		port (
			data	: out std_logic_vector(19 downto 0)
		);
	end component;

begin

	dm: delta_mem
	port map(
		addr(1 downto 0) => i_in(1 downto 0),
		addr(2) => '0',
		data_out => delta_mem_out
	);
	
	ksr: ks_rol
	port map(
		d => delta_mem_out,
		i => i_in,
		o => ks_rol_out,
		rot_dir => rot_dirs(0)
	);
	
	tmux: mux_2_to_1
	generic map(
		n => 128
	)
	port map(
		a => ks_mux_out, 
		b => k_data_in,
		s => t_mux,
		c => t_data_in
	);
	
	t: XT
	port map(
		data_in => t_data_in,
		clk => t_clk,
		rw => t_rw,
		data_out => t_data_out
	);
	
	kr: ks_round
	port map(
		d => ks_rol_out,
		t => t_data_out,
		rot_dir => rot_dirs(0),
		rot_const => ks_rot_sig,
		ot => ks_round_out
	);
	
	ikr: inv_ks_round
	port map(
		d => ks_rol_out,
		t => t_data_out,
		rot_dir => rot_dirs(1),
		rot_const => ks_rot_sig,
		ot => inv_ks_round_out
	);
	
	ks_mux: mux_2_to_1
	generic map(
		n => 128
	)
	port map(
		a => ks_round_out,
		b => inv_ks_round_out,
		s => ks_end_mux,
		c => ks_mux_out
	);

	ksrc: ks_rot_const
	port map(
		data => ks_rot_sig
	);
	
	t_out <= t_data_out;
	ks_out <= ks_round_out;
	
end ks_block_arch;