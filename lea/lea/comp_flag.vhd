library ieee;
use ieee.std_logic_1164.all;
use ieee.math_real.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;

entity comp_flag is
	
	port(
		data_in : in std_logic_vector(4 downto 0);
		lt_24 : out std_logic
	);
	
end comp_flag;

architecture comp_flag_behavioral of comp_flag is
begin

	lt_24_flag: process(data_in) is
	begin
		if (unsigned(data_in) < 24) then
			lt_24 <= '1';
		else
			lt_24 <= '0';
		end if;
	end process;
		
end comp_flag_behavioral;