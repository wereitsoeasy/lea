library ieee;
use ieee.std_logic_1164.all;

entity mem_out_addr is
	port(
		clk, clr : in std_logic;
		data_in : in std_logic_vector(1 downto 0);
		data_out : out std_logic_vector(1 downto 0)
	);
end mem_out_addr;

architecture mem_out_addr_arch of mem_out_addr is
begin
end mem_out_addr_arch;