library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.numeric_bit.all;
use ieee.std_logic_unsigned.all;

entity KPCmem is

	generic (
		n : integer := 32
	);
	port (
		p_data_in : in std_logic_vector(127 downto 0);
		data_in : in std_logic_vector(31 downto 0);
		clk, pi, rw : in std_logic;
		addr : in std_logic_vector(1 downto 0);
		p_data_out : out std_logic_vector(127 downto 0);
		data_out : out std_logic_vector(31 downto 0)
	);
 
end KPCmem;

architecture KPCmem_behavioral of KPCmem is

	type mem_arr is array (0 to 3) of std_logic_vector(31 downto 0);
	signal mem : mem_arr;

begin
	
	w: process(clk, rw, pi, addr, data_in, p_data_in)
	begin
		if falling_edge(clk) then
			if rw = '1' then
				if pi = '0' then
					mem(3) <= p_data_in(127 downto 96);
					mem(2) <= p_data_in(95 downto 64);
					mem(1) <= p_data_in(63 downto 32);
					mem(0) <= p_data_in(31 downto 0);
				elsif pi = '1' then	
					mem(conv_integer(addr)) <= data_in;
				end if;
			end if;
		end if;
	end process;
	
	data_out <= mem(conv_integer(addr));
	
	p_data_out(127 downto 96) <= mem(3);
	p_data_out(95 downto 64) <= mem(2);
	p_data_out(63 downto 32) <= mem(1);
	p_data_out(31 downto 0) <= mem(0);

end KPCmem_behavioral;