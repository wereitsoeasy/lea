library ieee;
use ieee.std_logic_1164.all; 
use ieee.numeric_std.all;

entity inv_ks_round_block is
	
	port (
		d : in std_logic_vector(31 downto 0);
		t : in std_logic_vector(31 downto 0);
		p_ot : out std_logic_vector(31 downto 0);
		rot_const : in std_logic_vector(4 downto 0);
		rot_dir : in std_logic
	);
 
end inv_ks_round_block;

architecture inv_ks_round_block_arch of inv_ks_round_block is

	signal ror_to_modsub : std_logic_vector(31 downto 0);
	
	component rot_component
	port (
		n      : in std_logic_vector(4 downto 0);
		op     : in std_logic_vector(31 downto 0);
		lr     : in std_logic;
		result : out std_logic_vector(31 downto 0)
	);
	end component;
	
	component mod_sub
	generic(
		n : integer := 32
	);
	port (
		opa, opb : in std_logic_vector(n-1 downto 0);
		result: out std_logic_vector(n-1 downto 0)
	);
	end component;

begin
	
	ror0: rot_component
	port map(
		n => rot_const,
		op => t,
		lr => rot_dir,
		result => ror_to_modsub
	);
	
	modsub0: mod_sub
	port map(
		opa => ror_to_modsub,
		opb => d,
		result => p_ot
	);

end inv_ks_round_block_arch;