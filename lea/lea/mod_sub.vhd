library ieee;
use ieee.std_logic_1164.all; 
use ieee.numeric_std.all;


entity mod_sub is
	
	generic(
		n : integer := 32
	);
	port (
		opa, opb : in std_logic_vector(n-1 downto 0);
		result: out std_logic_vector(n-1 downto 0)
	);
 
end mod_sub;

architecture mod_sub_arch of mod_sub is


begin
	
	result<= std_logic_vector(unsigned(opa) - unsigned(opb));
	
end mod_sub_arch;