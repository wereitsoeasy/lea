library ieee;
use ieee.std_logic_1164.all; 
use ieee.numeric_std.all;

entity decrypt_round_block is

	port (
		x0 : in std_logic_vector(31 downto 0);
		x1 : in std_logic_vector(31 downto 0);
		t0 : in std_logic_vector(31 downto 0);
		t1 : in std_logic_vector(31 downto 0);
		p_ox : out std_logic_vector(31 downto 0);
		lr : in std_logic;
		rot_const_in : in std_logic_vector(4 downto 0)
	);
	
end decrypt_round_block;

architecture decrypt_round_block_arch of decrypt_round_block is
	
	signal ror_to_modsub : std_logic_vector(31 downto 0);
	signal xor0_to_modsub : std_logic_vector(31 downto 0);
	signal modsub_to_xor1 : std_logic_vector(31 downto 0);
	
	component xor_gate 
	port (
		opa, opb : in std_logic_vector(31 downto 0);
		result : out std_logic_vector(31 downto 0)
	);
	end component;
	
	component mod_sub
	port (
		opa, opb : in std_logic_vector(31 downto 0);
		result: out std_logic_vector(31 downto 0)
	);
	end component;
	
	component rot_component 
	port (
		n      : in std_logic_vector(4 downto 0);
		op     : in std_logic_vector(31 downto 0);
		lr : in std_logic;
		result : out std_logic_vector(31 downto 0)
	);
	end component;

begin

	ror0: rot_component 
	port map(
		n => rot_const_in,
		op => x0,
		lr => lr,
		result => ror_to_modsub
	);
	
	xor0: xor_gate 
	port map(
		opa => x1,
		opb => t0,
		result => xor0_to_modsub
	);
	
	modsub0: mod_sub
	port map(
		opa => ror_to_modsub,
		opb => xor0_to_modsub,
		result => modsub_to_xor1
	);
	
	xor1: xor_gate
	port map(
		opa => modsub_to_xor1,
		opb => t1,
		result => p_ox
	);

end decrypt_round_block_arch;