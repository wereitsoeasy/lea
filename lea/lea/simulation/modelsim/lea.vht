-- Copyright (C) 1991-2013 Altera Corporation
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, Altera MegaCore Function License 
-- Agreement, or other applicable license agreement, including, 
-- without limitation, that your use is for the sole purpose of 
-- programming logic devices manufactured by Altera and sold by 
-- Altera or its authorized distributors.  Please refer to the 
-- applicable agreement for further details.

-- ***************************************************************************
-- This file contains a Vhdl test bench template that is freely editable to   
-- suit user's needs .Comments are provided in each section to help the user  
-- fill out necessary details.                                                
-- ***************************************************************************
-- Generated on "10/29/2020 09:45:45"
                                                            
-- Vhdl Test Bench template for design  :  lea
-- 
-- Simulation tool : ModelSim-Altera (VHDL)
-- 

LIBRARY ieee;                                               
USE ieee.std_logic_1164.all;                                

ENTITY lea_vhd_tst IS
END lea_vhd_tst;
ARCHITECTURE lea_arch OF lea_vhd_tst IS
-- constants                                                 
-- signals                                                   
SIGNAL busy : STD_LOGIC;
SIGNAL clk : STD_LOGIC:= '1';
SIGNAL inst_in : STD_LOGIC_VECTOR(37 DOWNTO 0);
SIGNAL mem_out : STD_LOGIC_VECTOR(31 DOWNTO 0);
SIGNAL rst,exec : STD_LOGIC;
COMPONENT lea
	PORT (
	busy : OUT STD_LOGIC;
	clk, exec : IN STD_LOGIC;
	inst_in : IN STD_LOGIC_VECTOR(37 DOWNTO 0);
	mem_out : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
	rst : IN STD_LOGIC
	);
END COMPONENT;
BEGIN
	i1 : lea
	PORT MAP (
-- list connections between master ports and signals
	busy => busy,
	clk => clk,
	inst_in => inst_in,
	mem_out => mem_out,
	rst => rst,
	exec => exec
	);
	
	clk <= not clk after 1 ns;
                                    
always : PROCESS                                          
BEGIN                                                         
	rst <= '1';
	wait for 2 ns;
	rst <= '0';
	wait for 2 ns;

	-- write k
inst_in <= "00011100111100001011010001111000001111";
exec <= '1';
wait until busy = '1';
exec <= '0';
wait until busy = '0';
inst_in <= "00011001111000011010010101101001001011";
exec <= '1';
wait until busy = '1';
exec <= '0';
wait until busy = '0';
inst_in <= "00010110110100101001011001011010000111";
exec <= '1';
wait until busy = '1';
exec <= '0';
wait until busy = '0';
inst_in <= "00010011110000111000011101001011000011";
exec <= '1';
wait until busy = '1';
exec <= '0';
wait until busy = '0';

--write p
inst_in <= "00111100010011000100100001000100010000";
exec <= '1';
wait until busy = '1';
exec <= '0';
wait until busy = '0';
inst_in <= "00111000010111000101100001010100010100";
exec <= '1';
wait until busy = '1';
exec <= '0';
wait until busy = '0';
inst_in <= "00110100011011000110100001100100011000";
exec <= '1';
wait until busy = '1';
exec <= '0';
wait until busy = '0';
inst_in <= "00110000011111000111100001110100011100";
exec <= '1';
wait until busy = '1';
exec <= '0';
wait until busy = '0';


-- encrypt
inst_in <= "01100000000000000000000000000000000000";
exec <= '1';
wait until busy = '1';
exec <= '0';
wait until busy = '0';
inst_in <= "00000000000000000000000000000000000000";
exec <= '1';
wait until busy = '1';
exec <= '0';
wait until busy = '0';
wait for 120 ns;

--read p
inst_in <= "00101100000000000000000000000000000000";
exec <= '1';
wait until busy = '1';
exec <= '0';
wait until busy = '0';
inst_in <= "00101000000000000000000000000000000000";
exec <= '1';
wait until busy = '1';
exec <= '0';
wait until busy = '0';
inst_in <= "00100100000000000000000000000000000000";
exec <= '1';
wait until busy = '1';
exec <= '0';
wait until busy = '0';
inst_in <= "00100000000000000000000000000000000000";
exec <= '1';
wait until busy = '1';
exec <= '0';
wait until busy = '0';
--read c
inst_in <= "01001100000000000000000000000000000000";
exec <= '1';
wait until busy = '1';
exec <= '0';
wait until busy = '0';
inst_in <= "01001000000000000000000000000000000000";
exec <= '1';
wait until busy = '1';
exec <= '0';
wait until busy = '0';
inst_in <= "01000100000000000000000000000000000000";
exec <= '1';
wait until busy = '1';
exec <= '0';
wait until busy = '0';
inst_in <= "01000000000000000000000000000000000000";
exec <= '1';
wait until busy = '1';
exec <= '0';
wait until busy = '0';



WAIT;                                                        
END PROCESS always;                                          
END lea_arch;
