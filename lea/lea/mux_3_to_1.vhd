library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.numeric_bit.all;
use ieee.std_logic_unsigned.all;

entity mux_3_to_1 is

	generic (
		n : integer := 128
	);
	port (
		a: in  std_logic_vector(n-1 downto 0);
		b: in  std_logic_vector(n-1 downto 0);
		c: in  std_logic_vector(n-1 downto 0);
		s: in  std_logic_vector(1 downto 0);
		d: out std_logic_vector(n-1 downto 0)
	);
 
end mux_3_to_1;

architecture mux_3_to_1_arch of mux_3_to_1 is
begin

	d <= c when s = "10" else
		b when s = "01" else a;
	
end mux_3_to_1_arch;