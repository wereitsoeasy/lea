library ieee;
use ieee.std_logic_1164.all; 
use ieee.numeric_std.all;

entity mem_block is
	port(
		p_p_data_in, c_p_data_in : std_logic_vector(127 downto 0);
		data_in : in std_logic_vector(31 downto 0);
		clk, k_pi, k_rw, p_pi, p_rw, c_pi, c_rw : in std_logic;
		addr, mem_out_sel : in std_logic_vector(1 downto 0);
		k_p_data_out, p_p_data_out, c_p_data_out : out std_logic_vector(127 downto 0);
		mem_out : out std_logic_vector(31 downto 0)
	);
end mem_block;


architecture mem_block_arch of mem_block is

	signal k_data_out, p_data_out, c_data_out : std_logic_vector(31 downto 0);

	component KPCmem 
		port (
			p_data_in : in std_logic_vector(127 downto 0);
			data_in : in std_logic_vector(31 downto 0);
			clk, pi, rw : in std_logic;
			addr : in std_logic_vector(1 downto 0);
			p_data_out : out std_logic_vector(127 downto 0);
			data_out : out std_logic_vector(31 downto 0)
		);
	end component;
	
	component mux_3_to_1 
		generic (
			n : integer
		);
		port (
			a: in  std_logic_vector(n-1 downto 0);
			b: in  std_logic_vector(n-1 downto 0);
			c: in  std_logic_vector(n-1 downto 0);
			s: in  std_logic_vector(1 downto 0);
			d: out std_logic_vector(n-1 downto 0)
		);
	end component;

begin 

	K: KPCmem
	port map(
		clk => clk,
		p_data_in => (others => '-'),
		data_in => data_in,
		pi => k_pi,
		rw => k_rw,
		addr => addr,
		p_data_out => k_p_data_out,
		data_out => k_data_out
	);
	
	P: KPCmem
	port map(
		clk => clk,
		p_data_in => p_p_data_in,
		data_in => data_in,
		pi => p_pi,
		rw => p_rw,
		addr => addr,
		p_data_out => p_p_data_out,
		data_out => p_data_out
	);
	
	C: KPCmem
	port map(
		clk => clk,
		p_data_in => c_p_data_in,
		data_in => data_in,
		pi => c_pi,
		rw => c_rw,
		addr => addr,
		p_data_out => c_p_data_out,
		data_out => c_data_out
	);
	
	mux: mux_3_to_1
	generic map(
		n => 32
	)
	port map(
		a => k_data_out,
		b => p_data_out,
		c => c_data_out,
		s => mem_out_sel,
		d => mem_out
	);

end mem_block_arch;