# key
#K = '00000000000000000000000111111111111111110111011101110111000001110111011101110111011101110111011101110111000000000000000000000000'
K = '0f1e2d3c4b5a69788796a5b4c3d2e1f0'
# plaintext
#P = '00110001001111111111111111111101000100010011110100010011111100010001000111111001001111110001000111111101001111111001000100010011'
P = '101112131415161718191a1b1c1d1e1f'
# key schedule constants
delta = ['11000011111011111110100111011011',
         '01000100011000100110101100000010',
         '01111001111000100111110010001010',
         '01111000110111110011000011101100',
         '01110001010111101010010010011110',
         '11000111100001011101101000001010',
         '11100000010011101111001000101010',
         '11100101110001000000100101010111']

# this is defined in the paper
T = [] # round key state
final_T = []
RK = [] # round keys
X = [] # encryption state
C = [] # cipher text (segmented)
RKi = [] # inverse round keys

# this is just used for our own test purposes
Dr = [] # delta rol
# segment K and P (K_s means 'segmented K' and P_s means 'segmented P')
K_s = []
P_s = []

def initialize_errthang():
    print('arranged K' + arrange(K))
    print('arranged P' + arrange(P))
    Kaux = bin(int(arrange(K), 16))[2:].zfill(128)
    Paux = bin(int(arrange(P), 16))[2:].zfill(128)
    for i in range (0, 4):
        K_s.append(Kaux[i*32:32+(i*32)])
        P_s.append(Paux[i*32:32+(i*32)])

        # set T[i] = K[i]
    for i in range(0, 4):
        Dr.append(K_s[i])
        T.append(K_s[i])

def arrange_c(C):
    arranged = ''
    temp = ''
    deftemp = ''
    for i in range(4):
        temp = hex(int(C[i], 2))[2:].zfill(8)
        deftemp = ''
        for i in range(4):
            deftemp += temp[7-1-(2*i)]
            deftemp += temp[7-(2*i)]
        arranged += deftemp
    return arranged
        
def arrange(hex_string):
    arranged = ''
    temp  = ''
    deftemp = ''
    for i in range(4):
        temp = hex_string[i*8:8+(i*8)]
        deftemp = ''
        for i in range(4):
            deftemp += temp[7-1-(2*i)]
            deftemp += temp[7-(2*i)] 
        arranged += deftemp 
    print(arranged)
    return arranged

def ROL(i, x):
    # i is the number of times to rotate, x is the number to be rotated
    return x[i:] + x[:i]

def ROR(i, x):
    return x[-i:] + x[:-i]

def XOR(x, y):
    res = (int(x, 2) ^ int(y, 2))
    res = format(res, 'b').zfill(32)
    return res
    
def mod_add(x, y):
    # addition modulo 2^32
    res = (int(x, 2) + int(y, 2)) % 2**32
    res = format(res, 'b').zfill(32)
    return res

def mod_sub(x, y):
    res = (int(x, 2) - int(y, 2)) % 2**32
    res = format(res, 'b').zfill(32)
    return res

def key_schedule_rol():
    for i in range(24):
        Dr[0] = ROL(i, delta[i%4])
        Dr[1] = ROL(i+1, delta[i%4])
        Dr[2] = ROL(i+2, delta[i%4])
        Dr[3] = ROL(i+3, delta[i%4])
        #print('ks rol round %d' % (i) )
        #print_arr(Dr)

def key_schedule():
    # 128 bit key, 24 round keys. Each round key consists of 6 32 bit chunks, meaning 192 bits total for each round key
    # T's value is modified each round. Each T[i] has a different value each round
    for i in range(24):
        T[0] = ROL(1, mod_add(T[0], ROL(i, delta[i % 4])))
        T[1] = ROL(3, mod_add(T[1], ROL(i+1, delta[i % 4])))
        T[2] = ROL(6, mod_add(T[2], ROL(i+2, delta[i % 4])))
        T[3] = ROL(11, mod_add(T[3], ROL(i+3, delta[i % 4])))
        auxlist=[T[0],T[1],T[2],T[1],T[3],T[1]]
        RK.append(auxlist)

def inv_key_schedule():
    for i in range(23,-1,-1):
        T[0] = mod_sub(ROR(1, T[0]), ROL(i, delta[i%4]))
        T[1] = mod_sub(ROR(3, T[1]), ROL(i+1, delta[i%4]))
        T[2] = mod_sub(ROR(6, T[2]), ROL(i+2, delta[i%4]))
        T[3] = mod_sub(ROR(11, T[3]), ROL(i+3, delta[i%4]))
        auxlist=[T[0],T[1],T[2],T[1],T[3],T[1]]
        RK.append(auxlist)
        
def encrypt():
    # 24 rounds for 128 bit keys
    # set the 128 bit intermediate value X[0] to the plaintext P (segmented P, that is)
    X.append(P_s)
    # run the key schedule to generate 24 round keys
    key_schedule()
    for i in range(0, 24):
        # (implementation-specific) make an aux variable for X[i+1]
        aux_x = []
        aux_x.append(ROL(9, mod_add(XOR(X[i][0], RK[i][0]), XOR(X[i][1], RK[i][1]))))
        aux_x.append(ROR(5, mod_add(XOR(X[i][1], RK[i][2]), XOR(X[i][2], RK[i][3]))))
        aux_x.append(ROR(3, mod_add(XOR(X[i][2], RK[i][4]), XOR(X[i][3], RK[i][5]))))
        aux_x.append(X[i][0])
        print("enc round %d %s" % (i, print_arr2(aux_x)))
        X.append(aux_x)
        
    # finalization. set ciphertext to last encryption state (also known as X)
    for i in range(0, 4):
        C.append(X[len(X)-1][i])
    #print("encrypted value is:")
    #for i in range(0, 4):
    #    print(hex(int(C[i], 2)))

def decrypt():
    X = []
    X.append(C)
    for i in range(24):
        aux_x = []
        aux_x.append(X[i][3])
        aux_x.append(XOR(mod_sub(ROR(9, X[i][0]), XOR(X[i][3], RK[23-i][0])), RK[23-i][1]))
        aux_x.append(XOR(mod_sub(ROL(5, X[i][1]), XOR(aux_x[1], RK[23-i][2])), RK[23-i][3]))
        aux_x.append(XOR(mod_sub(ROL(3, X[i][2]), XOR(aux_x[2], RK[23-i][4])), RK[23-i][5]))
        #print("dec round %d" % (i))
        #print_arr(aux_x)
        #print("rki %d" % (i))
        #print_rki(RK[i])
        X.append(aux_x)
        
    auxstr = []
    for i in range(0, 4):
        auxstr.append(X[len(X)-1][i])
    print("decrypted value is:")
    for i in range(0, 4):
        print(hex(int(auxstr[i], 2)))


def print_arr(arr):
    for i in range(4):
        print(hex(int(arr[i], 2)))


def print_arr2(arr):
    tempstring = ''
    for i in range(4):
        tempstring += hex(int(arr[i], 2))[2:].zfill(8)
    return tempstring

def print_rki(arr):
    print(hex(int(arr[0], 2)))
    print(hex(int(arr[1], 2)))
    print(hex(int(arr[2], 2)))
    print(hex(int(arr[4], 2)))

def print_bin(arr):
    for i in range(4):
        print(arr[i])

def gen_vhdl_testbench():
    inst_dict = {
        'kopc': '000',
        'popc': '001',
        'copc': '010',
        'enc' : '011',
        'dec' : '100',
        'w' : '1',
        'r' : '0',
        'blank32' : '00000000000000000000000000000000'
    }
    
    print('testbench gen:')
    print('-- write k')
    for i in range(4):
        print_tb_inst(inst_dict['kopc'] + inst_dict['w'] + format(3-i, 'b').zfill(2) + K_s[i])
    print()
    print('--write p')
    for i in range(4):
        print_tb_inst(inst_dict['popc'] + inst_dict['w'] + format(3-i, 'b').zfill(2) + P_s[i])
    print()
    print('--write c')
    for i in range(4):
        print_tb_inst(inst_dict['copc'] + inst_dict['w'] + format(3-i, 'b').zfill(2) + C[i])
    print()
    # encrypt
    print('-- encrypt')
    print_tb_inst(inst_dict['enc'] + inst_dict['r'] + '00' + inst_dict['blank32'])
    print_tb_inst('000000' + inst_dict['blank32'])
    print('wait for 120 ns;')
    print()
    # decrypt
    print('-- decrypt')
    print_tb_inst(inst_dict['dec'] + inst_dict['r'] + '00' + inst_dict['blank32'])
    print_tb_inst('000000' + inst_dict['blank32'])
    print('wait for 120 ns;')
    print()
    print('--read p')
    for i in range(4):
        print_tb_inst(inst_dict['popc'] + inst_dict['r'] + format(3-i, 'b').zfill(2) + inst_dict['blank32'])

    print('--read c')
    for i in range(4):
        print_tb_inst(inst_dict['copc'] + inst_dict['r'] + format(3-i, 'b').zfill(2) + inst_dict['blank32'])
    print()

def tb_inst_format(word):
    return 'inst_in <= \"' + word + '\";'

def print_tb_inst(word):
    print(tb_inst_format(word))
    print('exec <= \'1\';')
    print('wait until busy = \'1\';')
    print('exec <= \'0\';')
    print('wait until busy = \'0\';')

def main():
    arrange(P)
    initialize_errthang()
    print("key:")
    print_arr(K_s)
    print("p:")
    print_arr(P_s)
    key_schedule()
    encrypt()
    print(arrange_c(C))
    #X = []
    #decrypt()
    #print('binary values---------')
    #print('key schedule------------------')
    #print_bin(K_s)
    #print('plaintext-----------------')
    #print_bin(P_s)
    #print('ciphertext------------------')
    #print_bin(C)
    gen_vhdl_testbench()

main()
